----------------------------------------------------
====================================================
    __ __          __            __
   / //_/__  _____/ /_________  / /
  / ,< / _ \/ ___/ __/ ___/ _ \/ /
 / /| /  __(__  ) /_/ /  /  __/ /
/_/_|_\___/____/\__/_/ __\___/_/ _________
  / ___/____  / __/ /_/ __ )/  |/  / ____/
  \__ \/ __ \/ /_/ __/ __  / /|_/ / /
 ___/ / /_/ / __/ /_/ /_/ / /  / / /___
/____/\____/_/  \__/_____/_/  /_/\____/

====================================================
----------------------------------------------------

# Kestrel SoftBMC Project
## Zephyr firmware files
Copyright (c) 2020 - 2021 Raptor Engineering, LLC

====================================================

**NOTE**

This application is developed on OpenPOWER (Raptor Computing Systems POWER9) host systems.  Other development systems may or may not work without significant effort.

With that disclaimer out of the way...

# How to build

1. Create directory to hold development sources and builds

        mkdir kestrel-zephyr
        cd kestrel-zephyr

2. Clone all repositories

        git clone https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-firmware/zephyr-rtos
        git clone https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-firmware/zephyr-firmware
        git clone https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-firmware/zephyr-littlefs
        git clone https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-firmware/zephyr-mbedtls
        git clone https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-firmware/zephyr-civetweb
        git clone https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-tooling/gcc

3. Build soft floating point library from GCC toolchain sources

    **NOTE**

    This is a bit of a hack.  Unfortunately, our target platform is soft float, while most (all?) "real" OpenPOWER distros are hard float.
    As a result, we need to compile a copy of the soft float static library for our target.

        cd gcc/libgcc/soft-fp/
        gcc -c -O2 -msoft-float -mno-string -mno-multiple -mno-vsx -mno-altivec -mlittle-endian -mstrict-align -fno-stack-protector -m64 -mabi=elfv2 -mcmodel=small -I../config/rs6000/ -I../../include -I.. *.c
        <ignore expected errors>
        ar -crv libsoft-fp.a *.o
        cp -Rp libsoft-fp.a ../../../
        cd ../../../

4. Build Zephyr application

        export SOURCE_ROOT_DIR=$(pwd)
        cd zephyr-rtos
        mkdir build
        cd build
        ZEPHYR_BASE=${SOURCE_ROOT_DIR}/zephyr-rtos ZEPHYR_TOOLCHAIN_VARIANT=host cmake -DZEPHYR_MODULES="${SOURCE_ROOT_DIR}/zephyr-littlefs;${SOURCE_ROOT_DIR}/zephyr-mbedtls" -DZEPHYR_LITTLEFS_MODULE_DIR=${SOURCE_ROOT_DIR}/zephyr-littlefs -DZEPHYR_LZ4_MODULE_DIR=${SOURCE_ROOT_DIR}/zephyr-lz4 -DBOARD=arctic_tern/kestrel_openpower ${SOURCE_ROOT_DIR}/zephyr-firmware
        cp -Rp ../../libsoft-fp.a zephyr/
        make -j144

    Output is in zephyr/zephyr.bin

# How to use

1. Soft-load application onto FPGA

        python3 litex/tools/litex_term.py --speed 115200 /dev/ttyUSB1 --kernel zephyr/build/zephyr/zephyr.bin
        <using a second terminal, program the FPGA with openocd>
        openocd --log_output openocd.log 3 -f "/usr/share/trellis/misc/openocd/ecp5-versa.cfg" -c "transport select jtag; init; svf build/versa_ecp5/gateware/versa_ecp5.svf; exit"

2. Flash Kestrel onto the FPGA board (optional)

    Programming Kestrel onto the FPGA will provide a very rapid (under 10 seconds) startup from cold / no power applied to online and ready to IPL the host.  It requires three separate programming files, and Kestrel to be connected to the network and online via the openocd soft load process.  The files required are:

        versa_ecp5.bit
        zephyr.bin
        pnor.img

    First, determine the IP address of the Kestrel system.  You can use the Zephyr shell (`exit` from the Kestrel shell, then `net iface`) to determine the IP address granted to the Kestrel system by the DHCP server.

    Once the IP address is known, use a Web browser to naviate to http://<BMC IP>:8080  Click the Firmware link, then select the `versa_ecp5.bit` file and upload.  After the upload has finished, run `kestrel reflash fpga` on the terminal.

    Next, navigate to the Firmware link again, and upload the `zephyr.bin` file.  After the upload has finished, run `kestrel reflash bmc` on the terminal.

    At this point, Kestrel itself is now installed and will survive a power cycle of the FPGA board.  In and of itself it is not very useful until the host firmware is uploaded, however.

    Uploading the host firmware will take a long time, as the PNOR is 64MB for Raptor Computing Systems devices.  After starting the upload grab a cup of coffee, then once the upload is finished run `kestrel reflash pnor`

3. Control the host

    Most commands to interact with the host are contained within the `kestrel` subcommand.  Important commands include:

        obmcutil poweron
        obmcutil chassisoff
        obmc-console-client

# Redfish v1 API

A basic Redfish API implementation is available.  Currently only the basic HTTP API is available, and authentication support is not yet implemented.

To interact with the Redfish API, install redfishtool (available as a package on all Debian systems).

Supported commands include:

    redfishtool -r <BMC IP address> -A None -S IfSendingCredentials Systems list
    redfishtool -r <BMC IP address> -A None -S IfSendingCredentials Chassis Thermal
    redfishtool -r <BMC IP address> -A None -S IfSendingCredentials Chassis Sensors
