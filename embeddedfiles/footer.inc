		</div>

		<div class="footer">
			<table width=100%>
				<tr>
					<td width=100% align=center>
						<table class="footer_text">
							<tr>
								<td rowspan=2>
									<a href="https://www.raptorengineering.com"><img src="raptor_logo.jpg" height="30" border="0"></a>
								</td>
								<td>
									Kestrel brought to you by <a href="https://www.raptorengineering.com">Raptor Engineering</a>
								</td>
							</tr>
							<tr>
								<td>
									Get the <a href="https://gitlab.raptorengineering.com/kestrel-collaboration">source code</a> and collaborate on our GitLab server today!
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
