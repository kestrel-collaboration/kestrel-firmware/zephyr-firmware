include(${KESTREL_SOURCE_DIR}/../bin2hex.cmake)

SET(static_file_header ${KESTREL_SOURCE_DIR}/src/static_files.h)

file(WRITE ${static_file_header} "// © 2021-2024 Raptor Engineering, LLC\n//\n// Released under the terms of the GPL v3\n// See the LICENSE file for full details\n\n#ifndef _STATIC_FILES\n#define _STATIC_FILES\n\n")
foreach(file ${embedded_file_sources})
	get_filename_component(variableName ${file} NAME)
	bin2h(SOURCE_FILE ${file} HEADER_FILE ${static_file_header} VARIABLE_NAME ${variableName} APPEND NULL_TERMINATE)
	file(APPEND ${static_file_header} "\n")
endforeach()
foreach(file ${embedded_lz4_file_sources})
	get_filename_component(variableName ${file} NAME)
	bin2h(SOURCE_FILE ${file} HEADER_FILE ${static_file_header} VARIABLE_NAME ${variableName} APPEND NULL_TERMINATE)
	file(APPEND ${static_file_header} "\n")
endforeach()
file(APPEND ${static_file_header} "\n\n#endif // _STATIC_FILES\n")