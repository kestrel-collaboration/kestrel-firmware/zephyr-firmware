// © 2020 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _SIMPLE_PWM_H
#define _SIMPLE_PWM_H

#include <stdint.h>

#define SIMPLE_PWM_MASTER_DEVICE_ID_HIGH 0x0
#define SIMPLE_PWM_MASTER_DEVICE_ID_LOW  0x4
#define SIMPLE_PWM_MASTER_DEVICE_VERSION 0x8
#define SIMPLE_PWM_MASTER_PWM_CTL        0xc
#define SIMPLE_PWM_MASTER_TACH_01        0x10
#define SIMPLE_PWM_MASTER_TACH_23        0x14
#define SIMPLE_PWM_MASTER_BASE_CLK_HZ    0x1c
#define SIMPLE_PWM_MASTER_PHY_CFG_1      0x20
#define SIMPLE_PWM_MASTER_PHY_CFG_2      0x24
#define SIMPLE_PWM_MASTER_PHY_CFG_3      0x28
#define SIMPLE_PWM_MASTER_PHY_CFG_4      0x2c

#define SIMPLE_PWM_DEVICE_ID_HIGH 0x7c525054
#define SIMPLE_PWM_DEVICE_ID_LOW  0x5350574d

#define SIMPLE_PWM_VERSION_MAJOR_MASK  0xffff
#define SIMPLE_PWM_VERSION_MAJOR_SHIFT 16
#define SIMPLE_PWM_VERSION_MINOR_MASK  0xff
#define SIMPLE_PWM_VERSION_MINOR_SHIFT 8
#define SIMPLE_PWM_VERSION_PATCH_MASK  0xff
#define SIMPLE_PWM_VERSION_PATCH_SHIFT 0

#define SIMPLE_PWM_MASTER_PWM_0_MASK   0xff
#define SIMPLE_PWM_MASTER_PWM_0_SHIFT  0
#define SIMPLE_PWM_MASTER_PWM_1_MASK   0xff
#define SIMPLE_PWM_MASTER_PWM_1_SHIFT  8
#define SIMPLE_PWM_MASTER_PWM_2_MASK   0xff
#define SIMPLE_PWM_MASTER_PWM_2_SHIFT  16
#define SIMPLE_PWM_MASTER_PWM_3_MASK   0xff
#define SIMPLE_PWM_MASTER_PWM_3_SHIFT  24
#define SIMPLE_PWM_MASTER_TACH_0_MASK  0xffff
#define SIMPLE_PWM_MASTER_TACH_0_SHIFT 0
#define SIMPLE_PWM_MASTER_TACH_1_MASK  0xffff
#define SIMPLE_PWM_MASTER_TACH_1_SHIFT 16
#define SIMPLE_PWM_MASTER_TACH_2_MASK  0xffff
#define SIMPLE_PWM_MASTER_TACH_2_SHIFT 0
#define SIMPLE_PWM_MASTER_TACH_3_MASK  0xffff
#define SIMPLE_PWM_MASTER_TACH_3_SHIFT 16

// These two values are currently hard-wired into the HDL to save die area
#define SIMPLE_PWM_OUTPUT_CTL_BITS     8
#define SIMPLE_PWM_TACH_SAMPLE_RATE_HZ 5

int initialize_pwm_controller(uint8_t *base_address, int64_t frequency_hz);
int set_pwm_frequency(uint8_t *base_address, uint8_t channel, int64_t frequency_hz);
int set_pwm_value(uint8_t *base_address, uint8_t channel, uint8_t value);
uint8_t get_pwm_value(uint8_t *base_address, uint8_t channel);
int get_tach_value(uint8_t *base_address, uint8_t channel);

#endif // _SIMPLE_PWM_H
