/*
 * Copyright (c) 2021-2024 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: GPL-3.0
 */

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(sensors, LOG_LEVEL_INF);

#include <stdio.h>
#include <stdlib.h>

#include <zephyr/kernel.h>
#include <zephyr/drivers/w1.h>

#include "configuration.h"
#include "flash_filesystem.h"
#include "onewire_functions.h"

#include "sensors.h"

struct k_sem thermal_sensor_data_array_lock;
struct k_sem pressure_sensor_data_array_lock;

// Utility functions
double temp_celsius_to_fahrenheit(double celsius)
{
    return ((9.0 * celsius) / 5.0) + 32.0;
}

double pressure_bar_to_pa(double bar)
{
    return bar * 100000.0;
}

// Thermal sensors
struct kestrel_thermal_sensor_data_temp_t kestrel_temp_sensor_data[MAX_THERMAL_SENSORS];
int kestrel_temp_sensor_count = 0;

// Pressure sensors
struct kestrel_pressure_sensor_data_t kestrel_pressure_sensor_data[MAX_THERMAL_SENSORS];
int kestrel_pressure_sensor_count = 0;

static int kestrel_thermal_sensor_sort_callback(const void * a, const void * b)
{
    const struct kestrel_thermal_sensor_data_temp_t * sensor_data_a = a;
    const struct kestrel_thermal_sensor_data_temp_t * sensor_data_b = b;

    if (sensor_data_a->sensor_id < sensor_data_b->sensor_id) {
        return -1;
    }
    else if (sensor_data_a->sensor_id > sensor_data_b->sensor_id) {
        return 1;
    }

    return 0;
}

void kestrel_sort_temperature_sensors(void)
{
    qsort(kestrel_temp_sensor_data, kestrel_temp_sensor_count, sizeof(struct kestrel_thermal_sensor_data_temp_t), kestrel_thermal_sensor_sort_callback);
}

static int kestrel_pressure_sensor_sort_callback(const void * a, const void * b)
{
    const struct kestrel_pressure_sensor_data_t * sensor_data_a = a;
    const struct kestrel_pressure_sensor_data_t * sensor_data_b = b;

    if (sensor_data_a->sensor_id < sensor_data_b->sensor_id) {
        return -1;
    }
    else if (sensor_data_a->sensor_id > sensor_data_b->sensor_id) {
        return 1;
    }

    return 0;
}

void kestrel_sort_pressure_sensors(void)
{
    qsort(kestrel_pressure_sensor_data, kestrel_pressure_sensor_count, sizeof(struct kestrel_pressure_sensor_data_t), kestrel_pressure_sensor_sort_callback);
}

void zephyr_onewire_search_callback(struct w1_rom rom, void *user_data)
{
    onewire_device_list_t* detected_onewire_devices = (onewire_device_list_t*)user_data;
    uint64_t onewire_rom_address = BSWAP_64(w1_rom_to_uint64(&rom));
    size_t max_entries = sizeof(detected_onewire_devices->devices) / sizeof(detected_onewire_devices->devices[0]);

    LOG_INF("OneWire device found; family: 0x%02x, serial: 0x%016llx", rom.family, onewire_rom_address);

    if (detected_onewire_devices->device_count < max_entries) {
        detected_onewire_devices->devices[detected_onewire_devices->device_count] = onewire_rom_address;
        detected_onewire_devices->device_type[detected_onewire_devices->device_count] = rom.family;
        detected_onewire_devices->device_count++;
    }
    else {
        LOG_WRN("More than %ld OneWire devices present, not adding device to active sensor list!", max_entries);
    }
}

struct kestrel_thermal_sensor_data_temp_t * get_temperature_sensor_by_id(uint32_t sensor_id)
{
    int i;

    for (i = 0; i < kestrel_temp_sensor_count; i++)
    {
        if (kestrel_temp_sensor_data[i].sensor_id == sensor_id)
        {
            return &kestrel_temp_sensor_data[i];
        }
    }

    return NULL;
}

struct kestrel_pressure_sensor_data_t * get_pressure_sensor_by_id(uint32_t sensor_id)
{
    int i;

    for (i = 0; i < kestrel_pressure_sensor_count; i++)
    {
        if (kestrel_pressure_sensor_data[i].sensor_id == sensor_id)
        {
            return &kestrel_pressure_sensor_data[i];
        }
    }

    return NULL;
}

bool thermal_sensor_valid_last_n_ms(struct kestrel_thermal_sensor_data_temp_t * sensor_data, uint64_t ms)
{
    uint64_t timestamp = k_uptime_get();

    if (sensor_data->last_valid_timestamp == 0) {
        return sensor_data->valid;
    }

    if (sensor_data->last_valid_timestamp > timestamp) {
        // Something has gone badly wrong somewhere...
        return false;
    }

    if ((timestamp - sensor_data->last_valid_timestamp) < ms) {
        return true;
    }

    return false;
}

bool pressure_sensor_valid_last_n_ms(struct kestrel_pressure_sensor_data_t * sensor_data, uint64_t ms)
{
    uint64_t timestamp = k_uptime_get();

    if (sensor_data->last_valid_timestamp == 0) {
        return sensor_data->valid;
    }

    if (sensor_data->last_valid_timestamp > timestamp) {
        // Something has gone badly wrong somewhere...
        return false;
    }

    if ((timestamp - sensor_data->last_valid_timestamp) < ms) {
        return true;
    }

    return false;
}

int64_t get_thermal_sensor_update_age_ms(struct kestrel_thermal_sensor_data_temp_t * sensor_data)
{
    uint64_t timestamp = k_uptime_get();

    if (!sensor_data->valid) {
        return -1;
    }

    if (sensor_data->last_valid_timestamp == 0) {
        return -1;
    }

    if (sensor_data->last_valid_timestamp > timestamp) {
        // Something has gone badly wrong somewhere...
        return -2;
    }

    return timestamp - sensor_data->last_valid_timestamp;
}

int64_t get_pressure_sensor_update_age_ms(struct kestrel_pressure_sensor_data_t * sensor_data)
{
    uint64_t timestamp = k_uptime_get();

    if (!sensor_data->valid) {
        return -1;
    }

    if (sensor_data->last_valid_timestamp == 0) {
        return -1;
    }

    if (sensor_data->last_valid_timestamp > timestamp) {
        // Something has gone badly wrong somewhere...
        return -2;
    }

    return timestamp - sensor_data->last_valid_timestamp;
}

void apply_temperature_calibration_for_thermal_sensor(struct kestrel_thermal_sensor_data_temp_t * sensor_data)
{
    if (sensor_data->temperature_calibration_point_count > 0)
    {
        // TODO
        // Implement more advanced calibration algorithm
        // For now, just use the first calibration point pair to determine a fixed offset and apply that
        double offset = sensor_data->temperature_calibration_points[0].sensor_reading - sensor_data->temperature_calibration_points[0].observed_value;
        sensor_data->temperature_c = sensor_data->temperature_c - offset;
    }
}

void apply_calibration_for_pressure_sensor(struct kestrel_pressure_sensor_data_t * sensor_data)
{
    if (sensor_data->pressure_calibration_point_count > 0)
    {
        // TODO
        // Implement more advanced calibration algorithm
        // For now, just use the first calibration point pair to determine a fixed offset and apply that
        double offset = sensor_data->pressure_calibration_points[0].sensor_reading - sensor_data->pressure_calibration_points[0].observed_value;
        sensor_data->pressure_pa = sensor_data->pressure_pa - offset;
    }
}

void load_temperature_calibration_data_for_thermal_sensor(struct kestrel_thermal_sensor_data_temp_t * sensor_data)
{
    char * pos1;
    char * pos2;
    size_t file_len;
    size_t len;

    // Load any calibration data from the configuration ROM, if present
    char temperature_calibration_filename_string[MAX_PATH_LEN];
    snprintf(temperature_calibration_filename_string, sizeof(temperature_calibration_filename_string)-1, "temperature-sensor-calibration-data/%x", sensor_data->sensor_id);
    temperature_calibration_filename_string[sizeof(temperature_calibration_filename_string)-1] = 0x0;
    char * temperature_calibration_file_contents = read_vpd_file_contents(temperature_calibration_filename_string);
    if (temperature_calibration_file_contents) {
        LOG_INF("Found calibration data for sensor ID 0x%08x, loading...", sensor_data->sensor_id);
    }

    char temperature_calibration_point_string[64];
    file_len = strlen(temperature_calibration_file_contents);
    pos1 = temperature_calibration_file_contents;
    while ((pos1 - temperature_calibration_file_contents) < file_len) {
        pos2 = strstr(pos1, ":");
        if (pos2) {
            len = pos2 - pos1;
            if (len > (sizeof(temperature_calibration_point_string) - 1)) {
                len = sizeof(temperature_calibration_point_string) - 1;
            }
            memcpy(temperature_calibration_point_string, pos1, len);
            temperature_calibration_point_string[len] = 0x0;
            sensor_data->temperature_calibration_points[sensor_data->temperature_calibration_point_count].sensor_reading = strtoull(temperature_calibration_point_string, NULL, 10) / 100.0;
        }
        else {
            break;
        }
        pos1 = pos2 + 1;
        pos2 = strstr(pos1, "\n");
        if (pos2) {
            len = pos2 - pos1;
            if (len > (sizeof(temperature_calibration_point_string) - 1)) {
                len = sizeof(temperature_calibration_point_string) - 1;
            }
            memcpy(temperature_calibration_point_string, pos1, len);
            temperature_calibration_point_string[len] = 0x0;
            sensor_data->temperature_calibration_points[sensor_data->temperature_calibration_point_count].observed_value = strtoull(temperature_calibration_point_string, NULL, 10) / 100.0;
        }
        else {
            break;
        }
        pos1 = pos2 + 1;

        sensor_data->temperature_calibration_point_count++;
    }

    LOG_INF("Loaded %d calibration points for sensor ID 0x%08x", sensor_data->temperature_calibration_point_count, sensor_data->sensor_id);

    free(temperature_calibration_file_contents);
}

void load_calibration_data_for_pressure_sensor(struct kestrel_pressure_sensor_data_t * sensor_data)
{
    char * pos1;
    char * pos2;
    size_t file_len;
    size_t len;

    // Load any calibration data from the configuration ROM, if present
    char pressure_calibration_filename_string[MAX_PATH_LEN];
    snprintf(pressure_calibration_filename_string, sizeof(pressure_calibration_filename_string)-1, "pressure-sensor-calibration-data/%x", sensor_data->sensor_id);
    pressure_calibration_filename_string[sizeof(pressure_calibration_filename_string)-1] = 0x0;
    char * pressure_calibration_file_contents = read_vpd_file_contents(pressure_calibration_filename_string);
    if (pressure_calibration_file_contents) {
        LOG_INF("Found calibration data for sensor ID 0x%08x, loading...", sensor_data->sensor_id);
    }

    char pressure_calibration_point_string[64];
    file_len = strlen(pressure_calibration_file_contents);
    pos1 = pressure_calibration_file_contents;
    while ((pos1 - pressure_calibration_file_contents) < file_len) {
        pos2 = strstr(pos1, ":");
        if (pos2) {
            len = pos2 - pos1;
            if (len > (sizeof(pressure_calibration_point_string) - 1)) {
                len = sizeof(pressure_calibration_point_string) - 1;
            }
            memcpy(pressure_calibration_point_string, pos1, len);
            pressure_calibration_point_string[len] = 0x0;
            sensor_data->pressure_calibration_points[sensor_data->pressure_calibration_point_count].sensor_reading = strtoull(pressure_calibration_point_string, NULL, 10) / 100.0;
        }
        else {
            break;
        }
        pos1 = pos2 + 1;
        pos2 = strstr(pos1, "\n");
        if (pos2) {
            len = pos2 - pos1;
            if (len > (sizeof(pressure_calibration_point_string) - 1)) {
                len = sizeof(pressure_calibration_point_string) - 1;
            }
            memcpy(pressure_calibration_point_string, pos1, len);
            pressure_calibration_point_string[len] = 0x0;
            sensor_data->pressure_calibration_points[sensor_data->pressure_calibration_point_count].observed_value = strtoull(pressure_calibration_point_string, NULL, 10) / 100.0;
        }
        else {
            break;
        }
        pos1 = pos2 + 1;

        sensor_data->pressure_calibration_point_count++;
    }

    LOG_INF("Loaded %d calibration points for sensor ID 0x%08x", sensor_data->pressure_calibration_point_count, sensor_data->sensor_id);

    free(pressure_calibration_file_contents);
}

static void scan_onewire_devices_on_controller(int controller_id)
{
    onewire_device_list_t detected_onewire_devices = {0};
    int i;
    int sensor;
    int sensor_present;
    int sensor_index;
    char * pos1;
    char * pos2;
    size_t len;

    const struct device * dev;
#if DT_NODE_HAS_STATUS(DT_NODELABEL(onewire0), okay)
    if (controller_id == 0) {
        dev = DEVICE_DT_GET(DT_NODELABEL(onewire0));
    }
#endif
#if DT_NODE_HAS_STATUS(DT_NODELABEL(onewire1), okay)
    else if (controller_id == 1) {
        dev = DEVICE_DT_GET(DT_NODELABEL(onewire1));
    }
#endif
    else {
        return;
    }

    if (!device_is_ready(dev)) {
            LOG_ERR("OneWire master not ready!");
            return;
    }

    w1_search_rom(dev, zephyr_onewire_search_callback, &detected_onewire_devices);

    LOG_INF("Detected %d 1-wire devices", detected_onewire_devices.device_count);

    char * onewire_vpd_map_file_contents = read_vpd_file_contents("onewire-fru-map");
    if (onewire_vpd_map_file_contents) {
        LOG_INF("Found OneWire FRU mapping file");
    }

    // For now, assume all detected devices with the temperature sensor family ID are temperature sensors that we want to use
    for (i = 0; i < detected_onewire_devices.device_count; i++)
    {
        if ((detected_onewire_devices.devices[i] & 0xff) != ONEWIRE_DEVICE_FAMILY_TEMPERATURE_SENSOR)
        {
            continue;
        }
        // Is sensor already present?
        sensor_present = 0;
        sensor_index = kestrel_temp_sensor_count;
        for (sensor = 0; sensor < kestrel_temp_sensor_count; sensor++)
        {
            if (kestrel_temp_sensor_data[sensor].fru_type == FRU_TYPE_THERMAL_ONEWIRE)
            {
                if (kestrel_temp_sensor_data[sensor].device_handle == detected_onewire_devices.devices[i])
                {
                    sensor_present = 1;
                    sensor_index = sensor;
                    break;
                }
            }
        }

        if (k_sem_take(&thermal_sensor_data_array_lock, K_FOREVER) == 0) {
            kestrel_temp_sensor_data[sensor_index].sensor_id = 0x100 | (i & 0xff);
            kestrel_temp_sensor_data[sensor_index].fru_type = FRU_TYPE_THERMAL_ONEWIRE;
            kestrel_temp_sensor_data[sensor_index].device_handle = detected_onewire_devices.devices[i];
            if (!sensor_present)
            {
                kestrel_temp_sensor_data[sensor_index].driver_data_ptr = malloc(sizeof(onewire_sensor_driver_data_t));
            }
            kestrel_temp_sensor_data[sensor_index].description[0] = 0x0;
            kestrel_temp_sensor_data[sensor_index].valid = false;
            kestrel_temp_sensor_data[sensor_index].last_valid_timestamp = 0;
            kestrel_temp_sensor_data[sensor_index].temperature_calibration_point_count = 0;

            // Copy driver ID and OneWire configuration data
            if (kestrel_temp_sensor_data[sensor_index].driver_data_ptr) {
                onewire_sensor_driver_data_t * driver_data = kestrel_temp_sensor_data[sensor_index].driver_data_ptr;
#if defined(ONEWIRE0_BASE)
                if (controller_id == 0) {
                    driver_data->onewire_channel = 0;
                    driver_data->controller_address = (uint8_t*)ONEWIRE0_BASE;
                }
#if defined(ONEWIRE1_BASE)
                else if (controller_id == 1) {
                    driver_data->onewire_channel = 1;
                    driver_data->controller_address = (uint8_t*)ONEWIRE1_BASE;
                }
#endif
                else {
                    driver_data->onewire_channel = -1;
                    driver_data->controller_address = NULL;
                }
#else
                driver_data->onewire_channel = -1;
                driver_data->controller_address = NULL
#endif
                snprintf(driver_data->sensor_driver_id, sizeof(driver_data->sensor_driver_id) - 1, "%s", "DS18B20");
                driver_data->sensor_driver_id[sizeof(driver_data->sensor_driver_id) - 1] = 0x0;
            }

            // Try to locate this device by handle in the configuration ROM, and assign the correct sensor ID if present
            char device_handle_string[64];
            char sensor_id_string[64];
            snprintf(device_handle_string, sizeof(device_handle_string)-1, "%llx:", kestrel_temp_sensor_data[sensor_index].device_handle);
            device_handle_string[sizeof(device_handle_string)-1] = 0x0;
            pos1 = strstr(onewire_vpd_map_file_contents, device_handle_string);
            if (pos1) {
                pos1 += strlen(device_handle_string);
                pos2 = strstr(pos1, ":");
                if (pos2) {
                    len = pos2 - pos1;
                    if (len > (sizeof(sensor_id_string) - 1)) {
                        len = sizeof(sensor_id_string) - 1;
                    }
                    memcpy(sensor_id_string, pos1, len);
                    sensor_id_string[len] = 0x0;
                    kestrel_temp_sensor_data[sensor_index].sensor_id = strtoull(sensor_id_string, NULL, 16);
                }
                pos1 = pos2 + 1;
                pos2 = strstr(pos1, "\n");
                if (pos2) {
                    len = pos2 - pos1;
                    if (len > (MAX_SENSOR_DESC_LEN - 1)) {
                        len = MAX_SENSOR_DESC_LEN - 1;
                    }
                    memcpy(kestrel_temp_sensor_data[sensor_index].description, pos1, len);
                    kestrel_temp_sensor_data[sensor_index].description[len] = 0x0;
                }
            }

            LOG_INF("Found %s temperature sensor with serial number 0x%016llx, enabling as ID 0x%08x",
                          (sensor_present)?"existing":"new",
                          kestrel_temp_sensor_data[sensor_index].device_handle,
                          kestrel_temp_sensor_data[sensor_index].sensor_id);

            // Load any calibration data from the configuration ROM, if present
            load_temperature_calibration_data_for_thermal_sensor(&kestrel_temp_sensor_data[sensor_index]);

            if (!sensor_present) {
                kestrel_temp_sensor_count++;
            }

            k_sem_give(&thermal_sensor_data_array_lock);
        }
    }

    free(onewire_vpd_map_file_contents);
}

void scan_onewire_devices(void)
{
    scan_onewire_devices_on_controller(0);
    scan_onewire_devices_on_controller(1);
}
