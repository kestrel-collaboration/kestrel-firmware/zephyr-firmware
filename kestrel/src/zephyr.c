// © 2021-2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#include "build_config.h"

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(kestrel_init, LOG_LEVEL_DBG);

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log_ctrl.h>
#include <zephyr/net/net_config.h>
#include <zephyr/device.h>
#include <generated/mem.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/watchdog.h>

#include "webservice.h"
#include "flash_filesystem.h"
#include "sbe_fsi.h"
#include "onewire_functions.h"
#include "configuration.h"
#include "nvram.h"
#include "sensors.h"
#include "fan_tach.h"
#include "thermal_control.h"
#include "time.h"
#include "kestrel.h"

static struct gpio_callback host_power_button_callback_data;
static struct gpio_callback host_reset_button_callback_data;

static const struct gpio_dt_spec kestrel_power_button_hog = KESTREL_HOST_POWER_BUTTON_HOG_GPIO_BUTTON;
static const struct gpio_dt_spec kestrel_power_button = KESTREL_HOST_POWER_BUTTON_GPIO_BUTTON;
static const struct gpio_dt_spec kestrel_reset_button_hog = KESTREL_HOST_RESET_BUTTON_HOG_GPIO_BUTTON;
static const struct gpio_dt_spec kestrel_reset_button = KESTREL_HOST_RESET_BUTTON_GPIO_BUTTON;

// OpenPOWER stacks are large to begin with...
#define STANDARD_THREAD_STACK_SIZE	32768
// .. and Kestrel has a nasty habit of long call chains with lots of
// stack-allocated variables at every call!
#define DEEP_CALL_STACK_THREAD_SIZE	(STANDARD_THREAD_STACK_SIZE * 2)

#define KESTREL_INIT_THREAD_PRIORITY		K_PRIO_PREEMPT(CONFIG_NUM_PREEMPT_PRIORITIES - 1)
static K_THREAD_STACK_DEFINE(kestrel_init_thread_stack, STANDARD_THREAD_STACK_SIZE);
static struct k_thread kestrel_init_thread_data;
static struct k_sem kestrel_init_thread_lock;

static K_THREAD_STACK_DEFINE(onewire_init_thread_stack, DEEP_CALL_STACK_THREAD_SIZE);
static struct k_thread onewire_init_thread_data;
static struct k_sem onewire_temperature_access_lock;

static K_THREAD_STACK_DEFINE(fan_tach_init_thread_stack, DEEP_CALL_STACK_THREAD_SIZE);
static struct k_thread fan_tach_init_thread_data;
static struct k_sem fan_tach_device_access_lock;

static K_THREAD_STACK_DEFINE(kestrel_service_thread_stack, STANDARD_THREAD_STACK_SIZE);
static struct k_thread kestrel_service_thread_data;

static K_THREAD_STACK_DEFINE(fan_tach_service_thread_stack, DEEP_CALL_STACK_THREAD_SIZE);
static struct k_thread fan_tach_service_thread_data;

static K_THREAD_STACK_DEFINE(thermal_service_thread_stack, DEEP_CALL_STACK_THREAD_SIZE);
static struct k_thread thermal_service_thread_data;

static K_THREAD_STACK_DEFINE(sensor_service_thread_stack, DEEP_CALL_STACK_THREAD_SIZE);
static struct k_thread sensor_service_thread_data;

static K_THREAD_STACK_DEFINE(time_service_thread_stack, STANDARD_THREAD_STACK_SIZE);
static struct k_thread time_service_thread_data;

static K_THREAD_STACK_DEFINE(watchdog_service_thread_stack, STANDARD_THREAD_STACK_SIZE);
static struct k_thread watchdog_service_thread_data;

#define KESTREL_IDLE_THREAD_PRIORITY		K_PRIO_PREEMPT(CONFIG_NUM_PREEMPT_PRIORITIES - 1)

#define BUTTON_PRESS_DEBOUNCE_DELAY_MS	200

#define POWER9_OCC_FAIL_COUNT_LIMIT	6
#define POWER9_OCC_POLLING_RATE_HZ	2

static int desired_host_power_operation = 0;
static uint64_t host_power_button_last_press = 0;

bool thermal_service_thread_online = false;
bool sensor_service_thread_online = false;
bool fan_tach_service_thread_online = false;

bool rescan_all_thermal_sensors = false;
bool clear_all_thermal_sensors = false;

void flush_zephyr_log_buffer(void)
{
	while (LOG_PROCESS()) {}
}

static void kestrel_net_init_workqueue_handler(struct k_work *work)
{
	net_config_init_app(NULL, "Kestrel BMC network services");
}

static K_WORK_DEFINE(kestrel_net_config_init, kestrel_net_init_workqueue_handler);

static void kestrel_init_thread(void)
{
	kestrel_init();
	k_sem_give(&kestrel_init_thread_lock);
}

static void kestrel_service_thread(void)
{
	while (1) {
		if (host_background_service_task_active || host_console_service_task_requested) {
			if (primary_service_event_loop() != -EAGAIN) {
			    k_usleep(1000000 / CONFIG_SYS_CLOCK_TICKS_PER_SEC);
			}
		}
		else {
			k_usleep(1000000 / CONFIG_SYS_CLOCK_TICKS_PER_SEC);
		}
	}
}

static void thermal_service_thread(void)
{
	uint8_t response_data[IBM_POWER9_SBE_OCC_RESPONSE_SIZE];
	size_t response_length = 0;
	static uint8_t consecutive_occ_error_count;
	int ret;

	if (initialize_sbe_fifo_access()) {
		LOG_ERR("Unable to initialize SBE FIFO access, aborting! %s:%d\n", __FILE__, __LINE__);
		return;
	}

	// FIXME
	// Try to find a better way to do this than to wait for the slowest sensor bank to initialize / configure
	if (k_sem_take(&onewire_temperature_access_lock, K_FOREVER) == 0) {
		if (k_sem_take(&thermal_sensor_data_array_lock, K_FOREVER) == 0) {
			kestrel_sort_temperature_sensors();
			k_sem_give(&thermal_sensor_data_array_lock);
		}

		k_sem_give(&onewire_temperature_access_lock);
	}

	thermal_service_thread_online = true;

	LOG_INF("Thermal service thread online");

	consecutive_occ_error_count = 0;
	while (1) {
		if (rescan_all_thermal_sensors) {
			if ((k_sem_take(&onewire_temperature_access_lock, K_MSEC(500)) == 0)
				&& (k_sem_take(&thermal_sensor_data_array_lock, K_MSEC(500)) == 0)) {

				k_sem_give(&thermal_sensor_data_array_lock);

				if (clear_all_thermal_sensors) {
					kestrel_temp_sensor_count = 0;
				}

				scan_onewire_devices();
				kestrel_sort_temperature_sensors();

				rescan_all_thermal_sensors = false;

				k_sem_give(&onewire_temperature_access_lock);

				LOG_INF("Thermal sensor rescan complete!");
			}
		}

		if (HAS_FSI_CONTROLLER) {
			if (host_power_status == HOST_POWER_STATUS_RUNNING)
			{
				if (k_mutex_lock(&occ_access_mutex, K_MSEC(1000)) != 0) {
					LOG_WRN("Unable to acquire OCC mutex in a timely manner! %s:%d\n", __FILE__, __LINE__);
					continue;
				}

				if (occ_poll(0, 0x2400, IBM_POWER9_SBE_OCC_RESPONSE_SIZE, response_data, &response_length))
				{
					// Unable to read sensors
					consecutive_occ_error_count++;
					if (consecutive_occ_error_count > POWER9_OCC_FAIL_COUNT_LIMIT)
					{
						host_cpu_temperature_sensor_count = 0;
					}
				}
				else {
					consecutive_occ_error_count = 0;
				}

				k_mutex_unlock(&occ_access_mutex);

				// NOTE: The OCC polling process is somewhat CPU intensive
				// There is no point in polling much over 5Hz or so due to thermal lag in the CPU package,
				// and keeping the polling rate low improves overall BMC responsiveness
				k_usleep(1000000 / POWER9_OCC_POLLING_RATE_HZ);
			}
			else
			{
				host_cpu_temperature_sensor_count = 0;
				k_usleep(1000000);
			}
		}

		// Synchronize OneWire device access
		if (k_sem_take(&onewire_temperature_access_lock, K_MSEC(500)) == 0)
		{
			// Read OneWire temperature sensors
			if (kestrel_temp_sensor_count > 0)
			{
				int i;
				int retry_count;
				float temperature;

#if defined(ONEWIRE0_BASE)
				start_simultaneous_ds18b20_temperature_conversions((uint8_t*)ONEWIRE0_BASE);
#endif
#if defined(ONEWIRE1_BASE)
				start_simultaneous_ds18b20_temperature_conversions((uint8_t*)ONEWIRE1_BASE);
#endif
				k_sem_give(&onewire_temperature_access_lock);

				k_usleep(1000000);

				if (k_sem_take(&onewire_temperature_access_lock, K_MSEC(500)) == 0)
				{
					for (i=0; i<kestrel_temp_sensor_count; i++)
					{
						if (kestrel_temp_sensor_data[i].fru_type == FRU_TYPE_THERMAL_ONEWIRE)
						{
							if (kestrel_temp_sensor_data[i].driver_data_ptr) {
								// OneWire can be flaky in the presence of RFI...retry the read up to 5 times before giving up
								for (retry_count = 0; retry_count < 5; retry_count++) {
									ret = read_ds18b20_temperature(
										((onewire_sensor_driver_data_t*)kestrel_temp_sensor_data[i].driver_data_ptr)->controller_address,
										kestrel_temp_sensor_data[i].device_handle, 1, &temperature);
									if (!ret)
									{
										kestrel_temp_sensor_data[i].temperature_c = temperature;
										apply_temperature_calibration_for_thermal_sensor(&kestrel_temp_sensor_data[i]);
										kestrel_temp_sensor_data[i].valid = 1;
										kestrel_temp_sensor_data[i].last_valid_timestamp = k_uptime_get();
										break;
									}
								}
								if (ret)
								{
									LOG_WRN("OneWire DS18B20 read for sensor ID 0x%08x failed, retcode %d", kestrel_temp_sensor_data[i].sensor_id, ret);
									kestrel_temp_sensor_data[i].valid = 0;
								}
							}
							else {
								kestrel_temp_sensor_data[i].valid = 0;
							}
						}
					}

					k_sem_give(&onewire_temperature_access_lock);
				}
			}
		}
	}
}

static void sensor_service_thread(void)
{
	// FIXME
	// Try to find a better way to do this than to wait for the slowest sensor bank to initialize / configure
	if (k_sem_take(&pressure_sensor_data_array_lock, K_FOREVER) == 0) {
		kestrel_sort_pressure_sensors();
		k_sem_give(&pressure_sensor_data_array_lock);
	}

	sensor_service_thread_online = true;

	LOG_INF("Sensor service thread online");

	while (1) {
		// Add non-thermal sensor reads here

		k_msleep(100);
	}
}

static void fan_tach_service_thread(void)
{
	if (k_sem_take(&fan_tach_device_access_lock, K_FOREVER) == 0) {
		if (k_sem_take(&fan_tach_data_array_lock, K_FOREVER) == 0) {
			kestrel_sort_fan_tach_devices();
			k_sem_give(&fan_tach_data_array_lock);
		}

		k_sem_give(&fan_tach_device_access_lock);
	}

	fan_tach_service_thread_online = true;

	LOG_INF("Fan / tach service thread online");

	while (1) {
		sync_all_fan_data_to_hw();
		read_all_fan_data_from_hw();
		read_all_tach_data_from_hw();
		k_msleep(100);
	}
}

static void time_service_thread(void)
{
	uint64_t current_uptime = 0;
	uint64_t last_time_sync = 0;
	uint64_t time_sync_interval = 0;
	const char* time_sync_server = NULL;
	const char* interval_string = NULL;
	bool time_sync_enabled = false;

	while (1) {
		time_sync_server = nvram_get_value_for_key(nvram_settings, "sntp_server");
		interval_string = nvram_get_value_for_key(nvram_settings, "sntp_sync_interval_s");
		if (time_sync_server && interval_string) {
			time_sync_interval = strtoull(interval_string, NULL, 10);
			if (time_sync_interval > 0) {
				time_sync_enabled = true;
				current_uptime = k_uptime_get();
				if (((current_uptime - last_time_sync) > (time_sync_interval * 1000))
					|| (last_time_sync == 0)) {
					if (sync_time_with_sntp_server(time_sync_server)) {
						// Try again in 60 seconds, as this may have been a transient failure
						LOG_WRN("Unable to synchronize with SNTP server, trying again in 60 seconds");
						k_msleep(60 * 1000);
						if (sync_time_with_sntp_server(time_sync_server)) {
							LOG_ERR("Unable to synchronize with SNTP server, waiting for next configured sync interval");
						}
					}
					last_time_sync = current_uptime;
				}
			}
			else {
				time_sync_enabled = false;
			}
		}
		else {
			time_sync_enabled = false;
		}

		k_msleep(1000);
	}
}

static void onewire_init_thread(void)
{
	scan_onewire_devices();

	k_sem_give(&onewire_temperature_access_lock);
}

static void fan_tach_init_thread(void)
{
	configure_fan_devices();
	configure_tach_devices();

	k_sem_give(&fan_tach_device_access_lock);
}

static void watchdog_service_thread(void)
{
	int retry = 0;
	int ret = 0;
	int watchdog_channel_id = 0;
	uint64_t current_uptime = 0;
	uint64_t last_time_delta = 0;

	const struct device *const watchdog0 = DEVICE_DT_GET(DT_ALIAS(watchdog0));

	struct wdt_timeout_cfg watchdog_configuration = {
		.flags = WDT_FLAG_RESET_SOC,

		.window.min = 0,
		.window.max = 15 * 1000
	};

	LOG_INF("Setting up protective watchdog");

	for (retry = 0; retry < 10; retry++ ) {
		if (device_is_ready(watchdog0)) {
			break;
		}

		LOG_WRN("%s: device not ready, retrying...", watchdog0->name);
		k_msleep(10 * 1000);
	}
	if (!device_is_ready(watchdog0)) {
		LOG_ERR("%s: device not ready, aborting watchdog thread. System WILL NOT automatically recover from kernel panics!", watchdog0->name);
		return;
	}

	watchdog_channel_id = wdt_install_timeout(watchdog0, &watchdog_configuration);
	if (watchdog_channel_id < 0) {
		LOG_ERR("Unable to configure watchdog, aborting watchdog thread. System WILL NOT automatically recover from kernel panics!");
		return;
	}

	ret = wdt_setup(watchdog0, WDT_FLAG_RESET_SOC);
	if (ret < 0) {
		LOG_ERR("Unable to activate watchdog, aborting watchdog thread. System WILL NOT automatically recover from kernel panics!");
		return;
        }

	LOG_INF("Watchdog online");

	current_uptime = k_uptime_get();

	while (1) {
		// Ideally, feed the watchdog every 5 seconds
		k_msleep(5 * 1000);

		wdt_feed(watchdog0, watchdog_channel_id);

		last_time_delta = k_uptime_delta(&current_uptime);
		if (last_time_delta > (6 * 1000)) {
			LOG_WRN("Watchdog deadline missed, waited %lld ms", last_time_delta);
		}
	}
}

// TODO
// Implement support for more than one CPU module
// The code below assumes all OCC sensors are part of CPU 0
static void fan_control_step_handler(struct k_work *work)
{
	int i;
	double highest_cpu_core_temperature = 0;

	for (i = 0; i < host_cpu_temperature_sensor_count; i++) {
		if (host_cpu_temperature_sensor_data[i].temperature_c > highest_cpu_core_temperature) {
			highest_cpu_core_temperature = host_cpu_temperature_sensor_data[i].temperature_c;
		}
	}

	if (host_cpu_temperature_sensor_count > 0) {
		for (i = 0; i < kestrel_pid_controller_count; i++) {
			if (!kestrel_pid_controller_data[i].enabled) {
				continue;
			}

			kestrel_pid_controller_data[i].sense = highest_cpu_core_temperature;
			kestrel_pid_controller_data[i].timestep = KESTREL_FAN_CONTROL_TIMER_STEP_MS / 1000.0;
			run_pid_step(&kestrel_pid_controller_data[i]);

			kestrel_fan_data[i].fan_setting_percent = kestrel_pid_controller_data[i].drive * 100.0;
			kestrel_fan_data[i].fan_set_online = true;
		}
	}
	else {
		// Sensor failure!
		// Run all PID controlled fans at full speed
		for (i = 0; i < kestrel_pid_controller_count; i++) {
			if (kestrel_pid_controller_data[i].enabled) {
				kestrel_fan_data[i].fan_setting_percent = kestrel_pid_controller_data[i].drive * 100.0;
				kestrel_fan_data[i].fan_set_online = true;
			}
		}
	}
}

K_WORK_DEFINE(fan_control_step, fan_control_step_handler);

void fan_control_step_timer_handler(struct k_timer *timer)
{
	k_work_submit(&fan_control_step);
}

K_TIMER_DEFINE(fan_control_timer, fan_control_step_timer_handler, NULL);

static void kestrel_host_power_button_workqueue_handler(struct k_work *work)
{
	if (desired_host_power_operation == 1) {
		power_on_host();
	}
	else {
		power_off_chassis(0);
	}
}

static K_WORK_DEFINE(kestrel_host_power_button_pressed, kestrel_host_power_button_workqueue_handler);

void host_power_button_pressed(const struct device *dev, struct gpio_callback *cb, uint32_t pins)
{
	int want_power_on = 0;
	int want_power_off = 0;

	// Deactivate interrupts on entering critical section
	int key = irq_lock();

	if (host_power_status == HOST_POWER_STATUS_OFFLINE) {
		want_power_on = 1;
	}
	else {
		want_power_off = 1;
	}

	// Re-activate interupts on exiting critical section
	irq_unlock(key);

	if ((k_uptime_get() - host_power_button_last_press) > BUTTON_PRESS_DEBOUNCE_DELAY_MS) {
		// The debounce delay has passed since last press, handle request
		LOG_INF("Host power button pressed");

		host_power_button_last_press = k_uptime_get();

		// Queue action...
		if (want_power_on) {
			desired_host_power_operation = 1;
		}
		else if (want_power_off) {
			desired_host_power_operation = 0;
		}

		// .. and submit to system workqueue for execution
		k_work_submit(&kestrel_host_power_button_pressed);
	}
}

void host_reset_button_pressed(const struct device *dev, struct gpio_callback *cb, uint32_t pins)
{
	static uint64_t last_press = 0;

	// Deactivate interrupts on entering critical section
	int key = irq_lock();

	// FIXME
	// Implement host reset functionality

	// Re-activate interupts on exiting critical section
	irq_unlock(key);

	if ((k_uptime_get() - last_press) > BUTTON_PRESS_DEBOUNCE_DELAY_MS) {
		// The debounce delay has passed since last press, handle request
		LOG_WRN("[FIXME] Reset button pressed, but not implemented!");
	}

	last_press = k_uptime_get();
}

static int configure_gpios(void)
{
	const struct gpio_dt_spec * button;
	int retcode;
	int ret;

	retcode = 0;

	// Set up power button
	button = &kestrel_power_button_hog;
	ret = gpio_pin_configure_dt(button, GPIO_OUTPUT_HIGH);
	if (ret != 0) {
		printk("[ERROR] failed to configure hog on %s pin %d (returned %d)\n", KESTREL_HOST_POWER_BUTTON_HOG_GPIO_NODE, button->pin, ret);
		retcode = -1;
	}
	button = &kestrel_power_button;
	ret = gpio_pin_interrupt_configure_dt(button, GPIO_INT_EDGE_TO_ACTIVE);
	if (ret != 0) {
		printk("[ERROR] failed to configure interrupt on %s pin %d (returned %d)\n", KESTREL_HOST_POWER_BUTTON_GPIO_NODE, button->pin, ret);
		retcode = -1;
	}
	else {
		gpio_init_callback(&host_power_button_callback_data, host_power_button_pressed, BIT(button->pin));
		gpio_add_callback(button->port, &host_power_button_callback_data);
	}

	// Set up reset button
	button = &kestrel_reset_button_hog;
	ret = gpio_pin_configure_dt(button, GPIO_OUTPUT_HIGH);
	if (ret != 0) {
		printk("[ERROR] failed to configure hog on %s pin %d (returned %d)\n", KESTREL_HOST_RESET_BUTTON_HOG_GPIO_NODE, button->pin, ret);
		retcode = -1;
	}
	button = &kestrel_reset_button;
	ret = gpio_pin_interrupt_configure_dt(button, GPIO_INT_EDGE_TO_ACTIVE);
	if (ret != 0) {
		printk("[ERROR] failed to configure interrupt on %s pin %d (returned %d)\n", KESTREL_HOST_RESET_BUTTON_GPIO_NODE, button->pin, ret);
		retcode = -1;
	}
	else {
		gpio_init_callback(&host_reset_button_callback_data, host_reset_button_pressed, BIT(button->pin));
		gpio_add_callback(button->port, &host_reset_button_callback_data);
	}

	return retcode;
}

void setup_shell_ux(void)
{
	// Set root command and prompt for all shells
	STRUCT_SECTION_FOREACH(shell, sh) {
		shell_prompt_change(sh, "FSP0>");
	}
	shell_set_root_cmd("kestrel");
}

void main(void)
{
	// Early shell setup
	setup_shell_ux();

	k_tid_t kestrel_init_thread_id;

	k_sem_init(&kestrel_init_thread_lock, 0, UINT_MAX);
	k_sem_init(&nvram_access_lock, 0, UINT_MAX);
	k_sem_init(&onewire_temperature_access_lock, 0, UINT_MAX);
	k_sem_init(&thermal_sensor_data_array_lock, 0, UINT_MAX);
	k_sem_init(&pressure_sensor_data_array_lock, 0, UINT_MAX);
	k_sem_init(&fan_tach_device_access_lock, 0, UINT_MAX);
	k_sem_init(&fan_tach_data_array_lock, 0, UINT_MAX);

	// Run early setup
	initialize_flash_filesystem();

	// Set up Ethernet controller
	set_ethernet_mac_from_vpd();

	// Initialize internal time offset from RTC
	load_current_hw_rtc_time();

	// Initialize network shell(s)
	enable_shell_telnet();
	// Late shell setup (network shells)
	setup_shell_ux();

	// Load NVRAM settings
	nvram_settings = load_nvram_settings_from_persistent_storage();
	k_sem_give(&nvram_access_lock);

	// Start watchdog service
	watchdog_service_thread_id = k_thread_create(&watchdog_service_thread_data, watchdog_service_thread_stack,
			K_THREAD_STACK_SIZEOF(watchdog_service_thread_stack),
			(k_thread_entry_t)watchdog_service_thread,
			NULL, NULL, NULL, WATCHDOG_SERVICE_THREAD_PRIORITY, 0, K_NO_WAIT);
	k_thread_name_set(watchdog_service_thread_id, "watchdog_service");

	// Kick off Kestrel initialization thread
	kestrel_init_thread_id = k_thread_create(&kestrel_init_thread_data, kestrel_init_thread_stack,
			K_THREAD_STACK_SIZEOF(kestrel_init_thread_stack),
			(k_thread_entry_t)kestrel_init_thread,
			NULL, NULL, NULL, KESTREL_INIT_THREAD_PRIORITY, 0, K_NO_WAIT);
	k_thread_name_set(kestrel_init_thread_id, "sys_init");

	// Start background services
	start_webservice();

	// Wait for Kestrel initialization to finish
	k_sem_take(&kestrel_init_thread_lock, K_FOREVER);

	// Allow fan / tach setup routines to run
	k_sem_give(&fan_tach_data_array_lock);

	// Allow thermal sensor setup routines to run
	k_sem_give(&thermal_sensor_data_array_lock);

	// Allow pressure sensor setup routines to run
	k_sem_give(&pressure_sensor_data_array_lock);

	// Start fan / tach device configuration
	fan_tach_init_thread_id = k_thread_create(&fan_tach_init_thread_data, fan_tach_init_thread_stack,
			K_THREAD_STACK_SIZEOF(fan_tach_init_thread_stack),
			(k_thread_entry_t)fan_tach_init_thread,
			NULL, NULL, NULL, KESTREL_INIT_THREAD_PRIORITY, 0, K_NO_WAIT);
	k_thread_name_set(fan_tach_init_thread_id, "fan_tach_init");

	// Start OneWire device configuration
	onewire_init_thread_id = k_thread_create(&onewire_init_thread_data, onewire_init_thread_stack,
			K_THREAD_STACK_SIZEOF(onewire_init_thread_stack),
			(k_thread_entry_t)onewire_init_thread,
			NULL, NULL, NULL, KESTREL_INIT_THREAD_PRIORITY, 0, K_NO_WAIT);
	k_thread_name_set(onewire_init_thread_id, "onewire_init");

	kestrel_service_thread_id = k_thread_create(&kestrel_service_thread_data, kestrel_service_thread_stack,
			K_THREAD_STACK_SIZEOF(kestrel_service_thread_stack),
			(k_thread_entry_t)kestrel_service_thread,
			NULL, NULL, NULL, KESTREL_SERVICE_THREAD_PRIORITY, K_ESSENTIAL, K_NO_WAIT);
	k_thread_name_set(kestrel_service_thread_id, "kestrel_service");

	fan_tach_service_thread_id = k_thread_create(&fan_tach_service_thread_data, fan_tach_service_thread_stack,
			K_THREAD_STACK_SIZEOF(fan_tach_service_thread_stack),
			(k_thread_entry_t)fan_tach_service_thread,
			NULL, NULL, NULL, THERMAL_SERVICE_THREAD_PRIORITY, K_ESSENTIAL, K_NO_WAIT);
	k_thread_name_set(fan_tach_service_thread_id, "fan_tach_service");

	thermal_service_thread_id = k_thread_create(&thermal_service_thread_data, thermal_service_thread_stack,
			K_THREAD_STACK_SIZEOF(thermal_service_thread_stack),
			(k_thread_entry_t)thermal_service_thread,
			NULL, NULL, NULL, THERMAL_SERVICE_THREAD_PRIORITY, K_ESSENTIAL, K_NO_WAIT);
	k_thread_name_set(thermal_service_thread_id, "thermal_service");

	sensor_service_thread_id = k_thread_create(&sensor_service_thread_data, sensor_service_thread_stack,
			K_THREAD_STACK_SIZEOF(sensor_service_thread_stack),
			(k_thread_entry_t)sensor_service_thread,
			NULL, NULL, NULL, THERMAL_SERVICE_THREAD_PRIORITY, K_ESSENTIAL, K_NO_WAIT);
	k_thread_name_set(sensor_service_thread_id, "sensor_service");

	time_service_thread_id = k_thread_create(&time_service_thread_data, time_service_thread_stack,
			K_THREAD_STACK_SIZEOF(time_service_thread_stack),
			(k_thread_entry_t)time_service_thread,
			NULL, NULL, NULL, TIME_SERVICE_THREAD_PRIORITY, 0, K_NO_WAIT);
	k_thread_name_set(time_service_thread_id, "time_service");

	// Run late setup
	configure_gpios();

	LOG_INF("Kestrel system ready, launching network configuration\n");

	// Initialize network in system workqueue
	k_work_submit(&kestrel_net_config_init);

	LOG_INF("Kestrel system initialization complete\n");
}
