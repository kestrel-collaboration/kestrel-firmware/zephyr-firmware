// * Copyright (c) 2024 Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#include "build_config.h"

#if (WITH_ZEPHYR)
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(kestrel_time, LOG_LEVEL_DBG);
#endif

#include <zephyr/net/sntp.h>
#include <zephyr/drivers/rtc.h>
#include <zephyr/sys/timeutil.h>
#include <arpa/inet.h>

#include "time.h"

uint64_t uptime_rtc_offset = 0;
uint64_t boot_timestamp_rtc = 0;

int get_current_hw_rtc_time(time_t * time) {
	const struct device *const rtc_dev = DEVICE_DT_GET(KESTREL_PRIMARY_RTC);
	struct rtc_time zephyr_rtc_time;
	struct tm * rtc_time;
	int ret;

	if (!time) {
		return -EINVAL;
	}

	if (!rtc_dev) {
		return -ENODEV;
	}

	ret = rtc_get_time(rtc_dev, &zephyr_rtc_time);
	if (ret < 0) {
		LOG_ERR("RTC read failed: %d", ret);
		return ret;
	}

	rtc_time = rtc_time_to_tm(&zephyr_rtc_time);

	*time = timeutil_timegm64(rtc_time);

	return 0;
}

int load_current_hw_rtc_time(void) {
	time_t rtc_timestamp;
	int ret;

	ret = get_current_hw_rtc_time(&rtc_timestamp);
	if (ret) {
		return ret;
	}

	uptime_rtc_offset = rtc_timestamp - (k_uptime_get() / 1000);

	if (boot_timestamp_rtc == 0) {
		// Looks like the first time the system time was loaded from the RTC
		// since bootup.
		// Set the approximate boot timestamp from the RTC data...
		boot_timestamp_rtc = uptime_rtc_offset;
	}

	return 0;
}

int sync_time_with_sntp_server(const char* sntp_server) {
	const struct device *const rtc_dev = DEVICE_DT_GET(KESTREL_PRIMARY_RTC);
	time_t unix_time;
	struct tm *civil_time;
	struct rtc_time zephyr_rtc_time;
	struct sntp_ctx ctx;
	struct sockaddr_in addr;
	struct sntp_time sntp_time;
	int ret;

	if (!sntp_server) {
		return -EINVAL;
	}

	if (!rtc_dev) {
		return -ENODEV;
	}

	/* ipv4 */
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(SNTP_PORT);
	inet_pton(AF_INET, sntp_server, &addr.sin_addr);

	ret = sntp_init(&ctx, (struct sockaddr *) &addr,
		       sizeof(struct sockaddr_in));
	if (ret < 0) {
		LOG_ERR("Failed to initialize SNTP context, retcode %d", ret);
		sntp_close(&ctx);
		return ret;
	}

	LOG_INF("Sending SNTP request to server %s", sntp_server);
	ret = sntp_query(&ctx, 4 * MSEC_PER_SEC, &sntp_time);
	sntp_close(&ctx);

	if (ret < 0) {
		LOG_ERR("SNTP request failed: %d", ret);
		return ret;
	}
	else {
		unix_time = sntp_time.seconds;
		civil_time = gmtime(&unix_time);

		uptime_rtc_offset = unix_time - (k_uptime_get() / 1000);

		zephyr_rtc_time.tm_sec = civil_time->tm_sec;
		zephyr_rtc_time.tm_min = civil_time->tm_min;
		zephyr_rtc_time.tm_hour = civil_time->tm_hour;
		zephyr_rtc_time.tm_mday = civil_time->tm_mday;
		zephyr_rtc_time.tm_mon = civil_time->tm_mon;
		zephyr_rtc_time.tm_year = civil_time->tm_year;
		zephyr_rtc_time.tm_wday = civil_time->tm_wday;
		zephyr_rtc_time.tm_yday = civil_time->tm_yday;
		zephyr_rtc_time.tm_isdst = civil_time->tm_isdst;

		ret = rtc_set_time(rtc_dev, &zephyr_rtc_time);

		if (ret) {
			LOG_ERR("Unable to set RTC to UNIX timestamp %llu, error %d", unix_time, ret);
			return ret;
		}

		LOG_INF("Set RTC to UNIX timestamp %llu", unix_time);

		if (boot_timestamp_rtc == 0) {
			// Reload system time offset from RTC
			load_current_hw_rtc_time();
		}
	}

	return 0;
}