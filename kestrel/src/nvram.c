// © 2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#include "build_config.h"

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(nvram_storage, LOG_LEVEL_INF);

#include <stdio.h>
#include <stdlib.h>

#include <zephyr/kernel.h>
#include <zephyr/fs/fs.h>
#include <zephyr/data/json.h>

#include "flash_filesystem.h"

#include "nvram.h"

char * nvram_settings_string = NULL;
struct json_nvram_v1_key_value_list * nvram_settings = NULL;
struct k_sem nvram_access_lock;

static const struct json_obj_descr json_nvram_v1_key_value_pair_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_nvram_v1_key_value_pair, "key", key, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_nvram_v1_key_value_pair, "value", value, JSON_TOK_STRING),
};

static const struct json_obj_descr json_nvram_v1_key_value_list_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_nvram_v1_key_value_list, "Name", list_name, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_nvram_v1_key_value_list, "Version", list_version, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_nvram_v1_key_value_list, "Revision", list_revision, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_OBJ_ARRAY_NAMED(struct json_nvram_v1_key_value_list, "Elements", elements, NVRAM_MAX_KEY_VALUE_PAIRS, element_count, json_nvram_v1_key_value_pair_desc, ARRAY_SIZE(json_nvram_v1_key_value_pair_desc)),
};

char * read_nvram_file_contents(const char * nvram_filename) {
	int ret;
	char nvram_directory_path[MAX_PATH_LEN];
	char nvram_file_path[MAX_PATH_LEN];
	char * nvram_file_contents = NULL;
	snprintf(nvram_directory_path, sizeof(nvram_directory_path)-1, "%s/config", kestrel_config_lfs_fs_mount.mnt_point);
	nvram_file_path[sizeof(nvram_directory_path)-1] = 0x0;
	struct fs_dirent nvram_path_entry;
	if (fs_stat(nvram_directory_path, &nvram_path_entry) == -ENOENT) {
		ret = fs_mkdir(nvram_directory_path);
		if (ret && (ret != -EEXIST)) {
			LOG_WRN("Unable to create NVRAM storage directory!  Check persistent storage for failure?");
			return NULL;
		}
	}
	snprintf(nvram_file_path, sizeof(nvram_file_path)-1, "%s/config/%s", kestrel_config_lfs_fs_mount.mnt_point, nvram_filename);
	nvram_file_path[sizeof(nvram_file_path)-1] = 0x0;
	struct fs_dirent nvram_file_entry;
	struct fs_file_t nvram_file;
	if (fs_stat(nvram_file_path, &nvram_file_entry) == 0) {
		nvram_file_contents = malloc(nvram_file_entry.size + 1);
		if (nvram_file_contents) {
			fs_file_t_init(&nvram_file);
			ret = fs_open(&nvram_file, nvram_file_path, FS_O_READ);
			if (ret >= 0) {
				fs_read(&nvram_file, nvram_file_contents, nvram_file_entry.size);
				*(nvram_file_contents + nvram_file_entry.size) = 0x0;
				fs_close(&nvram_file);
			}
			else {
				*nvram_file_contents = 0x0;
			}
		}
	}

	return nvram_file_contents;
}

int write_nvram_file_contents(const char * nvram_filename, const char * file_contents) {
	int ret;
	size_t len;
	char nvram_directory_path[MAX_PATH_LEN];
	char nvram_file_path_temp[MAX_PATH_LEN];
	char nvram_file_path[MAX_PATH_LEN];
	snprintf(nvram_directory_path, sizeof(nvram_directory_path)-1, "%s/config", kestrel_config_lfs_fs_mount.mnt_point);
	nvram_file_path[sizeof(nvram_directory_path)-1] = 0x0;
	struct fs_dirent nvram_path_entry;
	if (fs_stat(nvram_directory_path, &nvram_path_entry) == -ENOENT) {
		ret = fs_mkdir(nvram_directory_path);
		if (ret && (ret != -EEXIST)) {
			LOG_WRN("Unable to create NVRAM storage directory!  Check persistent storage for failure?");
			return -1;
		}
	}
	snprintf(nvram_file_path, sizeof(nvram_file_path)-1, "%s/config/%s", kestrel_config_lfs_fs_mount.mnt_point, nvram_filename);
	nvram_file_path[sizeof(nvram_file_path)-1] = 0x0;
	snprintf(nvram_file_path_temp, sizeof(nvram_file_path_temp)-1, "%s/.%s.tmp", kestrel_config_lfs_fs_mount.mnt_point, nvram_filename);
	nvram_file_path[sizeof(nvram_file_path_temp)-1] = 0x0;
	struct fs_file_t nvram_file;
	fs_file_t_init(&nvram_file);
	ret = fs_open(&nvram_file, nvram_file_path_temp, FS_O_CREATE | FS_O_TRUNC | FS_O_WRITE);
	if (ret >= 0) {
		len = strlen(file_contents);
		ret = fs_write(&nvram_file, file_contents, len);
		fs_close(&nvram_file);
		if ((ret < 0) || (ret < len)) {
			return -2;
		}

		if (fs_rename(nvram_file_path_temp, nvram_file_path)) {
			return -3;
		}
	}
	else {
		return -1;
	}

	return 0;
}

struct json_nvram_v1_key_value_list * load_nvram_settings_from_persistent_storage(void) {
	struct json_nvram_v1_key_value_list * settings = malloc(sizeof(struct json_nvram_v1_key_value_list));
	int ret;

	if (!settings) {
		return NULL;
	}

	memset(settings, 0, sizeof(struct json_nvram_v1_key_value_list));

	free(nvram_settings_string);
	nvram_settings_string = read_nvram_file_contents("nvram");
	if (!nvram_settings_string) {
		return settings;
	}

	ret = json_obj_parse(nvram_settings_string, strlen(nvram_settings_string), json_nvram_v1_key_value_list_desc,
				ARRAY_SIZE(json_nvram_v1_key_value_list_desc), settings);
	if (ret < 0) {
		memset(settings, 0, sizeof(struct json_nvram_v1_key_value_list));
		return settings;
	}

	return settings;
}

int write_nvram_settings_to_persistent_storage(struct json_nvram_v1_key_value_list * settings) {
	char * json_string_buffer = malloc(NVRAM_JSON_STRING_BUFFER_SIZE);
	int ret;

	if (!json_string_buffer) {
		return -1;
	}

	// Set invariant data
	settings->list_name = "nvram";
	settings->list_version = 1;
	settings->list_revision++;

	ret = json_obj_encode_buf(json_nvram_v1_key_value_list_desc,
				ARRAY_SIZE(json_nvram_v1_key_value_list_desc), settings,
				json_string_buffer,
				NVRAM_JSON_STRING_BUFFER_SIZE - 1);
	if (ret < 0) {
		free(json_string_buffer);
		return -2;
	}

	ret = write_nvram_file_contents("nvram", json_string_buffer);
	free(json_string_buffer);

	// Settings storage string is no longer valid
	free(nvram_settings_string);
	nvram_settings_string = NULL;

	return ret;
}

int nvram_get_index_of_key(struct json_nvram_v1_key_value_list * settings, const char * key) {
	int i;

	if (!key) {
		return -1;
	}

	for (i = 0; i < settings->element_count; i++) {
		if (strcmp(settings->elements[i].key, key) == 0) {
			return i;
		}
	}

	return -1;
}

const char * nvram_get_value_for_key(struct json_nvram_v1_key_value_list * settings, const char * key) {
	int i;
	bool key_found;

	if (!key) {
		return NULL;
	}

	key_found = false;
	for (i = 0; i < settings->element_count; i++) {
		if (strcmp(settings->elements[i].key, key) == 0) {
			key_found = true;
			break;
		}
	}
	if (key_found) {
		return settings->elements[i].value;
	}

	return NULL;
}

static int nvram_set_key_value_create_pair_or_delete_key(struct json_nvram_v1_key_value_list * settings, const char * key, const char * value) {
	struct json_nvram_v1_key_value_list * old_settings = malloc(sizeof(struct json_nvram_v1_key_value_list));
	char * json_string_buffer = malloc(NVRAM_JSON_STRING_BUFFER_SIZE);
	int key_index = nvram_get_index_of_key(settings, key);
	int i;
	int ret;
	bool key_found;

	if (!json_string_buffer || !old_settings || !key) {
		free(json_string_buffer);
		free(old_settings);
		return -1;
	}

	// Copy current settings to backup area
	memcpy(old_settings, settings, sizeof(struct json_nvram_v1_key_value_list));

	if (value) {
		if (key_index < 0) {
			if (settings->element_count >= (NVRAM_MAX_KEY_VALUE_PAIRS - 1)) {
				free(json_string_buffer);
				free(old_settings);
				return -ENOSPC;
			}

			key_index = settings->element_count;
			settings->element_count++;
		}

		settings->elements[key_index].key = key;
		settings->elements[key_index].value = value;
	}
	else {
		// A NULL value indicates the key should be deleted
		key_found = false;
		for (i = 0; i < settings->element_count; i++) {
			if (strcmp(settings->elements[i].key, key) == 0) {
				key_found = true;
				break;
			}
		}
		if (key_found) {
			for (i = i; i < settings->element_count - 1; i++) {
				settings->elements[i].key = settings->elements[i+1].key;
				settings->elements[i].value = settings->elements[i+1].value;
			}
			settings->element_count--;
		}
	}

	// Because of the way Zephyr's JSON library handles strings, it is now necessary to regenerate the JSON settings string
	// in RAM, deallocating the old string and all associated data in the process.  This essentially copies the new string
	// data into allocated RAM prior to the new string value going out of scope on function return.

	// Encode data
	ret = json_obj_encode_buf(json_nvram_v1_key_value_list_desc,
				ARRAY_SIZE(json_nvram_v1_key_value_list_desc), settings,
				json_string_buffer,
				NVRAM_JSON_STRING_BUFFER_SIZE - 1);

	if (ret < 0) {
		// Restore the original settings
		memcpy(settings, old_settings, sizeof(struct json_nvram_v1_key_value_list));

		free(json_string_buffer);
		free(old_settings);

		return -EINVAL;
	}

	ret = json_obj_parse(json_string_buffer, strlen(json_string_buffer), json_nvram_v1_key_value_list_desc,
			ARRAY_SIZE(json_nvram_v1_key_value_list_desc), settings);

	if (ret < 0) {
		// Restore the original settings
		memcpy(settings, old_settings, sizeof(struct json_nvram_v1_key_value_list));

		free(json_string_buffer);
		free(old_settings);

		return -EINVAL;
	}

	free(nvram_settings_string);
	nvram_settings_string = json_string_buffer;

	free(old_settings);

	return 0;
}

int nvram_set_key_value_or_create_pair(struct json_nvram_v1_key_value_list * settings, const char * key, const char * value) {
	return nvram_set_key_value_create_pair_or_delete_key(settings, key, value);
}