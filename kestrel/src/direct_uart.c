/*
 * Copyright (c) 2019 Michael Neuling <mikey@neuling.org>
 * Copyright (c) 2019 Anton Blanchard <anton@linux.ibm.com>
 * Copyright (c) 2021 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/*
 * This file contains a very minimal direct-mode driver for the LiteX UART
 * present on Kestrel systems.  It is primarily useful for debugging.
 */

#include <stddef.h>
#include <stdint.h>

/*
* Core UART functions to implement for a port
*/

#define UART_BASE 0xc0002000

#define POTATO_CONSOLE_RXTX             0x00
#define POTATO_CONSOLE_STATUS_TX_FULL   0x04
#define POTATO_CONSOLE_STATUS_RX_EMPTY  0x08
#define POTATO_CONSOLE_EV_PENDING	0x10

#define POTATO_CONSOLE_EV_RX		(1 << 1)

static uint8_t potato_uart_reg_read(int offset)
{
	uint64_t addr;
	uint8_t val;

	addr = UART_BASE + offset;

	val = *(volatile uint8_t *)addr;

	return val;
}

static void potato_uart_reg_write(int offset, uint8_t val)
{
	uint64_t addr;

	addr = UART_BASE + offset;

	*(volatile uint8_t *)addr = val;
}

static int potato_uart_rx_empty(void)
{
	uint8_t val;

	val = potato_uart_reg_read(POTATO_CONSOLE_STATUS_RX_EMPTY);

	if (val)
		return 1;

	return 0;
}

static int potato_uart_tx_full(void)
{
	uint8_t val;

	val = potato_uart_reg_read(POTATO_CONSOLE_STATUS_TX_FULL);

	if (val)
		return 1;

	return 0;
}

static char potato_uart_read(void)
{
	uint8_t val;

	val = potato_uart_reg_read(POTATO_CONSOLE_RXTX);
	potato_uart_reg_write(POTATO_CONSOLE_EV_PENDING, POTATO_CONSOLE_EV_RX);

	return (char)val;
}

static void potato_uart_write(char c)
{
	uint8_t val;

	val = c;

	potato_uart_reg_write(POTATO_CONSOLE_RXTX, val);
}

int direct_serial_getchar(void)
{
	if (potato_uart_rx_empty())
		return -1;

	return potato_uart_read();
}

void direct_serial_putchar(unsigned char c)
{
	while (potato_uart_tx_full())
		/* Do Nothing */;

	potato_uart_write(c);
}