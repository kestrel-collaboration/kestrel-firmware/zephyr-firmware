// © 2021-2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _FAN_TACH_H
#define _FAN_TACH_H

#include <generated/mem.h>

#define FRU_TYPE_FAN_SINGLE_PWM		1
#define FRU_TYPE_FAN_SINGLE_TACH	1

#define MAX_FAN_DEVICES 16

// For now, all SimplePWM tach channels come with an associated PWM channel
// WARNING: This assumption is baked in at various places in the codebase!
// If it changes, corresponding updates will be needed in other files
#define MAX_TACH_DEVICES MAX_FAN_DEVICES

#define MAX_FAN_TACH_DESC_LEN 256

// 2 seconds @ 10Hz polling rate
#define TACH_VALUE_HISTORY_LEN 20

typedef struct kestrel_fan_control_limits {
	double idle_cutoff_pwm;
	double min_normal_pwm;
	double max_normal_pwm;
	double max_emergency_pwm;
} kestrel_fan_control_limits_t;

typedef struct kestrel_fan_alert_thresholds {
	double lower_rpm_threshold_warning;
	double lower_rpm_threshold_critical;
	double lower_rpm_threshold_fatal;
	double upper_rpm_threshold_warning;
	double upper_rpm_threshold_critical;
	double upper_rpm_threshold_fatal;
	double max_design_rpm;
	double min_design_rpm;
} kestrel_fan_alert_thresholds_t;

typedef struct kestrel_fan_data_s {
	uint32_t fan_id;
	uint8_t fru_type;
	char controller_driver_id[64];
	uint32_t channel_id;
	void * driver_data_ptr;
	uint32_t associated_tach_id[MAX_TACH_DEVICES];
	size_t associated_tach_id_count;
	double fan_setting_percent;
	bool fan_set_online;
	double fan_readback_percent;
	char description[MAX_FAN_TACH_DESC_LEN];
	kestrel_fan_control_limits_t fan_limits;
	bool fan_online;
	bool hot_plug_capable;
	bool set_indicator_led_on;
	bool indicator_led_on;
	bool valid;
} kestrel_fan_data_t;

typedef struct kestrel_tach_data_s {
	uint32_t tach_id;
	uint8_t fru_type;
	char controller_driver_id[64];
	uint32_t channel_id;
	void * driver_data_ptr;
	int pulses_per_revolution;
	uint32_t associated_fan_id[MAX_FAN_DEVICES];
	size_t associated_fan_id_count;
	double tach_value_rpm;
	double tach_value_rpm_averaged;
	double tach_value_rpm_history[TACH_VALUE_HISTORY_LEN];
	size_t tach_value_rpm_history_ptr;
	char description[MAX_FAN_TACH_DESC_LEN];
	kestrel_fan_alert_thresholds_t fan_alert_thresholds;
	bool valid;
} kestrel_tach_data_t;

extern kestrel_fan_data_t kestrel_fan_data[];
extern int kestrel_fan_count;
extern kestrel_tach_data_t kestrel_tach_data[];
extern int kestrel_tach_count;
extern struct k_sem fan_tach_data_array_lock;

void kestrel_sort_fan_tach_devices(void);

kestrel_fan_data_t * get_fan_device_by_id(uint32_t device_id);
kestrel_tach_data_t * get_tach_device_by_id(uint32_t device_id);

int read_all_fan_data_from_hw(void);
int read_fan_data_from_hw(kestrel_fan_data_t * fan_data);
int read_fan_data_from_hw_simplepwm(kestrel_fan_data_t * fan_data);

int sync_all_fan_data_to_hw(void);
int sync_fan_data_to_hw(kestrel_fan_data_t * fan_data);
int sync_fan_data_to_hw_simplepwm(kestrel_fan_data_t * fan_data);

int read_all_tach_data_from_hw(void);
int read_tach_data_from_hw(kestrel_tach_data_t * tach_data);
int read_tach_data_from_hw_simplepwm(kestrel_tach_data_t * tach_data);

void configure_fan_devices(void);
void configure_tach_devices(void);

void process_tach_data_update(kestrel_tach_data_t * tach_data);

#endif // _FAN_TACH_H
