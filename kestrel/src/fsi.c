// © 2020 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(kestrel_fsi, LOG_LEVEL_DBG);

#include "fsi.h"

#include <generated/mem.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <zephyr/kernel.h>

// #define DEBUG

#define KESTREL_LOG(...) LOG_INF(__VA_ARGS__)
#define KESTREL_LOG_IMMEDIATE(...) LOG_INF(__VA_ARGS__); flush_zephyr_log_buffer();

// Defined in kestrel.c
void flush_zephyr_log_buffer(void);

static int check_device_id(void)
{
    uint32_t devid_high = read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_DEVICE_ID_HIGH);
    uint32_t devid_low = read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_DEVICE_ID_LOW);

    if ((devid_high == 0x7c525054) && (devid_low == 0x4653494d))
    {
        return 0;
    }

    KESTREL_LOG("Raptor OpenFSI master not found!");
    return -1;
}

int access_fsi_mem(uint8_t slave_id, uint32_t address, fsi_data_length_t data_length, uint8_t write, uint32_t *data)
{
    int ret = 0;
    uint32_t word;
    uint8_t status_code;

    if (!data)
    {
        return -1;
    }

    // Clear any stale operation request flags
    word = read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_CONTROL);
    word &= ~(FSI_MASTER_CTL_CYCLE_START_MASK << FSI_MASTER_CTL_CYCLE_START_SHIFT);
    word |= ((0 & FSI_MASTER_CTL_CYCLE_START_MASK) << FSI_MASTER_CTL_CYCLE_START_SHIFT);
    write_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_CONTROL, word);

    // Wait for any running operation(s) to complete
    while ((read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_STATUS) & (FSI_MASTER_CTL_CYCLE_START_MASK << FSI_MASTER_CTL_CYCLE_START_SHIFT)))
    {
    }

    // Set up request
    word = read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_SID_ADR);
    word &= ~(FSI_MASTER_SID_SLAVE_ID_MASK << FSI_MASTER_SID_SLAVE_ID_SHIFT);
    word |= ((slave_id & FSI_MASTER_SID_SLAVE_ID_MASK) << FSI_MASTER_SID_SLAVE_ID_SHIFT);
    word &= ~(FSI_MASTER_SID_ADDRESS_MASK << FSI_MASTER_SID_ADDRESS_SHIFT);
    word |= ((address & FSI_MASTER_SID_ADDRESS_MASK) << FSI_MASTER_SID_ADDRESS_SHIFT);
    word &= ~(FSI_MASTER_SID_DATA_LENGTH_MASK << FSI_MASTER_SID_DATA_LENGTH_SHIFT);
    word |= ((data_length & FSI_MASTER_SID_DATA_LENGTH_MASK) << FSI_MASTER_SID_DATA_LENGTH_SHIFT);
    write_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_SID_ADR, word);
    if (write)
    {
        write_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_TX_DATA, *data);
    }

    // Set direction and start operation
    word = read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_CONTROL);
    word &= ~(FSI_MASTER_CTL_DATA_DIRECTION_MASK << FSI_MASTER_CTL_DATA_DIRECTION_SHIFT);
    word |= ((write & FSI_MASTER_CTL_DATA_DIRECTION_MASK) << FSI_MASTER_CTL_DATA_DIRECTION_SHIFT);
    word &= ~(FSI_MASTER_CTL_CYCLE_START_MASK << FSI_MASTER_CTL_CYCLE_START_SHIFT);
    word |= ((1 & FSI_MASTER_CTL_CYCLE_START_MASK) << FSI_MASTER_CTL_CYCLE_START_SHIFT);
    write_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_CONTROL, word);

    // Wait for operation to complete
    while (!(read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_STATUS) & (FSI_MASTER_CTL_CYCLE_START_MASK << FSI_MASTER_CTL_CYCLE_START_SHIFT)))
    {
    }

    // Read status register
    word = read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_STATUS);
    status_code = (word >> FSI_MASTER_STAT_CYCLE_ERROR_SHIFT) & FSI_MASTER_STAT_CYCLE_ERROR_MASK;

    // Read data
    if (!write)
    {
        *data = read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_RX_DATA);
    }

#ifdef DEBUG
    KESTREL_LOG("%s(): address 0x%06x, data: 0x%08x sta: 0x%08x", __FUNCTION__, address, *data, word);
#endif

    if (status_code)
    {
#ifdef DEBUG
        KESTREL_LOG("[WARNING] FSI master returned error code %d on access to FSI "
               "address 0x%06x",
               status_code, address);
#endif
        if (!ret)
        {
            ret = -1;
        }
    }

    // Clear the previously set cycle start flag
    word = read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_CONTROL);
    word &= ~(FSI_MASTER_CTL_CYCLE_START_MASK << FSI_MASTER_CTL_CYCLE_START_SHIFT);
    word |= ((0 & FSI_MASTER_CTL_CYCLE_START_MASK) << FSI_MASTER_CTL_CYCLE_START_SHIFT);
    write_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_CONTROL, word);

    return ret;
}

int access_cfam(uint8_t slave_id, uint32_t cfam_address, fsi_data_length_t data_length, uint8_t write, uint32_t *data)
{
    // CFAM to FSI address mangling
    uint32_t fsi_address = (cfam_address & 0xfc00) | ((cfam_address & 0x3ff) << 2);

    return access_fsi_mem(slave_id, fsi_address, data_length, write, data);
}

static int access_scom_fsi_handler(uint8_t slave_id, uint32_t scom_address, uint8_t write, uint64_t *data, uint32_t *scom_status)
{
    uint32_t upload_data;
    uint32_t response_data;
    uint32_t cmd_data = scom_address;
    if (write)
    {
        cmd_data |= IBM_SCOM_FSI_CMD_WRITE;

        // Write SCOM data to buffer
        upload_data = (*data >> 32) & 0xffffffff;
        if (access_fsi_mem(slave_id, IBM_POWER9_SCOM_PIB_BASE + IBM_SCOM_FSI_DATA0_REG, FSI_DATA_LENGTH_WORD, 1, &upload_data))
        {
            goto fail;
        }
        upload_data = (*data >> 0) & 0xffffffff;
        if (access_fsi_mem(slave_id, IBM_POWER9_SCOM_PIB_BASE + IBM_SCOM_FSI_DATA1_REG, FSI_DATA_LENGTH_WORD, 1, &upload_data))
        {
            goto fail;
        }

        // Write SCOM access request
        if (access_fsi_mem(slave_id, IBM_POWER9_SCOM_PIB_BASE + IBM_SCOM_FSI_CMD_REG, FSI_DATA_LENGTH_WORD, 1, &cmd_data))
        {
            goto fail;
        }

        // Read SCOM status register
        response_data = 0;
        if (access_fsi_mem(slave_id, IBM_POWER9_SCOM_PIB_BASE + IBM_SCOM_FSI_STATUS_REG, FSI_DATA_LENGTH_WORD, 0, &response_data))
        {
            goto fail;
        }
        *scom_status = response_data;
    }
    else
    {
        *data = 0;
        cmd_data |= IBM_SCOM_FSI_CMD_READ;

        // Write SCOM access request
        if (access_fsi_mem(slave_id, IBM_POWER9_SCOM_PIB_BASE + IBM_SCOM_FSI_CMD_REG, FSI_DATA_LENGTH_WORD, 1, &cmd_data))
        {
            goto fail;
        }

        // Read SCOM status register
        response_data = 0;
        if (access_fsi_mem(slave_id, IBM_POWER9_SCOM_PIB_BASE + IBM_SCOM_FSI_STATUS_REG, FSI_DATA_LENGTH_WORD, 0, &response_data))
        {
            goto fail;
        }
        *scom_status = response_data;

        // Read SCOM data from buffer
        response_data = 0;
        if (access_fsi_mem(slave_id, IBM_POWER9_SCOM_PIB_BASE + IBM_SCOM_FSI_DATA0_REG, FSI_DATA_LENGTH_WORD, 0, &response_data))
        {
            goto fail;
        }
        *data = ((uint64_t)response_data) << 32;
        response_data = 0;
        if (access_fsi_mem(slave_id, IBM_POWER9_SCOM_PIB_BASE + IBM_SCOM_FSI_DATA1_REG, FSI_DATA_LENGTH_WORD, 0, &response_data))
        {
            goto fail;
        }
        (*data) |= response_data;
    }

    return 0;

fail:
    return -1;
}

int access_scom(uint8_t slave_id, uint32_t scom_address, uint8_t write, uint64_t *data)
{
    uint32_t scom_status;
    uint8_t scom_response_code;
    if (access_scom_fsi_handler(slave_id, scom_address, write, data, &scom_status))
    {
        return -1;
    }
    scom_response_code = (scom_status >> 12) & 0x7;
    if (scom_response_code != IBM_SCOM_PIB_RESULT_SUCCESS)
    {
#ifdef DEBUG
        KESTREL_LOG("%s(): SCOM access returned error code 0x%01x\n", __FUNCTION__, scom_response_code);
#endif
        return -1;
    }

    return 0;
}

static int initialize_fsi_master(void)
{
    uint32_t word;

    if (check_device_id())
    {
        return -1;
    }

    // Configure PHY for 1.5MHz operation at a 50MHz bus clock speed
    // FIXME Need to compute the clock divisor here, and allow passing of a clock speed argument to the initialization routine
    word = read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_PHY_CFG);
    word &= ~(FSI_MASTER_PHY_CFG_CLK_DIV_MASK << FSI_MASTER_PHY_CFG_CLK_DIV_SHIFT);
    word |= ((0x15 & FSI_MASTER_PHY_CFG_CLK_DIV_MASK) << FSI_MASTER_PHY_CFG_CLK_DIV_SHIFT);
    write_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_PHY_CFG, word);

    // Clear any pending operation requests
    word = read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_CONTROL);
    word &= ~(FSI_MASTER_CTL_CYCLE_START_MASK << FSI_MASTER_CTL_CYCLE_START_SHIFT);
    word |= ((0 & FSI_MASTER_CTL_CYCLE_START_MASK) << FSI_MASTER_CTL_CYCLE_START_SHIFT);
    write_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_CONTROL, word);

    // Wait for any running operation(s) to complete
    while (read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_STATUS) & (FSI_MASTER_CTL_CYCLE_START_MASK << FSI_MASTER_CTL_CYCLE_START_SHIFT))
    {
    }

    // Set up ACK to CMD turnaround delay and enable CRC protection / enhanced
    // error recovery
    word = read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_CONTROL);
    word &= ~(FSI_MASTER_CTL_CMD_ISSUE_DELAY_MASK << FSI_MASTER_CTL_CMD_ISSUE_DELAY_SHIFT);
    word |= ((20 & FSI_MASTER_CTL_CMD_ISSUE_DELAY_MASK) << FSI_MASTER_CTL_CMD_ISSUE_DELAY_SHIFT);
    word |= 1 << FSI_MASTER_CTL_ENABLE_CRC_SHIFT;
    word |= 1 << FSI_MASTER_CTL_ENABLE_EER_SHIFT;
    write_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_CONTROL, word);

    // Calculate and dump configured FSI clock speed
    uint8_t fsi_divisor =
        (read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_PHY_CFG) >> FSI_MASTER_PHY_CFG_CLK_DIV_SHIFT) & FSI_MASTER_PHY_CFG_CLK_DIV_MASK;
    if (fsi_divisor > 1)
    {
        fsi_divisor = (fsi_divisor - 1) * 2;
    }
    uint32_t sys_clk_freq = read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_SYS_CLK_FREQ);
    KESTREL_LOG("Raptor OpenFSI master found, configured for operation at %d KHz (bus frequency %d MHz)\n",
            (sys_clk_freq / fsi_divisor) / 1000, sys_clk_freq / 1000000);

#ifdef DEBUG
    KESTREL_LOG("%s(): after setup: ctl 0x%08x, sta 0x%08x", __FUNCTION__, read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_CONTROL),
           read_openfsi_register(OPENFSIMASTER_BASE, FSI_MASTER_REG_STATUS));
#endif

    return 0;
}

int run_pre_ipl_fixups(void)
{
    uint32_t word;
    uint32_t fsi_cfam_error_register;

    // Set up FSI slave...
    // [31]=1:	Warm start done
    // [29]=1:	HW CRC check enabled
    // [26:24]:	FSI slave ID (0)
    // [23:20]:	Echo delay (7)
    // [19:16]:	Send delay (7)
    // [11:8]:	Clock ratio (8)
    word = 0xa0ff0800;
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_CFAM_FSI_SMODE, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    // Ensure asynchronous clock mode is set
    word = 0x0000001;
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_LL_MODE_REG, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    // Configure SBE to pre-IPL state
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_LL_MODE_REG, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    word &= ~(0x1 << 31); // Clear SBE IPL start flag
    word &= ~(0x1 << 29); // Run SCAN0 and CLOCKSTART during IPL
    word &= ~(0x1 << 28); // Clear SBE start prevention flag
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_LL_MODE_REG, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    // Clear SBE status mailbox
    word = 0x00000000;
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_SBE_MSG_REGISTER, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    // Read CFAM error IRQ register
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_CFAM_FSI_SISC, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }
    fsi_cfam_error_register = word;

    // Read CFAM status register
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_CFAM_FSI_SSTAT, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    // Clear CFAM error IRQ register
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_CFAM_FSI_SISC, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &fsi_cfam_error_register))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    return 0;

fail:
    return -1;
}

int start_ipl(int side)
{
    uint32_t word;

    if (initialize_fsi_master())
    {
        KESTREL_LOG("%s():%d FSI master did not initialize", __FUNCTION__, __LINE__);
        goto fail;
    }

    if ((side < 0) || (side > 1))
    {
        KESTREL_LOG("Invalid side %d specified", side);
        return -2;
    }

    KESTREL_LOG("Starting IPL on side %d", side);

    if (run_pre_ipl_fixups())
    {
        goto fail;
    }

    // Ensure asynchronous clock mode is set
    word = 0x0000001;
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_LL_MODE_REG, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    // Clock mux select override
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_ROOT_CTRL8, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }
    word |= 0xc;
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_ROOT_CTRL8, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    // Setup FSI2PIB to report checkstop
    word = 0x20000000;
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_FSI_A_SI1S, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    // Enable XSTOP/ATTN interrupt
    word = 0x60000000;
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_FSI2PIB_TRUE_MASK, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    // Arm XSTOP/ATTN interrupt
    word = 0xffffffff;
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_FSI2PIB_INTERRUPT, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    // SBE boot side configuration
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_SBE_CTRL_STATUS, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    if (side == 0)
    {
        word &= ~(0x1 << 14);
    }
    else
    {
        word |= 0x1 << 14;
    }

    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_SBE_CTRL_STATUS, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    // Ensure edge-triggered SBE start bit is deasserted prior to ignition...
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_CBS_CS, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    word &= ~(0x1 << 31);
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_CBS_CS, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    // ...and light the fuse!
    word |= 0x1 << 31;
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_CBS_CS, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &word))
    {
        KESTREL_LOG("%s():%d CFAM access failed", __FUNCTION__, __LINE__);
        goto fail;
    }

    return 0;

fail:
    KESTREL_LOG("IPL failed!");
    return -1;
}

void sbe_status_code_to_text(char *buffer, uint8_t code)
{
    buffer[0] = 0;
    switch (code)
    {
        case 0x0:
            sprintf(buffer, "SBE_STATE_UNKNOWN");
            break;
        case 0x1:
            sprintf(buffer, "SBE_STATE_IPLING");
            break;
        case 0x2:
            sprintf(buffer, "SBE_STATE_ISTEP");
            break;
        case 0x3:
            sprintf(buffer, "SBE_STATE_MPIPL");
            break;
        case 0x4:
            sprintf(buffer, "SBE_STATE_RUNTIME");
            break;
        case 0x5:
            sprintf(buffer, "SBE_STATE_DMT");
            break;
        case 0x6:
            sprintf(buffer, "SBE_STATE_DUMP");
            break;
        case 0x7:
            sprintf(buffer, "SBE_STATE_FAILURE");
            break;
        case 0x8:
            sprintf(buffer, "SBE_STATE_QUIESCE");
            break;
        case 0x9:
            sprintf(buffer, "SBE_STATE_QUIESCE");
            break;
        default:
            sprintf(buffer, "SBE_INVALID_STATE");
            break;
    }
}

int print_sbe_status(const struct shell *shell)
{
    uint32_t word;
    uint64_t qword;
    char status_text_buffer[128];

    // IBM_POWER9_LL_MODE_REG
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_LL_MODE_REG, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        shell_print(shell, "CFAM 0x%06x: <UNKNOWN>", IBM_POWER9_LL_MODE_REG);
    }
    else
    {
        shell_print(shell, "CFAM 0x%06x: 0x%08x", IBM_POWER9_LL_MODE_REG, word);
    }

    // CFAM 0x1007
    if (access_cfam(IBM_POWER9_SLAVE_ID, 0x1007, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        shell_print(shell, "CFAM 0x%06x: <UNKNOWN>", 0x1007);
    }
    else
    {
        shell_print(shell, "CFAM 0x%06x: 0x%08x", 0x1007, word);
        if (word & (0x1 << 15))
        {
            shell_print(shell, "\tVDN Power:\t\t\tON");
        }
        else
        {
            shell_print(shell, "\tVDN Power:\t\t\tOFF");
        }
        shell_print(shell, "\tOSC Status:\t\t\t0x%01x", (word >> 8) & 0xf);
        shell_print(shell, "\tSpread Spectrum PLL Locked:\t%d", (word >> 7) & 0x1);
        shell_print(shell, "\tCP Filter PLL Locked:\t\t%d", (word >> 6) & 0x1);
        shell_print(shell, "\tI/O Filter PLL Locked:\t\t%d", (word >> 5) & 0x1);
        shell_print(shell, "\tNest PLL Locked:\t\t%d", (word >> 4) & 0x1);
    }

    // CFAM 0x2801
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_CBS_CS, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        shell_print(shell, "CFAM 0x%06x: <UNKNOWN>", IBM_POWER9_CBS_CS);
    }
    else
    {
        shell_print(shell, "CFAM 0x%06x: 0x%08x", IBM_POWER9_CBS_CS, word);
        if (word & (0x1 << 26))
        {
            shell_print(shell, "\tSecure Mode Disable:\t\tON");
        }
        else
        {
            shell_print(shell, "\tSecure Mode Disable:\t\tOFF");
        }
        shell_print(shell, "\tCBS State Delay:\t\t0x%03x", (word >> 16) & 0x3ff);
        shell_print(shell, "\tCBS State Vector:\t\t0x%03x", (word >> 0) & 0xffff);
    }

    // CFAM 0x2802 (CBS trace)
    if (access_cfam(IBM_POWER9_SLAVE_ID, 0x2802, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        shell_print(shell, "CFAM 0x%06x: <UNKNOWN>", 0x2802);
    }
    else
    {
        shell_print(shell, "CFAM 0x%06x: 0x%08x", 0x2802, word);
    }

    // CFAM 0x2803 (CBS event log)
    if (access_cfam(IBM_POWER9_SLAVE_ID, 0x2803, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        shell_print(shell, "CFAM 0x%06x: <UNKNOWN>", 0x2803);
    }
    else
    {
        shell_print(shell, "CFAM 0x%06x: 0x%08x", 0x2803, word);
        if (word & (0x1 << 15))
        {
            shell_print(shell, "\tVDN PGOOD dropped during CBS sequence");
        }
        if (word & (0x1 << 14))
        {
            shell_print(shell, "\tVDN PGOOD dropped after CBS sequence completion");
        }
        if (word & (0x1 << 13))
        {
            shell_print(shell, "\tPervasive register write occurred during CBS execution");
        }
        if (word & (0x1 << 12))
        {
            shell_print(shell, "\tInvalid state reached during CBS execution");
        }
    }

    // CFAM 0x280b (CBS extended status)
    if (access_cfam(IBM_POWER9_SLAVE_ID, 0x280b, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        shell_print(shell, "CFAM 0x%06x: <UNKNOWN>", 0x280b);
    }
    else
    {
        shell_print(shell, "CFAM 0x%06x: 0x%08x", 0x280b, word);
        shell_print(shell, "\tVital HLD Stopped:\t\t%d", (word >> 29) & 0x1);
        shell_print(shell, "\tSecurity Mode Bit:\t\t%d", (word >> 18) & 0x1);
        shell_print(shell, "\tCBS Protocol Error:\t\t%d", (word >> 17) & 0x1);
        shell_print(shell, "\tCurrent OPCG Mode:\t\t0x%01x", (word >> 12) & 0xf);
        shell_print(shell, "\tPrevious OPCG Mode:\t\t0x%01x", (word >> 8) & 0xf);
        shell_print(shell, "\tPCB Interface Error:\t\t%d", (word >> 7) & 0x1);
        shell_print(shell, "\tPCB Parity Error:\t\t%d", (word >> 6) & 0x1);
        shell_print(shell, "\tOther Clock Control Error:\t%d", (word >> 5) & 0x1);
        shell_print(shell, "\tChiplet Aligned:\t\t%d", (word >> 4) & 0x1);
        shell_print(shell, "\tPCB Request RXed After Reset:\t%d", (word >> 3) & 0x1);
    }

    // Root Control 0
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_ROOT_CTRL0, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        shell_print(shell, "CFAM 0x%06x: <UNKNOWN>", IBM_POWER9_ROOT_CTRL0);
    }
    else
    {
        shell_print(shell, "CFAM 0x%06x: 0x%08x", IBM_POWER9_ROOT_CTRL0, word);
        shell_print(shell, "\tPCB Reset:\t\t\t%d", (word >> 1) & 0x1);
        shell_print(shell, "\tGlobal Endpoint Reset:\t\t%d", (word >> 0) & 0x1);
    }

    // Pervasive Control 0
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_PERV_CTRL0, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        shell_print(shell, "CFAM 0x%06x: <UNKNOWN>", IBM_POWER9_PERV_CTRL0);
    }
    else
    {
        shell_print(shell, "CFAM 0x%06x: 0x%08x", IBM_POWER9_PERV_CTRL0, word);
        shell_print(shell, "\tChiplet Enable:\t\t\t%d", (word >> 31) & 0x1);
        shell_print(shell, "\tPCB Reset:\t\t\t%d", (word >> 30) & 0x1);
    }

    // SBE Control / Status Register
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_SBE_CTRL_STATUS, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        shell_print(shell, "CFAM 0x%06x: <UNKNOWN>", IBM_POWER9_SBE_CTRL_STATUS);
    }
    else
    {
        shell_print(shell, "CFAM 0x%06x: 0x%08x", IBM_POWER9_SBE_CTRL_STATUS, word);
        if (word & (0x1 << 31))
        {
            shell_print(shell, "\tSecure Debug Mode:\t\tON");
        }
        else
        {
            shell_print(shell, "\tSecure Debug Mode:\t\tOFF");
        }
    }

    // SBE Message Register
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_SBE_MSG_REGISTER, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        shell_print(shell, "CFAM 0x%06x: <UNKNOWN>", IBM_POWER9_SBE_MSG_REGISTER);
    }
    else
    {
        shell_print(shell, "CFAM 0x%06x: 0x%08x", IBM_POWER9_SBE_MSG_REGISTER, word);
        shell_print(shell, "\tAsync FFDC:\t\t\t%d", (word >> 30) & 0x1);
        shell_print(shell, "\tSBE Booted:\t\t\t%d", (word >> 31) & 0x1);
        sbe_status_code_to_text(status_text_buffer, (word >> 24) & 0xf);
        shell_print(shell, "\tSBE Previous State:\t\t0x%01x [%s]", (word >> 24) & 0xf, status_text_buffer);
        sbe_status_code_to_text(status_text_buffer, (word >> 20) & 0xf);
        shell_print(shell, "\tSBE Current State:\t\t0x%01x [%s]", (word >> 20) & 0xf, status_text_buffer);
        shell_print(shell, "\tMajor ISTEP:\t\t\t%d", (word >> 12) & 0xff);
        shell_print(shell, "\tMinor ISTEP:\t\t\t%d", (word >> 6) & 0x3f);
    }

    // CFAM 0x283a (SBE scratch register 3)
    if (access_cfam(IBM_POWER9_SLAVE_ID, 0x283a, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
         shell_print(shell, "CFAM 0x%06x: <UNKNOWN>", 0x283a);
    }
    else
    {
        shell_print(shell, "CFAM 0x%06x: 0x%08x", 0x283a, word);
    }

    // Hostboot
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_HB_MBX5_REG, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        shell_print(shell, "CFAM 0x%06x: <UNKNOWN>", IBM_POWER9_HB_MBX5_REG);
    }
    else
    {
        shell_print(shell, "CFAM 0x%06x: 0x%08x", IBM_POWER9_HB_MBX5_REG, word);
    }

    // IBM_POWER9_ROOT_CTRL8
    if (access_cfam(IBM_POWER9_SLAVE_ID, IBM_POWER9_ROOT_CTRL8, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &word))
    {
        shell_print(shell, "CFAM 0x%06x: <UNKNOWN>", IBM_POWER9_ROOT_CTRL8);
    }
    else
    {
        shell_print(shell, "CFAM 0x%06x: 0x%08x", IBM_POWER9_ROOT_CTRL8, word);
    }

    // SCOM 0x50001 (IBM_POWER9_CBS_CS)
    if (access_scom(IBM_POWER9_SLAVE_ID, 0x50001, FSI_DIRECTION_READ, &qword))
    {
        shell_print(shell, "SCOM 0x%016x: <UNKNOWN>", 0x50001);
    }
    else
    {
        shell_print(shell, "SCOM 0x%016x: 0x%016llx", 0x50001, qword);
    }

    return 0;
}
