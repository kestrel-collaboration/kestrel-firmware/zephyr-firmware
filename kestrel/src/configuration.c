/*
 * Copyright (c) 2021-2024 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: GPL-3.0
 */

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(configuration, LOG_LEVEL_INF);

#include <stdio.h>
#include <stdlib.h>

#include <zephyr/kernel.h>
#include <zephyr/fs/fs.h>
#include <zephyr/net/ethernet.h>

#include "flash_filesystem.h"

#include "configuration.h"

char * read_vpd_file_contents(const char * vpd_filename)
{
    int ret;
    char vpd_file_path[MAX_PATH_LEN];
    char * vpd_file_contents = NULL;
    snprintf(vpd_file_path, sizeof(vpd_file_path)-1, "%s/vpd/%s", kestrel_config_lfs_fs_mount.mnt_point, vpd_filename);
    vpd_file_path[sizeof(vpd_file_path)-1] = 0x0;
    struct fs_dirent vpd_file_entry;
    struct fs_file_t vpd_file;
    if (fs_stat(vpd_file_path, &vpd_file_entry) == 0) {
        vpd_file_contents = malloc(vpd_file_entry.size + 1);
        if (vpd_file_contents) {
            fs_file_t_init(&vpd_file);
            ret = fs_open(&vpd_file, vpd_file_path, FS_O_READ);
            if (ret >= 0) {
                fs_read(&vpd_file, vpd_file_contents, vpd_file_entry.size);
                *(vpd_file_contents + vpd_file_entry.size) = 0x0;
                fs_close(&vpd_file);
            }
            else {
                *vpd_file_contents = 0x0;
            }
        }
    }

    return vpd_file_contents;
}

// This is required to be statically allocated, as net_if_set_link_addr() will simply redirect the driver's pointer to this location
static uint8_t eth0_mac_address[6] = {0};
void set_ethernet_mac_from_vpd(void)
{
    char mac_string[64];
    char * pos1;
    char * pos2;
    size_t len;
    int i;

    const struct device *const net_dev = DEVICE_DT_GET(DT_NODELABEL(ETH0_NODE));
    struct net_if *iface = net_if_lookup_by_dev(net_dev);
    if (!iface) {
        LOG_WRN("Unable to locate system Ethernet device, using default MAC!");
        return;
    }

    char * ethernet_mac_vpd_file_contents = read_vpd_file_contents("ethernet-mac-addresses");
    if (ethernet_mac_vpd_file_contents) {
        LOG_INF("Found Ethernet MAC configuration file");

        // Find first entry
        pos1 = ethernet_mac_vpd_file_contents;
        pos2 = strstr(ethernet_mac_vpd_file_contents, "\n");
        if (pos2) {
                len = pos2 - pos1;
                if (len > (sizeof(mac_string) - 1)) {
                    len = sizeof(mac_string) - 1;
                }
                memcpy(mac_string, pos1, len);
                mac_string[len] = 0x0;

                if (strlen(mac_string) != 12) {
                    LOG_WRN("Invalid MAC address provided in configuration file, using default!");
                }
                else {
                    uint64_t mac_word;
                    mac_word = strtoull(mac_string, NULL, 16);
                    for (i = 0; i < 6; i++) {
                        eth0_mac_address[5-i] = mac_word & 0xff;
                        mac_word = mac_word >> 8;
                    }

                    LOG_INF("Using MAC address %02x:%02x:%02x:%02x:%02x:%02x for device '%s'",
                        eth0_mac_address[0], eth0_mac_address[1], eth0_mac_address[2],
                        eth0_mac_address[3], eth0_mac_address[4], eth0_mac_address[5],
                        net_dev->name);

                    net_if_down(iface);
                    if (net_if_set_link_addr(iface, eth0_mac_address, sizeof(eth0_mac_address), NET_LINK_ETHERNET)) {
                        LOG_WRN("Unable to set MAC address for device '%s', using default!",  net_dev->name);
                    }
                    net_if_up(iface);
                }
        }
    }
    free(ethernet_mac_vpd_file_contents);
}