// © 2020 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(kestrel_onewire, LOG_LEVEL_DBG);

#include "opencores_1wire.h"

#include "utility.h"

#include <generated/csr.h>
#include <generated/soc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define KESTREL_LOG(...) LOG_INF(__VA_ARGS__)

int initialize_onewire_master(uint8_t *base_address)
{
    KESTREL_LOG("Configuring 1-wire master at address %p...", base_address);

    if ((*((volatile uint32_t *)(base_address + OPENCORES_1WIRE_MASTER_DEVICE_ID_HIGH)) != OPENCORES_1WIRE_DEVICE_ID_HIGH) ||
        (*((volatile uint32_t *)(base_address + OPENCORES_1WIRE_MASTER_DEVICE_ID_LOW)) != OPENCORES_1WIRE_DEVICE_ID_LOW))
    {
        return -1;
    }
    uint32_t opencores_onewire_version = *((volatile uint32_t *)(base_address +  OPENCORES_1WIRE_MASTER_DEVICE_VERSION));
    KESTREL_LOG("OpenCores 1-wire master found, device version %0d.%0d.%d",
           (opencores_onewire_version >>  OPENCORES_1WIRE_VERSION_MAJOR_SHIFT) &  OPENCORES_1WIRE_VERSION_MAJOR_MASK,
           (opencores_onewire_version >>  OPENCORES_1WIRE_VERSION_MINOR_SHIFT) &  OPENCORES_1WIRE_VERSION_MINOR_MASK,
           (opencores_onewire_version >>  OPENCORES_1WIRE_VERSION_PATCH_SHIFT) &  OPENCORES_1WIRE_VERSION_PATCH_MASK);

    // Compute prescale value from system clock and 1-wire bus specifications
    // Overdrive is 1us, normal is 5us...
    uint16_t normal_prescale = ((CONFIG_CLOCK_FREQUENCY * 5) / 1000000) - 1;
    uint16_t overdrive_prescale = ((CONFIG_CLOCK_FREQUENCY * 1) / 1000000) - 1;
    KESTREL_LOG("Desired prescale registers: normal 0x%04x, overdrive 0x%04x (system clock %dMHz)",
           normal_prescale, overdrive_prescale, CONFIG_CLOCK_FREQUENCY / 1000000);
    *((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_NORMAL_LOW)) = normal_prescale & 0xff;
    *((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_NORMAL_HIGH)) = (normal_prescale >> 8) & 0xff;
    *((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_OVERDR_LOW)) = overdrive_prescale & 0xff;
    *((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_OVERDR_HIGH)) = (overdrive_prescale >> 8) & 0xff;
    if ((*((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_NORMAL_LOW)) == (normal_prescale & 0xff)) &&
        (*((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_NORMAL_HIGH)) == ((normal_prescale >> 8) & 0xff)) &&
        (*((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_OVERDR_LOW)) == (overdrive_prescale & 0xff)) &&
        (*((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_OVERDR_HIGH)) == ((overdrive_prescale >> 8) & 0xff)))
    {
        KESTREL_LOG("Enabling 1-wire core");
        return 0;
    }

    return 1;
}

int issue_onewire_reset(uint8_t *base_address,  uint8_t enable_overdrive, uint8_t *presence_detect)
{
    uint32_t onewire_op_timeout_counter;
    uint8_t onewire_op_failed;

    // Send reset
    onewire_op_timeout_counter = 0;
    onewire_op_failed = 0;
    *((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA)) =
        (OPENCORES_1WIRE_MASTER_CTL_CYCLE_START_MASK << OPENCORES_1WIRE_MASTER_CTL_CYCLE_START_SHIFT) |
        (enable_overdrive & (OPENCORES_1WIRE_MASTER_CTL_OVERDR_EN_MASK << OPENCORES_1WIRE_MASTER_CTL_OVERDR_EN_SHIFT)) |
        (OPENCORES_1WIRE_MASTER_CTL_RST_REQ_MASK << OPENCORES_1WIRE_MASTER_CTL_RST_REQ_SHIFT);

    // Wait for cycle completion
    while ((*((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA)) >> OPENCORES_1WIRE_MASTER_CTL_CYCLE_STATUS_SHIFT) &
           OPENCORES_1WIRE_MASTER_CTL_CYCLE_STATUS_MASK)
    {
        if (onewire_op_timeout_counter > ONEWIRE_MASTER_OPERATION_TIMEOUT_VALUE)
        {
            KESTREL_LOG("[WARNING] 1-wire operation timed out in bus reset!");
            onewire_op_failed = 1;
            break;
        }
        usleep(2000);
        onewire_op_timeout_counter++;
    }

    // Return received bit if timeout not hit
    if (!onewire_op_failed)
    {
        if (presence_detect)
        {
            *presence_detect = (*((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA)) >> OPENCORES_1WIRE_MASTER_CTL_DAT_RX_STA_SHIFT) &
                     OPENCORES_1WIRE_MASTER_CTL_DAT_RX_STA_MASK;
        }
    }

    return onewire_op_failed;
}

int issue_onewire_delay(uint8_t *base_address, uint8_t enable_overdrive, uint8_t enable_power)
{
    uint32_t onewire_op_timeout_counter;
    uint8_t onewire_op_failed;

    // Send reset
    onewire_op_timeout_counter = 0;
    onewire_op_failed = 0;
    *((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA)) =
        (OPENCORES_1WIRE_MASTER_CTL_CYCLE_START_MASK << OPENCORES_1WIRE_MASTER_CTL_CYCLE_START_SHIFT) |
        (enable_power & (OPENCORES_1WIRE_MASTER_CTL_POWER_EN_MASK << OPENCORES_1WIRE_MASTER_CTL_POWER_EN_SHIFT)) |
        (enable_overdrive & (OPENCORES_1WIRE_MASTER_CTL_OVERDR_EN_MASK << OPENCORES_1WIRE_MASTER_CTL_OVERDR_EN_SHIFT)) |
        (OPENCORES_1WIRE_MASTER_CTL_RST_REQ_MASK << OPENCORES_1WIRE_MASTER_CTL_RST_REQ_SHIFT) |
        (OPENCORES_1WIRE_MASTER_CTL_DAT_TX_REQ_MASK << OPENCORES_1WIRE_MASTER_CTL_DAT_TX_REQ_SHIFT);

    // Wait for cycle completion
    while ((*((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA)) >> OPENCORES_1WIRE_MASTER_CTL_CYCLE_STATUS_SHIFT) &
           OPENCORES_1WIRE_MASTER_CTL_CYCLE_STATUS_MASK)
    {
        if (onewire_op_timeout_counter > ONEWIRE_MASTER_OPERATION_TIMEOUT_VALUE)
        {
            KESTREL_LOG("[WARNING] 1-wire operation timed out in bus reset!");
            onewire_op_failed = 1;
            break;
        }
        usleep(2000);
        onewire_op_timeout_counter++;
    }

    return onewire_op_failed;
}

int transfer_onewire_bit(uint8_t *base_address, uint8_t enable_overdrive, uint8_t tx_data, uint8_t *rx_data)
{
    uint32_t onewire_op_timeout_counter;
    uint8_t onewire_op_failed;

    // Send and receive a single bit
    onewire_op_timeout_counter = 0;
    onewire_op_failed = 0;
    *((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA)) =
        (OPENCORES_1WIRE_MASTER_CTL_CYCLE_START_MASK << OPENCORES_1WIRE_MASTER_CTL_CYCLE_START_SHIFT) |
        (enable_overdrive & (OPENCORES_1WIRE_MASTER_CTL_OVERDR_EN_MASK << OPENCORES_1WIRE_MASTER_CTL_OVERDR_EN_SHIFT)) |
        ((!!tx_data) & (OPENCORES_1WIRE_MASTER_CTL_DAT_TX_REQ_MASK << OPENCORES_1WIRE_MASTER_CTL_DAT_TX_REQ_SHIFT));

    // Wait for cycle completion
    while ((*((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA)) >> OPENCORES_1WIRE_MASTER_CTL_CYCLE_STATUS_SHIFT) &
           OPENCORES_1WIRE_MASTER_CTL_CYCLE_STATUS_MASK)
    {
        if (onewire_op_timeout_counter > ONEWIRE_MASTER_OPERATION_TIMEOUT_VALUE)
        {
            KESTREL_LOG("[WARNING] 1-wire operation timed out in bus transaction!");
            onewire_op_failed = 1;
            break;
        }
        usleep(2000);
        onewire_op_timeout_counter++;
    }

    // Return received bit if timeout not hit
    if (!onewire_op_failed)
    {
        if (rx_data)
        {
            *rx_data = (*((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA)) >> OPENCORES_1WIRE_MASTER_CTL_DAT_RX_STA_SHIFT) &
                     OPENCORES_1WIRE_MASTER_CTL_DAT_RX_STA_MASK;
        }
    }

    return onewire_op_failed;
}

int transfer_onewire_byte(uint8_t *base_address, uint8_t enable_overdrive, uint8_t tx_data, uint8_t *rx_data)
{
    uint8_t onewire_op_failed;
    uint8_t rx_byte;
    uint8_t byte;
    int i;

    // Send and receive a single byte
    onewire_op_failed = 0;
    rx_byte = 0;
    for (i=0; i<8; i++)
    {
        if (transfer_onewire_bit(base_address, enable_overdrive, (tx_data >> i) & 0x1, &byte))
        {
            onewire_op_failed = 1;
            break;
        }
        rx_byte |= (byte & 0x1) << i;
    }

    // Return received byte if timeout not hit
    if (!onewire_op_failed)
    {
        if (rx_data)
        {
            *rx_data = rx_byte;
        }
    }

    return onewire_op_failed;
}

int transfer_onewire_data_block(uint8_t *base_address, uint8_t enable_overdrive, uint8_t *tx_data, uint8_t *rx_data, int count)
{
    uint8_t onewire_op_failed;
    uint8_t byte;
    int i;

    // Send and receive a block of bytes
    onewire_op_failed = 0;
    for (i=0; i<count; i++)
    {
        if (transfer_onewire_byte(base_address, enable_overdrive, tx_data[i], &byte))
        {
            onewire_op_failed = 1;
            break;
        }
        else
        {
            if (rx_data)
            {
                rx_data[i] = byte;
            }
        }
    }

    return onewire_op_failed;
}
