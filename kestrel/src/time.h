// © 2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _KESTREL_TIME_H
#define _KESTREL_TIME_H

#define SNTP_PORT		123

#define KESTREL_PRIMARY_RTC	DT_NODELABEL(rtc1)

#include <time.h>

extern uint64_t uptime_rtc_offset;
extern uint64_t boot_timestamp_rtc;

int load_current_hw_rtc_time(void);
int get_current_hw_rtc_time(time_t * time);
int sync_time_with_sntp_server(const char* sntp_server);

#endif // _KESTREL_TIME_H