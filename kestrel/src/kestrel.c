// © 2020 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#define WITH_SPI 1

#define ENABLE_ONEWIRE 1

#include "build_config.h"

#if (WITH_ZEPHYR)
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(kestrel_core, LOG_LEVEL_DBG);
#endif

#include "fsi.h"
#include "utility.h"

#if (WITH_ZEPHYR)
#include <zephyr/sys/crc.h>
#endif
#include <generated/csr.h>
#include <generated/mem.h>
#if (WITH_ZEPHYR)
#include <zephyr/irq.h>
#include <zephyr/drivers/gpio.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if (WITH_ZEPHYR)
#include <unistd.h>
#include <sys/types.h>
#endif
#if (WITH_SPI)
#include "micron_n25q_flash.h"
#include "tercel_spi.h"
#endif

#if (WITH_ZEPHYR)
#include "configuration.h"
#include "sensors.h"
#endif

#include "aquila.h"
#include "ipmi_bt.h"
#include "opencores_i2c.h"
#include "opencores_1wire.h"
#include "onewire_functions.h"
#include "fan_tach.h"
#include "simple_pwm.h"
#include "flash_filesystem.h"
#include "thermal_control.h"

#include "kestrel.h"
#include "host_platform_settings.h"

// Performance controls
#define ENABLE_LPC_FW_CYCLE_IRQ_HANDLER  1 // Set to 1 to enable LPC master transfer interrupts to the BMC soft core
#define ENABLE_LPC_IO_CYCLE_IRQ_HANDLER  1 // Set to 1 to enable LPC I/O transfer interrupts to the BMC soft core
#define ENABLE_LPC_TPM_CYCLE_IRQ_HANDLER 1 // Set to 1 to enable LPC TPM transfer interrupts to the BMC soft core
#define ENABLE_LPC_FW_CYCLE_DMA          1 // Set to 1 to allow the LPC master to DMA data to/from the Wishbone bus
#define ALLOW_SPI_QUAD_MODE              1 // Set to 1 to allow quad-mode SPI transfers if the hardware supports them

// Debug knobs
#define DEBUG_HOST_SPI_FLASH_READ           0 // Set to 1 to enable verbose logging of SPI flash read process
#define SPI_FLASH_TRIPLE_READ               0 // Set to 1 to enable triple-read data checks (slow)
#define ENABLE_IPMI_DEBUG                   0 // Set to 1 to enable verbose logging of IPMI requests and responses
#define CLEAR_FLASH_CACHE_BUFFER_ON_STARTUP 0 // Set to 1 to clear the Flash cache buffer on startup

// General RCS platform registers
#define HOST_PLATFORM_FPGA_I2C_REG_STATUS  0x7
#define HOST_PLATFORM_FPGA_I2C_REG_STA_LED 0x10
#define HOST_PLATFORM_FPGA_I2C_REG_MFR_OVR 0x33

// Limits
#define FLASH_WRITE_LOCATION_RETRY_LIMIT 5

// SoC configuration
#define CPU0_I2C_MASTER_BASE_ADDR  I2CMASTER0_BASE
#define CPU1_I2C_MASTER_BASE_ADDR  I2CMASTER1_BASE
#define P9PS_I2C_MASTER_BASE_ADDR  I2CMASTER4_BASE
#define HDMI_I2C_MASTER_BASE_ADDR  I2CMASTER4_BASE

// Peripheral configuration
// On the Arctic Tern carrier, the following switches must be turned ON to enable QSPI mode:
// BMC Flash:  SW9803
// Host Flash: SW9804
#define BMC_SPI_FLASH_QSPI_COMPATIBLE  1
#define HOST_SPI_FLASH_QSPI_COMPATIBLE 1

#if HOST_TYPE_BLACKBIRD
#  ifdef I2CMASTER2_BASE
#    define HHDT_I2C_MASTER_BASE_ADDR  I2CMASTER2_BASE
#  else
#    undef ENABLE_HDMI_TRANSCEIVER
#    define ENABLE_HDMI_TRANSCEIVER 0
#  endif
#endif

// Front panel interface
// FIXME: Convert LEDs to device tree
#define POWER_LED_OUT_GPIO    10

extern uint32_t irq_unhandled_vector;
extern uint32_t irq_unhandled_source;
extern uint8_t irq_unhandled_vector_valid;
extern uint8_t irq_unhandled_source_valid;

#define POST_CODE_INTERRUPT_TRANSIENT_BUFFER_SIZE 32
#define VUART_INTERRUPT_TRANSIENT_BUFFER_SIZE 32
#define VUART_RING_BUFFER_SIZE 512

// Interrupt transient POST code buffer
static uint16_t post_code_incoming_interrupt_transient_buffer[POST_CODE_INTERRUPT_TRANSIENT_BUFFER_SIZE];
static unsigned int post_code_incoming_interrupt_transient_buffer_pos = 0;
static uint8_t post_code_incoming_interrupt_transient_buffer_overflow = 0;

// Interrupt transient VUART1 buffer
static uint8_t vuart1_incoming_interrupt_transient_buffer[VUART_INTERRUPT_TRANSIENT_BUFFER_SIZE];
static unsigned int vuart1_incoming_interrupt_transient_buffer_pos = 0;
static uint8_t vuart1_incoming_interrupt_transient_buffer_overflow = 0;

// BMC to host VUART1 buffer
static uint8_t vuart1_outgoing_buffer[VUART_RING_BUFFER_SIZE];
static unsigned int vuart1_outgoing_buffer_read_pos = 0;
static unsigned int vuart1_outgoing_buffer_write_pos = 0;

// Host to BMC VUART1 buffer
static uint8_t vuart1_incoming_buffer[VUART_RING_BUFFER_SIZE];
static unsigned int vuart1_incoming_buffer_read_pos = 0;
static unsigned int vuart1_incoming_buffer_write_pos = 0;

// Interrupt transient VUART2 buffer
static uint8_t vuart2_incoming_interrupt_transient_buffer[VUART_INTERRUPT_TRANSIENT_BUFFER_SIZE];
static unsigned int vuart2_incoming_interrupt_transient_buffer_pos = 0;
static uint8_t vuart2_incoming_interrupt_transient_buffer_overflow = 0;

// // BMC to host VUART2 buffer
// static uint8_t vuart2_outgoing_buffer[VUART_RING_BUFFER_SIZE];
// static unsigned int vuart2_outgoing_buffer_read_pos = 0;
// static unsigned int vuart2_outgoing_buffer_write_pos = 0;

// Host to BMC VUART2 buffer
static uint8_t vuart2_incoming_buffer[VUART_RING_BUFFER_SIZE];
// static unsigned int vuart2_incoming_buffer_read_pos = 0;
static unsigned int vuart2_incoming_buffer_write_pos = 0;

// IPMI BT buffer
static ipmi_request_message_t ipmi_bt_interrupt_transient_request;
static uint8_t ipmi_bt_interrupt_transient_request_valid = 0;
static ipmi_request_message_t ipmi_bt_current_request;
uint8_t ipmi_bt_transaction_state;

// HIOMAP
static uint8_t *hiomap_write_buffer;
static uint8_t *host_flash_buffer;
static hiomap_configuration_data_t hiomap_config;
uint8_t hiomap_use_direct_access = 0;

// Background service tasks
uint8_t host_background_service_task_active = 0;
uint8_t host_console_service_task_active = 0;
uint8_t host_console_service_task_requested = 0;
uint8_t host_fan_control_service_running = 0;
uint8_t host_power_status = HOST_POWER_STATUS_OFFLINE;
uint16_t host_active_post_code = 0;
static int configured_cpu_count = 1;

// POST codes
uint8_t post_code_high = 0;
uint8_t post_code_low = 0;

// Global configuration
static uint8_t allow_flash_write = 0;
static uint8_t enable_post_code_console_output = 1;

// External service interface
struct firmware_buffer_region main_firmware_buffer;

// PID controllers
struct pid_data_t kestrel_pid_controller_data[MAX_PID_CONTROLLERS];
int kestrel_pid_controller_count = MAX_PID_CONTROLLERS;

// System status
uint8_t kestrel_basic_init_complete = 0;

#if (WITH_ZEPHYR)
// Thread identifiers
k_tid_t kestrel_service_thread_id = NULL;
k_tid_t kestrel_console_thread_id = NULL;
k_tid_t fan_tach_service_thread_id = NULL;
k_tid_t thermal_service_thread_id = NULL;
k_tid_t sensor_service_thread_id = NULL;
k_tid_t time_service_thread_id = NULL;
k_tid_t watchdog_service_thread_id = NULL;
k_tid_t fan_tach_init_thread_id = NULL;
k_tid_t onewire_init_thread_id = NULL;

// Mutexes
struct k_mutex vuart1_access_mutex;
struct k_mutex occ_access_mutex;

// Semaphores
struct k_sem chassis_control_semaphore;

// Console structures
const struct shell *host_console_service_task_shell = NULL;
#endif

#define KESTREL_LOG(...) LOG_INF(__VA_ARGS__)
#define KESTREL_LOG_IMMEDIATE(...) LOG_INF(__VA_ARGS__); flush_zephyr_log_buffer()

#if (WITH_ZEPHYR)
#if (DEBUG_HOST_SPI_FLASH_READ)
static uint32_t crc32(const uint8_t *data, size_t len)
{
    return crc32_ieee(data, len);
}
#endif
#endif

typedef struct
{
    int8_t index;
    uint8_t *i2c_master;
    uint32_t i2c_frequency;
    uint8_t vdd_regulator_addr;
    uint8_t vdd_regulator_page;
    uint8_t vcs_regulator_addr;
    uint8_t vcs_regulator_page;
    uint8_t vdn_regulator_addr;
    uint8_t vdn_regulator_page;
    uint8_t vdd_smbus_addr;
    uint8_t vdn_smbus_addr;
} cpu_info_t;
static const cpu_info_t g_cpu_info[] = {
#ifdef CPU0_I2C_MASTER_BASE_ADDR
    {
        .index = 0,
        .i2c_master = (uint8_t *)CPU0_I2C_MASTER_BASE_ADDR,
        .i2c_frequency = 100000,
        .vdd_regulator_addr = 0x70,
        .vdd_regulator_page = 0x00,
        .vcs_regulator_addr = 0x70,
        .vcs_regulator_page = 0x01,
        .vdn_regulator_addr = 0x73,
        .vdn_regulator_page = 0x00,
        .vdd_smbus_addr = 0x28,
        .vdn_smbus_addr = 0x2b,

    },
#endif
#ifdef CPU1_I2C_MASTER_BASE_ADDR
    {
        .index = 1,
        .i2c_master = (uint8_t *)CPU1_I2C_MASTER_BASE_ADDR,
        .i2c_frequency = 100000,
        .vdd_regulator_addr = 0x70,
        .vdd_regulator_page = 0x00,
        .vcs_regulator_addr = 0x70,
        .vcs_regulator_page = 0x01,
        .vdn_regulator_addr = 0x73,
        .vdn_regulator_page = 0x00,
        .vdd_smbus_addr = 0x28,
        .vdn_smbus_addr = 0x2b,
    },
#endif
};
#define MAX_CPUS_SUPPORTED (sizeof(g_cpu_info) / sizeof(g_cpu_info[0]))

static const struct power_limit_data_desc board_power_limits[] = {
    [PowerLimitDataGeneric] =
    {
        .packet =
        {
            .fail_response = POWERLIMIT_EXECTPION_ACT_HARD_SHUTDOWN,
            .max_watts = 0,
        },
        .completion_code = DCMI_CC_NO_POWER_LIMIT,
    },
};

static void memcpy32(uint32_t *destination, uint32_t *source, int words)
{
    int word;
    for (word = 0; word < words; word++)
    {
        *destination = *source;
        destination++;
        source++;
    }
}

static void memset32(uint32_t *destination, uint32_t value, int words)
{
    int word;
    for (word = 0; word < words; word++)
    {
        *destination = value;
        destination++;
    }
}

#if (!(WITH_ZEPHYR))
static char *readstr(void)
{
    char c[2];
    static char s[64];
    static int ptr = 0;

    if (readchar_nonblock())
    {
        c[0] = readchar();
        c[1] = 0;
        switch (c[0])
        {
            case 0x7f:
            case 0x08:
                if (ptr > 0)
                {
                    ptr--;
                    putsnonl("\x08 \x08");
                }
                break;
            case 0x07:
                break;
            case '\r':
            case '\n':
                s[ptr] = 0x00;
                putsnonl("\n");
                ptr = 0;
                return s;
            default:
                if (ptr >= (sizeof(s) - 1))
                {
                    break;
                }
                putsnonl(c);
                s[ptr] = c[0];
                ptr++;
                break;
        }
    }

    primary_service_event_loop();

    return NULL;
}

static char *get_token(char **str)
{
    char *c, *d;

    c = (char *)strchr(*str, ' ');
    if (c == NULL)
    {
        d = *str;
        *str = *str + strlen(*str);
        return d;
    }
    *c = 0;
    d = *str;
    *str = c + 1;
    return d;
}

static void prompt(void)
{
    KESTREL_LOG("FSP0>");
}

static void help(void)
{
    puts("Available commands:");
    puts("help                            - this command");
    puts("reboot                          - reboot BMC CPU");
    puts("poweron                         - Turn chassis power on, start IPL, "
         "and attach to host console");
    puts("console                         - Attach to host console");
    puts("status                          - Print system status");
    puts("ipl                             - Start IPL sequence");
    puts("chassison                       - Turn chassis power on and prepare "
         "for IPL");
    puts("chassisoff                      - Turn chassis power off");
    puts("sbe_status                      - Get SBE status register");
    puts("post_codes                      - Enable or disable output of POST "
         "codes on console");
    puts("mr <address> <length>           - Read data from BMC internal address "
         "in 32-bit words");
    puts("mw <address> <length> <data>    - Write data from BMC internal address "
         "in 32-bit words");
}

static void reboot(void)
{
    ctrl_reset_write(1);
}
#endif

static void display_character(char character, int dp)
{
    int i;

    // Switch display into alphanumeric mode
#if (WITH_ZEPHYR)
    struct gpio_dt_spec alphanumeric_display_enable = GPIO_DT_SPEC_GET(DT_ALIAS(alphanumeric_display_enable), gpios);
    if (gpio_is_ready_dt(&alphanumeric_display_enable)) {
        gpio_pin_configure_dt(&alphanumeric_display_enable, GPIO_OUTPUT_ACTIVE);
    }
#else
    gpio1_out_write(gpio1_out_read() | (0x1 << 18));
#endif

    if (host_power_status != HOST_POWER_STATUS_IPLING)
    {
        // Write character
        gpio2_out_write((gpio2_out_read() & ~0xff) | (character & 0xff));
    }

#if (WITH_ZEPHYR)
    if (host_power_status == HOST_POWER_STATUS_OFFLINE) {
        if (host_fan_control_service_running) {
            LOG_INF("Stopping host fan control service");
            k_timer_stop(&fan_control_timer);
            host_fan_control_service_running = 0;
        }

        // Reset fans to full speed as a safe fallback
        for (i = 0; i < kestrel_fan_count; i++) {
            kestrel_fan_data[i].fan_setting_percent = 100.0;
            kestrel_fan_data[i].fan_set_online = true;
            sync_fan_data_to_hw(&kestrel_fan_data[i]);
        }
    }
    else {
        if (!host_fan_control_service_running) {
            LOG_INF("Starting host fan control service");
            for (i = 0; i < kestrel_pid_controller_count; i++) {
                intialize_pid_structure(&kestrel_pid_controller_data[i]);

                // Configure PID with sane defaults
                if ((i == CPU_FAN_0_CHANNEL) || (i == CPU_FAN_1_CHANNEL)) {
                    // CPU fans
                    kestrel_pid_controller_data[i].setpoint = 63.0;
                    kestrel_pid_controller_data[i].upper_drive_limit = 1.0;
                    kestrel_pid_controller_data[i].lower_drive_limit = 0.0;
                    kestrel_pid_controller_data[i].upper_integrator_clamp = 1.0;
                    kestrel_pid_controller_data[i].lower_integrator_clamp = -0.25;
                    kestrel_pid_controller_data[i].p = 3.50;
                    kestrel_pid_controller_data[i].i = 0.10;
                    kestrel_pid_controller_data[i].d = 0.00;
                    kestrel_pid_controller_data[i].enabled = 1;
                }
                else {
                    // Chassis fans
                    kestrel_pid_controller_data[i].setpoint = 42.0;
                    kestrel_pid_controller_data[i].upper_drive_limit = 1.0;
                    kestrel_pid_controller_data[i].lower_drive_limit = 0.0;
                    kestrel_pid_controller_data[i].upper_integrator_clamp = 1.0;
                    kestrel_pid_controller_data[i].lower_integrator_clamp = -0.25;
                    kestrel_pid_controller_data[i].p = 2.50;
                    kestrel_pid_controller_data[i].i = 0.05;
                    kestrel_pid_controller_data[i].d = 0.00;
                    kestrel_pid_controller_data[i].enabled = 1;
                }
            }
            k_timer_start(&fan_control_timer, K_MSEC(KESTREL_FAN_CONTROL_TIMER_STEP_MS), K_MSEC(KESTREL_FAN_CONTROL_TIMER_STEP_MS));
            host_fan_control_service_running = 1;
        }
    }
#endif
}

static void host_power_status_changed(void)
{
    int power_led_on = 0;

    // Set power LED based on host status
    if (host_power_status == HOST_POWER_STATUS_OFFLINE)
    {
        power_led_on = 0;
    }
    else {
        power_led_on = 1;
    }

    // Write power LED status to GPIO register
    if (power_led_on) {
        gpio1_out_write(gpio1_out_read() | (0x01 << POWER_LED_OUT_GPIO));
        gpio2_out_write((gpio2_out_read() & ~(0xff << 24)) | ('I' << 24));
    }
    else {
        gpio2_out_write((gpio2_out_read() & ~(0xff << 24)) | ('O' << 24));
        gpio1_out_write(gpio1_out_read() & ~(0x01 << POWER_LED_OUT_GPIO));
    }

#if (WITH_ZEPHYR)
    int i;

    if (host_power_status == HOST_POWER_STATUS_OFFLINE) {

        LOG_INF("Stopping host fan control service");
        k_timer_stop(&fan_control_timer);

        // Reset fans to full speed as a safe fallback
        for (i = 0; i < kestrel_fan_count; i++) {
            kestrel_fan_data[i].fan_setting_percent = 100.0;
            kestrel_fan_data[i].fan_set_online = true;
            sync_fan_data_to_hw(&kestrel_fan_data[i]);
        }
    }
    else {
        LOG_INF("Starting host fan control service");
        k_timer_start(&fan_control_timer, K_MSEC(KESTREL_FAN_CONTROL_TIMER_STEP_MS), K_MSEC(KESTREL_FAN_CONTROL_TIMER_STEP_MS));
    }
#endif
}

static void set_led_bank_display(uint8_t bitfield)
{
    gpio1_out_write((gpio1_out_read() & ~0xf) | (bitfield & 0xf));
}

static void set_led_post_code_alphanumeric_display(uint16_t value)
{
    uint32_t dword;

    // Convert POST code to string
    unsigned char post_code_string[5];
    snprintf(post_code_string, 5, "%02x%02x", (value >> 8) & 0xff, value & 0xff);

    if ((host_power_status == HOST_POWER_STATUS_IPLING) && (value != 0x0))
    {
        // Display value on the alphanumeric LED display
        dword = gpio2_out_read();
        dword &= ~0xffffffff;
        dword |= post_code_string[0] << 24;
        dword |= post_code_string[1] << 16;
        dword |= post_code_string[2] << 8;
        dword |= post_code_string[3];
        gpio2_out_write(dword);

        // Set decimal point
        gpio1_out_write((gpio1_out_read() & ~0xf) | 0x2);
    }
    else
    {
        // Clear middle two segments when IPL is not in progress
        dword = gpio2_out_read();
        dword &= ~0xffffffff;
        dword |= (' ' << 24) | (' ' << 16) | (' ' << 8) | ' ';
        gpio2_out_write(dword);

        // Clear decimal point
       gpio1_out_write(gpio1_out_read() & ~0xf);

        // Ensure upper power status character is rewritten
        host_power_status_changed();

        // Ensure lower host status character is rewritten
        display_character('0' + 2 + host_power_status, 0); // STATUS CODE
    }
}

static void display_post_code(uint16_t post_code)
{
    uint8_t host_status_led_post_code;
    uint8_t major_istep = ((post_code >> 8) & 0xff);
    uint8_t led_post_code = (((post_code >> 8) & 0xf) << 4) | (post_code & 0xf);

    if (post_code == 0xfefe)
    {
        // IPL complete!
        set_led_bank_display(0);
        set_led_post_code_alphanumeric_display(0);
        i2c_write_register_byte((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, HOST_PLATFORM_FPGA_I2C_ADDRESS, HOST_PLATFORM_FPGA_I2C_REG_STA_LED, 0x00);
    }
    else if (post_code == 0x0)
    {
        // System offline
        set_led_bank_display(0);
        set_led_post_code_alphanumeric_display(0);
        if (HOST_TYPE_BLACKBIRD)
        {
            // Blackbird has some planar / chassis interaction quirks that are patched out via BMC
            // Technically, the issue is more of a Supermicro front panel design flaw than a Blackbird issue,
            // but it needs to be worked around regardless.

            // Force all front panel lights -- including the UID/ALERT light, off
            i2c_write_register_byte((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, HOST_PLATFORM_FPGA_I2C_ADDRESS, HOST_PLATFORM_FPGA_I2C_REG_STA_LED, 0x80 | 0x00);
        }
        else
        {
            i2c_write_register_byte((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, HOST_PLATFORM_FPGA_I2C_ADDRESS, HOST_PLATFORM_FPGA_I2C_REG_STA_LED, 0x00);
        }
    }
    else {
        // Show major ISTEP on LED bank
        // On Talos we only have three LEDs plus a fourth indicator modification bit, but the major ISTEPs range from 2 to 21
        // Try to condense that down to something more readily displayable
        switch (major_istep)
        {
            case 2: host_status_led_post_code = 1; break;
            case 3: host_status_led_post_code = 1; break;
            case 4: host_status_led_post_code = 2; break;
            case 5: host_status_led_post_code = 2; break;
            case 6: host_status_led_post_code = 3; break;
            case 7: host_status_led_post_code = 3; break;
            case 8: host_status_led_post_code = 4; break;
            case 9: host_status_led_post_code = 4; break;
            case 10: host_status_led_post_code = 5; break;
            case 11: host_status_led_post_code = 5; break;
            case 12: host_status_led_post_code = 6; break;
            case 13: host_status_led_post_code = 6; break;
            case 14: host_status_led_post_code = 7; break;
            case 15: host_status_led_post_code = 7; break;
            case 16: host_status_led_post_code = 9; break;
            case 17: host_status_led_post_code = 9; break;
            case 18: host_status_led_post_code = 10; break;
            case 19: host_status_led_post_code = 10; break;
            case 20: host_status_led_post_code = 11; break;
            case 21: host_status_led_post_code = 11; break;
            case 22: host_status_led_post_code = 12; break;
            case 23: host_status_led_post_code = 12; break;
            default: host_status_led_post_code = 0;
        }

        // Only one way to display the POST codes is strictly necessary...
        if (0)
        {
            set_led_bank_display(led_post_code);
        }
        else
        {
            set_led_post_code_alphanumeric_display(post_code);
        }
        i2c_write_register_byte((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, HOST_PLATFORM_FPGA_I2C_ADDRESS, HOST_PLATFORM_FPGA_I2C_REG_STA_LED, 0x80 | host_status_led_post_code);
    }
}

static void gpio_init(void)
{
    // Disable LPC clock buffer (GPIO 107)
    gpio1_out_write(gpio1_out_read() & ~(0x01 << 12));
    gpio1_oe_write(gpio1_oe_read() | (0x01 << 12));

    // Deassert fan speed override (GPIO 106)
    gpio1_out_write(gpio1_out_read() | 0x01 << 11);
    gpio1_oe_write(gpio1_oe_read() | (0x01 << 11));

    // Set up discrete LED bank
    set_led_bank_display(0x00);
    gpio1_oe_write(gpio1_oe_read() | 0xf);

    // Set up alphanumeric display / power LED
    gpio2_out_write((' ' << 24) | (' ' << 16) | (' ' << 8) | ' ');

    // Set up and turn off power LED
    gpio1_out_write(gpio1_out_read() & ~(0x01 << POWER_LED_OUT_GPIO));
    gpio1_oe_write(gpio1_oe_read() | (0x01 << POWER_LED_OUT_GPIO));
}

static int configure_hdmi_transceiver(uint8_t *i2c_master_base_addr)
{
#if ENABLE_HDMI_TRANSCEIVER
    // ***** Set up the ITE HDMI transceiver *****

    KESTREL_LOG("Configuring HDMI transceiver");
    // ***** GENERAL *****
    // Reset device
    if (i2c_write_register_byte(i2c_master_base_addr, HDMI_TRANSCEIVER_I2C_ADDRESS, 0x4, 0x1c))
    {
        return -1;
    }

    // ***** VIDEO *****
    // Set bank 0
    if (i2c_write_register_byte(i2c_master_base_addr, HDMI_TRANSCEIVER_I2C_ADDRESS, 0x0f, 0x08)) // 0x0f[1:0] = 0
    {
        return -1;
    }

    // AVMute output
    if (i2c_write_register_byte(i2c_master_base_addr, HDMI_TRANSCEIVER_I2C_ADDRESS, 0xc1, 0x01)) // 0xc1[0] = 1
    {
        return -1;
    }

    // Take device out of reset
    if (i2c_write_register_byte(i2c_master_base_addr, HDMI_TRANSCEIVER_I2C_ADDRESS, 0x04, 0x00))
    {
        return -1;
    }

    // Enable HDMI transmitter reset
    if (i2c_write_register_byte(i2c_master_base_addr, HDMI_TRANSCEIVER_I2C_ADDRESS, 0x61, 0x10))
    {
        return -1;
    }

    // Configure input signal
    if (i2c_write_register_byte(i2c_master_base_addr, HDMI_TRANSCEIVER_I2C_ADDRESS, 0x70,
                            0x00)) // RGB mode, I/O latch at TxClk, non-CCIR656, non-embedded sync, single edge, no PCLK delay
    {
        return -1;
    }
    if (i2c_write_register_byte(
        i2c_master_base_addr, HDMI_TRANSCEIVER_I2C_ADDRESS, 0x90,
        0x00)) // PG horizontal total = 0, H/V sync provided by external driver, active low VSYNC, active low HSYNC, Data Enable provided by external driver
    {
        return -1;
    }

    // Enable DVI mode (works for HDMI as well, host should configure HDMI later in the boot process)
    if (i2c_write_register_byte(i2c_master_base_addr, HDMI_TRANSCEIVER_I2C_ADDRESS, 0xc0, 0x00)) // 0xc0[0] = 0
    {
        return -1;
    }

    // Release HDMI transmitter reset
    if (i2c_write_register_byte(i2c_master_base_addr, HDMI_TRANSCEIVER_I2C_ADDRESS, 0x61, 0x00)) // 0x61[4] = 0
    {
        return -1;
    }

    // Un-AVMute output
    if (i2c_write_register_byte(i2c_master_base_addr, HDMI_TRANSCEIVER_I2C_ADDRESS, 0xc1, 0x00)) // 0xc1[0] = 0
    {
        return -1;
    }

    // ***** AUDIO *****
    // Disable audio channel
    if (i2c_write_register_byte(i2c_master_base_addr, HDMI_TRANSCEIVER_I2C_ADDRESS, 0xe0, 0x08)) // 0xe0[3:0] = 0
    {
        return -1;
    }

    return 0;
#else
    return 0;
#endif
}

static void set_lpc_slave_irq_enable(uint8_t enabled)
{
    if (!enabled)
    {
        hostlpcslave_ev_enable_write(0);
        irq_disable(HOSTLPCSLAVE_INTERRUPT);
    }

    // Clear pending interrupts
    hostlpcslave_ev_pending_write(hostlpcslave_ev_pending_read());

    if (enabled)
    {
        hostlpcslave_ev_enable_write(AQUILA_EV_MASTER_IRQ);
        irq_enable(HOSTLPCSLAVE_INTERRUPT);
    }
}

static int process_host_to_bmc_ipmi_bt_transactions(void);

void lpc_slave_isr(void)
{
#if ((ENABLE_LPC_FW_CYCLE_IRQ_HANDLER) || (ENABLE_LPC_IO_CYCLE_IRQ_HANDLER) || (ENABLE_LPC_TPM_CYCLE_IRQ_HANDLER))
    int byte;
    int word;
#endif
    uint32_t dword;
    uint32_t ev_status;
    uint32_t address;
    uint32_t physical_flash_address;
    uint8_t write_not_read;
    uint32_t status1_reg;
    uint32_t status2_reg;
    uint32_t status4_reg;
    uint32_t vuart_status;
    volatile ipmi_request_message_t *ipmi_bt_request_ptr;

    ev_status = hostlpcslave_ev_pending_read();
    if (ev_status & AQUILA_EV_MASTER_IRQ)
    {
        // Master IRQ asserted
        // Determine source within the LPC slave core
        status4_reg = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_STATUS4);
#if (ENABLE_LPC_FW_CYCLE_IRQ_HANDLER)
        if (status4_reg & AQUILA_LPC_FW_CYCLE_IRQ_ASSERTED)
        {
            // Firmware cycle request has caused IRQ assert
            // This should remain at the beginning of the ISR for maximum transfer
            // performance
            status1_reg = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_STATUS1);
            status2_reg = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_STATUS2);
            address = (status2_reg >> AQUILA_LPC_STATUS_ACT_ADDR_SHIFT) & AQUILA_LPC_STATUS_ACT_ADDR_MASK;
            write_not_read = (status1_reg >> AQUILA_LPC_STATUS_CYC_WNR_SHIFT) & AQUILA_LPC_STATUS_CYC_WNR_MASK;

            if (((status1_reg >> AQUILA_LPC_STATUS_CYCLE_TYPE_SHIFT) & AQUILA_LPC_STATUS_CYCLE_TYPE_MASK) == AQUILA_LPC_STATUS_CYCLE_TYPE_FW)
            {
                uint8_t fw_cycle_idsel = (status1_reg >> AQUILA_LPC_STATUS_FW_CYCLE_IDSEL_SHIFT) & AQUILA_LPC_STATUS_FW_CYCLE_IDSEL_MASK;
                uint8_t fw_cycle_msize = (status1_reg >> AQUILA_LPC_STATUS_FW_CYCLE_MSIZE_SHIFT) & AQUILA_LPC_STATUS_FW_CYCLE_MSIZE_MASK;

                if (fw_cycle_idsel == 0)
                {
                    // Limit firmware address to Flash device size (wrap around)
                    address &= (FLASH_SIZE_BYTES - 1);

                    // Compute active window address
                    physical_flash_address = address;
                    uint8_t *active_host_flash_buffer;
                    if (hiomap_use_direct_access)
                    {
                        if (hiomap_config.window_type == HIOMAP_WINDOW_TYPE_WRITE)
                        {
                            active_host_flash_buffer = hiomap_write_buffer + (physical_flash_address - hiomap_config.window_start_address);
                        }
                        else
                        {
                            active_host_flash_buffer = (void*)(HOSTSPIFLASH_BASE + physical_flash_address);
                        }
                    }
                    else
                    {
                        active_host_flash_buffer = host_flash_buffer + physical_flash_address;
                    }

                    if ((address >= hiomap_config.window_start_address) && ((address - hiomap_config.window_start_address) < hiomap_config.window_length_bytes))
                    {
                        if (!write_not_read &&
                            ((hiomap_config.window_type == HIOMAP_WINDOW_TYPE_READ) || (hiomap_config.window_type == HIOMAP_WINDOW_TYPE_WRITE)))
                        {
                            if (lpc_fw_msize_to_bytes(fw_cycle_msize) >= 4)
                            {
                                for (word = 0; word < (lpc_fw_msize_to_bytes(fw_cycle_msize) / 4); word++)
                                {
                                    *((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_FW_DATA_BLOCK_OFFSET + (word * 4))) =
                                        *((uint32_t *)(active_host_flash_buffer + (word * 4)));
                                }
                            }
                            else
                            {
                                for (byte = 0; byte < lpc_fw_msize_to_bytes(fw_cycle_msize); byte++)
                                {
                                    *((volatile uint8_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_FW_DATA_BLOCK_OFFSET + byte)) =
                                        *((uint8_t *)(active_host_flash_buffer + byte));
                                }
                            }

                            // Transfer success -- do not send error
                            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                            dword &= ~((AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
                            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
                        }
                        else if (write_not_read && (hiomap_config.window_type == HIOMAP_WINDOW_TYPE_WRITE))
                        {
                            if (lpc_fw_msize_to_bytes(fw_cycle_msize) >= 4)
                            {
                                for (word = 0; word < (lpc_fw_msize_to_bytes(fw_cycle_msize) / 4); word++)
                                {
                                    *((uint32_t *)(active_host_flash_buffer + (word * 4))) =
                                        *((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_FW_DATA_BLOCK_OFFSET + (word * 4)));
                                }
                            }
                            else
                            {
                                for (byte = 0; byte < lpc_fw_msize_to_bytes(fw_cycle_msize); byte++)
                                {
                                    *((uint8_t *)(active_host_flash_buffer + byte)) =
                                        *((volatile uint8_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_FW_DATA_BLOCK_OFFSET + byte));
                                }
                            }

                            // Transfer success -- do not send error
                            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                            dword &= ~((AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
                            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
                        }
                        else
                        {
                            // Invalid access -- send error
                            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                            dword |= ((1 & AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
                            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
                        }
                    }
                    else
                    {
                        // Invalid access -- send error
                        dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                        dword |= ((1 & AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
                        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
                    }
                }
                else
                {
                    // Received firmware cycle request for unknown IDSEL!  Dazed and
                    // confused, but trying to continue... Do not send error
                    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                    dword &= ~((AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
                    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
                }

                // Acknowledge data transfer
                dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                dword |= ((1 & AQUILA_LPC_CTL_XFER_CONT_MASK) << AQUILA_LPC_CTL_XFER_CONT_SHIFT);
                write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
            }
        }
#endif
#if (ENABLE_LPC_IO_CYCLE_IRQ_HANDLER)
        if (status4_reg & AQUILA_LPC_IO_CYCLE_IRQ_ASSERTED)
        {
            status1_reg = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_STATUS1);
            status2_reg = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_STATUS2);
            address = (status2_reg >> AQUILA_LPC_STATUS_ACT_ADDR_SHIFT) & AQUILA_LPC_STATUS_ACT_ADDR_MASK;
            write_not_read = (status1_reg >> AQUILA_LPC_STATUS_CYC_WNR_SHIFT) & AQUILA_LPC_STATUS_CYC_WNR_MASK;

            if (((status1_reg >> AQUILA_LPC_STATUS_CYCLE_TYPE_SHIFT) & AQUILA_LPC_STATUS_CYCLE_TYPE_MASK) == AQUILA_LPC_STATUS_CYCLE_TYPE_IO)
            {
                if ((address >= 0x80) && (address <= 0x82))
                {
                    if (write_not_read)
                    {
                        uint8_t post_code = (read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_STATUS3) >> AQUILA_LPC_STATUS_ACT_WDATA_SHIFT) &
                                            AQUILA_LPC_STATUS_ACT_WDATA_MASK;
                        if (address == 0x81)
                        {
                            post_code_high = post_code;
                        }
                        else if (address == 0x82)
                        {
                            post_code_low = post_code;
                            if (post_code_incoming_interrupt_transient_buffer_pos < POST_CODE_INTERRUPT_TRANSIENT_BUFFER_SIZE)
                            {
                                post_code_incoming_interrupt_transient_buffer[post_code_incoming_interrupt_transient_buffer_pos] = ((post_code_high & 0xff) << 8) | (post_code_low & 0xff);
                                post_code_incoming_interrupt_transient_buffer_pos++;
                            }
                            else
                            {
                                // Transient buffer is full
                                // Discard incoming data and signal overflow
                                post_code_incoming_interrupt_transient_buffer_overflow = 1;
                            }
                        }
                    }

                    // Transfer success -- do not send error
                    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                    dword &= ~((AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
                    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);

                    // Acknowledge data transfer
                    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                    dword |= ((1 & AQUILA_LPC_CTL_XFER_CONT_MASK) << AQUILA_LPC_CTL_XFER_CONT_SHIFT);
                    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
                }
                else
                {
                    // Transfer failed -- send error
                    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                    dword |= ((1 & AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
                    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);

                    // Acknowledge data transfer
                    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                    dword |= ((1 & AQUILA_LPC_CTL_XFER_CONT_MASK) << AQUILA_LPC_CTL_XFER_CONT_SHIFT);
                    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
                }
            }
            else
            {
                // Mask LPC I/O cycle IRQ
                dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
                dword &= ~((1 & AQUILA_LPC_CTL_EN_IO_CYCLE_IRQ_MASK) << AQUILA_LPC_CTL_EN_IO_CYCLE_IRQ_SHIFT);
                write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);
            }
        }
#endif
#if (ENABLE_LPC_TPM_CYCLE_IRQ_HANDLER)
        if (status4_reg & AQUILA_LPC_TPM_CYCLE_IRQ_ASSERTED)
        {
            // Mask LPC TPM cycle IRQ
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
            dword &= ~((1 & AQUILA_LPC_CTL_EN_TPM_CYCLE_IRQ_MASK) << AQUILA_LPC_CTL_EN_TPM_CYCLE_IRQ_SHIFT);
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);
        }
#endif
        if ((status4_reg & AQUILA_LPC_VUART1_IRQ_ASSERTED) || (status4_reg & AQUILA_LPC_VUART2_IRQ_ASSERTED))
        {
            // VUART1 or VUART2 has asserted its IRQ
            // Copy received characters to IRQ receive buffer
            do
            {
                vuart_status = *((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + 0x0));
                if (!(vuart_status & AQUILA_LPC_VUART1_FIFO_EMPTY)) {
                    if (vuart1_incoming_interrupt_transient_buffer_pos < VUART_INTERRUPT_TRANSIENT_BUFFER_SIZE)
                    {
                        vuart1_incoming_interrupt_transient_buffer[vuart1_incoming_interrupt_transient_buffer_pos] =
                            (vuart_status >> AQUILA_LPC_VUART1_FIFO_READ_SHIFT) & AQUILA_LPC_VUART1_FIFO_READ_MASK;
                        vuart1_incoming_interrupt_transient_buffer_pos++;
                    }
                    if (vuart1_incoming_interrupt_transient_buffer_pos >= VUART_INTERRUPT_TRANSIENT_BUFFER_SIZE)
                    {
                        // Transient buffer is full
                        // Disable VUART1 interrupts, since we are no longer able to service
                        // them, then exit the copy routine
                        dword = (*((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + AQUILA_LPC_VUART1_CONTROL_REG)));
                        dword &= ~((1 & AQUILA_LPC_VUART_IRQ_EN_MASK) << AQUILA_LPC_VUART_IRQ_EN_SHIFT);
                        (*((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + AQUILA_LPC_VUART1_CONTROL_REG))) = dword;
                        vuart1_incoming_interrupt_transient_buffer_overflow = 1;
                    }
                }
                if (!(vuart_status & AQUILA_LPC_VUART2_FIFO_EMPTY)) {
                    if (vuart2_incoming_interrupt_transient_buffer_pos < VUART_INTERRUPT_TRANSIENT_BUFFER_SIZE)
                    {
                        vuart2_incoming_interrupt_transient_buffer[vuart2_incoming_interrupt_transient_buffer_pos] =
                            (vuart_status >> AQUILA_LPC_VUART2_FIFO_READ_SHIFT) & AQUILA_LPC_VUART2_FIFO_READ_MASK;
                        vuart2_incoming_interrupt_transient_buffer_pos++;
                    }
                    if (vuart2_incoming_interrupt_transient_buffer_pos >= VUART_INTERRUPT_TRANSIENT_BUFFER_SIZE)
                    {
                        // Transient buffer is full
                        // Disable VUART2 interrupts, since we are no longer able to service
                        // them, then exit the copy routine
                        dword = (*((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + AQUILA_LPC_VUART2_CONTROL_REG)));
                        dword &= ~((1 & AQUILA_LPC_VUART_IRQ_EN_MASK) << AQUILA_LPC_VUART_IRQ_EN_SHIFT);
                        (*((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + AQUILA_LPC_VUART2_CONTROL_REG))) = dword;
                        vuart2_incoming_interrupt_transient_buffer_overflow = 1;
                    }
                }
            } while (((!(vuart_status & AQUILA_LPC_VUART1_FIFO_EMPTY)) && (!vuart1_incoming_interrupt_transient_buffer_overflow))
                        || ((!(vuart_status & AQUILA_LPC_VUART2_FIFO_EMPTY)) && (!vuart2_incoming_interrupt_transient_buffer_overflow)));
        }
        if (status4_reg & AQUILA_LPC_IPMI_BT_IRQ_ASSERTED)
        {
            // The IPMI BT module has asserted its IRQ
            // Copy IPMI BT request to IRQ receive buffer

            // Signal BMC read starting
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_IPMI_BT_STATUS) & (1 << IPMI_BT_CTL_B_BUSY_SHIFT);
            if (!(dword & (1 << IPMI_BT_CTL_B_BUSY_SHIFT)))
            {
                // Set B_BUSY
                dword |= (1 << IPMI_BT_CTL_B_BUSY_SHIFT);
            }
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_IPMI_BT_STATUS, dword);

            // Clear H2B_ATN
            dword = 0;
            dword |= (1 << IPMI_BT_CTL_H2B_ATN_SHIFT);
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_IPMI_BT_STATUS, dword);

            ipmi_bt_request_ptr = (ipmi_request_message_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_IPMI_BT_DATA_BLOCK_OFFSET);
            ipmi_bt_interrupt_transient_request = *ipmi_bt_request_ptr;

            // Signal BMC read complete
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_IPMI_BT_STATUS) & (1 << IPMI_BT_CTL_B_BUSY_SHIFT);
            if (dword & (1 << IPMI_BT_CTL_B_BUSY_SHIFT))
            {
                // Clear B_BUSY
                dword |= (1 << IPMI_BT_CTL_B_BUSY_SHIFT);
            }
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_IPMI_BT_STATUS, dword);


            if (ENABLE_IPMI_DEBUG) {
                // Queue IPMI transaction due to enabled IPMI debug functionality
                // If interrupt-unsafe code is included anywhere in the IPMI handler, the deferred path (slow path) *must* be used
                ipmi_bt_interrupt_transient_request_valid = 1;
            }
            else
            {
                // Call three times to get to the response transmission state in the fast path,
                // i.e. if HOST_BUSY is not set
                process_host_to_bmc_ipmi_bt_transactions();
                process_host_to_bmc_ipmi_bt_transactions();
                process_host_to_bmc_ipmi_bt_transactions();

                // In case this request requires deferered processing...
                ipmi_bt_interrupt_transient_request_valid = 1;
            }
        }
    }

    hostlpcslave_ev_pending_write(AQUILA_EV_MASTER_IRQ);
}

static void configure_flash_write_enable(uintptr_t spi_ctl_cfgaddr, uintptr_t spi_ctl_baseaddr, uint8_t enable_writes)
{
    // Set user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) |
                              (TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    if (enable_writes)
    {
        // Send write enable command
        *((volatile uint8_t *)spi_ctl_baseaddr) = 0x06;
    }
    else
    {
        // Send write disable command
        *((volatile uint8_t *)spi_ctl_baseaddr) = 0x04;
    }

    // Clear user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) &
                              ~(TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));
}

static uint8_t read_flash_flag_status_register(uintptr_t spi_ctl_cfgaddr, uintptr_t spi_ctl_baseaddr)
{
    uint8_t flag_status = 0;

    // Set user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) |
                              (TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    // Send Read Flag Status Register command
    *((volatile uint8_t *)spi_ctl_baseaddr) = 0x70;

    // Read response
    flag_status = *((volatile uint8_t *)spi_ctl_baseaddr);

    // Clear user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) &
                              ~(TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    return flag_status;
}

static void reset_flash_device(uintptr_t spi_ctl_cfgaddr, uintptr_t spi_ctl_baseaddr)
{
    // Set user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) |
                              (TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    // Issue RESET ENABLE command
    *((volatile uint8_t *)spi_ctl_baseaddr) = 0x66;

    // Clear user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) &
                              ~(TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    // Set user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) |
                              (TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    // Issue RESET MEMORY command
    *((volatile uint8_t *)spi_ctl_baseaddr) = 0x99;

    // Clear user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) &
                              ~(TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));
}

static void configure_flash_device(uintptr_t spi_ctl_cfgaddr, uintptr_t spi_ctl_baseaddr)
{
    uint8_t config_byte;

    // Set user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) |
                              (TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    // Enable 4 byte addressing mode
    *((volatile uint8_t *)spi_ctl_baseaddr) = 0xb7;

    // Clear user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) &
                              ~(TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    configure_flash_write_enable(spi_ctl_cfgaddr, spi_ctl_baseaddr, 1);

    // Set user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) |
                              (TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    // Initialize volatile configuration register
    *((volatile uint8_t *)spi_ctl_baseaddr) = 0x81;

    config_byte = 0;
    config_byte |= (MICRON_N25Q_SPI_FAST_READ_DUMMY_CLOCK_CYCLES & 0xf) << 4;
    config_byte |= (1 & 0x1) << 3;
    config_byte |= (0 & 0x1) << 2;
    config_byte |= (3 & 0x3) << 0;

    *((volatile uint8_t *)spi_ctl_baseaddr) = config_byte;

    // Clear user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) &
                              ~(TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    configure_flash_write_enable(spi_ctl_cfgaddr, spi_ctl_baseaddr, 0);
}

static void erase_flash_subsector(uintptr_t spi_ctl_cfgaddr, uintptr_t spi_ctl_baseaddr, uint32_t address)
{
    // Limit Flash address to active memory
    address = address & 0x0fffffff;

    while (!(read_flash_flag_status_register(spi_ctl_cfgaddr, spi_ctl_baseaddr) & 0x80))
    {
        // Wait for pending operation to complete
    }

    // Set user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) |
                              (TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    // Send subsector erase command
    *((volatile uint8_t *)spi_ctl_baseaddr) = 0x21;

    // Send address
    *((volatile uint8_t *)spi_ctl_baseaddr) = (address >> 24) & 0xff;
    *((volatile uint8_t *)spi_ctl_baseaddr) = (address >> 16) & 0xff;
    *((volatile uint8_t *)spi_ctl_baseaddr) = (address >> 8) & 0xff;
    *((volatile uint8_t *)spi_ctl_baseaddr) = address & 0xff;

    // Clear user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) &
                              ~(TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    while (!(read_flash_flag_status_register(spi_ctl_cfgaddr, spi_ctl_baseaddr) & 0x80))
    {
        // Wait for pending operation to complete
    }
}

static void hiomap_remove_write_window(uint32_t start_address)
{
    int i;
    int j;

    // Remove mapping
    for (i = 0; i < hiomap_config.write_window_count; i++)
    {
        if (hiomap_config.write_windows[i].start_address == start_address)
        {
            if (hiomap_config.write_windows[i].active)
            {
                free(hiomap_config.write_windows[i].buffer);
                hiomap_config.write_windows[i].buffer = NULL;
                hiomap_config.write_windows[i].active = 0;
                break;
            }
        }
    }

    // Garbage collect
    for (i = 0; i < hiomap_config.write_window_count; i++)
    {
        if (!hiomap_config.write_windows[i].active)
        {
            for (j = i + 1; j < hiomap_config.write_window_count; j++)
            {
                hiomap_config.write_windows[j-1] = hiomap_config.write_windows[j];
            }
            hiomap_config.write_window_count--;
        }
    }
}

#if (WITH_SPI)
static uint32_t read_host_spi_flash_id(uintptr_t spi_ctl_cfgaddr, uintptr_t spi_ctl_baseaddr)
{
    uint32_t flash_id = 0;

    // Set user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) |
                              (TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    // Send Flash ID command
    *((volatile uint8_t *)spi_ctl_baseaddr) = 0x9e;

    // Read response
    flash_id = (flash_id << 8) | (*((volatile uint8_t *)spi_ctl_baseaddr) & 0xff);
    flash_id = (flash_id << 8) | (*((volatile uint8_t *)spi_ctl_baseaddr) & 0xff);
    flash_id = (flash_id << 8) | (*((volatile uint8_t *)spi_ctl_baseaddr) & 0xff);
    flash_id = (flash_id << 8) | (*((volatile uint8_t *)spi_ctl_baseaddr) & 0xff);

    // Clear user mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) &
                              ~(TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    return flash_id;
}

static int write_data_to_flash(uintptr_t spi_ctl_cfgaddr, uintptr_t spi_ctl_baseaddr, uint8_t *data, uint32_t bytes, uint32_t flash_offset, uint8_t erase_before_write)
{
    uint32_t flash_device_id;
    uint32_t flash_address;
    uint32_t bytes_remaining;
    uint8_t flag_status_register;
    uint8_t retry_operation;
    uint8_t retry_count;
    size_t max_bytes_per_write;
    int micron_n25q_device_found;
    int write_is_32bits;
    int i;
    int retcode;

    // Check alignment
    const uintptr_t alignment_mask = sizeof(mem_word_t) - 1;
    if ((uintptr_t)data & alignment_mask)
    {
        KESTREL_LOG("\n[ERROR] Unaligned data passed to write_data_to_flash()\n");
        return -2;
    }
    write_is_32bits = 1;
    if (bytes & 0x3)
    {
        write_is_32bits = 0;
    }

    // Limit Flash address to active memory region of SPI Flash controller
    flash_offset = flash_offset & 0x0fffffff;

    // Read device ID in case special handling is required
    flash_device_id = read_host_spi_flash_id(spi_ctl_cfgaddr, spi_ctl_baseaddr);
    micron_n25q_device_found = 0;
    for (i = 0; i < (sizeof(micron_n25q_spi_device_ids) / sizeof(micron_n25q_spi_device_ids[0])); i++)
    {
        if (flash_device_id == micron_n25q_spi_device_ids[i])
        {
            micron_n25q_device_found = 1;
        }
    }
    if (micron_n25q_device_found)
    {
        // The N25Q512A and related devices are "special"
        // For some reason, it requires a flag status register read after every PAGE PROGRAM command
        // This means we can only write up to four bytes at a time in 32-bit copy mode
        if (write_is_32bits)
        {
            max_bytes_per_write = 4;
        }
        else
        {
            max_bytes_per_write = 1;
        }
    }
    else
    {
        max_bytes_per_write = FLASH_PAGE_SIZE_BYTES;
    }

    retcode = 0;
    if (allow_flash_write)
    {
        // Flash erase if needed, then write data
        if (erase_before_write)
        {
            for (flash_address = flash_offset; (flash_address - flash_offset) < bytes; flash_address = flash_address + FLASH_ERASE_GRAN_BYTES)
            {
                configure_flash_write_enable(spi_ctl_cfgaddr, spi_ctl_baseaddr, 1);
                erase_flash_subsector(spi_ctl_cfgaddr, spi_ctl_baseaddr, flash_address);
            }

            configure_flash_write_enable(spi_ctl_cfgaddr, spi_ctl_baseaddr, 0);
        }

        retry_operation = 0;
        retry_count = 0;
        for (flash_address = flash_offset; (flash_address - flash_offset) < bytes; flash_address = flash_address + max_bytes_per_write)
        {
            if (retry_operation)
            {
                flash_address = flash_address - max_bytes_per_write;
                retry_operation = 0;
                retry_count++;
            }
            bytes_remaining = bytes - (flash_address - flash_offset);
            configure_flash_write_enable(spi_ctl_cfgaddr, spi_ctl_baseaddr, 1);
            while (!(read_flash_flag_status_register(spi_ctl_cfgaddr, spi_ctl_baseaddr) & 0x80))
            {
                // Wait for pending operation to complete
            }
            if (write_is_32bits)
            {
                memcpy32((uint32_t *)(spi_ctl_baseaddr + flash_address), (uint32_t *)(data + (flash_address - flash_offset)),
                       (bytes_remaining > max_bytes_per_write) ? (max_bytes_per_write / 4) : (bytes_remaining / 4));
            }
            else
            {
                memcpy((uint8_t *)(spi_ctl_baseaddr + flash_address), data + (flash_address - flash_offset),
                       (bytes_remaining > max_bytes_per_write) ? max_bytes_per_write : bytes_remaining);
            }
            while (!((flag_status_register = read_flash_flag_status_register(spi_ctl_cfgaddr, spi_ctl_baseaddr)) & 0x80))
            {
                // Wait for pending operation to complete
            }
            if (flag_status_register & 0x10)
            {
                retry_operation = 1;
            }
            else {
                if (memcmp((uint8_t *)(spi_ctl_baseaddr + flash_address), data + (flash_address - flash_offset),
                   (bytes_remaining > max_bytes_per_write) ? max_bytes_per_write : bytes_remaining) != 0)
                   {
                       retry_operation = 1;
                   }
            }
            if (retry_operation && (retry_count > FLASH_WRITE_LOCATION_RETRY_LIMIT))
            {
                KESTREL_LOG("\n[WARNING] Program failed at address 0x%08x (flag status 0x%02x)!  Retry limit reached, aborting write to location.\n", flash_address, flag_status_register);
                retry_operation = 0;
                retry_count = 0;
                retcode = -1;
            }
#if (WITH_ZEPHYR)
            if (retry_operation)
            {
                // Give the Flash device some time to recover / flush data
                k_usleep(100000);
            }
#endif
        }

        configure_flash_write_enable(spi_ctl_cfgaddr, spi_ctl_baseaddr, 0);
    }

    return retcode;
}

static int write_hiomap_data_to_flash(uintptr_t spi_ctl_cfgaddr, uintptr_t spi_ctl_baseaddr, uint32_t bytes, uint32_t flash_offset, uint8_t erase_before_write)
{
    uint32_t start_address;
    uint8_t *write_buffer;
    int i;
    int retcode;

    // Determine origin buffer address
    start_address = flash_offset;
    if (hiomap_use_direct_access)
    {
        write_buffer = NULL;
        for (i = 0; i < hiomap_config.write_window_count; i++)
        {
            if (hiomap_config.write_windows[i].start_address == start_address)
            {
                if (hiomap_config.write_windows[i].active)
                {
                    write_buffer = hiomap_config.write_windows[i].buffer;
                    break;
                }
            }
        }
        if (!write_buffer)
        {
            KESTREL_LOG("[WARNING] Flash data write called with no active data buffer!\n");
            return -1;
        }
    }
    else
    {
        write_buffer = (uint8_t *)(host_flash_buffer + (start_address & (FLASH_SIZE_BYTES - 1)));
    }

    retcode = write_data_to_flash(spi_ctl_cfgaddr, spi_ctl_baseaddr, write_buffer, bytes, flash_offset, erase_before_write);

    return retcode;
}
#endif

// NOTE
// The POWER9 host uses true multitasking (kernel preemptive), so it is entirely
// possible to receive various LPC commands during processing of others.  As a
// result, we need at least a primitive multitasking system for the BMC. For
// now, use cooperative multitasking in this basic bare metal firmware... All
// functions called from the main TX/RX loop should return within some
// timeframe, e.g. 10ms
static int process_host_to_bmc_ipmi_bt_transactions(void)
{
    uint32_t dword;

    static uint8_t unhandled_ipmi_command;
    volatile ipmi_response_message_t *response_ptr;
    static ipmi_response_message_t response;
    static uint8_t request_netfn;
    static uint8_t request_lun;

    uint32_t offset_bytes = 0;
    uint32_t length_bytes = 0;
    uint8_t flags = 0;

    int work_executed = 0;

    int i;

    switch (ipmi_bt_transaction_state)
    {
        case 0:
            // Idle
            break;
        case 1:
            // Extract NETFN/LUN from request
            request_netfn = ipmi_bt_current_request.netfn_lun >> 2;
            request_lun = ipmi_bt_current_request.netfn_lun & 0x3;

            // Set up basic response parameters
            response.netfn_lun = (((request_netfn + 1) & 0x3f) << 2) | (request_lun & 0x3);
            response.sequence = ipmi_bt_current_request.sequence;
            response.command = ipmi_bt_current_request.command;
            response.length = BASE_IPMI_RESPONSE_LENGTH;
            response.completion_code = IPMI_CC_INVALID_COMMAND;
            memset(response.data, 0, sizeof(response.data));

            if (ENABLE_IPMI_DEBUG) {
                printk("[IPMI] Received request netfn 0x%02x lun 0x%02x cmd 0x%02x\n", request_netfn, request_lun, ipmi_bt_current_request.command);
            }

            unhandled_ipmi_command = 0;
            switch (request_netfn)
            {
                case IPMI_NETFN_SENS_ET_REQ:
                    unhandled_ipmi_command = 1;
                    break;
                case IPMI_NETFN_APP_REQUEST:
                    switch (ipmi_bt_current_request.command)
                    {
                        case IPMI_CMD_GET_DEVICE_ID:
                            response.data[0] = 0x00;
                            response.data[1] = 0x00;
                            response.data[2] = 0x00;
                            response.data[3] = 0x00;
                            response.data[4] = 0x02;
                            response.data[5] = 0x00;
                            response.data[6] = 0x05;
                            response.data[7] = 0xcb;
                            response.data[8] = 0x00;
                            response.data[9] = 0x01;
                            response.data[10] = 0x00;
                            response.data[11] = 0x00;
                            response.data[12] = 0x00;
                            response.data[13] = 0x00;
                            response.data[14] = 0x00;
                            response.length = BASE_IPMI_RESPONSE_LENGTH + 15;
                            response.completion_code = IPMI_CC_NO_ERROR;
                            break;
                        case IPMI_CMD_GET_BT_INT_CAP:
                            response.data[0] = 0x01;
                            response.data[1] = 0x3f;
                            response.data[2] = 0x3f;
                            response.data[3] = 0x01;
                            response.data[4] = 0x01;
                            response.length = BASE_IPMI_RESPONSE_LENGTH + 5;
                            response.completion_code = IPMI_CC_NO_ERROR;
                            break;
                        default:
                            unhandled_ipmi_command = 1;
                            break;
                    }
                    break;
                case IPMI_NETFN_STORAGE_REQ:
                    unhandled_ipmi_command = 1;
                    break;
                case IPMI_NETFN_DCMI_GP_REQ:
                    switch (ipmi_bt_current_request.command)
                    {
                        case DCMI_CMD_GET_POWER_CAP:
                        {
                            /* Only a generic P9 profile with no power
                             * limits is included at the moment.*/
                            uint32_t limit_index = PowerLimitDataGeneric;
                            memcpy(response.data, &board_power_limits[limit_index].packet, sizeof(board_power_limits[0].packet));

                            response.completion_code = board_power_limits[limit_index].completion_code;
                            response.length = BASE_DCMI_RESPONSE_LENGTH + sizeof(board_power_limits[0].packet);
                        }
                        break;
                        default:
                            unhandled_ipmi_command = 1;
                            break;
                    }
                    break;
                case IPMI_NETFN_OEM_IBM_REQ:
                    switch (ipmi_bt_current_request.command)
                    {
                        case IPMI_CMD_IBM_HIOMAP_REQ:
                            switch (ipmi_bt_current_request.data[0])
                            {
                                case HIOMAP_CMD_GET_INFO:
                                    if (ipmi_bt_current_request.data[2] > 3)
                                    {
                                        // We only support up to the HIOMAP v3 protocol
                                        hiomap_config.protocol_version = 3;
                                    }
                                    else
                                    {
                                        hiomap_config.protocol_version = ipmi_bt_current_request.data[2];
                                    }
                                    switch (hiomap_config.protocol_version)
                                    {
                                        case 1:
                                            response.data[2] = hiomap_config.protocol_version;
                                            response.data[3] = FLASH_SIZE_BLOCKS & 0xff;
                                            response.data[4] = (FLASH_SIZE_BLOCKS >> 8) & 0xff;
                                            response.data[5] = FLASH_SIZE_BLOCKS & 0xff;
                                            response.data[6] = (FLASH_SIZE_BLOCKS >> 8) & 0xff;
                                            response.length = BASE_HIOMAP_RESPONSE_LENGTH + 5;
                                            break;
                                        case 2:
                                            response.data[2] = hiomap_config.protocol_version;
                                            response.data[3] = FLASH_BLOCK_SIZE_SHIFT;
                                            response.data[4] = HIOMAP_SUGGESTED_TIMEOUT_S & 0xff;
                                            response.data[5] = (HIOMAP_SUGGESTED_TIMEOUT_S >> 8) & 0xff;
                                            response.length = BASE_HIOMAP_RESPONSE_LENGTH + 4;
                                            break;
                                        case 3:
                                            response.data[2] = hiomap_config.protocol_version;
                                            response.data[3] = FLASH_BLOCK_SIZE_SHIFT;
                                            response.data[4] = HIOMAP_SUGGESTED_TIMEOUT_S & 0xff;
                                            response.data[5] = (HIOMAP_SUGGESTED_TIMEOUT_S >> 8) & 0xff;
                                            response.data[6] = HIOMAP_PNOR_DEVICE_COUNT;
                                            response.length = BASE_HIOMAP_RESPONSE_LENGTH + 5;
                                            break;
                                    }
                                    response.data[0] = ipmi_bt_current_request.data[0];
                                    response.data[1] = ipmi_bt_current_request.data[1];
                                    response.completion_code = IPMI_CC_NO_ERROR;
                                    break;
                                case HIOMAP_CMD_GET_FLASH_INFO:
                                    switch (hiomap_config.protocol_version)
                                    {
                                        case 1:
                                            response.data[2] = FLASH_SIZE_BYTES & 0xff;
                                            response.data[3] = (FLASH_SIZE_BYTES >> 8) & 0xff;
                                            response.data[4] = (FLASH_SIZE_BYTES >> 16) & 0xff;
                                            response.data[5] = (FLASH_SIZE_BYTES >> 24) & 0xff;
                                            response.data[6] = FLASH_ERASE_GRAN_BYTES & 0xff;
                                            response.data[7] = (FLASH_ERASE_GRAN_BYTES >> 8) & 0xff;
                                            response.data[8] = (FLASH_ERASE_GRAN_BYTES >> 16) & 0xff;
                                            response.data[9] = (FLASH_ERASE_GRAN_BYTES >> 24) & 0xff;
                                            response.length = BASE_HIOMAP_RESPONSE_LENGTH + 8;
                                            break;
                                        case 2:
                                            // Fall through, same format as protocol version 3
                                        case 3:
                                            response.data[2] = FLASH_SIZE_BLOCKS & 0xff;
                                            response.data[3] = (FLASH_SIZE_BLOCKS >> 8) & 0xff;
                                            response.data[4] = FLASH_ERASE_GRAN_BLOCKS & 0xff;
                                            response.data[5] = (FLASH_ERASE_GRAN_BLOCKS >> 8) & 0xff;
                                            response.length = BASE_HIOMAP_RESPONSE_LENGTH + 4;
                                            break;
                                    }
                                    response.data[0] = ipmi_bt_current_request.data[0];
                                    response.data[1] = ipmi_bt_current_request.data[1];
                                    response.completion_code = IPMI_CC_NO_ERROR;
                                    break;
                                case HIOMAP_CMD_CLOSE_WINDOW:
                                    // Mark window inactive
                                    hiomap_config.window_type = HIOMAP_WINDOW_INACTIVE;

                                    // Implicitly flush any dirty ranges
                                    for (i = 0; i < hiomap_config.dirty_range_count; i++)
                                    {
                                        write_hiomap_data_to_flash(HOSTSPIFLASHCFG_BASE, HOSTSPIFLASH_BASE,
                                                            hiomap_config.dirty_ranges[i].bytes, hiomap_config.dirty_ranges[i].start_address,
                                                            !hiomap_config.dirty_ranges[i].erased);
                                    }
                                    hiomap_config.dirty_range_count = 0;

                                    // Close active window
                                    hiomap_remove_write_window(hiomap_config.window_start_address);

                                    response.data[0] = ipmi_bt_current_request.data[0];
                                    response.data[1] = ipmi_bt_current_request.data[1];
                                    response.length = BASE_HIOMAP_RESPONSE_LENGTH;
                                    response.completion_code = IPMI_CC_NO_ERROR;
                                    break;
                                case HIOMAP_CMD_CREATE_RD_WIN:
                                case HIOMAP_CMD_CREATE_WR_WIN:
                                    // Implicitly shut down any active window
                                    if (hiomap_config.window_type == HIOMAP_WINDOW_TYPE_WRITE)
                                    {
                                        // Implicitly flush any dirty ranges
                                        for (i = 0; i < hiomap_config.dirty_range_count; i++)
                                        {
                                            write_hiomap_data_to_flash(HOSTSPIFLASHCFG_BASE, HOSTSPIFLASH_BASE,
                                                                hiomap_config.dirty_ranges[i].bytes, hiomap_config.dirty_ranges[i].start_address,
                                                                !hiomap_config.dirty_ranges[i].erased);
                                        }
                                        hiomap_config.dirty_range_count = 0;

                                        // Close active window
                                        hiomap_remove_write_window(hiomap_config.window_start_address);
                                    }
                                    hiomap_config.window_type = HIOMAP_WINDOW_INACTIVE;

                                    // Parse request data
                                    hiomap_config.window_start_address =
                                        (((((uint32_t)ipmi_bt_current_request.data[3]) << 8) | ipmi_bt_current_request.data[2]) << FLASH_BLOCK_SIZE_SHIFT) &
                                        ((1 << LPC_ADDRESS_BITS) - 1);
                                    hiomap_config.window_length_bytes =
                                        (((((uint32_t)ipmi_bt_current_request.data[5]) << 8) | ipmi_bt_current_request.data[4]) << FLASH_BLOCK_SIZE_SHIFT) &
                                        ((1 << LPC_ADDRESS_BITS) - 1);
                                    hiomap_config.active_device_id = ipmi_bt_current_request.data[6];
                                    if (ENABLE_IPMI_DEBUG) {
                                        printk("[HIOM] Received window request, 0x%08x len %d bytes read: %d\n", hiomap_config.window_start_address, hiomap_config.window_length_bytes, (ipmi_bt_current_request.data[0] == HIOMAP_CMD_CREATE_RD_WIN));
                                    }
                                    if (ipmi_bt_current_request.data[0] == HIOMAP_CMD_CREATE_RD_WIN)
                                    {
                                        hiomap_config.window_type = HIOMAP_WINDOW_TYPE_READ;
                                    }
                                    else if (ipmi_bt_current_request.data[0] == HIOMAP_CMD_CREATE_WR_WIN)
                                    {
                                        hiomap_config.window_type = HIOMAP_WINDOW_TYPE_WRITE;
                                    }
                                    else
                                    {
                                        hiomap_config.window_type = HIOMAP_WINDOW_INACTIVE;
                                    }

                                    // Sanitize input
                                    switch (hiomap_config.protocol_version)
                                    {
                                        case 1:
                                            if (ipmi_bt_current_request.data[0] == HIOMAP_CMD_CREATE_RD_WIN)
                                            {
                                                // Size unspecified, use one block as the size
                                                hiomap_config.window_length_bytes = 1 << FLASH_BLOCK_SIZE_SHIFT;
                                            }
                                            if (ipmi_bt_current_request.data[0] == HIOMAP_CMD_CREATE_WR_WIN)
                                            {
                                                // Size unspecified, use one block or the maximum write
                                                // cache size as the returned size, whichever is smaller...
                                                if (FLASH_MAX_WR_WINDOW_BYTES < (1 << FLASH_BLOCK_SIZE_SHIFT))
                                                {
                                                    hiomap_config.window_length_bytes = FLASH_MAX_WR_WINDOW_BYTES;
                                                }
                                                else
                                                {
                                                    hiomap_config.window_length_bytes = 1 << FLASH_BLOCK_SIZE_SHIFT;
                                                }
                                            }
                                            break;
                                        case 2:
                                        case 3:
                                            if (ipmi_bt_current_request.data[0] == HIOMAP_CMD_CREATE_RD_WIN)
                                            {
                                                // Zero sized window indicates undefined size, but must be at
                                                // least one block Just use one block as the size in this corner
                                                // case...
                                                if (hiomap_config.window_length_bytes == 0)
                                                {
                                                    hiomap_config.window_length_bytes = 1 << FLASH_BLOCK_SIZE_SHIFT;
                                                }
                                            }
                                            if (ipmi_bt_current_request.data[0] == HIOMAP_CMD_CREATE_WR_WIN)
                                            {
                                                // Zero sized window indicates undefined size, but must be at
                                                // least one block Just use one block as the size in this corner
                                                // case...
                                                if (hiomap_config.window_length_bytes == 0)
                                                {
                                                    hiomap_config.window_length_bytes = 1 << FLASH_BLOCK_SIZE_SHIFT;
                                                }
                                                else
                                                {
                                                    // The host can only request a window size, not demand one
                                                    // If the request is larger than our write cache size, limit
                                                    // the returned window to the write cache size...
                                                    if (hiomap_config.window_length_bytes > FLASH_MAX_WR_WINDOW_BYTES)
                                                    {
                                                        hiomap_config.window_length_bytes = FLASH_MAX_WR_WINDOW_BYTES;
                                                    }
                                                }
                                            }
                                            break;
                                    }

                                    if (hiomap_use_direct_access)
                                    {
                                        // Locate or allocate Flash sector buffer as required
                                        hiomap_write_buffer = NULL;
                                        for (i = 0; i < hiomap_config.write_window_count; i++)
                                        {
                                            if (hiomap_config.write_windows[i].start_address == hiomap_config.window_start_address)
                                            {
                                                if (hiomap_config.write_windows[i].active)
                                                {
                                                    hiomap_write_buffer = hiomap_config.write_windows[i].buffer;
                                                    break;
                                                }
                                            }
                                        }
                                        if ((!hiomap_write_buffer) && (hiomap_config.window_type == HIOMAP_WINDOW_TYPE_WRITE))
                                        {
                                            hiomap_config.write_windows[hiomap_config.write_window_count].start_address = hiomap_config.window_start_address;
                                            hiomap_config.write_windows[hiomap_config.write_window_count].bytes = hiomap_config.window_length_bytes;
                                            hiomap_config.write_windows[hiomap_config.write_window_count].buffer = malloc(FLASH_MAX_WR_WINDOW_BYTES);
                                            if (hiomap_config.write_windows[hiomap_config.write_window_count].buffer == NULL)
                                            {
                                                KESTREL_LOG("[ERROR] Unable to allocate HIOMAP buffer space!\n");
                                                response.length = BASE_IPMI_RESPONSE_LENGTH;
                                                response.completion_code = IPMI_CC_OUT_OF_SPACE;
                                                break;
                                            }
                                            hiomap_config.write_windows[hiomap_config.write_window_count].active = 1;
                                            hiomap_write_buffer = hiomap_config.write_windows[hiomap_config.write_window_count].buffer;
                                            hiomap_config.write_window_count++;
                                        }
                                    }

#if (ENABLE_LPC_FW_CYCLE_DMA)
                                    // Deactivate interrupts on entering critical section
                                    int key = irq_lock();

                                    // Disable DMA engine
                                    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG1);
                                    dword &= ~((1 & AQUILA_LPC_CTL_EN_FW_DMA_R_MASK) << AQUILA_LPC_CTL_EN_FW_DMA_R_SHIFT);
                                    dword &= ~((1 & AQUILA_LPC_CTL_EN_FW_DMA_W_MASK) << AQUILA_LPC_CTL_EN_FW_DMA_W_SHIFT);
                                    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG1, dword);
#endif

                                    // Set up any required buffers while DMA engine is disabled
                                    if ((hiomap_use_direct_access) && (hiomap_config.window_type == HIOMAP_WINDOW_TYPE_WRITE))
                                    {
                                        // Buffer data in write window from external Flash
                                        if (hiomap_config.window_length_bytes > FLASH_MAX_WR_WINDOW_BYTES)
                                        {
                                            KESTREL_LOG("[WARNING] HIOMAP write window set larger than maximum allowed write window size!  Clamping window to %d bytes\n", FLASH_MAX_WR_WINDOW_BYTES);
                                            hiomap_config.window_length_bytes = FLASH_MAX_WR_WINDOW_BYTES;
                                        }
                                        memcpy32((uint32_t *)hiomap_write_buffer, (uint32_t *)(uintptr_t)(HOSTSPIFLASH_BASE + hiomap_config.window_start_address), (hiomap_config.window_length_bytes / 4));
                                    }

#if (ENABLE_LPC_FW_CYCLE_DMA)
                                    // Reconfigure LPC firmware cycle DMA ranges
                                    if ((hiomap_use_direct_access) && (hiomap_config.window_type == HIOMAP_WINDOW_TYPE_WRITE))
                                    {
                                        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG2, (uintptr_t)hiomap_write_buffer);
                                        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG3, hiomap_config.window_length_bytes);
                                        // This is a little trick used to hide the moving buffer location in LPC firmware space from the host
                                        // By masking off all but the lowest address lines coming from the host, the host continues
                                        // to send addresses for higher regions of the mapped LPC firmware memory, but they end up diverted
                                        // into the small write buffer allocated earlier.
                                        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG4, 0);
                                        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG5, hiomap_config.window_length_bytes);
                                        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG6, FLASH_MAX_WR_WINDOW_BYTES - 1);
                                    }
                                    else
                                    {
                                        if (hiomap_use_direct_access)
                                        {
                                            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG2, (uintptr_t)HOSTSPIFLASH_BASE);
                                        }
                                        else
                                        {
                                            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG2, (uintptr_t)host_flash_buffer);
                                        }
                                        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG3, FLASH_SIZE_BYTES);
                                        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG4, hiomap_config.window_start_address);
                                        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG5,
                                                              hiomap_config.window_start_address + hiomap_config.window_length_bytes);
                                        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG6, FLASH_SIZE_BYTES - 1);
                                    }

                                    // Enable DMA engine
                                    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG1);
                                    dword |= ((1 & AQUILA_LPC_CTL_EN_FW_DMA_R_MASK) << AQUILA_LPC_CTL_EN_FW_DMA_R_SHIFT);
                                    if (hiomap_config.window_type == HIOMAP_WINDOW_TYPE_WRITE)
                                    {
                                        dword |= ((1 & AQUILA_LPC_CTL_EN_FW_DMA_W_MASK) << AQUILA_LPC_CTL_EN_FW_DMA_W_SHIFT);
                                    }
                                    else
                                    {
                                        dword &= ~((1 & AQUILA_LPC_CTL_EN_FW_DMA_W_MASK) << AQUILA_LPC_CTL_EN_FW_DMA_W_SHIFT);
                                    }
                                    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG1, dword);

                                    // Re-activate interupts on exiting critical section
                                    irq_unlock(key);
#endif

                                    // Generate response
                                    switch (hiomap_config.protocol_version)
                                    {
                                        case 1:
                                            // Use 1:1 mapping between LPC firmware address and SPI Flash
                                            // address
                                            response.data[2] = (hiomap_config.window_start_address >> FLASH_BLOCK_SIZE_SHIFT) & 0xff;
                                            response.data[3] = ((hiomap_config.window_start_address >> FLASH_BLOCK_SIZE_SHIFT) >> 8) & 0xff;
                                            response.length = BASE_HIOMAP_RESPONSE_LENGTH + 2;
                                            break;
                                        case 2:
                                        case 3:
                                            // Use 1:1 mapping between LPC firmware address and SPI Flash
                                            // address
                                            response.data[2] = (hiomap_config.window_start_address >> FLASH_BLOCK_SIZE_SHIFT) & 0xff;
                                            response.data[3] = ((hiomap_config.window_start_address >> FLASH_BLOCK_SIZE_SHIFT) >> 8) & 0xff;
                                            // Echo configured Flash window start / length
                                            response.data[4] = (hiomap_config.window_length_bytes >> FLASH_BLOCK_SIZE_SHIFT) & 0xff;
                                            response.data[5] = ((hiomap_config.window_length_bytes >> FLASH_BLOCK_SIZE_SHIFT) >> 8) & 0xff;
                                            response.data[6] = (hiomap_config.window_start_address >> FLASH_BLOCK_SIZE_SHIFT) & 0xff;
                                            response.data[7] = ((hiomap_config.window_start_address >> FLASH_BLOCK_SIZE_SHIFT) >> 8) & 0xff;
                                            response.length = BASE_HIOMAP_RESPONSE_LENGTH + 6;
                                            break;
                                    }

                                    response.data[0] = ipmi_bt_current_request.data[0];
                                    response.data[1] = ipmi_bt_current_request.data[1];
                                    response.completion_code = IPMI_CC_NO_ERROR;
                                    break;
                                case HIOMAP_CMD_MARK_DIRTY:
                                    flags = 0;
                                    switch (hiomap_config.protocol_version)
                                    {
                                        case 1:
                                            offset_bytes = (((((uint32_t)ipmi_bt_current_request.data[3]) << 8) | ipmi_bt_current_request.data[2])
                                                            << FLASH_BLOCK_SIZE_SHIFT) &
                                                           ((1 << LPC_ADDRESS_BITS) - 1);
                                            length_bytes =
                                                ((((uint32_t)ipmi_bt_current_request.data[7]) << 24) | (((uint32_t)ipmi_bt_current_request.data[6]) << 16) |
                                                 (((uint32_t)ipmi_bt_current_request.data[5]) << 8) | ipmi_bt_current_request.data[4]);
                                            break;
                                        case 2:
                                        case 3:
                                            offset_bytes = hiomap_config.window_start_address +
                                                           ((((((uint32_t)ipmi_bt_current_request.data[3]) << 8) | ipmi_bt_current_request.data[2])
                                                             << FLASH_BLOCK_SIZE_SHIFT) &
                                                            ((1 << LPC_ADDRESS_BITS) - 1));
                                            length_bytes = (((((uint32_t)ipmi_bt_current_request.data[5]) << 8) | ipmi_bt_current_request.data[4])
                                                            << FLASH_BLOCK_SIZE_SHIFT) &
                                                           ((1 << LPC_ADDRESS_BITS) - 1);
                                            if (hiomap_config.protocol_version == 3)
                                            {
                                                flags = ipmi_bt_current_request.data[6];
                                            }
                                            break;
                                    }

                                    // Record dirty page
                                    hiomap_config.dirty_ranges[hiomap_config.dirty_range_count].start_address = offset_bytes;
                                    hiomap_config.dirty_ranges[hiomap_config.dirty_range_count].bytes = length_bytes;
                                    hiomap_config.dirty_ranges[hiomap_config.dirty_range_count].erased = flags & 0x1;
                                    hiomap_config.dirty_range_count++;
                                    if (ENABLE_IPMI_DEBUG) {
                                        printk("[HIOM] Mark range 0x%08x len %d dirty, now %d dirty ranges\n", offset_bytes, length_bytes, hiomap_config.dirty_range_count);
                                    }

                                    response.data[0] = ipmi_bt_current_request.data[0];
                                    response.data[1] = ipmi_bt_current_request.data[1];
                                    response.length = BASE_HIOMAP_RESPONSE_LENGTH;
                                    response.completion_code = IPMI_CC_NO_ERROR;
                                    break;
                                case HIOMAP_CMD_FLUSH:
                                    if (hiomap_config.protocol_version == 1)
                                    {
                                        // Only HIOMAP protocol v1 has the ability to mark a page dirty in
                                        // the FLUSH command
                                        offset_bytes =
                                            (((((uint32_t)ipmi_bt_current_request.data[3]) << 8) | ipmi_bt_current_request.data[2]) << FLASH_BLOCK_SIZE_SHIFT) &
                                            ((1 << LPC_ADDRESS_BITS) - 1);
                                        length_bytes =
                                            ((((uint32_t)ipmi_bt_current_request.data[7]) << 24) | (((uint32_t)ipmi_bt_current_request.data[6]) << 16) |
                                             (((uint32_t)ipmi_bt_current_request.data[5]) << 8) | ipmi_bt_current_request.data[4]);

                                        // Record dirty page
                                        hiomap_config.dirty_ranges[hiomap_config.dirty_range_count].start_address = offset_bytes;
                                        hiomap_config.dirty_ranges[hiomap_config.dirty_range_count].bytes = length_bytes;
                                        hiomap_config.dirty_ranges[hiomap_config.dirty_range_count].erased = 0;
                                        hiomap_config.dirty_range_count++;
                                    }

                                    for (i = 0; i < hiomap_config.dirty_range_count; i++)
                                    {
                                        write_hiomap_data_to_flash(HOSTSPIFLASHCFG_BASE, HOSTSPIFLASH_BASE,
                                                            hiomap_config.dirty_ranges[i].bytes, hiomap_config.dirty_ranges[i].start_address,
                                                            !hiomap_config.dirty_ranges[i].erased);
                                    }
                                    hiomap_config.dirty_range_count = 0;

                                    response.data[0] = ipmi_bt_current_request.data[0];
                                    response.data[1] = ipmi_bt_current_request.data[1];
                                    response.length = BASE_HIOMAP_RESPONSE_LENGTH;
                                    response.completion_code = IPMI_CC_NO_ERROR;
                                case HIOMAP_CMD_ACK:
                                    // Mask is in ipmi_bt_current_request.data[2]
                                    // For now just ignore and claim sucess
                                    response.data[0] = ipmi_bt_current_request.data[0];
                                    response.data[1] = ipmi_bt_current_request.data[1];
                                    response.length = BASE_HIOMAP_RESPONSE_LENGTH;
                                    response.completion_code = IPMI_CC_NO_ERROR;
                                    break;
                                default:
                                    unhandled_ipmi_command = 1;
                                    break;
                            }
                            break;
                        default:
                            unhandled_ipmi_command = 1;
                            break;
                    }
                    break;
                default:
                    unhandled_ipmi_command = 1;
                    break;
            }

            if (unhandled_ipmi_command)
            {
                response.length = BASE_IPMI_RESPONSE_LENGTH;
                response.completion_code = IPMI_CC_INVALID_COMMAND;
            }

            if (ENABLE_IPMI_DEBUG) {
                printk("[IPMI] Responding with completion code 0x%02x data [0x%02x 0x%02x 0x%02x 0x%02x 0x%02x]\n", response.completion_code, ((uint8_t*)&response)[0], ((uint8_t*)&response)[1], ((uint8_t*)&response)[2], ((uint8_t*)&response)[3], ((uint8_t*)&response)[4]);
            }

            ipmi_bt_transaction_state = 2;
            work_executed = 1;
            break;
        case 2:
            // Wait for H_BUSY clear
            if (!(read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_IPMI_BT_STATUS) & (1 << IPMI_BT_CTL_H_BUSY_SHIFT)))
            {
                ipmi_bt_transaction_state = 3;
            }
            work_executed = 1;
            break;
        case 3:
            {
                // Initialize pointer
                response_ptr = (ipmi_response_message_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_IPMI_BT_DATA_BLOCK_OFFSET);

                // Send response
                // A full copy is done so as to ensure any potentially sensitive data stored
                // in the IPMI BT buffer from a previous request is overwritten
                *response_ptr = response;

                // Signal BMC data ready
                dword = 0;
                dword |= (1 << IPMI_BT_CTL_B2H_ATN_SHIFT);
                write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_IPMI_BT_STATUS, dword);
            }

            ipmi_bt_transaction_state = 4;
            work_executed = 1;
            break;
        case 4:
            // Wait for processing to complete
            // If B2H_ATN, and H_BUSY are both clear, processing has been completed
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_IPMI_BT_STATUS);
            if ((!(dword & (1 << IPMI_BT_CTL_B2H_ATN_SHIFT))) && (!(dword & (1 << IPMI_BT_CTL_H_BUSY_SHIFT))))
            {
                if (ENABLE_IPMI_DEBUG) {
                    printk("[IPMI] Response complete, returning to idle\n");
                }
                ipmi_bt_transaction_state = 0;
            }
            break;
        default:
            ipmi_bt_transaction_state = 0;
            break;
    }

    if (work_executed)
        return -EAGAIN;

    return 0;
}

#if !(ENABLE_LPC_FW_CYCLE_IRQ_HANDLER)
static uint32_t previous_fw_read_address;
#endif

static int process_interrupts_stage2(void)
{
    uint32_t dword;
    int read_position;
    int work_executed;

#if (WITH_ZEPHYR)
    if (k_mutex_lock(&vuart1_access_mutex, K_MSEC(100)) != 0) {
        LOG_WRN("Unable to acquire VUART1 mutex in a timely manner! %s:%d\n", __FILE__, __LINE__);
        return -EAGAIN;
    }
#endif

    work_executed = 0;

    // Deactivate interrupts on entering critical section
    int key = irq_lock();

    // CRITICAL SECTION
    // No LPC interrupts can fire here!
    // All code in this section must be able to run in bounded time -- do NOT wait
    // on external events etc. here, just move and enqueue data as needed for
    // further processing at a later time

    // Process incoming POST code data
    if (post_code_incoming_interrupt_transient_buffer_pos > 0)
    {
        read_position = 0;
        while (read_position < post_code_incoming_interrupt_transient_buffer_pos)
        {
            uint16_t post_code = post_code_incoming_interrupt_transient_buffer[read_position];
            host_active_post_code = post_code;
            display_post_code(host_active_post_code);

            if (post_code == 0xfefe)
            {
                host_power_status = HOST_POWER_STATUS_RUNNING;
                host_power_status_changed();
                display_character('0' + 2 + host_power_status, 0); // STATUS CODE
            }

            if (enable_post_code_console_output)
            {
                KESTREL_LOG("[POST CODE] %d.%d", (post_code >> 8) & 0xff, post_code & 0xff);
            }
            read_position++;
        }
        post_code_incoming_interrupt_transient_buffer_pos = 0;
        if (post_code_incoming_interrupt_transient_buffer_overflow)
        {
            // Clear overflow flag
            post_code_incoming_interrupt_transient_buffer_overflow = 0;
        }
        work_executed = 1;
    }

    // Process incoming VUART data
    if (vuart1_incoming_interrupt_transient_buffer_pos > 0)
    {
        read_position = 0;
        while (read_position < vuart1_incoming_interrupt_transient_buffer_pos)
        {
            vuart1_incoming_buffer[vuart1_incoming_buffer_write_pos] = vuart1_incoming_interrupt_transient_buffer[read_position];
            vuart1_incoming_buffer_write_pos++;
            if (vuart1_incoming_buffer_write_pos >= VUART_RING_BUFFER_SIZE)
            {
                vuart1_incoming_buffer_write_pos = 0;
            }
            read_position++;
            if (read_position >= VUART_RING_BUFFER_SIZE)
            {
                break;
            }
        }
        vuart1_incoming_interrupt_transient_buffer_pos = 0;
        if (vuart1_incoming_interrupt_transient_buffer_overflow)
        {
            // Reenable VUART1 interrupts
            dword = (*((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + AQUILA_LPC_VUART1_CONTROL_REG)));
            dword |= (1 & AQUILA_LPC_VUART_IRQ_EN_MASK) << AQUILA_LPC_VUART_IRQ_EN_SHIFT;
            (*((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + AQUILA_LPC_VUART1_CONTROL_REG))) = dword;
            vuart1_incoming_interrupt_transient_buffer_overflow = 0;
        }
        work_executed = 1;
    }
    if (vuart2_incoming_interrupt_transient_buffer_pos > 0)
    {
        read_position = 0;
        while (read_position < vuart2_incoming_interrupt_transient_buffer_pos)
        {
            vuart2_incoming_buffer[vuart2_incoming_buffer_write_pos] = vuart2_incoming_interrupt_transient_buffer[read_position];
            vuart2_incoming_buffer_write_pos++;
            if (vuart2_incoming_buffer_write_pos >= VUART_RING_BUFFER_SIZE)
            {
                vuart2_incoming_buffer_write_pos = 0;
            }
            read_position++;
            if (read_position >= VUART_RING_BUFFER_SIZE)
            {
                break;
            }
        }
        vuart2_incoming_interrupt_transient_buffer_pos = 0;
        if (vuart2_incoming_interrupt_transient_buffer_overflow)
        {
            // Reenable VUART1 interrupts
            dword = (*((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + AQUILA_LPC_VUART1_CONTROL_REG)));
            dword |= (1 & AQUILA_LPC_VUART_IRQ_EN_MASK) << AQUILA_LPC_VUART_IRQ_EN_SHIFT;
            (*((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + AQUILA_LPC_VUART1_CONTROL_REG))) = dword;
            vuart2_incoming_interrupt_transient_buffer_overflow = 0;
        }
        work_executed = 1;
    }

    // Process incoming IPMI BT request data
    if (ipmi_bt_interrupt_transient_request_valid)
    {
        if (ipmi_bt_transaction_state == 0)
        {
            ipmi_bt_current_request = ipmi_bt_interrupt_transient_request;
            ipmi_bt_interrupt_transient_request_valid = 0;
            ipmi_bt_transaction_state = 1;
            work_executed = 1;
        }
    }

    // Re-activate interupts on exiting critical section
    irq_unlock(key);

#if (WITH_ZEPHYR)
    k_mutex_unlock(&vuart1_access_mutex);
#endif

    if (work_executed) {
        return -EAGAIN;
    }
    else {
        return 0;
    }
}

static void run_pre_ipl_bmc_peripheral_setup(void)
{
    uint32_t dword;

    // Reset POST codes and display
    post_code_high = 0;
    post_code_low = 0;
    host_active_post_code = 0x0000;
    display_post_code(host_active_post_code);

    // Deactivate interrupts on entering critical section
    int key = irq_lock();

    // Reset POST code FIFO pointers
    post_code_incoming_interrupt_transient_buffer_pos = 0;
    post_code_incoming_interrupt_transient_buffer_overflow = 0;

    // Reset VUART1 FIFO pointers
    vuart1_incoming_interrupt_transient_buffer_pos = 0;
    vuart1_incoming_interrupt_transient_buffer_overflow = 0;
    vuart1_outgoing_buffer_read_pos = 0;
    vuart1_outgoing_buffer_write_pos = 0;
    vuart1_incoming_buffer_read_pos = 0;
    vuart1_incoming_buffer_write_pos = 0;

    // Re-activate interupts on exiting critical section
    irq_unlock(key);

    // Configure VUART1
    dword = 0;
    dword |= (1 & AQUILA_LPC_VUART_FIFO_TRIG_LVL_MASK) << AQUILA_LPC_VUART_FIFO_TRIG_LVL_SHIFT;
    dword |= (1 & AQUILA_LPC_VUART_IRQ_EN_MASK) << AQUILA_LPC_VUART_IRQ_EN_SHIFT;
    dword |= (1 & AQUILA_LPC_VUART_FIFO_IRQ_EN_MASK) << AQUILA_LPC_VUART_FIFO_IRQ_EN_SHIFT;
    (*((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + AQUILA_LPC_VUART1_CONTROL_REG))) = dword;

    // Enable LPC slave IRQs
    set_lpc_slave_irq_enable(1);

    // Clear IPMI BT B_BUSY flag
    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_IPMI_BT_STATUS) & (1 << IPMI_BT_CTL_B_BUSY_SHIFT);
    if (dword & (1 << IPMI_BT_CTL_B_BUSY_SHIFT))
    {
        dword |= (1 << IPMI_BT_CTL_B_BUSY_SHIFT);
    }
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_IPMI_BT_STATUS, dword);

    // Enable IPMI BT IRQ
    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
    dword |= ((1 & AQUILA_LPC_CTL_EN_IPMI_BT_IRQ_MASK) << AQUILA_LPC_CTL_EN_IPMI_BT_IRQ_SHIFT);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);

#if (ENABLE_LPC_FW_CYCLE_IRQ_HANDLER)
    // Enable LPC firmware cycle IRQ
    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
    dword |= ((1 & AQUILA_LPC_CTL_EN_FW_CYCLE_IRQ_MASK) << AQUILA_LPC_CTL_EN_FW_CYCLE_IRQ_SHIFT);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);
#endif

#if (ENABLE_LPC_IO_CYCLE_IRQ_HANDLER)
    // Enable LPC I/O cycle IRQ
    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
    dword |= ((1 & AQUILA_LPC_CTL_EN_IO_CYCLE_IRQ_MASK) << AQUILA_LPC_CTL_EN_IO_CYCLE_IRQ_SHIFT);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);
#endif

#if (ENABLE_LPC_TPM_CYCLE_IRQ_HANDLER)
    // Enable LPC TPM cycle IRQ
    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
    dword |= ((1 & AQUILA_LPC_CTL_EN_TPM_CYCLE_IRQ_MASK) << AQUILA_LPC_CTL_EN_TPM_CYCLE_IRQ_SHIFT);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);
#endif

    // Reset HIOMAP windows
    hiomap_config.protocol_version = 0;
    hiomap_config.window_start_address = 0;
    hiomap_config.window_length_bytes = FLASH_SIZE_BYTES;
    hiomap_config.active_device_id = 0;
    hiomap_config.window_type = HIOMAP_WINDOW_TYPE_READ;
    hiomap_config.dirty_range_count = 0;

#if (ENABLE_LPC_FW_CYCLE_DMA)
    // Configure and enable LPC firmware cycle DMA
    // Set up default window with address masking based on physical ROM size
    if (hiomap_use_direct_access)
    {
        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG2, (uintptr_t)HOSTSPIFLASH_BASE);
    }
    else
    {
        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG2, (uintptr_t)host_flash_buffer);
    }
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG3, FLASH_SIZE_BYTES);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG4, 0x0);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG5, FLASH_SIZE_BYTES);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG6, FLASH_SIZE_BYTES - 1);

    // Enable DMA engine
    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG1);
    dword |= ((1 & AQUILA_LPC_CTL_EN_FW_DMA_R_MASK) << AQUILA_LPC_CTL_EN_FW_DMA_R_SHIFT);
    dword &= ~((1 & AQUILA_LPC_CTL_EN_FW_DMA_W_MASK) << AQUILA_LPC_CTL_EN_FW_DMA_W_SHIFT);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG1, dword);
#endif

    // Enable host background service task
    host_background_service_task_active = 1;

    // Assume console service task is inactive at startup
    host_console_service_task_active = 0;
}

static void run_post_shutdown_bmc_peripheral_teardown(void)
{
    int i;
    uint32_t dword;

    // Disable host and console background service tasks
    host_background_service_task_active = 0;
    host_console_service_task_active = 0;

    // Reset internal state variables
    ipmi_bt_transaction_state = 0;

    // Set IPMI BT B_BUSY flag
    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_IPMI_BT_STATUS) & (1 << IPMI_BT_CTL_B_BUSY_SHIFT);
    if (!(dword & (1 << IPMI_BT_CTL_B_BUSY_SHIFT)))
    {
        // Set B_BUSY
        dword |= (1 << IPMI_BT_CTL_B_BUSY_SHIFT);
    }
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_IPMI_BT_STATUS, dword);

#if (ENABLE_LPC_FW_CYCLE_DMA)
    // Disable DMA engine
    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG1);
    dword &= ~((1 & AQUILA_LPC_CTL_EN_FW_DMA_R_MASK) << AQUILA_LPC_CTL_EN_FW_DMA_R_SHIFT);
    dword &= ~((1 & AQUILA_LPC_CTL_EN_FW_DMA_W_MASK) << AQUILA_LPC_CTL_EN_FW_DMA_W_SHIFT);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DMA_CONFIG1, dword);
#endif

#if (ENABLE_LPC_TPM_CYCLE_IRQ_HANDLER)
    // Disable LPC TPM cycle IRQ
    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
    dword &= ~((1 & AQUILA_LPC_CTL_EN_TPM_CYCLE_IRQ_MASK) << AQUILA_LPC_CTL_EN_TPM_CYCLE_IRQ_SHIFT);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);
#endif

#if (ENABLE_LPC_IO_CYCLE_IRQ_HANDLER)
    // Disable LPC I/O cycle IRQ
    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
    dword &= ~((1 & AQUILA_LPC_CTL_EN_IO_CYCLE_IRQ_MASK) << AQUILA_LPC_CTL_EN_IO_CYCLE_IRQ_SHIFT);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);
#endif

#if (ENABLE_LPC_FW_CYCLE_IRQ_HANDLER)
    // Disable LPC firmware cycle IRQ
    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
    dword &= ~((1 & AQUILA_LPC_CTL_EN_FW_CYCLE_IRQ_MASK) << AQUILA_LPC_CTL_EN_FW_CYCLE_IRQ_SHIFT);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);
#endif

    // Disable IPMI BT IRQ
    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
    dword &= ~((1 & AQUILA_LPC_CTL_EN_IPMI_BT_IRQ_MASK) << AQUILA_LPC_CTL_EN_IPMI_BT_IRQ_SHIFT);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);

    // Disable LPC slave IRQs
    set_lpc_slave_irq_enable(0);

    // Reset HIOMAP windows
    for (i = 0; i < hiomap_config.write_window_count; i++)
    {
        if (hiomap_config.write_windows[i].active)
        {
            free(hiomap_config.write_windows[i].buffer);
            hiomap_config.write_windows[i].buffer = NULL;
        }
    }
    hiomap_config.protocol_version = 0;
    hiomap_config.window_start_address = 0;
    hiomap_config.window_length_bytes = FLASH_SIZE_BYTES;
    hiomap_config.active_device_id = 0;
    hiomap_config.window_type = HIOMAP_WINDOW_TYPE_READ;
    hiomap_config.dirty_range_count = 0;
    hiomap_config.write_window_count = 0;

    // Reset POST codes and display
    post_code_high = 0;
    post_code_low = 0;
    host_active_post_code = 0x0000;
    display_post_code(host_active_post_code);
}

static int apply_avsbus_workarounds_cpu(const cpu_info_t *cpu)
{
    KESTREL_LOG("\tVDD/VCS %d: Enabling AVSBus CLK/MDAT pullups and selecting "
           "VIH/VIL 0x2 (0.65V/0.55V)",
           cpu->index);
    if (i2c_write_register_byte(cpu->i2c_master, cpu->vdd_smbus_addr, 0x2e, 0x23))
    {
        return -1;
    }

    KESTREL_LOG("\tVDN %d: Enabling AVSBus CLK/MDAT pullups and selecting VIH/VIL "
           "0x2 (0.65V/0.55V)",
           cpu->index);
    if (i2c_write_register_byte(cpu->i2c_master, cpu->vdn_smbus_addr, 0x2e, 0x23))
    {
        return -1;
    }

    return 0;
}

static int apply_avsbus_workarounds(const cpu_info_t *cpu_info, int cpu_count)
{
    KESTREL_LOG("Applying AVSBus workarounds...");

    for (int i = 0; i < cpu_count; i++)
    {
        if (apply_avsbus_workarounds_cpu(&cpu_info[i]))
        {
            return -1;
        }
    }

    KESTREL_LOG("\tAVSBus workaround application complete!");
    return 0;
}

static int enable_avsbus_pmbus_cpu(const cpu_info_t *cpu)
{
    KESTREL_LOG("\tVDD %d: Placing device in AVSBus voltage command mode", cpu->index);
    if (i2c_write_register_byte(cpu->i2c_master, cpu->vdd_regulator_addr, 0x00, cpu->vdd_regulator_page))
    {
        return -1;
    }
    if (i2c_write_register_byte(cpu->i2c_master, cpu->vdd_regulator_addr, 0x01, 0xb0))
    {
        return -1;
    }

    KESTREL_LOG("\tVCS %d: Placing device in AVSBus voltage command mode", cpu->index);
    if (i2c_write_register_byte(cpu->i2c_master, cpu->vcs_regulator_addr, 0x00, cpu->vcs_regulator_page))
    {
        return -1;
    }
    if (i2c_write_register_byte(cpu->i2c_master, cpu->vcs_regulator_addr, 0x01, 0xb0))
    {
        return -1;
    }

    KESTREL_LOG("\tVDN %d: Placing device in AVSBus voltage command mode", cpu->index);
    if (i2c_write_register_byte(cpu->i2c_master, cpu->vdn_regulator_addr, 0x00, cpu->vdn_regulator_page))
    {
        return -1;
    }
    if (i2c_write_register_byte(cpu->i2c_master, cpu->vdn_regulator_addr, 0x01, 0xb0))
    {
        return -1;
    }

    return 0;
}

static int enable_avsbus_pmbus(const cpu_info_t *cpu_info, int cpu_count)
{
    KESTREL_LOG("Enabling AVSbus PMBUS functionality...");

    for (int i = 0; i < cpu_count; i++)
    {
        if (enable_avsbus_pmbus_cpu(&cpu_info[i]))
        {
            return -1;
        }
    }

    KESTREL_LOG("\tAVSBus PMBUS functionality enabled!");
    return 0;
}

static int disable_avsbus_pmbus_cpu(const cpu_info_t *cpu)
{
    KESTREL_LOG("\tVDD %d: Placing device in immediate off mode", cpu->index);
    if (i2c_write_register_byte(cpu->i2c_master, cpu->vdd_regulator_addr, 0x00, cpu->vdd_regulator_page))
    {
        return -1;
    }
    if (i2c_write_register_byte(cpu->i2c_master, cpu->vdd_regulator_addr, 0x01, 0x80))
    {
        return -1;
    }

    KESTREL_LOG("\tVCS %d: Placing device in immediate off mode", cpu->index);
    if (i2c_write_register_byte(cpu->i2c_master, cpu->vcs_regulator_addr, 0x00, cpu->vcs_regulator_page))
    {
        return -1;
    }
    if (i2c_write_register_byte(cpu->i2c_master, cpu->vcs_regulator_addr, 0x01, 0x80))
    {
        return -1;
    }

    KESTREL_LOG("\tVDN %d: Placing device in immediate off mode", cpu->index);
    if (i2c_write_register_byte(cpu->i2c_master, cpu->vdn_regulator_addr, 0x00, cpu->vdn_regulator_page))
    {
        return -1;
    }
    if (i2c_write_register_byte(cpu->i2c_master, cpu->vdn_regulator_addr, 0x01, 0x80))
    {
        return -1;
    }

    return 0;
}
static int disable_avsbus_pmbus(const cpu_info_t *cpu_info, int cpu_count)
{
    int status = 0;
    KESTREL_LOG("Disabling AVSbus PMBUS functionality...");

    for (int i = 0; i < cpu_count; i++)
    {
        // Attempt to turn of power on all CPUs, even if one isn't responding.
        if (disable_avsbus_pmbus_cpu(&cpu_info[i]))
        {
            status = -1;
        }
    }

    KESTREL_LOG("\tAVSBus PMBUS functionality disabled!");
    return status;
}

void power_off_chassis(int has_lock)
{
    if (!has_lock) {
        if (k_sem_take(&chassis_control_semaphore, K_MSEC(10)) != 0) {
            // Unable to obtain chassis exclusive lock
            return;
        }
    }

    host_power_status = HOST_POWER_STATUS_POWERING_OFF;
    host_power_status_changed();
    display_character('0' + 2 + host_power_status, 0); // STATUS CODE

    // Disable PMBUS
    if (disable_avsbus_pmbus(g_cpu_info, configured_cpu_count))
    {
        KESTREL_LOG("PMBUS disable failed!");
    }

    // Power off host via platform FPGA commands
    i2c_write_register_byte((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, HOST_PLATFORM_FPGA_I2C_ADDRESS, HOST_PLATFORM_FPGA_I2C_REG_MFR_OVR, 0x00);

    run_post_shutdown_bmc_peripheral_teardown();

    host_power_status = HOST_POWER_STATUS_OFFLINE;
    host_power_status_changed();
    display_character('0' + 2 + host_power_status, 0); // STATUS CODE

    if (!has_lock) {
        k_sem_give(&chassis_control_semaphore);
    }
}

int start_lpc_clock()
{
    uint32_t dword;

    // Force the LPC clock PLL into reset, if present
    // If no PLL is present, this operation does nothing
    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
    dword |= (AQUILA_LPC_CLK_PLL_RESET_MASK << AQUILA_LPC_CLK_PLL_RESET_SHIFT);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);

    // Enable LPC clock buffer
    gpio1_out_write(gpio1_out_read() | (0x01 << 12));

    // Wait a few ms for the external clock buffer to stabilize
    k_busy_wait(16 * 1000);

    // Bring the LPC PLL back out of reset, if present
    // If no PLL is present, this operation does nothing
    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
    dword &= ~(AQUILA_LPC_CLK_PLL_RESET_MASK << AQUILA_LPC_CLK_PLL_RESET_SHIFT);
    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);

    // Wait for LPC clock to stabilize
    uint64_t clock_valid_timer = k_uptime_get();
    do
    {
        if ((k_uptime_get() - clock_valid_timer) > 1000)
        {
            KESTREL_LOG("LPC clock did not come online, aborting!");
            power_off_chassis(1);
            return -1;
        }
    } while (!((read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_STATUS4) >> AQUILA_LPC_CLOCK_VALID_SHIFT) & AQUILA_LPC_CLOCK_VALID_MASK));
    KESTREL_LOG("LPC bus clock online!");

    return 0;
}

int power_on_chassis(int has_lock)
{
    uint8_t platform_fpga_identifier[4];
    int platform_power_on_timeout_counter;
    int cpu_count = 1;
    int i2c_read_retcode;
    uint8_t byte;

    if (!has_lock) {
        if (k_sem_take(&chassis_control_semaphore, K_MSEC(10)) != 0) {
            // Unable to obtain chassis exclusive lock
            return -EAGAIN;
        }
    }

    host_power_status = HOST_POWER_STATUS_POWERING_ON;
    host_power_status_changed();
    display_character('0' + 2 + host_power_status, 0); // STATUS CODE

    // Verify communication with platform control FPGA
    platform_fpga_identifier[0] = i2c_read_register_byte((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, HOST_PLATFORM_FPGA_I2C_ADDRESS, 0x0c, NULL);
    if (platform_fpga_identifier[0] == 0xff)
    {
        host_power_status = HOST_POWER_STATUS_OFFLINE;
        host_power_status_changed();
        display_character('0' + 2 + host_power_status, 0); // STATUS CODE
        if (!has_lock) {
            k_sem_give(&chassis_control_semaphore);
        }
        return -1;
    }
    platform_fpga_identifier[1] = i2c_read_register_byte((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, HOST_PLATFORM_FPGA_I2C_ADDRESS, 0x0d, NULL);
    platform_fpga_identifier[2] = i2c_read_register_byte((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, HOST_PLATFORM_FPGA_I2C_ADDRESS, 0x0e, NULL);
    platform_fpga_identifier[3] = i2c_read_register_byte((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, HOST_PLATFORM_FPGA_I2C_ADDRESS, 0x0f, NULL);
    if ((platform_fpga_identifier[0] != 0x52) || (platform_fpga_identifier[1] != 0x43) || (platform_fpga_identifier[2] != 0x53) ||
        (platform_fpga_identifier[3] != 0x20))
    {
        host_power_status = HOST_POWER_STATUS_OFFLINE;
        host_power_status_changed();
        display_character('0' + 2 + host_power_status, 0); // STATUS CODE
        if (!has_lock) {
            k_sem_give(&chassis_control_semaphore);
        }
        return -1;
    }
    KESTREL_LOG("Platform FPGA communication verified");

    // Enable BMC runtime support tasks
    run_pre_ipl_bmc_peripheral_setup();

    // Power on host via platform FPGA commands
    KESTREL_LOG("Commanding chassis power ON...");
    i2c_write_register_byte((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, HOST_PLATFORM_FPGA_I2C_ADDRESS, HOST_PLATFORM_FPGA_I2C_REG_MFR_OVR, 0x01);
    platform_power_on_timeout_counter = 0;
    byte = i2c_read_register_byte((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, HOST_PLATFORM_FPGA_I2C_ADDRESS, HOST_PLATFORM_FPGA_I2C_REG_STATUS, &i2c_read_retcode);
    while (i2c_read_retcode || (((byte)&0x03) != 0x03))
    {
        if (platform_power_on_timeout_counter > 20000)
        {
            KESTREL_LOG("Chassis poweron timeout!");
            power_off_chassis(1);
            if (!has_lock) {
                k_sem_give(&chassis_control_semaphore);
            }
            return -2;
        }
        usleep(100);
        platform_power_on_timeout_counter++;
        byte = i2c_read_register_byte((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, HOST_PLATFORM_FPGA_I2C_ADDRESS, HOST_PLATFORM_FPGA_I2C_REG_STATUS, &i2c_read_retcode);
    }
    if (i2c_read_retcode)
    {
        KESTREL_LOG("FPGA communication failure during poweron!");
        power_off_chassis(1);
        if (!has_lock) {
            k_sem_give(&chassis_control_semaphore);
        }
        return -3;
    }
    if (byte & 0x20)
    {
        cpu_count = 2;
    }
    KESTREL_LOG("Chassis power verified active");
    configured_cpu_count = cpu_count;
    if (cpu_count > MAX_CPUS_SUPPORTED)
    {
        configured_cpu_count = cpu_count = MAX_CPUS_SUPPORTED;
        KESTREL_LOG("Limiting number of CPUs to %ld", MAX_CPUS_SUPPORTED);
    }
    KESTREL_LOG("%d CPU(s) installed", cpu_count);

    // Apply AVSBus workarounds
    if (apply_avsbus_workarounds(g_cpu_info, cpu_count))
    {
        KESTREL_LOG("AVSBus setup failed!");
        power_off_chassis(1);
        if (!has_lock) {
            k_sem_give(&chassis_control_semaphore);
        }
        return -4;
    }

    // Enable PMBUS
    if (enable_avsbus_pmbus(g_cpu_info, cpu_count))
    {
        KESTREL_LOG("PMBUS enable failed!");
        power_off_chassis(1);
        if (!has_lock) {
            k_sem_give(&chassis_control_semaphore);
        }
        return -5;
    }

    if (start_lpc_clock())
    {
        return -6;
    }

    if ((ENABLE_HDMI_TRANSCEIVER) && (HOST_TYPE_BLACKBIRD)) {
#ifdef HHDT_I2C_MASTER_BASE_ADDR
        if (configure_hdmi_transceiver((uint8_t *)HHDT_I2C_MASTER_BASE_ADDR)) {
            KESTREL_LOG("Host HDMI transceiver could not be configured, aborting!");
            power_off_chassis(1);
            return -7;
        }
#endif
    }

    host_power_status = HOST_POWER_STATUS_IPLING;
    host_power_status_changed();
    display_character('0' + 2 + host_power_status, 0); // STATUS CODE

    if (!has_lock) {
        k_sem_give(&chassis_control_semaphore);
    }

    return 0;
}

int power_on_host(void)
{
    if (k_sem_take(&chassis_control_semaphore, K_MSEC(10)) != 0) {
        // Unable to obtain chassis exclusive lock
        return -EAGAIN;
    }

    if (power_on_chassis(1))
    {
        k_sem_give(&chassis_control_semaphore);
        return -1;
    }

    if (start_ipl(0))
    {
        k_sem_give(&chassis_control_semaphore);
        return -1;
    }

    k_sem_give(&chassis_control_semaphore);
    return 0;
}

void print_chassis_status(void)
{
    int i2c_read_retcode;
    uint8_t byte;

    byte = i2c_read_register_byte((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, HOST_PLATFORM_FPGA_I2C_ADDRESS, HOST_PLATFORM_FPGA_I2C_REG_STATUS, &i2c_read_retcode);
    if (i2c_read_retcode)
    {
        KESTREL_LOG("Unable to communicate with platform control FPGA!");
    }

    KESTREL_LOG("Platform FPGA status:");
    if (byte & 0x1)
    {
        KESTREL_LOG("\tPSU commanded ON");
    }
    else
    {
        KESTREL_LOG("\tPSU commanded OFF");
    }
    if (byte & 0x2)
    {
        KESTREL_LOG("\tPSU PGOOD asserted");
    }
    else
    {
        KESTREL_LOG("\tPSU PGOOD deasserted");
    }
    KESTREL_LOG("Platform overall status:");
    if (byte & 0x20)
    {
        KESTREL_LOG("\t2 CPUs installed");
    }
    else
    {
        KESTREL_LOG("\t1 CPU installed");
    }
    KESTREL_LOG("BMC status:");
    if (host_background_service_task_active)
    {
        KESTREL_LOG("\tBackground host services active");
    }
    else
    {
        KESTREL_LOG("\tBackground host services inactive");
    }
}

static int host_background_service_task_event_loop(void)
{
    uint32_t address;
    uint8_t write_not_read;
    uint32_t dword;
#if !(ENABLE_LPC_FW_CYCLE_IRQ_HANDLER)
    int byte;
    int word;
    uint32_t physical_flash_address;
#endif

    int work_executed;

    // IRQ debugging routines
    if (irq_unhandled_source_valid)
    {
        KESTREL_LOG("[WARNING] Interrupt triggered without external IRQ set!  source: %d", irq_unhandled_source);
        irq_unhandled_source_valid = 0;
    }
    if (irq_unhandled_vector_valid)
    {
        KESTREL_LOG("[ERROR] Unhandled exception 0x%03d", irq_unhandled_vector);
        irq_unhandled_vector_valid = 0;
    }

    work_executed = 0;

    if (read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_STATUS1) & (AQUILA_LPC_STATUS_ATTN_REQ_MASK << AQUILA_LPC_STATUS_ATTN_REQ_SHIFT))
    {
        work_executed = 1;

        // Store / retrieve data
        uint32_t status1_reg = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_STATUS1);
        uint32_t status2_reg = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_STATUS2);
        address = (status2_reg >> AQUILA_LPC_STATUS_ACT_ADDR_SHIFT) & AQUILA_LPC_STATUS_ACT_ADDR_MASK;
        write_not_read = (status1_reg >> AQUILA_LPC_STATUS_CYC_WNR_SHIFT) & AQUILA_LPC_STATUS_CYC_WNR_MASK;
#if !(ENABLE_LPC_FW_CYCLE_IRQ_HANDLER)
        if (((status1_reg >> AQUILA_LPC_STATUS_CYCLE_TYPE_SHIFT) & AQUILA_LPC_STATUS_CYCLE_TYPE_MASK) == AQUILA_LPC_STATUS_CYCLE_TYPE_FW)
        {
            uint8_t fw_cycle_idsel = (status1_reg >> AQUILA_LPC_STATUS_FW_CYCLE_IDSEL_SHIFT) & AQUILA_LPC_STATUS_FW_CYCLE_IDSEL_MASK;
            uint8_t fw_cycle_msize = (status1_reg >> AQUILA_LPC_STATUS_FW_CYCLE_MSIZE_SHIFT) & AQUILA_LPC_STATUS_FW_CYCLE_MSIZE_MASK;

            if (fw_cycle_idsel == 0)
            {
                // Limit firmware address to Flash device size (wrap around)
                address &= (FLASH_SIZE_BYTES - 1);

                // Compute active window address
                previous_fw_read_address = address;
                physical_flash_address = address;
                uint8_t *active_host_flash_buffer;
                if (hiomap_use_direct_access)
                {
                    if (hiomap_config.window_type == HIOMAP_WINDOW_TYPE_WRITE)
                    {
                        active_host_flash_buffer = hiomap_write_buffer + (physical_flash_address - hiomap_config.window_start_address);
                    }
                    else
                    {
                        active_host_flash_buffer = (void*)(HOSTSPIFLASH_BASE + physical_flash_address);
                    }
                }
                else
                {
                    active_host_flash_buffer = host_flash_buffer + physical_flash_address;
                }

                if ((address >= hiomap_config.window_start_address) && ((address - hiomap_config.window_start_address) < hiomap_config.window_length_bytes))
                {
                    if (!write_not_read && ((hiomap_config.window_type == HIOMAP_WINDOW_TYPE_READ) || (hiomap_config.window_type == HIOMAP_WINDOW_TYPE_WRITE)))
                    {
                        if (lpc_fw_msize_to_bytes(fw_cycle_msize) >= 4)
                        {
                            for (word = 0; word < (lpc_fw_msize_to_bytes(fw_cycle_msize) / 4); word++)
                            {
                                *((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_FW_DATA_BLOCK_OFFSET + (word * 4))) =
                                    *((uint32_t *)(active_host_flash_buffer + (word * 4)));
                            }
                        }
                        else
                        {
                            for (byte = 0; byte < lpc_fw_msize_to_bytes(fw_cycle_msize); byte++)
                            {
                                *((volatile uint8_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_FW_DATA_BLOCK_OFFSET + byte)) =
                                    *((uint8_t *)(active_host_flash_buffer + byte));
                            }
                        }

                        // Transfer success -- do not send error
                        dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                        dword &= ~((AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
                        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
                    }
                    else if (write_not_read && (hiomap_config.window_type == HIOMAP_WINDOW_TYPE_WRITE))
                    {
                        if (lpc_fw_msize_to_bytes(fw_cycle_msize) >= 4)
                        {
                            for (word = 0; word < (lpc_fw_msize_to_bytes(fw_cycle_msize) / 4); word++)
                            {
                                *((uint32_t *)(active_host_flash_buffer + (word * 4))) =
                                    *((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_FW_DATA_BLOCK_OFFSET + (word * 4)));
                            }
                        }
                        else
                        {
                            for (byte = 0; byte < lpc_fw_msize_to_bytes(fw_cycle_msize); byte++)
                            {
                                *((uint8_t *)(active_host_flash_buffer + byte)) =
                                    *((volatile uint8_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_FW_DATA_BLOCK_OFFSET + byte));
                            }
                        }

                        // Transfer success -- do not send error
                        dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                        dword &= ~((AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
                        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
                    }
                    else
                    {
                        KESTREL_LOG("[WARNING] Data transfer attempted without active HIOMAP "
                               "window!  Returning error to host...");

                        // Invalid access -- send error
                        dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                        dword |= ((1 & AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
                        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
                    }
                }
                else
                {
                    KESTREL_LOG("[WARNING] Data transfer attempted outside configured HIOMAP "
                           "window!  Returning error to host...");

                    // Invalid access -- send error
                    dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                    dword |= ((1 & AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
                    write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
                }
            }
            else
            {
                KESTREL_LOG("[WARNING] Received firmware cycle request for IDSEL 0x%02x "
                       "(address 0x%08x)!  Dazed and confused, but trying to "
                       "continue...",
                       fw_cycle_idsel, address);

                // Do not send error
                dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                dword &= ~((AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
                write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
            }

            // Acknowledge data transfer
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
            dword |= ((1 & AQUILA_LPC_CTL_XFER_CONT_MASK) << AQUILA_LPC_CTL_XFER_CONT_SHIFT);
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
        }
        else
#endif
            if (((status1_reg >> AQUILA_LPC_STATUS_CYCLE_TYPE_SHIFT) & AQUILA_LPC_STATUS_CYCLE_TYPE_MASK) == AQUILA_LPC_STATUS_CYCLE_TYPE_IO)
        {
#if (ENABLE_LPC_IO_CYCLE_IRQ_HANDLER)
            // Transfer failed -- send error
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
            dword |= ((1 & AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);

            // Acknowledge data transfer
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
            dword |= ((1 & AQUILA_LPC_CTL_XFER_CONT_MASK) << AQUILA_LPC_CTL_XFER_CONT_SHIFT);
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);

            // Unmask LPC I/O cycle IRQ
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
            dword |= ((1 & AQUILA_LPC_CTL_EN_IO_CYCLE_IRQ_MASK) << AQUILA_LPC_CTL_EN_IO_CYCLE_IRQ_SHIFT);
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);
#else
            if ((address >= 0x80) && (address <= 0x82))
            {
                if (write_not_read)
                {
                    uint8_t post_code = (read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_STATUS3) >> AQUILA_LPC_STATUS_ACT_WDATA_SHIFT) &
                                        AQUILA_LPC_STATUS_ACT_WDATA_MASK;
                    if (address == 0x81)
                    {
                        post_code_high = post_code;
                    }
                    else if (address == 0x82)
                    {
                        post_code_low = post_code;
                        host_active_post_code = ((post_code_high & 0xff) << 8) | (post_code_low & 0xff);
                        display_post_code(host_active_post_code);

                        if (enable_post_code_console_output)
                        {
                            KESTREL_LOG("[POST CODE] %d.%d", post_code_high, post_code_low);
                        }
                    }
                }

                // Transfer success -- do not send error
                dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                dword &= ~((AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
                write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);

                // Acknowledge data transfer
                dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                dword |= ((1 & AQUILA_LPC_CTL_XFER_CONT_MASK) << AQUILA_LPC_CTL_XFER_CONT_SHIFT);
                write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
            }
            else
            {
                KESTREL_LOG("[WARNING] LPC I/O transfer attempted to invalid address 0x%04x", address);

                // Transfer failed -- send error
                dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                dword |= ((1 & AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
                write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);

                // Acknowledge data transfer
                dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
                dword |= ((1 & AQUILA_LPC_CTL_XFER_CONT_MASK) << AQUILA_LPC_CTL_XFER_CONT_SHIFT);
                write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
            }
#endif
        }
        else if (((status1_reg >> AQUILA_LPC_STATUS_CYCLE_TYPE_SHIFT) & AQUILA_LPC_STATUS_CYCLE_TYPE_MASK) == AQUILA_LPC_STATUS_CYCLE_TYPE_TPM)
        {
            // Transfer failed -- send error
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
            dword |= ((1 & AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);

            // Acknowledge data transfer
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
            dword |= ((1 & AQUILA_LPC_CTL_XFER_CONT_MASK) << AQUILA_LPC_CTL_XFER_CONT_SHIFT);
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);

#if (ENABLE_LPC_TPM_CYCLE_IRQ_HANDLER)
            // Unmask LPC TPM cycle IRQ
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
            dword |= ((1 & AQUILA_LPC_CTL_EN_TPM_CYCLE_IRQ_MASK) << AQUILA_LPC_CTL_EN_TPM_CYCLE_IRQ_SHIFT);
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);
#endif
        }
        else
        {
            // Transfer failed -- send error
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
            dword |= ((1 & AQUILA_LPC_CTL_XFER_ERR_MASK) << AQUILA_LPC_CTL_XFER_ERR_SHIFT);
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);

            // Acknowledge data transfer
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2);
            dword |= ((1 & AQUILA_LPC_CTL_XFER_CONT_MASK) << AQUILA_LPC_CTL_XFER_CONT_SHIFT);
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL2, dword);
        }
    }

    if (work_executed)
        return -EAGAIN;

    return 0;
}

int primary_service_event_loop(void)
{
    int work_executed = 0;

    // ===================================================================================
    // Main service loop
    // ===================================================================================
    // This loop is called as frequently as practical to keep response times low
    // All background tasks, from LPC I/O transfers to IPMI requests, are handled
    // here
    // ===================================================================================

    if (host_console_service_task_requested) {
        host_console_service_task_active = 1;
    }

    if (host_background_service_task_active)
    {
        // Run background service task event loop
        if (host_background_service_task_event_loop() == -EAGAIN) {
            work_executed = 1;
        }

        // Process queued interrupt tasks
        if (process_interrupts_stage2() == -EAGAIN) {
            work_executed = 1;
        }

        // Process cooperative multitasking threads

        // Deactivate interrupts on entering critical section
        int key = irq_lock();
        if (process_host_to_bmc_ipmi_bt_transactions() == -EAGAIN) {
            work_executed = 1;
        }
        // Re-activate interupts on exiting critical section
        irq_unlock(key);
    }

    // Process console I/O
    if (host_console_event_loop(host_console_service_task_shell) == -EAGAIN) {
        work_executed = 1;
    }

    if (work_executed) {
        return -EAGAIN;
    }
    else {
        return 0;
    }
}

#if (WITH_ZEPHYR)
int host_console_event_loop(const struct shell *shell)
{
    static uint8_t escape_sequence_state;
    uint8_t character;
    uint8_t character_read;
    size_t count;
    int work_executed;

    work_executed = 0;

    if (shell && host_console_service_task_active)
    {
        if (k_mutex_lock(&vuart1_access_mutex, K_MSEC(100)) != 0) {
            LOG_WRN("Unable to acquire VUART1 mutex in a timely manner! %s:%d\n", __FILE__, __LINE__);
            return -EAGAIN;
        }

        // Escape sequence handler
        character_read = 0;
        (void)shell->iface->api->read(shell->iface, &character, 1, &count);
        if (count > 0)
        {
            character_read = 1;
            switch (escape_sequence_state)
            {
                case 0:
                    if ((character == '\n') || (character == '\r'))
                    {
                        escape_sequence_state = 1;
                    }
                    break;
                case 1:
                    if (character == '~')
                    {
                        escape_sequence_state = 2;
                    }
                    else
                    {
                        escape_sequence_state = 0;
                    }
                    break;
                case 2:
                    if (character == '.')
                    {
                        escape_sequence_state = 3;
                    }
                    else
                    {
                        escape_sequence_state = 0;
                    }
                    break;
                default:
                    escape_sequence_state = 0;
            }
        }
        if (escape_sequence_state == 3)
        {
            // Unlock mutex
            k_mutex_unlock(&vuart1_access_mutex);

            // Discard remaining buffer contents
            do
            {
                (void)shell->iface->api->read(shell->iface, &character, 1, &count);
            } while (count > 0);
            escape_sequence_state = 0;
            host_console_service_task_requested = 0;
            host_console_service_task_active = 0;
            k_thread_resume(kestrel_console_thread_id);
            return 1;
        }

        while (vuart1_outgoing_buffer_write_pos != vuart1_outgoing_buffer_read_pos)
        {
            uint32_t vuart1_status_register = *((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + AQUILA_LPC_VUART1_STATUS_REG));
            if (!((vuart1_status_register >> AQUILA_LPC_VUART_WFIFO_FULL_SHIFT) & AQUILA_LPC_VUART_WFIFO_FULL_MASK))
            {
                // VUART FIFO now has room, send queued character to VUART hardware
                *((volatile uint8_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + 0x0)) = vuart1_outgoing_buffer[vuart1_outgoing_buffer_read_pos];
                vuart1_outgoing_buffer_read_pos++;
                if (vuart1_outgoing_buffer_read_pos >= VUART_RING_BUFFER_SIZE)
                {
                    vuart1_outgoing_buffer_read_pos = 0;
                }
            }
            else
            {
                break;
            }
        }

        if (character_read)
        {
            // Attempt to send character to host
            uint32_t vuart1_status_register = *((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + AQUILA_LPC_VUART1_STATUS_REG));
            if ((vuart1_status_register >> AQUILA_LPC_VUART_WFIFO_FULL_SHIFT) & AQUILA_LPC_VUART_WFIFO_FULL_MASK)
            {
                // VUART FIFO full, add to soft buffer
                vuart1_outgoing_buffer[vuart1_outgoing_buffer_write_pos] = character;
                vuart1_outgoing_buffer_write_pos++;
                if (vuart1_outgoing_buffer_write_pos >= VUART_RING_BUFFER_SIZE)
                {
                    vuart1_outgoing_buffer_write_pos = 0;
                }
            }
            else
            {
                // VUART FIFO still has room, send character to VUART hardware
                *((volatile uint8_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + 0x0)) = character;
            }
            work_executed = 1;
        }

        // Send any queued VUART output from host to BMC console
        size_t chars_to_tx;
        size_t chars_sent;
        if (vuart1_incoming_buffer_read_pos <= vuart1_incoming_buffer_write_pos) {
            chars_to_tx = vuart1_incoming_buffer_write_pos - vuart1_incoming_buffer_read_pos;
        }
        else {
            chars_to_tx = VUART_RING_BUFFER_SIZE - vuart1_incoming_buffer_read_pos;
        }
        if (chars_to_tx > 0)
        {
            chars_sent = 0;
            if (shell->iface->api->write(shell->iface, vuart1_incoming_buffer + vuart1_incoming_buffer_read_pos, chars_to_tx, &chars_sent) == 0) {
                if (chars_sent > 0) {
                    vuart1_incoming_buffer_read_pos += chars_sent;
                }
            }
            if (vuart1_incoming_buffer_read_pos >= VUART_RING_BUFFER_SIZE)
            {
                vuart1_incoming_buffer_read_pos = 0;
            }
            work_executed = 1;
        }

        k_mutex_unlock(&vuart1_access_mutex);
    }

    if (work_executed) {
        return -EAGAIN;
    }
    else {
        return 0;
    }
}
int attach_to_host_console(const struct shell *shell)
{
    if (k_mutex_lock(&vuart1_access_mutex, K_MSEC(100)) != 0) {
        LOG_WRN("Unable to acquire VUART1 mutex in a timely manner! %s:%d\n", __FILE__, __LINE__);
        return -EAGAIN;
    }

    // Reset VUART1 ring buffer pointers
    vuart1_outgoing_buffer_read_pos = vuart1_outgoing_buffer_write_pos;
    vuart1_incoming_buffer_write_pos = vuart1_incoming_buffer_read_pos;

    host_console_service_task_requested = 1;
    host_console_service_task_shell = shell;

    k_mutex_unlock(&vuart1_access_mutex);

    return 0;
}
#else
int attach_to_host_console(void)
{
    uint8_t escape_sequence_state;
    uint8_t character;
    uint8_t character_read;

    // Deactivate interrupts on entering critical section
    int key = irq_lock();

    // Reset VUART1 FIFO pointers
    vuart1_incoming_interrupt_transient_buffer_pos = 0;
    vuart1_incoming_interrupt_transient_buffer_overflow = 0;
    vuart1_outgoing_buffer_read_pos = 0;
    vuart1_outgoing_buffer_write_pos = 0;
    vuart1_incoming_buffer_read_pos = 0;
    vuart1_incoming_buffer_write_pos = 0;

    // Re-activate interupts on exiting critical section
    irq_unlock(key);

    // Signal host console service is now active
    host_console_service_task_active = 1;

    // Enter polling loop
    escape_sequence_state = 0;

    while (1)
    {
        // Escape sequence handler
        character_read = 0;
        if (readchar_nonblock())
        {
            character = readchar();
            character_read = 1;
            switch (escape_sequence_state)
            {
                case 0:
                    if (character == '\n')
                    {
                        escape_sequence_state = 1;
                    }
                    break;
                case 1:
                    if (character == '~')
                    {
                        escape_sequence_state = 2;
                    }
                    else
                    {
                        escape_sequence_state = 0;
                    }
                    break;
                case 2:
                    if (character == '.')
                    {
                        escape_sequence_state = 3;
                    }
                    else
                    {
                        escape_sequence_state = 0;
                    }
                    break;
                default:
                    escape_sequence_state = 0;
            }
        }
        if (escape_sequence_state == 3)
        {
            break;
        }

        while (vuart1_outgoing_buffer_write_pos != vuart1_outgoing_buffer_read_pos)
        {
            uint32_t vuart1_status_register = *((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + AQUILA_LPC_VUART1_STATUS_REG));
            if (!((vuart1_status_register >> AQUILA_LPC_VUART_WFIFO_FULL_SHIFT) & AQUILA_LPC_VUART_WFIFO_FULL_MASK))
            {
                // VUART FIFO now has room, send queued character to VUART hardware
                *((volatile uint8_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + 0x0)) = vuart1_outgoing_buffer[vuart1_outgoing_buffer_read_pos];
                vuart1_outgoing_buffer_read_pos++;
                if (vuart1_outgoing_buffer_read_pos >= VUART_RING_BUFFER_SIZE)
                {
                    vuart1_outgoing_buffer_read_pos = 0;
                }
            }
            else
            {
                break;
            }
        }

        if (character_read)
        {
            // Attempt to send character to host
            uint32_t vuart1_status_register = *((volatile uint32_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + AQUILA_LPC_VUART1_STATUS_REG));
            if ((vuart1_status_register >> AQUILA_LPC_VUART_WFIFO_FULL_SHIFT) & AQUILA_LPC_VUART_WFIFO_FULL_MASK)
            {
                // VUART FIFO full, add to soft buffer
                vuart1_outgoing_buffer[vuart1_outgoing_buffer_write_pos] = character;
                vuart1_outgoing_buffer_write_pos++;
                if (vuart1_outgoing_buffer_write_pos >= VUART_RING_BUFFER_SIZE)
                {
                    vuart1_outgoing_buffer_write_pos = 0;
                }
            }
            else
            {
                // VUART FIFO still has room, send character to VUART hardware
                *((volatile uint8_t *)(HOSTLPCSLAVE_BASE + AQUILA_LPC_VUART_BLOCK_OFFSET + 0x0)) = character;
            }
        }

        // Send any queued VUART output from host to BMC console
        while (vuart1_incoming_buffer_write_pos != vuart1_incoming_buffer_read_pos)
        {
            printf("%c", vuart1_incoming_buffer[vuart1_incoming_buffer_read_pos]);
            vuart1_incoming_buffer_read_pos++;
            if (vuart1_incoming_buffer_read_pos >= VUART_RING_BUFFER_SIZE)
            {
                vuart1_incoming_buffer_read_pos = 0;
            }
        }

        primary_service_event_loop();
    }

    // Signal host console service is now inactive
    host_console_service_task_active = 0;

    return 0;
}
#endif

#if (!(WITH_ZEPHYR))
static uint64_t parse_user_provided_number(const char *string)
{
    if (((*(string + 0)) == '0') && (((*(string + 1)) == 'x') || ((*(string + 1)) == 'X')))
    {
        return strtoul(string, NULL, 16);
    }
    return strtoul(string, NULL, 10);
}

static uint8_t sanitize_ascii(uint8_t char_in)
{
    if ((char_in >= 32) && (char_in <= 126))
    {
        return char_in;
    }
    return '.';
}

static void console_service(void)
{
    char *str;
    char *token;
    uint64_t address;
    uint32_t data;
    unsigned int i;
    unsigned int length;

    str = readstr();
    if (str == NULL)
    {
        return;
    }
    token = get_token(&str);
    if (strcmp(token, "help") == 0)
    {
        help();
    }
    else if (strcmp(token, "reboot") == 0)
    {
        reboot();
    }
    else if (strcmp(token, "ipl") == 0)
    {
        start_ipl(0);
    }
    else if (strcmp(token, "sbe_status") == 0)
    {
        get_sbe_status();
    }
    else if (strcmp(token, "mr") == 0)
    {
        if (*str)
        {
            token = get_token(&str);
            address = parse_user_provided_number(token);
            if (*str)
            {
                token = get_token(&str);
                length = parse_user_provided_number(token);
            }
            else
            {
                length = 1;
            }
            for (i = 0; i < length; i++)
            {
                KESTREL_LOG("0x%08x: 0x%08x\t%02x%02x%02x%02x\t%c%c%c%c", address + (i * 4), *((volatile uint32_t *)(address + (i * 4))),
                       *((volatile uint8_t *)(address + (i * 4) + 0)), *((volatile uint8_t *)(address + (i * 4) + 1)),
                       *((volatile uint8_t *)(address + (i * 4) + 2)), *((volatile uint8_t *)(address + (i * 4) + 3)),
                       sanitize_ascii(*((volatile uint8_t *)(address + (i * 4) + 0))), sanitize_ascii(*((volatile uint8_t *)(address + (i * 4) + 1))),
                       sanitize_ascii(*((volatile uint8_t *)(address + (i * 4) + 2))), sanitize_ascii(*((volatile uint8_t *)(address + (i * 4) + 3))));
            }
        }
        else
        {
            KESTREL_LOG("USAGE: mr <memory address>");
        }
    }
    else if (strcmp(token, "mw") == 0)
    {
        if (*str)
        {
            token = get_token(&str);
            address = parse_user_provided_number(token);
            if (*str)
            {
                token = get_token(&str);
                length = parse_user_provided_number(token);
            }
            else
            {
                length = 1;
            }
            i = 0;
            while (*str)
            {
                token = get_token(&str);
                data = parse_user_provided_number(token);
                *((volatile uint32_t *)(address)) = data;
                i++;
                if (i >= length)
                {
                    break;
                }
            }
        }
        else
        {
            KESTREL_LOG("USAGE: mr <memory address>");
        }
    }
    else if (strcmp(token, "flash_write") == 0)
    {
        if (*str)
        {
            token = get_token(&str);
            if (strcmp(token, "enable") == 0)
            {
                allow_flash_write = 1;
                KESTREL_LOG("Flash write ENABLED");
            }
            else if (strcmp(token, "disable") == 0)
            {
                allow_flash_write = 0;
                KESTREL_LOG("Flash write DISABLED");
            }
            else
            {
                KESTREL_LOG("USAGE: flash_write <enable|disable>");
            }
        }
        else
        {
            KESTREL_LOG("USAGE: flash_write <enable|disable>");
        }
    }
    else if (strcmp(token, "chassison") == 0)
    {
        power_on_chassis(0);
    }
    else if (strcmp(token, "chassisoff") == 0)
    {
        power_off_chassis(0);
        KESTREL_LOG("Chassis power commanded OFF");
    }
    else if (strcmp(token, "status") == 0)
    {
        print_chassis_status();
    }
    else if (strcmp(token, "poweron") == 0)
    {
        if (power_on_host() == 0)
        {
            attach_to_host_console();
        }
        else
        {
            KESTREL_LOG("Host poweron procedure FAILED");
        }
    }
    else if (strcmp(token, "post_codes") == 0)
    {
        if (*str)
        {
            token = get_token(&str);
            if (strcmp(token, "enable") == 0)
            {
                enable_post_code_console_output = 1;
                KESTREL_LOG("POST code console output ENABLED");
            }
            else if (strcmp(token, "disable") == 0)
            {
                enable_post_code_console_output = 0;
                KESTREL_LOG("POST code console output DISABLED");
            }
            else
            {
                KESTREL_LOG("USAGE: post_codes <enable|disable>");
            }
        }
        else
        {
            KESTREL_LOG("USAGE: post_codes <enable|disable>");
        }
    }
    else if (strcmp(token, "console") == 0)
    {
        attach_to_host_console();
    }
    else if (strcmp(token, "") != 0)
    {
        KESTREL_LOG("%s: command not found", token);
    }
    prompt();
}
#endif

#if (WITH_SPI)
static int tercel_spi_flash_init(uintptr_t spi_ctl_cfgaddr, uintptr_t spi_ctl_baseaddr, int clock_divisor, int qspi_capable, int enable_4ba)
{
    int i;
    uint32_t dword;
    uint32_t flash_device_id;

    if ((read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_DEVICE_ID_HIGH) != TERCEL_SPI_DEVICE_ID_HIGH) ||
        (read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_DEVICE_ID_LOW) != TERCEL_SPI_DEVICE_ID_LOW))
    {
        return -1;
    }

    uint32_t tercel_version = read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_DEVICE_VERSION);
    KESTREL_LOG("Raptor Tercel SPI master found, device version %0d.%0d.%d", (tercel_version >> TERCEL_SPI_VERSION_MAJOR_SHIFT) & TERCEL_SPI_VERSION_MAJOR_MASK,
           (tercel_version >> TERCEL_SPI_VERSION_MINOR_SHIFT) & TERCEL_SPI_VERSION_MINOR_MASK,
           (tercel_version >> TERCEL_SPI_VERSION_PATCH_SHIFT) & TERCEL_SPI_VERSION_PATCH_MASK);

    flash_device_id = read_host_spi_flash_id(spi_ctl_cfgaddr, spi_ctl_baseaddr);
    for (i = 0; i < (sizeof(micron_n25q_spi_device_ids) / sizeof(micron_n25q_spi_device_ids[0])); i++)
    {
        if (flash_device_id == micron_n25q_spi_device_ids[i])
        {
            KESTREL_LOG("%s Flash device detected, configuring", micron_n25q_spi_device_names[i]);

            // Set up Flash-specific commands
            dword = 0;
            dword |= (MICRON_N25Q_SPI_4BA_QSPI_READ_CMD & TERCEL_SPI_4BA_QSPI_CMD_MASK) << TERCEL_SPI_4BA_QSPI_CMD_SHIFT;
            dword |= (MICRON_N25Q_SPI_3BA_QSPI_READ_CMD & TERCEL_SPI_3BA_QSPI_CMD_MASK) << TERCEL_SPI_3BA_QSPI_CMD_SHIFT;
            dword |= (MICRON_N25Q_SPI_4BA_SPI_READ_CMD & TERCEL_SPI_4BA_SPI_CMD_MASK) << TERCEL_SPI_4BA_SPI_CMD_SHIFT;
            dword |= (MICRON_N25Q_SPI_3BA_SPI_READ_CMD & TERCEL_SPI_3BA_SPI_CMD_MASK) << TERCEL_SPI_3BA_SPI_CMD_SHIFT;
            write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_FLASH_CFG1, dword);

            dword = 0;
            dword |= (MICRON_N25Q_SPI_4BA_QSPI_FAST_READ_CMD & TERCEL_SPI_4BA_QSPI_CMD_MASK) << TERCEL_SPI_4BA_QSPI_CMD_SHIFT;
            dword |= (MICRON_N25Q_SPI_3BA_QSPI_FAST_READ_CMD & TERCEL_SPI_3BA_QSPI_CMD_MASK) << TERCEL_SPI_3BA_QSPI_CMD_SHIFT;
            dword |= (MICRON_N25Q_SPI_4BA_SPI_FAST_READ_CMD & TERCEL_SPI_4BA_SPI_CMD_MASK) << TERCEL_SPI_4BA_SPI_CMD_SHIFT;
            dword |= (MICRON_N25Q_SPI_3BA_SPI_FAST_READ_CMD & TERCEL_SPI_3BA_SPI_CMD_MASK) << TERCEL_SPI_3BA_SPI_CMD_SHIFT;
            write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_FLASH_CFG2, dword);

            dword = 0;
            dword |= (MICRON_N25Q_SPI_4BA_QSPI_PAGE_PROGRAM_CMD & TERCEL_SPI_4BA_QSPI_CMD_MASK) << TERCEL_SPI_4BA_QSPI_CMD_SHIFT;
            dword |= (MICRON_N25Q_SPI_3BA_QSPI_PAGE_PROGRAM_CMD & TERCEL_SPI_3BA_QSPI_CMD_MASK) << TERCEL_SPI_3BA_QSPI_CMD_SHIFT;
            dword |= (MICRON_N25Q_SPI_4BA_SPI_PAGE_PROGRAM_CMD & TERCEL_SPI_4BA_SPI_CMD_MASK) << TERCEL_SPI_4BA_SPI_CMD_SHIFT;
            dword |= (MICRON_N25Q_SPI_3BA_SPI_PAGE_PROGRAM_CMD & TERCEL_SPI_3BA_SPI_CMD_MASK) << TERCEL_SPI_3BA_SPI_CMD_SHIFT;
            write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_FLASH_CFG3, dword);

            // Enable extended QSPI read/write operations
            dword = read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1);
            dword |= TERCEL_SPI_PHY_QSPI_EXT_READ_EN_MASK << TERCEL_SPI_PHY_QSPI_EXT_READ_EN_SHIFT;
            dword |= TERCEL_SPI_PHY_QSPI_EXT_WRITE_EN_MASK << TERCEL_SPI_PHY_QSPI_EXT_WRITE_EN_SHIFT;
            write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1, dword);

            // Set SPI fast read dummy cycles to
            // MICRON_N25Q_SPI_FAST_READ_DUMMY_CLOCK_CYCLES
            dword = read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1);
            dword &= ~(TERCEL_SPI_PHY_DUMMY_CYCLES_MASK << TERCEL_SPI_PHY_DUMMY_CYCLES_SHIFT);
            dword |= ((MICRON_N25Q_SPI_FAST_READ_DUMMY_CLOCK_CYCLES & TERCEL_SPI_PHY_DUMMY_CYCLES_MASK) << TERCEL_SPI_PHY_DUMMY_CYCLES_SHIFT);
            write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1, dword);

            // Enable SPI fast read functionality
            dword = read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1);
            dword &= ~(TERCEL_SPI_PHY_FAST_READ_ENABLE_MASK << TERCEL_SPI_PHY_FAST_READ_ENABLE_SHIFT);
            dword |= ((1 & TERCEL_SPI_PHY_FAST_READ_ENABLE_MASK) << TERCEL_SPI_PHY_FAST_READ_ENABLE_SHIFT);
            write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1, dword);

            break;
        }
    }

    // Set SPI core to automatic mode
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CORE_CTL1) &
                              ~(TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

    // Set extra CS delay cycle count to 0
    dword = read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1);
    dword &= ~(TERCEL_SPI_PHY_CS_EXTRA_IDLE_CYC_MASK << TERCEL_SPI_PHY_CS_EXTRA_IDLE_CYC_SHIFT);
    dword |= ((0 & TERCEL_SPI_PHY_CS_EXTRA_IDLE_CYC_MASK) << TERCEL_SPI_PHY_CS_EXTRA_IDLE_CYC_SHIFT);
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1, dword);

    // Set maximum CS assert cycle count to 10000
    dword = read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_FLASH_CFG4);
    dword &= ~(TERCEL_SPI_FLASH_CS_EN_LIMIT_CYC_MASK << TERCEL_SPI_FLASH_CS_EN_LIMIT_CYC_SHIFT);
    dword |= ((10000 & TERCEL_SPI_FLASH_CS_EN_LIMIT_CYC_MASK) << TERCEL_SPI_FLASH_CS_EN_LIMIT_CYC_SHIFT);
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_FLASH_CFG4, dword);

    if (enable_4ba)
    {
        // Set SPI controller to 4BA mode
        dword = read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1);
        dword &= ~(TERCEL_SPI_PHY_4BA_ENABLE_MASK << TERCEL_SPI_PHY_4BA_ENABLE_SHIFT);
        dword |= ((TERCEL_SPI_PHY_4BA_MODE & TERCEL_SPI_PHY_4BA_ENABLE_MASK) << TERCEL_SPI_PHY_4BA_ENABLE_SHIFT);
        write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1, dword);
    }

#if (ALLOW_SPI_QUAD_MODE)
    if (qspi_capable)
    {
        // Set SPI controller to QSPI mode
        dword = read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1);
        dword &= ~(TERCEL_SPI_PHY_IO_TYPE_MASK << TERCEL_SPI_PHY_IO_TYPE_SHIFT);
        dword |= ((TERCEL_SPI_PHY_IO_TYPE_QUAD & TERCEL_SPI_PHY_IO_TYPE_MASK) << TERCEL_SPI_PHY_IO_TYPE_SHIFT);
        write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1, dword);
    }
#endif

    // Set SPI clock cycle divider
    dword = read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1);
    dword &= ~(TERCEL_SPI_PHY_CLOCK_DIVISOR_MASK << TERCEL_SPI_PHY_CLOCK_DIVISOR_SHIFT);
    dword |= ((clock_divisor & TERCEL_SPI_PHY_CLOCK_DIVISOR_MASK) << TERCEL_SPI_PHY_CLOCK_DIVISOR_SHIFT);
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1, dword);

    // Calculate and dump configured SPI clock speed
    uint8_t spi_divisor =
        (read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1) >> TERCEL_SPI_PHY_CLOCK_DIVISOR_SHIFT) & TERCEL_SPI_PHY_CLOCK_DIVISOR_MASK;
    if (spi_divisor > 1)
    {
        spi_divisor = (spi_divisor - 1) * 2;
    }
    uint8_t spi_dummy_cycles =
        (read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_PHY_CFG1) >> TERCEL_SPI_PHY_DUMMY_CYCLES_SHIFT) & TERCEL_SPI_PHY_DUMMY_CYCLES_MASK;
    KESTREL_LOG("Flash controller frequency configured to %d MHz (bus frequency %d MHz, "
           "dummy cycles %d)",
           (read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CLK_FREQ) / spi_divisor) / 1000000,
           read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_CLK_FREQ) / 1000000, spi_dummy_cycles);

    // Enable read merging
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_FLASH_CFG5,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_FLASH_CFG5) |
                              (TERCEL_SPI_FLASH_EN_MULTCYC_READ_MASK << TERCEL_SPI_FLASH_EN_MULTCYC_READ_SHIFT));

    // Enable write merging
    write_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_FLASH_CFG5,
                          read_tercel_register(spi_ctl_cfgaddr, TERCEL_SPI_REG_SYS_FLASH_CFG5) |
                              (TERCEL_SPI_FLASH_EN_MULTCYC_WRITE_MASK << TERCEL_SPI_FLASH_EN_MULTCYC_WRITE_SHIFT));

    return 0;
}
#endif

#if (WITH_SPI)
#define SPI_READ_TRANSFER_SIZE (1 * 1024 * 1024LL)
#define SPI_WRITE_TRANSFER_SIZE (1 * 1024 * 1024LL)
int copy_spi_flash_to_internal_buffer(uintptr_t flash_data, uintptr_t flash_ctl, uint8_t *buffer)
{
    int retcode;
    int chunk;
    int redundancy_chunk;
    uintptr_t redundancy_buffer_offset;
#if (SPI_FLASH_TRIPLE_READ)
    uintptr_t buffer_check_offset;
    uint32_t value1;
    uint32_t value2;
    uint32_t value3;
    uint32_t final_value;
#endif

    KESTREL_LOG("Copying host Flash ROM (%p) to internal buffer (%p)...", (void*)flash_data, (void*)buffer);
    retcode = 0;

    for (chunk = 0; chunk < FLASH_SIZE_BYTES / SPI_READ_TRANSFER_SIZE; chunk++)
    {
#if (SPI_FLASH_TRIPLE_READ)
        for (redundancy_chunk = 0; redundancy_chunk < 3; redundancy_chunk++)
        {
#else
        for (redundancy_chunk = 0; redundancy_chunk < 1; redundancy_chunk++)
        {
#endif
            redundancy_buffer_offset = redundancy_chunk * SPI_READ_TRANSFER_SIZE;
            memcpy32((uint32_t *)(buffer + redundancy_buffer_offset + (chunk * SPI_READ_TRANSFER_SIZE)),
                     (uint32_t *)(flash_data + (chunk * SPI_READ_TRANSFER_SIZE)), SPI_READ_TRANSFER_SIZE / 4);
            KESTREL_LOG("\r[%d/%lld]", chunk + 1, FLASH_SIZE_BYTES / SPI_READ_TRANSFER_SIZE);

            // Reset ongoing multibyte access due to die switch requirements on the N25Q
            // Flash devices
            write_tercel_register(flash_ctl, TERCEL_SPI_REG_SYS_FLASH_CFG5,
                                  read_tercel_register(flash_ctl, TERCEL_SPI_REG_SYS_FLASH_CFG5) &
                                      ~(TERCEL_SPI_FLASH_EN_MULTCYC_READ_MASK << TERCEL_SPI_FLASH_EN_MULTCYC_READ_SHIFT));
            write_tercel_register(flash_ctl, TERCEL_SPI_REG_SYS_FLASH_CFG5,
                                  read_tercel_register(flash_ctl, TERCEL_SPI_REG_SYS_FLASH_CFG5) |
                                      (TERCEL_SPI_FLASH_EN_MULTCYC_READ_MASK << TERCEL_SPI_FLASH_EN_MULTCYC_READ_SHIFT));
        }
#if (SPI_FLASH_TRIPLE_READ)
        for (buffer_check_offset = 0; buffer_check_offset < SPI_READ_TRANSFER_SIZE; buffer_check_offset = buffer_check_offset + 4)
        {
            value1 = *((uint32_t *)(buffer + buffer_check_offset + (chunk * SPI_READ_TRANSFER_SIZE)));
            value2 = *((uint32_t *)(buffer + buffer_check_offset + SPI_READ_TRANSFER_SIZE + (chunk * SPI_READ_TRANSFER_SIZE)));
            value3 = *((uint32_t *)(buffer + buffer_check_offset + (2 * SPI_READ_TRANSFER_SIZE) + (chunk * SPI_READ_TRANSFER_SIZE)));
            if (!((value1 == value2) && (value1 == value3)))
            {
                KESTREL_LOG("[WARNING] Triple read FAILED integrity check at address 0x%08x!  Values 0x%08x/0x%08x/0x%08x",
                       buffer + buffer_check_offset + (chunk * SPI_READ_TRANSFER_SIZE), value1, value2, value3);
                if (value1 == value2)
                {
                    final_value = value1;
                }
                else if (value2 == value3)
                {
                    final_value = value2;
                }
                else if (value1 == value3)
                {
                    final_value = value1;
                }
                else
                {
                    KESTREL_LOG("[ERROR] UNCORRECTABLE data read at address 0x%08x!", buffer + buffer_check_offset + (chunk * SPI_READ_TRANSFER_SIZE));
                    final_value = 0xdeadbeef;
                    retcode = -1;
                }
                *((uint32_t *)(buffer + buffer_check_offset + (chunk * SPI_READ_TRANSFER_SIZE))) = final_value;
                if (retcode)
                {
                    // Fast abort on fatal error
                    break;
                }
            }
        }
#endif
    }
    KESTREL_LOG("\r%dMB copied", chunk);

    return retcode;
}
#endif

#if (WITH_ZEPHYR)
int write_flash_buffer_to_flash(const struct shell *shell)
#else
int write_flash_buffer_to_flash(void)
#endif
{
    int retcode;
    int chunk;
    int allow_flash_write_prev;

    if (main_firmware_buffer.locked)
    {
        return -2;
    }

#if (WITH_ZEPHYR)
    if (shell)
        shell_print(shell, "Writing host Flash ROM from internal buffer (%p):\n", (void*)main_firmware_buffer.buffer_address);
#else
    KESTREL_LOG("Writing host Flash ROM from internal buffer (%p)...", (void*)main_firmware_buffer.buffer_address);
#endif
    retcode = 0;

    allow_flash_write_prev = allow_flash_write;
    allow_flash_write = 1;

    for (chunk = 0; chunk < FLASH_SIZE_BYTES / SPI_WRITE_TRANSFER_SIZE; chunk++)
    {
        if (write_data_to_flash(HOSTSPIFLASHCFG_BASE, HOSTSPIFLASH_BASE, main_firmware_buffer.buffer_address + (chunk * SPI_WRITE_TRANSFER_SIZE), SPI_WRITE_TRANSFER_SIZE, chunk * SPI_WRITE_TRANSFER_SIZE, 1))
        {
            // Fast abort on fatal error
#if (WITH_ZEPHYR)
                if (shell)
                    shell_print(shell, "FAILED!\n");
#else
            KESTREL_LOG("\rFAILED!\n");
#endif
            retcode = -1;
            break;
        }
#if (WITH_ZEPHYR)
        if (shell)
            shell_print(shell, "[%d/%lld]", chunk + 1, FLASH_SIZE_BYTES / SPI_WRITE_TRANSFER_SIZE);
#else
        KESTREL_LOG("\r[%d/%lld]", chunk + 1, FLASH_SIZE_BYTES / SPI_WRITE_TRANSFER_SIZE);
#endif
    }

    allow_flash_write = allow_flash_write_prev;

    // Verify write
#if (WITH_ZEPHYR)
    if (shell)
        shell_print(shell, "Verifying host Flash ROM...\n");
#else
    KESTREL_LOG("Verifying host Flash ROM...");
#endif
    for (chunk = 0; chunk < FLASH_SIZE_BYTES / SPI_READ_TRANSFER_SIZE; chunk++)
    {
        if (memcmp((uint8_t*)((uintptr_t)HOSTSPIFLASH_BASE + (chunk * SPI_READ_TRANSFER_SIZE)), main_firmware_buffer.buffer_address + (chunk * SPI_READ_TRANSFER_SIZE), SPI_READ_TRANSFER_SIZE) != 0)
        {
            // Fast abort on fatal error
#if (WITH_ZEPHYR)
            if (shell)
                shell_print(shell, "FAILED!\n");
#else
            KESTREL_LOG("\rFAILED!\n");
#endif
            retcode = -1;
            break;
        }
#if (WITH_ZEPHYR)
        if (shell)
            shell_print(shell, "[%d/%lld]\n", chunk + 1, FLASH_SIZE_BYTES / SPI_READ_TRANSFER_SIZE);
#else
        KESTREL_LOG("\r[%d/%lld]", chunk + 1, FLASH_SIZE_BYTES / SPI_READ_TRANSFER_SIZE);
#endif
    }

    return retcode;
}

#if !(WITH_ZEPHYR)
static void scan_onewire_devices(uint8_t* base_addr)
{
    onewire_device_list_t detected_onewire_devices = {0};
    int i;
    int sensor;
    int sensor_present;

    scan_onewire_bus(base_addr, detected_onewire_devices.devices, &detected_onewire_devices.device_count, sizeof(detected_onewire_devices.devices) / sizeof(detected_onewire_devices.devices[0]));

    KESTREL_LOG("Detected %d 1-wire devices", detected_onewire_devices.device_count);

    // For now, assume all detected devices with the temperature sensor family ID are temperature sensors that we want to use
    for (i = 0; i < detected_onewire_devices.device_count; i++)
    {
        if ((detected_onewire_devices.devices[i] & 0xff) != ONEWIRE_DEVICE_FAMILY_TEMPERATURE_SENSOR)
        {
            continue;
        }
        // Is sensor already present?
        sensor_present = 0;
        for (sensor = 0; sensor < kestrel_temp_sensor_count; sensor++)
        {
            if (kestrel_temp_sensor_data[sensor].fru_type == FRU_TYPE_THERMAL_ONEWIRE)
            {
                if (kestrel_temp_sensor_data[sensor].device_handle == detected_onewire_devices.devices[i])
                {
                    sensor_present = 1;
                    break;
                }
            }
        }
        if (sensor_present)
        {
            continue;
        }
        kestrel_temp_sensor_data[kestrel_temp_sensor_count].sensor_id = 0x100 | (i & 0xff);
        kestrel_temp_sensor_data[kestrel_temp_sensor_count].fru_type = FRU_TYPE_THERMAL_ONEWIRE;
        kestrel_temp_sensor_data[kestrel_temp_sensor_count].device_handle = detected_onewire_devices.devices[i];
        kestrel_temp_sensor_data[kestrel_temp_sensor_count].description[0] = 0x0;

        KESTREL_LOG("Found new temperature sensor with serial number 0x%016llx, enabling as ID 0x%08x",
                      kestrel_temp_sensor_data[kestrel_temp_sensor_count].device_handle,
                      kestrel_temp_sensor_data[kestrel_temp_sensor_count].sensor_id);
        kestrel_temp_sensor_count++;
    }
}
#endif

int kestrel_init(void)
{
    uint32_t dword;
    int i;

    // Disable PNOR RAM cache
    // This favors BMC startup speed over host IPL speed
    hiomap_use_direct_access = 1;
    allow_flash_write = 1;

    gpio_init();

    display_character('0', 0); // STATUS CODE: 0

#if (WITH_ZEPHYR)
    // Initialize mutexes
    k_mutex_init(&vuart1_access_mutex);
    k_mutex_init(&occ_access_mutex);

    // Initialize semaphores
    k_sem_init(&chassis_control_semaphore, 0, K_SEM_MAX_LIMIT);
#endif

#if !(WITH_ZEPHYR)
    for (i = 0; i < MAX_CPUS_SUPPORTED; i++)
    {
        initialize_i2c_master(g_cpu_info[i].i2c_master, g_cpu_info[i].i2c_frequency);
    }
    if (HOST_TYPE_BLACKBIRD)
    {
#ifdef HHDT_I2C_MASTER_BASE_ADDR
        initialize_i2c_master((uint8_t*)HHDT_I2C_MASTER_BASE_ADDR, 100000);
#endif
    }
    initialize_i2c_master((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, 100000);
#endif

#if !(WITH_ZEPHYR)
#ifdef ONEWIRE0_BASE
    initialize_onewire_master((uint8_t*)ONEWIRE0_BASE);
    if (ENABLE_ONEWIRE) {
        scan_onewire_devices((uint8_t*)ONEWIRE0_BASE);
    }
#endif
#endif

#ifdef SIMPLEPWM_BASE
    initialize_pwm_controller((uint8_t*)SIMPLEPWM_BASE, -1);
#endif
#ifdef SIMPLEPWM2_BASE
    initialize_pwm_controller((uint8_t*)SIMPLEPWM2_BASE, -1);
#endif

    // Check for Aquila core presence
    if ((read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DEVICE_ID_HIGH) == AQUILA_LPC_DEVICE_ID_HIGH) &&
        (read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DEVICE_ID_LOW) == AQUILA_LPC_DEVICE_ID_LOW))
    {
        uint32_t aquila_version = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_DEVICE_VERSION);
        KESTREL_LOG("Raptor Aquila LPC slave found, device version %0d.%0d.%d", (aquila_version >> AQUILA_LPC_VERSION_MAJOR_SHIFT) & AQUILA_LPC_VERSION_MAJOR_MASK,
               (aquila_version >> AQUILA_LPC_VERSION_MINOR_SHIFT) & AQUILA_LPC_VERSION_MINOR_MASK,
               (aquila_version >> AQUILA_LPC_VERSION_PATCH_SHIFT) & AQUILA_LPC_VERSION_PATCH_MASK);

        // Configure Aquila core to intercept I/O port ranges 0x80-0x82 and
        // 0x3f8-0x3ff 0x80
        dword = 0;
        dword |= ((0x82 & AQUILA_LPC_RANGE_END_ADDR_MASK) << AQUILA_LPC_RANGE_END_ADDR_SHIFT);
        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_RANGE1_END, dword);
        dword = 0;
        dword |= ((0x80 & AQUILA_LPC_RANGE_START_ADDR_MASK) << AQUILA_LPC_RANGE_START_ADDR_SHIFT);
        dword |= ((1 & AQUILA_LPC_RANGE_ALLOW_IO_MASK) << AQUILA_LPC_RANGE_ALLOW_IO_SHIFT);
        dword |= ((1 & AQUILA_LPC_RANGE_ENABLE_MASK) << AQUILA_LPC_RANGE_ENABLE_SHIFT);
        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_RANGE1_CONFIG, dword);

        // Enable I/O cycle intercept
        dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
        dword |= ((1 & AQUILA_LPC_CTL_EN_IO_CYCLES_MASK) << AQUILA_LPC_CTL_EN_IO_CYCLES_SHIFT);
        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);

        // Enable VUART1
        dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
        dword |= ((1 & AQUILA_LPC_CTL_EN_VUART1_MASK) << AQUILA_LPC_CTL_EN_VUART1_SHIFT);
        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);

        // Enable IPMI BT functionality
        dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
        dword |= ((1 & AQUILA_LPC_CTL_EN_IPMI_BT_MASK) << AQUILA_LPC_CTL_EN_IPMI_BT_SHIFT);
        dword &= ~((AQUILA_LPC_CTL_IPMI_BT_ADDR_MASK) << AQUILA_LPC_CTL_IPMI_BT_ADDR_SHIFT);
        dword |= ((0xe4 & AQUILA_LPC_CTL_IPMI_BT_ADDR_MASK) << AQUILA_LPC_CTL_IPMI_BT_ADDR_SHIFT);
        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);

        // Enable firmware cycle intercept
        dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
        dword |= ((1 & AQUILA_LPC_CTL_EN_FW_CYCLES_MASK) << AQUILA_LPC_CTL_EN_FW_CYCLES_SHIFT);
        write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);

        // Certain platforms contain a clock buffer on the LPC bus
        // If this buffer is present, it inserts a ~5ns delay that the Aquila core
        // can compensate for using its internal PLL
        if (HOST_HAS_LPC_CLOCK_BUFFER) {
            // Enable LPC clock buffer delay compensation
            KESTREL_LOG("Enabling compensation for LPC clock buffer insertion delay...");
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
            dword |= ((1 & AQUILA_LPC_CLK_PHASE_SEL_MASK) << AQUILA_LPC_CLK_PHASE_SEL_SHIFT);
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);
        }
        else {
            // Disable LPC clock buffer delay compensation
            dword = read_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1);
            dword &= ~((AQUILA_LPC_CLK_PHASE_SEL_MASK) << AQUILA_LPC_CLK_PHASE_SEL_SHIFT);
            write_aquila_register(HOSTLPCSLAVE_BASE, AQUILA_LPC_REG_CONTROL1, dword);
        }

#if (WITH_ZEPHYR)
        // Connect interrupt handler to interrupt
        IRQ_CONNECT(HOSTLPCSLAVE_INTERRUPT, 0x80, lpc_slave_isr, NULL, 0);
#endif
    }

    // Set up display output
    configure_hdmi_transceiver((uint8_t *)HDMI_I2C_MASTER_BASE_ADDR);

    main_firmware_buffer.locked = 1;
    main_firmware_buffer.buffer_length = (64 + 2) * 1024LL * 1024LL;
#if (WITH_ZEPHYR)
    main_firmware_buffer.buffer_address = malloc(main_firmware_buffer.buffer_length);
#else
    main_firmware_buffer.buffer_address = (uint8_t *)(MAIN_RAM_BASE + 0x3e00000);
#endif
    if (!main_firmware_buffer.buffer_address) {
        KESTREL_LOG("[ERROR] Unable to allocate host Flash ROM buffer, ABORTING");
        return -1;
    }
    host_flash_buffer = main_firmware_buffer.buffer_address;

    if (CLEAR_FLASH_CACHE_BUFFER_ON_STARTUP) {
        // Clear SPI Flash buffer
        KESTREL_LOG("Clearing host ROM internal buffer...");
        memset32((uint32_t *)host_flash_buffer, 0xdeadbeef, ((64 + 2) * 1024LL * 1024LL) / 4);
        KESTREL_LOG("\rInternal host ROM buffer cleared    ");
    }
    main_firmware_buffer.locked = 0;

    display_character('1', 0); // STATUS CODE: 1

#if (WITH_SPI)
    // Initialize FPGA Flash controller
    tercel_spi_flash_init(FPGASPIFLASHCFG_BASE, FPGASPIFLASH_BASE, 1, 0, 0);

    // Detect and print attached host SPI Flash ID
    KESTREL_LOG("FPGA SPI flash ID: 0x%08x", read_host_spi_flash_id(FPGASPIFLASHCFG_BASE, FPGASPIFLASH_BASE));

    reset_flash_device(FPGASPIFLASHCFG_BASE, FPGASPIFLASH_BASE);
    configure_flash_device(FPGASPIFLASHCFG_BASE, FPGASPIFLASH_BASE);

    // Initialize BMC Flash controller
    tercel_spi_flash_init(BMCSPIFLASHCFG_BASE, BMCSPIFLASH_BASE, 1, BMC_SPI_FLASH_QSPI_COMPATIBLE, 0);

    // Detect and print attached host SPI Flash ID
    KESTREL_LOG("BMC SPI flash ID: 0x%08x", read_host_spi_flash_id(BMCSPIFLASHCFG_BASE, BMCSPIFLASH_BASE));

    reset_flash_device(BMCSPIFLASHCFG_BASE, BMCSPIFLASH_BASE);
    configure_flash_device(BMCSPIFLASHCFG_BASE, BMCSPIFLASH_BASE);

    // Initialize host Flash controller
    tercel_spi_flash_init(HOSTSPIFLASHCFG_BASE, HOSTSPIFLASH_BASE, 1, HOST_SPI_FLASH_QSPI_COMPATIBLE, 1);

    // Detect and print attached host SPI Flash ID
    KESTREL_LOG("Host SPI flash ID: 0x%08x", read_host_spi_flash_id(HOSTSPIFLASHCFG_BASE, HOSTSPIFLASH_BASE));

    reset_flash_device(HOSTSPIFLASHCFG_BASE, HOSTSPIFLASH_BASE);
    configure_flash_device(HOSTSPIFLASHCFG_BASE, HOSTSPIFLASH_BASE);

    if (!hiomap_use_direct_access)
    {
        // Copy external SPI Flash ROM contents to internal host SPI Flash ROM buffer
        copy_spi_flash_to_internal_buffer(HOSTSPIFLASH_BASE, HOSTSPIFLASHCFG_BASE, host_flash_buffer);
    }

#if (DEBUG_HOST_SPI_FLASH_READ)
    printk("host_flash_buffer: %p First 1KB:\n", host_flash_buffer);
    int debug_byte = 0;
    int row = 0;
    for (row = 0; row < 32; row++)
    {
        for (debug_byte = 0; debug_byte < 32; debug_byte++)
        {
            printk("%02x ", host_flash_buffer[debug_byte + (row * 32)]);
        }
        printk("\n");
    }

    KESTREL_LOG_IMMEDIATE("[1/5] CRC of first 64B: %08x\n", crc32(host_flash_buffer, 1 * 64LL));
    KESTREL_LOG_IMMEDIATE("[2/5] CRC of first 1KB: %08x (next %08x, next %08x)\n", crc32(host_flash_buffer, 1024LL), crc32(host_flash_buffer + 1024LL, 1024LL),
           crc32(host_flash_buffer + (2 * 1024LL), 1024LL));
    KESTREL_LOG_IMMEDIATE("[3/5] CRC of first 1KB: %08x (next %08x, next %08x)\n", crc32(host_flash_buffer, 1024LL), crc32(host_flash_buffer + 1024LL, 1024LL),
           crc32(host_flash_buffer + (2 * 1024LL), 1024LL));
    KESTREL_LOG_IMMEDIATE("[4/5] CRC of first 1MB: %08x\n", crc32(host_flash_buffer, 1 * 1024 * 1024LL));
    KESTREL_LOG_IMMEDIATE("[5/5] CRC of full 64MB: %08x\n", crc32(host_flash_buffer, 64 * 1024 * 1024LL));

    // HBBL on test sytem is from 0x206200 to 0x207388 inclusive
    printk("\nHBBL region:\n");
    for (row = 0; row < 141; row++)
    {
        for (debug_byte = 0; debug_byte < 32; debug_byte++)
        {
            printk("%02x ", *(host_flash_buffer + 0x206200ULL + (debug_byte + (row * 32))));
            // printk("%02x ", *(host_flash_buffer + 0x1000ULL + (debug_byte + (row *
            // 32))));
        }
        printk("\n");
    }
    KESTREL_LOG_IMMEDIATE("CRC of HBBL region: %08x\n", crc32(host_flash_buffer + 0x206200ULL, 4488));
#endif
#endif

    // Ensure internal host power status matches platform status
    uint8_t byte;
    int i2c_read_retcode;
    byte = i2c_read_register_byte((uint8_t *)P9PS_I2C_MASTER_BASE_ADDR, HOST_PLATFORM_FPGA_I2C_ADDRESS, HOST_PLATFORM_FPGA_I2C_REG_STATUS, &i2c_read_retcode);
    if (i2c_read_retcode || (((byte)&0x03) != 0x03))
    {
        host_power_status = HOST_POWER_STATUS_OFFLINE;
    }
    else
    {
        run_pre_ipl_bmc_peripheral_setup();
        start_lpc_clock();
        host_power_status = HOST_POWER_STATUS_RUNNING;
    }
    host_power_status_changed();

    // Reset POST codes and display
    post_code_high = 0;
    post_code_low = 0;
    host_active_post_code = 0x0000;
    display_post_code(host_active_post_code);

    // BMC core initialization complete
    display_character('2', 0); // STATUS CODE: 2

    kestrel_basic_init_complete = 1;
    k_sem_give(&chassis_control_semaphore);

    return 0;
}
