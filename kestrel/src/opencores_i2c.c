// © 2020 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(kestrel_i2c, LOG_LEVEL_DBG);

#include "opencores_i2c.h"

#include "utility.h"

#include <generated/csr.h>
#include <generated/soc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define KESTREL_LOG(...) LOG_INF(__VA_ARGS__)

int initialize_i2c_master(uint8_t *base_address, int i2c_bus_frequency)
{
    KESTREL_LOG("Configuring I2C master at address %p...", (void*)base_address);

    if ((*((volatile uint32_t *)(base_address + OPENCORES_I2C_MASTER_DEVICE_ID_HIGH)) != OPENCORES_I2C_DEVICE_ID_HIGH) ||
        (*((volatile uint32_t *)(base_address + OPENCORES_I2C_MASTER_DEVICE_ID_LOW)) != OPENCORES_I2C_DEVICE_ID_LOW))
    {
        return -1;
    }
    uint32_t opencores_spi_version = *((volatile uint32_t *)(base_address + OPENCORES_I2C_MASTER_DEVICE_VERSION));
    KESTREL_LOG("OpenCores I2C master found, device version %0d.%0d.%d",
           (opencores_spi_version >> OPENCORES_I2C_VERSION_MAJOR_SHIFT) & OPENCORES_I2C_VERSION_MAJOR_MASK,
           (opencores_spi_version >> OPENCORES_I2C_VERSION_MINOR_SHIFT) & OPENCORES_I2C_VERSION_MINOR_MASK,
           (opencores_spi_version >> OPENCORES_I2C_VERSION_PATCH_SHIFT) & OPENCORES_I2C_VERSION_PATCH_MASK);

    // Compute prescale value from system clock and desired I2C frequency in HZ
    uint16_t i2c_prescale = (CONFIG_CLOCK_FREQUENCY / (5LL * i2c_bus_frequency)) - 1;
    KESTREL_LOG("Desired prescale register: 0x%04x (system clock %dMHz, bus frequency "
           "%dkHz)",
           i2c_prescale, CONFIG_CLOCK_FREQUENCY / 1000000LL, i2c_bus_frequency / 1000LL);
    *((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_PRESCALE_LOW)) = i2c_prescale & 0xff;
    *((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_PRESCALE_HIGH)) = (i2c_prescale >> 8) & 0xff;
    if ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_PRESCALE_LOW)) == (i2c_prescale & 0xff)) &&
        (*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_PRESCALE_HIGH)) == ((i2c_prescale >> 8) & 0xff)))
    {
        KESTREL_LOG("Enabling I2C core", i2c_prescale);
        *((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_PRESCALE_CTL)) =
            (OPENCORES_I2C_MASTER_CTL_CORE_EN_MASK << OPENCORES_I2C_MASTER_CTL_CORE_EN_SHIFT);

        return 0;
    }

    return 1;
}

int write_i2c_data(uint8_t *base_address, uint8_t slave_address, uint8_t *data, int data_length, uint8_t send_stop_signal)
{
    uint32_t i2c_op_timeout_counter;
    uint8_t i2c_op_failed;
    int active_byte;
    uint8_t byte;

    i2c_op_failed = 0;
    *((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_TX_RX)) =
        (slave_address << 1) | (OPENCORES_I2C_MASTER_TX_RX_WRITE_MASK << OPENCORES_I2C_MASTER_TX_RX_WRITE_SHIFT);
    *((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) =
        (OPENCORES_I2C_MASTER_CMD_STA_MASK << OPENCORES_I2C_MASTER_CMD_STA_SHIFT) | (OPENCORES_I2C_MASTER_CMD_WR_MASK << OPENCORES_I2C_MASTER_CMD_WR_SHIFT);
    i2c_op_timeout_counter = 0;
    while ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) >> OPENCORES_I2C_MASTER_STATUS_TIP_SHIFT) &
           OPENCORES_I2C_MASTER_STATUS_TIP_MASK)
    {
        if (i2c_op_timeout_counter > I2C_MASTER_OPERATION_TIMEOUT_VALUE)
        {
            KESTREL_LOG("[WARNING] I2C operation timed out in device select!");
            i2c_op_failed = 1;
            break;
        }
        usleep(100);
        i2c_op_timeout_counter++;
    }

    if (!i2c_op_failed)
    {
        if ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) >> OPENCORES_I2C_MASTER_STATUS_RXACK_SHIFT) &
            OPENCORES_I2C_MASTER_STATUS_RXACK_MASK)
        {
            KESTREL_LOG("[WARNING] I2C operation failed in device select!");
            i2c_op_failed = 1;
        }
    }

    for (active_byte = 0; active_byte < data_length; active_byte++)
    {
        if (!i2c_op_failed)
        {
            *((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_TX_RX)) = *(data + active_byte);
            byte = OPENCORES_I2C_MASTER_CMD_WR_MASK << OPENCORES_I2C_MASTER_CMD_WR_SHIFT;
            if ((active_byte + 1) == data_length)
            {
                // Final byte
                if (send_stop_signal)
                {
                    byte |= OPENCORES_I2C_MASTER_CMD_STO_MASK << OPENCORES_I2C_MASTER_CMD_STO_SHIFT;
                }
            }
            *((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) = byte;
            i2c_op_timeout_counter = 0;
            while ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) >> OPENCORES_I2C_MASTER_STATUS_TIP_SHIFT) &
                   OPENCORES_I2C_MASTER_STATUS_TIP_MASK)
            {
                if (i2c_op_timeout_counter > I2C_MASTER_OPERATION_TIMEOUT_VALUE)
                {
                    KESTREL_LOG("[WARNING] I2C operation timed out in register write!");
                    i2c_op_failed = 1;
                    break;
                }
                usleep(100);
                i2c_op_timeout_counter++;
            }
        }

        if (!i2c_op_failed)
        {
            if ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) >> OPENCORES_I2C_MASTER_STATUS_RXACK_SHIFT) &
                OPENCORES_I2C_MASTER_STATUS_RXACK_MASK)
            {
                KESTREL_LOG("[WARNING] I2C operation failed in register write!");
                i2c_op_failed = 1;
            }
        }

        if (i2c_op_failed)
        {
            break;
        }
    }

    return i2c_op_failed;
}

int read_i2c_data(uint8_t *base_address, uint8_t slave_address, uint8_t *data, int *data_length, int max_data_length, uint8_t send_stop_signal)
{
    uint32_t i2c_op_timeout_counter;
    uint8_t i2c_op_failed;
    int active_byte;
    uint8_t byte;

    i2c_op_failed = 0;
    *((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_TX_RX)) =
        (slave_address << 1) | (OPENCORES_I2C_MASTER_TX_RX_READ_MASK << OPENCORES_I2C_MASTER_TX_RX_READ_SHIFT);
    *((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) =
        (OPENCORES_I2C_MASTER_CMD_STA_MASK << OPENCORES_I2C_MASTER_CMD_STA_SHIFT) | (OPENCORES_I2C_MASTER_CMD_WR_MASK << OPENCORES_I2C_MASTER_CMD_WR_SHIFT);
    i2c_op_timeout_counter = 0;
    while ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) >> OPENCORES_I2C_MASTER_STATUS_TIP_SHIFT) &
           OPENCORES_I2C_MASTER_STATUS_TIP_MASK)
    {
        if (i2c_op_timeout_counter > I2C_MASTER_OPERATION_TIMEOUT_VALUE)
        {
            KESTREL_LOG("[WARNING] I2C operation timed out in device select!");
            i2c_op_failed = 1;
            break;
        }
        usleep(100);
        i2c_op_timeout_counter++;
    }

    if (!i2c_op_failed)
    {
        if ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) >> OPENCORES_I2C_MASTER_STATUS_RXACK_SHIFT) &
            OPENCORES_I2C_MASTER_STATUS_RXACK_MASK)
        {
            KESTREL_LOG("[WARNING] I2C operation failed in device select!");
            i2c_op_failed = 1;
        }
    }

    if (data_length)
    {
        *data_length = 0;
    }
    for (active_byte = 0; active_byte < max_data_length; active_byte++)
    {
        if (!i2c_op_failed)
        {
            if ((active_byte + 1) == max_data_length)
            {
                // Final byte, send NACK
                byte = (OPENCORES_I2C_MASTER_CMD_RD_MASK << OPENCORES_I2C_MASTER_CMD_RD_SHIFT) |
                       (OPENCORES_I2C_MASTER_CMD_NACK_MASK << OPENCORES_I2C_MASTER_CMD_NACK_SHIFT);
                if (send_stop_signal)
                {
                    byte |= OPENCORES_I2C_MASTER_CMD_STO_MASK << OPENCORES_I2C_MASTER_CMD_STO_SHIFT;
                }
                *((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) = byte;
            }
            else
            {
                // More bytes expected, send ACK
                *((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) =
                    (OPENCORES_I2C_MASTER_CMD_RD_MASK << OPENCORES_I2C_MASTER_CMD_RD_SHIFT) |
                    (OPENCORES_I2C_MASTER_CMD_ACK_MASK << OPENCORES_I2C_MASTER_CMD_ACK_SHIFT);
            }
            while ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) >> OPENCORES_I2C_MASTER_STATUS_TIP_SHIFT) &
                   OPENCORES_I2C_MASTER_STATUS_TIP_MASK)
            {
                if (i2c_op_timeout_counter > I2C_MASTER_OPERATION_TIMEOUT_VALUE)
                {
                    KESTREL_LOG("[WARNING] I2C operation timed out in register read!");
                    i2c_op_failed = 1;
                    break;
                }
                usleep(100);
                i2c_op_timeout_counter++;
            }
            if (!i2c_op_failed)
            {
                *(data + active_byte) = *((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_TX_RX));
                if (data_length)
                {
                    *data_length = *data_length + 1;
                }
            }
        }

        if (i2c_op_failed)
        {
            break;
        }
    }

    return i2c_op_failed;
}

uint8_t i2c_read_register_byte(uint8_t *base_address, uint8_t slave_address, uint8_t slave_register, int *error)
{
    uint8_t retval = 0xff;
    uint8_t byte;

    if (error)
    {
        *error = -1;
    }

    if (!write_i2c_data(base_address, slave_address, &slave_register, 1, 0))
    {
        if (!read_i2c_data(base_address, slave_address, &byte, NULL, 1, 1))
        {
            retval = byte;

            if (error)
            {
                *error = 0;
            }
        }
    }

    return retval;
}

uint16_t i2c_read_register_word(uint8_t *base_address, uint8_t slave_address, uint8_t slave_register, int *error)
{
    uint16_t retval = 0xffff;
    uint8_t byte[2];

    if (error)
    {
        *error = -1;
    }

    if (!write_i2c_data(base_address, slave_address, &slave_register, 1, 0))
    {
        if (!read_i2c_data(base_address, slave_address, &byte, NULL, 2, 1))
        {
            retval = byte[0] << 8;
            retval |= byte[1];
            if (error)
            {
                *error = 0;
            }
        }
    }

    return retval;
}

int i2c_write_register_byte(uint8_t *base_address, uint8_t slave_address, uint8_t slave_register, uint8_t data)
{
    uint8_t tx_data[2];

    // Assemble TX data structure
    tx_data[0] = slave_register;
    tx_data[1] = data;

    if (!write_i2c_data(base_address, slave_address, tx_data, 2, 1))
    {
        return 0;
    }

    return 1;
}

int i2c_write_register_word(uint8_t *base_address, uint8_t slave_address, uint8_t slave_register, uint16_t data)
{
    uint8_t tx_data[3];

    // Assemble TX data structure
    tx_data[0] = slave_register;
    tx_data[1] = (data >> 16) & 0xff;
    tx_data[2] = data & 0xff;

    if (!write_i2c_data(base_address, slave_address, tx_data, 3, 1))
    {
        return 0;
    }

    return 1;
}
