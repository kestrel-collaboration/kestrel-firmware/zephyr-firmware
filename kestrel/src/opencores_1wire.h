// © 2020 - 2022 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

#ifndef _OPENCORES_1WIRE_H
#define _OPENCORES_1WIRE_H

#include <stdint.h>

#define OPENCORES_1WIRE_MASTER_DEVICE_ID_LOW       0x0
#define OPENCORES_1WIRE_MASTER_DEVICE_ID_HIGH      0x4
#define OPENCORES_1WIRE_MASTER_DEVICE_VERSION      0x8
#define OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA     0x10
#define OPENCORES_1WIRE_MASTER_BUS_RDT_PWR_SET     0x11
#define OPENCORES_1WIRE_MASTER_CLK_DIV_NORMAL_LOW  0x12
#define OPENCORES_1WIRE_MASTER_CLK_DIV_NORMAL_HIGH 0x13
#define OPENCORES_1WIRE_MASTER_CLK_DIV_OVERDR_LOW  0x14
#define OPENCORES_1WIRE_MASTER_CLK_DIV_OVERDR_HIGH 0x15

#define OPENCORES_1WIRE_DEVICE_ID_HIGH 0x4f4e574d
#define OPENCORES_1WIRE_DEVICE_ID_LOW  0x4f504e43

#define OPENCORES_1WIRE_VERSION_MAJOR_MASK  0xffff
#define OPENCORES_1WIRE_VERSION_MAJOR_SHIFT 16
#define OPENCORES_1WIRE_VERSION_MINOR_MASK  0xff
#define OPENCORES_1WIRE_VERSION_MINOR_SHIFT 8
#define OPENCORES_1WIRE_VERSION_PATCH_MASK  0xff
#define OPENCORES_1WIRE_VERSION_PATCH_SHIFT 0

#define OPENCORES_1WIRE_MASTER_CTL_DAT_TX_REQ_MASK    0x1
#define OPENCORES_1WIRE_MASTER_CTL_DAT_TX_REQ_SHIFT   0
#define OPENCORES_1WIRE_MASTER_CTL_DAT_RX_STA_MASK    0x1
#define OPENCORES_1WIRE_MASTER_CTL_DAT_RX_STA_SHIFT   0
#define OPENCORES_1WIRE_MASTER_CTL_RST_REQ_MASK       0x1
#define OPENCORES_1WIRE_MASTER_CTL_RST_REQ_SHIFT      1
#define OPENCORES_1WIRE_MASTER_CTL_OVERDR_EN_MASK     0x1
#define OPENCORES_1WIRE_MASTER_CTL_OVERDR_EN_SHIFT    2
#define OPENCORES_1WIRE_MASTER_CTL_CYCLE_START_MASK   0x1
#define OPENCORES_1WIRE_MASTER_CTL_CYCLE_START_SHIFT  3
#define OPENCORES_1WIRE_MASTER_CTL_CYCLE_STATUS_MASK  0x1
#define OPENCORES_1WIRE_MASTER_CTL_CYCLE_STATUS_SHIFT 3
#define OPENCORES_1WIRE_MASTER_CTL_POWER_EN_MASK      0x1
#define OPENCORES_1WIRE_MASTER_CTL_POWER_EN_SHIFT     4
#define OPENCORES_1WIRE_MASTER_CTL_IRQ_STA_MASK       0x1
#define OPENCORES_1WIRE_MASTER_CTL_IRQ_STA_SHIFT      6
#define OPENCORES_1WIRE_MASTER_CTL_IRQ_EN_MASK        0x1
#define OPENCORES_1WIRE_MASTER_CTL_IRQ_EN_SHIFT       7

#define ONEWIRE_MASTER_OPERATION_TIMEOUT_VALUE 50

int initialize_onewire_master(uint8_t *base_address);
int issue_onewire_reset(uint8_t *base_address, uint8_t enable_overdrive, uint8_t *presence_detect);
int issue_onewire_delay(uint8_t *base_address, uint8_t enable_overdrive, uint8_t enable_power);
int transfer_onewire_bit(uint8_t *base_address, uint8_t enable_overdrive, uint8_t tx_data, uint8_t *rx_data);
int transfer_onewire_byte(uint8_t *base_address, uint8_t enable_overdrive, uint8_t tx_data, uint8_t *rx_data);
int transfer_onewire_data_block(uint8_t *base_address, uint8_t enable_overdrive, uint8_t *tx_data, uint8_t *rx_data, int count);

#endif // _OPENCORES_1WIRE_H
