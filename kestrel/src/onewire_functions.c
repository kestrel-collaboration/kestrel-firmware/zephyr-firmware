// © 2022-2024 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(onewire_core, LOG_LEVEL_DBG);

#include "onewire_functions.h"
#include "opencores_1wire.h"

#include "utility.h"

#include <generated/csr.h>
#include <generated/soc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// FIXME LOG_INF has issues with the long string of bytes that comprises the 1-wire device serial number, so fall back to direct printf() for now
//#define KESTREL_LOG(...) LOG_INF(__VA_ARGS__)
#define KESTREL_LOG(...) printf(__VA_ARGS__); printf("\n")

// Implementation of Maxim App Note #27
static uint8_t compute_onewire_crc_byte(uint8_t *data, int count)
{
    uint8_t crc;
    uint8_t byte;
    int lsb_set;
    int i;
    int bit;

    crc = 0;

    for (i=0; i<count; i++)
    {
        byte = data[i];
        for (bit=0; bit<8; bit++)
        {
            lsb_set = (crc ^ byte) & 0x1;
            crc = crc >> 1;
            if (lsb_set)
            {
                crc ^= 0x8c;
            }
            byte = byte >> 1;
        }
    }

    return crc;
}

static int scan_onewire_bus_device_handler(uint8_t *base_address, uint8_t *last_serial_number, int *scan_last_discrepancy, int *scan_was_last_device)
{
    uint8_t byte;
    uint8_t scan_byte;
    int ret;
    int scan_search_bit_value;
    int scan_bit_number;
    int scanned_byte_count;
    int last_zero_position;

    scanned_byte_count = 0;
    last_zero_position = 0;
    scan_bit_number = 0;

    // Reset bus
    ret = issue_onewire_reset(base_address, 0, &byte);
    if (ret || byte)
    {
        KESTREL_LOG("No devices responding on 1-wire bus, aborting scan!");
        return -1;
    }

    // Send search command
    if (transfer_onewire_byte(base_address, 0, ONEWIRE_BUS_CMD_SEARCH, NULL))
    {
        return -2;
    }

    // Scan bus
    while (scanned_byte_count < 8)
    {
        // Read true and complement bits
        if (transfer_onewire_bit(base_address, 0, 1, &byte))
        {
            return -3;
        }
        scan_byte = (byte & 0x1) << 1;
        if (transfer_onewire_bit(base_address, 0, 1, &byte))
        {
            return -4;
        }
        scan_byte |= byte & 0x1;
        if (scan_byte == 0x3)
        {
            KESTREL_LOG("No devices participating in 1-wire bus search, aborting scan!");
            return -1;
        }

        if (scan_byte > 0)
        {
            // All devices are showing the same bit value
            // Compute the true value from the complement (negation is faster than shifting)
            scan_search_bit_value = !(scan_byte & 0x1);
        }
        else
        {
            // At least one device has a bit value different than the rest
            if (scan_bit_number < *scan_last_discrepancy)
            {
                if ((last_serial_number[scanned_byte_count] & (0x1 << (scan_bit_number % 8))) > 0)
                {
                    scan_search_bit_value = 1;
                }
                else
                {
                    scan_search_bit_value = 0;
                }
            }
            else
            {
                if (scan_bit_number == *scan_last_discrepancy)
                {
                    scan_search_bit_value = 1;
                }
                else
                {
                    scan_search_bit_value = 0;
                }
            }

            if (scan_search_bit_value == 0)
            {
                last_zero_position = scan_bit_number;
            }
        }

        if (scan_search_bit_value == 1)
        {
            last_serial_number[scanned_byte_count] |= 0x1 << (scan_bit_number % 8);
        }
        else
        {
            last_serial_number[scanned_byte_count] &= ~(0x1 << (scan_bit_number % 8));
        }

        if (transfer_onewire_bit(base_address, 0, scan_search_bit_value, NULL))
        {
            return -5;
        }

        scan_bit_number++;
        if ((scan_bit_number % 8) == 0)
        {
            scanned_byte_count++;
        }
    }

    // Set up next device scan
    *scan_last_discrepancy = last_zero_position;
    if (last_zero_position == 0)
    {
        *scan_was_last_device = 1;
    }

    KESTREL_LOG("Found 1-wire device, serial number 0x%02x%02x%02x%02x%02x%02x%02x%02x",
        last_serial_number[7], last_serial_number[6], last_serial_number[5], last_serial_number[4],
        last_serial_number[3], last_serial_number[2], last_serial_number[1], last_serial_number[0]);

    return 0;
}

int scan_onewire_bus(uint8_t *base_address, uint64_t *detected_devices, int *device_count, int maximum_device_count)
{
    int scan_last_discrepancy;
    int scan_was_last_device;
    int ret;
    uint64_t detected_serial_number;
    uint8_t last_serial_number[8];

    scan_last_discrepancy = 0;
    scan_was_last_device = 0;

    ret = 0;
    *device_count = 0;
    memset(last_serial_number, 0, sizeof(last_serial_number));
    while (!scan_was_last_device && !ret)
    {
        ret = scan_onewire_bus_device_handler(base_address, last_serial_number, &scan_last_discrepancy, &scan_was_last_device);
        if (!ret)
        {
            detected_serial_number = (uint64_t)last_serial_number[0] & 0xff;
            detected_serial_number |= ((uint64_t)last_serial_number[1] & 0xff) << 8;
            detected_serial_number |= ((uint64_t)last_serial_number[2] & 0xff) << 16;
            detected_serial_number |= ((uint64_t)last_serial_number[3] & 0xff) << 24;
            detected_serial_number |= ((uint64_t)last_serial_number[4] & 0xff) << 32;
            detected_serial_number |= ((uint64_t)last_serial_number[5] & 0xff) << 40;
            detected_serial_number |= ((uint64_t)last_serial_number[6] & 0xff) << 48;
            detected_serial_number |= ((uint64_t)last_serial_number[7] & 0xff) << 56;
            detected_devices[*device_count] = detected_serial_number;
            (*device_count)++;
            if (*device_count >= maximum_device_count)
            {
                break;
            }
        }
        else
        {
            break;
        }
    }

    return 0;
}

int access_onewire_device(uint8_t *base_address, uint64_t device_serial_number, int match_all_devices)
{
    uint8_t byte;
    uint8_t tx_data_block[9];
    uint8_t rx_data_block[9];
    int ret;

    // Reset bus
    ret = issue_onewire_reset(base_address, 0, &byte);
    if (ret || byte)
    {
        KESTREL_LOG("No devices responding on 1-wire bus, aborting access!");
        return -1;
    }

    if (match_all_devices)
    {
        // Send skip ROM command
        if (transfer_onewire_byte(base_address, 0, ONEWIRE_BUS_CMD_SKIP_ROM, NULL))
        {
            return -2;
        }
    }
    else
    {
        // Send match serial command and serial number
        tx_data_block[0] = ONEWIRE_BUS_CMD_MATCH;
        tx_data_block[1] = device_serial_number & 0xff;
        tx_data_block[2] = (device_serial_number >> 8) & 0xff;
        tx_data_block[3] = (device_serial_number >> 16) & 0xff;
        tx_data_block[4] = (device_serial_number >> 24) & 0xff;
        tx_data_block[5] = (device_serial_number >> 32) & 0xff;
        tx_data_block[6] = (device_serial_number >> 40) & 0xff;
        tx_data_block[7] = (device_serial_number >> 48) & 0xff;
        tx_data_block[8] = (device_serial_number >> 56) & 0xff;
        memset(rx_data_block, 0, sizeof(rx_data_block));
        if (transfer_onewire_data_block(base_address, 0, tx_data_block, rx_data_block, 9))
        {
            return -2;
        }

        // Verify returned serial matches transmitted serial
        if (memcmp(tx_data_block, rx_data_block, 9))
        {
            KESTREL_LOG("1-wire device access returned data mismatch!");
            return -3;
        }
    }

    return 0;
}

int start_simultaneous_ds18b20_temperature_conversions(uint8_t *base_address)
{
    // Access devices
    if (access_onewire_device(base_address, 0x0, 1))
    {
        return -1;
    }

    // Send temperature conversion start command
    if (transfer_onewire_byte(base_address, 0, ONEWIRE_DS18B20_CMD_CONVERT, NULL))
    {
        return -2;
    }

    return 0;
}

int read_ds18b20_temperature(uint8_t *base_address, uint64_t device_serial_number, int skip_conversion, float *temperature)
{
    uint8_t tx_data_block[10];
    uint8_t rx_data_block[10];
    uint16_t word;
    char negative_temp;
    float converted_temp;

    if (!skip_conversion)
    {
        // Access device
        if (access_onewire_device(base_address, device_serial_number, 0))
        {
            return -1;
        }

        // Send temperature conversion start command
        if (transfer_onewire_byte(base_address, 0, ONEWIRE_DS18B20_CMD_CONVERT, NULL))
        {
            return -2;
        }

        usleep(1000000);
    }

    // Access device
    if (access_onewire_device(base_address, device_serial_number, 0))
    {
        return -1;
    }

    // Create data block for scratchpad read command
    memset(tx_data_block, 0xff, sizeof(tx_data_block));
    memset(rx_data_block, 0, sizeof(rx_data_block));
    tx_data_block[0] = ONEWIRE_BUS_CMD_READ_SCRATCHPAD;
    if (transfer_onewire_data_block(base_address, 0, tx_data_block, rx_data_block, 10))
    {
        return -2;
    }

    if (compute_onewire_crc_byte(rx_data_block + 1, 8) != rx_data_block[9])
    {
        KESTREL_LOG("Invalid data CRC reading scratchpad!");
        return -3;
    }

    negative_temp = 0;
    word = rx_data_block[2];
    word = word << 8;
    word |= rx_data_block[1];
    if (word & 0xf800)
    {
        negative_temp = 1;
        word &= ~0xf800;
    }
    converted_temp = word;
    converted_temp /= 16.0;
    if (negative_temp)
    {
        converted_temp = 128 - converted_temp;
        converted_temp *= -1;
    }

    if (temperature)
    {
        *temperature = converted_temp;
    }

    return 0;
}
