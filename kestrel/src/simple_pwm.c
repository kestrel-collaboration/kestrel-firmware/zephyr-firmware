// © 2020 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(kestrel_pwm, LOG_LEVEL_DBG);

#include "simple_pwm.h"

#include "utility.h"

#include <generated/csr.h>
#include <generated/soc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define KESTREL_LOG(...) LOG_INF(__VA_ARGS__)

int initialize_pwm_controller(uint8_t *base_address, int64_t frequency_hz)
{
    KESTREL_LOG("Configuring PWM controller at address %p...", (void*)base_address);

    if ((*((volatile uint32_t *)(base_address + SIMPLE_PWM_MASTER_DEVICE_ID_HIGH)) != SIMPLE_PWM_DEVICE_ID_HIGH) ||
        (*((volatile uint32_t *)(base_address + SIMPLE_PWM_MASTER_DEVICE_ID_LOW)) != SIMPLE_PWM_DEVICE_ID_LOW))
    {
        return -1;
    }
    uint32_t pwm_controller_version = *((volatile uint32_t *)(base_address + SIMPLE_PWM_MASTER_DEVICE_VERSION));
    KESTREL_LOG("PWM controller found, device version %0d.%0d.%d",
           (pwm_controller_version >> SIMPLE_PWM_VERSION_MAJOR_SHIFT) & SIMPLE_PWM_VERSION_MAJOR_MASK,
           (pwm_controller_version >> SIMPLE_PWM_VERSION_MINOR_SHIFT) & SIMPLE_PWM_VERSION_MINOR_MASK,
           (pwm_controller_version >> SIMPLE_PWM_VERSION_PATCH_SHIFT) & SIMPLE_PWM_VERSION_PATCH_MASK);
    {
        KESTREL_LOG("Disabling PWM outputs");

        set_pwm_value(base_address, 0, 0x00);
        set_pwm_value(base_address, 1, 0x00);
        set_pwm_value(base_address, 2, 0x00);
        set_pwm_value(base_address, 3, 0x00);

        if (frequency_hz > 0) {
            KESTREL_LOG("Configuring PWM frequencies");

            set_pwm_frequency(base_address, 0, frequency_hz);
            set_pwm_frequency(base_address, 1, frequency_hz);
            set_pwm_frequency(base_address, 2, frequency_hz);
            set_pwm_frequency(base_address, 3, frequency_hz);
        }

        return 0;
    }

    return 1;
}

int set_pwm_frequency(uint8_t *base_address, uint8_t channel, int64_t frequency_hz)
{
    uint32_t dword;
    uint32_t base_clock_hz;

    base_clock_hz = *((volatile uint32_t *)(base_address + SIMPLE_PWM_MASTER_BASE_CLK_HZ));
    dword = ((base_clock_hz / frequency_hz) / (1 << SIMPLE_PWM_OUTPUT_CTL_BITS));

    switch (channel) {
        case 0: *((volatile uint32_t *)(base_address + SIMPLE_PWM_MASTER_PHY_CFG_1)) = dword; break;
        case 1: *((volatile uint32_t *)(base_address + SIMPLE_PWM_MASTER_PHY_CFG_2)) = dword; break;
        case 2: *((volatile uint32_t *)(base_address + SIMPLE_PWM_MASTER_PHY_CFG_3)) = dword; break;
        case 3: *((volatile uint32_t *)(base_address + SIMPLE_PWM_MASTER_PHY_CFG_4)) = dword; break;
    }

    return 0;
}

int set_pwm_value(uint8_t *base_address, uint8_t channel, uint8_t value)
{
    uint32_t dword;

    dword = *((volatile uint32_t *)(base_address + SIMPLE_PWM_MASTER_PWM_CTL));
    dword &= ~(0xff << (channel * 8));
    dword |= (value & 0xff) << (channel * 8);
    *((volatile uint32_t *)(base_address + SIMPLE_PWM_MASTER_PWM_CTL)) = dword;

    return 0;
}

uint8_t get_pwm_value(uint8_t *base_address, uint8_t channel)
{
    return (*((volatile uint32_t *)(base_address + SIMPLE_PWM_MASTER_PWM_CTL)) >> (channel * 8)) & 0xff;
}

int get_tach_value(uint8_t *base_address, uint8_t channel)
{
    uint16_t raw_tach_value = 0;
    int rps = 0;
    int rpm = 0;

    if ((channel == 0) || (channel == 1))
    {
        raw_tach_value = (*((volatile uint32_t *)(base_address + SIMPLE_PWM_MASTER_TACH_01)) >> ((channel & 0x1) * 16)) & 0xffff;
    }
    else if ((channel == 2) || (channel == 3))
    {
        raw_tach_value = (*((volatile uint32_t *)(base_address + SIMPLE_PWM_MASTER_TACH_23)) >> ((channel & 0x1) * 16)) & 0xffff;
    }

    // Compute RPM
    rps = raw_tach_value * SIMPLE_PWM_TACH_SAMPLE_RATE_HZ;
    rpm = rps * 60;

    return rpm;
}
