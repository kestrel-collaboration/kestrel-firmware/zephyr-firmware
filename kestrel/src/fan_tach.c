/*
 * Copyright (c) 2021-2024 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: GPL-3.0
 */

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(fan_tach, LOG_LEVEL_INF);

#include <stdio.h>
#include <stdlib.h>

#include <zephyr/kernel.h>

#include "configuration.h"
#include "flash_filesystem.h"
#include "simple_pwm.h"

#include "fan_tach.h"

kestrel_fan_data_t kestrel_fan_data[MAX_FAN_DEVICES] = {0};
int kestrel_fan_count = 0;
kestrel_tach_data_t kestrel_tach_data[MAX_TACH_DEVICES] = {0};
int kestrel_tach_count = 0;
struct k_sem fan_tach_data_array_lock;

static int kestrel_fan_sort_callback(const void * a, const void * b)
{
    const kestrel_fan_data_t * fan_data_a = a;
    const kestrel_fan_data_t * fan_data_b = b;

    if (fan_data_a->fan_id < fan_data_b->fan_id) {
        return -1;
    }
    else if (fan_data_a->fan_id > fan_data_b->fan_id) {
        return 1;
    }

    return 0;
}

static int kestrel_tach_sort_callback(const void * a, const void * b)
{
    const kestrel_tach_data_t * tach_data_a = a;
    const kestrel_tach_data_t * tach_data_b = b;

    if (tach_data_a->tach_id < tach_data_b->tach_id) {
        return -1;
    }
    else if (tach_data_a->tach_id > tach_data_b->tach_id) {
        return 1;
    }

    return 0;
}

void kestrel_sort_fan_tach_devices(void) {
	int i;
	int j;

	// Sort fans and tachs
	// WARNING: This operation temporarily breaks the fan/tach links in each array, so this function
	// should only be called with a lock held on both the fan and tach data structures!
	qsort(kestrel_fan_data, kestrel_fan_count, sizeof(kestrel_fan_data_t), kestrel_fan_sort_callback);
	qsort(kestrel_tach_data, kestrel_tach_count, sizeof(kestrel_tach_data_t), kestrel_tach_sort_callback);

	// Relink tachs to fans
	for (i = 0; i < kestrel_fan_count; i++) {
		kestrel_fan_data[i].associated_tach_id_count = 0;

		for (j = 0; j < kestrel_tach_count; j++) {
			if (kestrel_fan_data[i].fan_id == kestrel_tach_data[j].tach_id) {
				if (kestrel_fan_data[i].associated_tach_id_count < MAX_TACH_DEVICES) {
					kestrel_fan_data[i].associated_tach_id[kestrel_fan_data[i].associated_tach_id_count] = j;
					kestrel_fan_data[i].associated_tach_id_count++;
				}
			}
		}
	}

	// Relink fans to tachs
	for (i = 0; i < kestrel_tach_count; i++) {
		kestrel_tach_data[i].associated_fan_id_count = 0;

		for (j = 0; j < kestrel_fan_count; j++) {
			if (kestrel_tach_data[i].tach_id == kestrel_fan_data[j].fan_id) {
				if (kestrel_tach_data[i].associated_fan_id_count < MAX_TACH_DEVICES) {
					kestrel_tach_data[i].associated_fan_id[kestrel_tach_data[i].associated_fan_id_count] = j;
					kestrel_tach_data[i].associated_fan_id_count++;
				}
			}
		}
	}
}

kestrel_fan_data_t * get_fan_device_by_id(uint32_t device_id) {
	int i;

	for (i = 0; i < kestrel_fan_count; i++)
	{
		if (kestrel_fan_data[i].fan_id == device_id)
		{
			return &kestrel_fan_data[i];
		}
	}

	return NULL;
}

kestrel_tach_data_t * get_tach_device_by_id(uint32_t device_id) {
	int i;

	for (i = 0; i < kestrel_tach_count; i++)
	{
		if (kestrel_tach_data[i].tach_id == device_id)
		{
			return &kestrel_tach_data[i];
		}
	}

	return NULL;
}

int read_all_fan_data_from_hw(void) {
	int i;
	int ret = 0;

	for (i = 0; i < kestrel_fan_count; i++) {
		if (read_fan_data_from_hw(&kestrel_fan_data[i])) {
			ret = -1;
		}
	}

	return ret;
}

int read_fan_data_from_hw(kestrel_fan_data_t * fan_data) {
	if (strcmp(fan_data->controller_driver_id, "SIMPLEPWM") == 0) {
		return read_fan_data_from_hw_simplepwm(fan_data);
	}

	return -1;
}

int sync_all_fan_data_to_hw(void) {
	int i;
	int ret = 0;

	for (i = 0; i < kestrel_fan_count; i++) {
		if (sync_fan_data_to_hw(&kestrel_fan_data[i])) {
			ret = -1;
		}
	}

	return ret;
}

int sync_fan_data_to_hw(kestrel_fan_data_t * fan_data) {
	if (strcmp(fan_data->controller_driver_id, "SIMPLEPWM") == 0) {
		return sync_fan_data_to_hw_simplepwm(fan_data);
	}

	return -1;
}

int read_fan_data_from_hw_simplepwm(kestrel_fan_data_t * fan_data) {
	bool have_data = false;
	int max_hw_pwm = 255;
	int min_hw_pwm = 0;
	uint8_t hw_readback = 0;

#ifdef SIMPLEPWM_BASE
	if ((fan_data->channel_id >= 0) && (fan_data->channel_id < 4)) {
		hw_readback = get_pwm_value((uint8_t*)SIMPLEPWM_BASE, fan_data->channel_id - 0);
		have_data = true;
	}
#endif
#ifdef SIMPLEPWM2_BASE
	if ((fan_data->channel_id >= 4) && (fan_data->channel_id < 8)) {
		hw_readback = get_pwm_value((uint8_t*)SIMPLEPWM2_BASE, fan_data->channel_id - 4);
		have_data = true;
	}
#endif

	if (have_data) {
		// Calculate scaling factor
		double hw_range = max_hw_pwm - min_hw_pwm;
		double normalized_hw_range = (hw_range / max_hw_pwm);
		double normalized_sw_range = (fan_data->fan_limits.max_normal_pwm - fan_data->fan_limits.min_normal_pwm) / 100.0;
		double scaling_factor = normalized_hw_range / normalized_sw_range;

		fan_data->fan_readback_percent = ((((hw_readback - min_hw_pwm) / hw_range) - (fan_data->fan_limits.min_normal_pwm / 100.0)) * scaling_factor) * 100.0;
		if (fan_data->fan_readback_percent < 0) {
			fan_data->fan_readback_percent = 0;
			fan_data->fan_online = false;
		}

		fan_data->valid = true;

		return 0;
	}

	return -1;
}

int sync_fan_data_to_hw_simplepwm(kestrel_fan_data_t * fan_data) {
	int max_hw_pwm = 255;
	int min_hw_pwm = 0;
	int hw_setting = max_hw_pwm;

	// Calculate scaling factor
	double hw_range = max_hw_pwm - min_hw_pwm;
	double normalized_hw_range = (hw_range / max_hw_pwm);
	double normalized_sw_range = (fan_data->fan_limits.max_normal_pwm - fan_data->fan_limits.min_normal_pwm) / 100.0;
	double scaling_factor = normalized_sw_range / normalized_hw_range;

	if (fan_data->fan_set_online) {
		hw_setting = ((fan_data->fan_setting_percent / 100.0) * scaling_factor * hw_range) + ((fan_data->fan_limits.min_normal_pwm / 100.0) * hw_range) + min_hw_pwm;
	}
	else {
		hw_setting = (fan_data->fan_limits.idle_cutoff_pwm / 100.0) * hw_range;
	}

	if (hw_setting > max_hw_pwm) {
		hw_setting = max_hw_pwm;
	}
	if (hw_setting < min_hw_pwm) {
		hw_setting = min_hw_pwm;
	}

#ifdef SIMPLEPWM_BASE
	if ((fan_data->channel_id >= 0) && (fan_data->channel_id < 4)) {
		set_pwm_value((uint8_t*)SIMPLEPWM_BASE, fan_data->channel_id - 0, hw_setting);
		return 0;
	}
#endif
#ifdef SIMPLEPWM2_BASE
	if ((fan_data->channel_id >= 4) && (fan_data->channel_id < 8)) {
		set_pwm_value((uint8_t*)SIMPLEPWM2_BASE, fan_data->channel_id - 4, hw_setting);
		return 0;
	}
#endif

	return -1;
}

void configure_fan_devices(void) {
	char * pos1;
	char * pos2;
	size_t file_len;
	size_t len;

	LOG_INF("Initializing fan controller(s)");

	// Load any configured fans from the configuration ROM
	char * fan_tach_vpd_map_file_contents = read_vpd_file_contents("pwm-fan-fru-map");
	if (fan_tach_vpd_map_file_contents) {
		char fan_config_buffer[64] = {0};
		uint32_t channel_id = 0;
		uint32_t fan_id = 0x0;
		char fan_tach_driver_id[64] = {0};
		kestrel_fan_control_limits_t fan_control_limits;

		file_len = strlen(fan_tach_vpd_map_file_contents);
		pos1 = fan_tach_vpd_map_file_contents;
		while ((pos1 - fan_tach_vpd_map_file_contents) < file_len) {
			pos2 = strstr(pos1, ":");
			if (pos2) {
				len = pos2 - pos1;
				if (len > (sizeof(fan_config_buffer) - 1)) {
					len = sizeof(fan_config_buffer) - 1;
				}
				memcpy(fan_config_buffer, pos1, len);
				fan_config_buffer[len] = 0x0;
				channel_id = strtoull(fan_config_buffer, NULL, 10);
			}
			pos1 = pos2 + 1;
			if ((pos1 - fan_tach_vpd_map_file_contents) >= file_len) {
				break;
			}
			pos2 = strstr(pos1, ":");
			if (pos2) {
				len = pos2 - pos1;
				if (len > (sizeof(fan_config_buffer) - 1)) {
					len = sizeof(fan_config_buffer) - 1;
				}
				memcpy(fan_config_buffer, pos1, len);
				fan_config_buffer[len] = 0x0;
				fan_id = strtoull(fan_config_buffer, NULL, 16);
			}
			pos1 = pos2 + 1;
			if ((pos1 - fan_tach_vpd_map_file_contents) >= file_len) {
				break;
			}
			pos2 = strstr(pos1, ":");
			if (pos2) {
				len = pos2 - pos1;
				if (len > (sizeof(fan_tach_driver_id) - 1)) {
					len = sizeof(fan_tach_driver_id) - 1;
				}
				memcpy(fan_tach_driver_id, pos1, len);
				fan_tach_driver_id[len] = 0x0;
			}
			pos1 = pos2 + 1;
			if ((pos1 - fan_tach_vpd_map_file_contents) >= file_len) {
				break;
			}
			pos2 = strstr(pos1, ":");
			if (pos2) {
				len = pos2 - pos1;
				if (len > (sizeof(fan_config_buffer) - 1)) {
					len = sizeof(fan_config_buffer) - 1;
				}
				memcpy(fan_config_buffer, pos1, len);
				fan_config_buffer[len] = 0x0;
				fan_control_limits.idle_cutoff_pwm = strtoull(fan_config_buffer, NULL, 10);
			}
			pos1 = pos2 + 1;
			if ((pos1 - fan_tach_vpd_map_file_contents) >= file_len) {
				break;
			}
			pos2 = strstr(pos1, ":");
			if (pos2) {
				len = pos2 - pos1;
				if (len > (sizeof(fan_config_buffer) - 1)) {
					len = sizeof(fan_config_buffer) - 1;
				}
				memcpy(fan_config_buffer, pos1, len);
				fan_config_buffer[len] = 0x0;
				fan_control_limits.min_normal_pwm = strtoull(fan_config_buffer, NULL, 10);
			}
			pos1 = pos2 + 1;
			if ((pos1 - fan_tach_vpd_map_file_contents) >= file_len) {
				break;
			}
			pos2 = strstr(pos1, ":");
			if (pos2) {
				len = pos2 - pos1;
				if (len > (sizeof(fan_config_buffer) - 1)) {
					len = sizeof(fan_config_buffer) - 1;
				}
				memcpy(fan_config_buffer, pos1, len);
				fan_config_buffer[len] = 0x0;
				fan_control_limits.max_normal_pwm = strtoull(fan_config_buffer, NULL, 10);
			}
			pos1 = pos2 + 1;
			if ((pos1 - fan_tach_vpd_map_file_contents) >= file_len) {
				break;
			}
			pos2 = strstr(pos1, ":");
			if (pos2) {
				len = pos2 - pos1;
				if (len > (sizeof(fan_config_buffer) - 1)) {
					len = sizeof(fan_config_buffer) - 1;
				}
				memcpy(fan_config_buffer, pos1, len);
				fan_config_buffer[len] = 0x0;
				fan_control_limits.max_emergency_pwm = strtoull(fan_config_buffer, NULL, 10);
			}
			pos1 = pos2 + 1;
			if ((pos1 - fan_tach_vpd_map_file_contents) >= file_len) {
				break;
			}
			pos2 = strstr(pos1, "\n");
			if (pos2) {
				len = pos2 - pos1;
				if (len > (MAX_FAN_TACH_DESC_LEN - 1)) {
					len = MAX_FAN_TACH_DESC_LEN - 1;
				}

				if (k_sem_take(&fan_tach_data_array_lock, K_FOREVER) == 0) {
					// Valid sensor descriptor found
					LOG_INF("Found configuration for fan on channel %d, driver '%s', enabling as ID 0x%08x", channel_id, fan_tach_driver_id, fan_id);

					kestrel_fan_data[kestrel_fan_count].fan_id = fan_id;
					kestrel_fan_data[kestrel_fan_count].fru_type = FRU_TYPE_FAN_SINGLE_PWM;
					kestrel_fan_data[kestrel_fan_count].channel_id = channel_id;
					kestrel_fan_data[kestrel_fan_count].driver_data_ptr = NULL;
					kestrel_fan_data[kestrel_fan_count].associated_tach_id_count = 0;
					kestrel_fan_data[kestrel_fan_count].fan_setting_percent = 0;
					kestrel_fan_data[kestrel_fan_count].fan_readback_percent = 0;
					kestrel_fan_data[kestrel_fan_count].description[0] = 0x0;
					kestrel_fan_data[kestrel_fan_count].fan_limits = fan_control_limits;
					kestrel_fan_data[kestrel_fan_count].fan_set_online = true;
					kestrel_fan_data[kestrel_fan_count].fan_online = false;
					kestrel_fan_data[kestrel_tach_count].hot_plug_capable = true;
					kestrel_fan_data[kestrel_tach_count].set_indicator_led_on = false;
					kestrel_fan_data[kestrel_tach_count].indicator_led_on = false;
					kestrel_fan_data[kestrel_fan_count].valid = false;

					// Copy description
					memcpy(kestrel_fan_data[kestrel_fan_count].description, pos1, len);
					kestrel_fan_data[kestrel_fan_count].description[len] = 0x0;

					// Copy driver ID
					strncpy(kestrel_fan_data[kestrel_fan_count].controller_driver_id, fan_tach_driver_id, sizeof(kestrel_fan_data[kestrel_fan_count].controller_driver_id) - 1);
					kestrel_fan_data[kestrel_fan_count].controller_driver_id[sizeof(kestrel_fan_data[kestrel_fan_count].controller_driver_id) - 1] = 0x0;

					kestrel_fan_count++;

					k_sem_give(&fan_tach_data_array_lock);
				}
			}
			pos1 = pos2 + 1;
			if ((pos1 - fan_tach_vpd_map_file_contents) >= file_len) {
				break;
			}
		}

		free(fan_tach_vpd_map_file_contents);
	}
}

void process_tach_data_update(kestrel_tach_data_t * tach_data) {
	double average;
	int i;

	// Update history and average
	tach_data->tach_value_rpm_history[tach_data->tach_value_rpm_history_ptr] = tach_data->tach_value_rpm;
	tach_data->tach_value_rpm_history_ptr++;
	if (tach_data->tach_value_rpm_history_ptr >= sizeof(tach_data->tach_value_rpm_history) / sizeof(tach_data->tach_value_rpm_history[0])) {
		tach_data->tach_value_rpm_history_ptr = 0;
	}

	average = 0;
	for (i = 0; i < sizeof(tach_data->tach_value_rpm_history) / sizeof(tach_data->tach_value_rpm_history[0]); i++) {
		average += tach_data->tach_value_rpm_history[i];
	}
	tach_data->tach_value_rpm_averaged = average / (sizeof(tach_data->tach_value_rpm_history) / sizeof(tach_data->tach_value_rpm_history[0]));
}

int read_all_tach_data_from_hw(void) {
	int i;
	int ret = 0;

	for (i = 0; i < kestrel_tach_count; i++) {
		if (read_tach_data_from_hw(&kestrel_tach_data[i])) {
			ret = -1;
		}
	}

	return ret;
}

int read_tach_data_from_hw(kestrel_tach_data_t * tach_data) {
	int ret = -1;

	if (strcmp(tach_data->controller_driver_id, "SIMPLEPWM") == 0) {
		ret = read_tach_data_from_hw_simplepwm(tach_data);
	}

	if (ret == 0) {
		// Update history and average
		process_tach_data_update(tach_data);
	}

	return ret;
}

int read_tach_data_from_hw_simplepwm(kestrel_tach_data_t * tach_data) {
	bool have_data = false;
	double tach_value_rpm = 0;

#ifdef SIMPLEPWM_BASE
	if ((tach_data->channel_id >= 0) && (tach_data->channel_id < 4)) {
		tach_value_rpm = get_tach_value((uint8_t*)SIMPLEPWM_BASE, tach_data->channel_id - 0);
		have_data = true;
	}
#endif
#ifdef SIMPLEPWM2_BASE
	if ((tach_data->channel_id >= 4) && (tach_data->channel_id < 8)) {
		tach_value_rpm = get_tach_value((uint8_t*)SIMPLEPWM_BASE, tach_data->channel_id - 4);
		have_data = true;
	}
#endif
	tach_value_rpm *= tach_data->pulses_per_revolution;

	if (have_data) {
		tach_data->tach_value_rpm = tach_value_rpm;
		tach_data->valid = true;

		return 0;
	}

	return -1;
}

void configure_tach_devices(void) {
	char * pos1;
	char * pos2;
	size_t file_len;
	size_t len;

	LOG_INF("Initializing tachometer controller(s)");

	// Load any configured tachs from the configuration ROM
	char * fan_tach_vpd_map_file_contents = read_vpd_file_contents("tach-fan-fru-map");
	if (fan_tach_vpd_map_file_contents) {
		char tach_config_buffer[64] = {0};
		uint32_t channel_id = 0;
		uint32_t tach_id = 0x0;
		int pulses_per_revolution = 1;
		char fan_tach_driver_id[64] = {0};
		kestrel_fan_alert_thresholds_t fan_alert_thresholds;

		file_len = strlen(fan_tach_vpd_map_file_contents);
		pos1 = fan_tach_vpd_map_file_contents;
		while ((pos1 - fan_tach_vpd_map_file_contents) < file_len) {
			pos2 = strstr(pos1, ":");
			if (pos2) {
				len = pos2 - pos1;
				if (len > (sizeof(tach_config_buffer) - 1)) {
					len = sizeof(tach_config_buffer) - 1;
				}
				memcpy(tach_config_buffer, pos1, len);
				tach_config_buffer[len] = 0x0;
				channel_id = strtoull(tach_config_buffer, NULL, 10);
			}
			pos1 = pos2 + 1;
			if ((pos1 - fan_tach_vpd_map_file_contents) >= file_len) {
				break;
			}
			pos2 = strstr(pos1, ":");
			if (pos2) {
				len = pos2 - pos1;
				if (len > (sizeof(tach_config_buffer) - 1)) {
					len = sizeof(tach_config_buffer) - 1;
				}
				memcpy(tach_config_buffer, pos1, len);
				tach_config_buffer[len] = 0x0;
				tach_id = strtoull(tach_config_buffer, NULL, 16);
			}
			pos1 = pos2 + 1;
			if ((pos1 - fan_tach_vpd_map_file_contents) >= file_len) {
				break;
			}
			pos2 = strstr(pos1, ":");
			if (pos2) {
				len = pos2 - pos1;
				if (len > (sizeof(fan_tach_driver_id) - 1)) {
					len = sizeof(fan_tach_driver_id) - 1;
				}
				memcpy(fan_tach_driver_id, pos1, len);
				fan_tach_driver_id[len] = 0x0;
			}
			pos1 = pos2 + 1;
			if ((pos1 - fan_tach_vpd_map_file_contents) >= file_len) {
				break;
			}
			pos2 = strstr(pos1, ":");
			if (pos2) {
				len = pos2 - pos1;
				if (len > (sizeof(tach_config_buffer) - 1)) {
					len = sizeof(tach_config_buffer) - 1;
				}
				memcpy(tach_config_buffer, pos1, len);
				tach_config_buffer[len] = 0x0;
				pulses_per_revolution = strtoull(tach_config_buffer, NULL, 10);
			}
			pos1 = pos2 + 1;
			if ((pos1 - fan_tach_vpd_map_file_contents) >= file_len) {
				break;
			}
			pos2 = strstr(pos1, "\n");
			if (pos2) {
				len = pos2 - pos1;
				if (len > (MAX_FAN_TACH_DESC_LEN - 1)) {
					len = MAX_FAN_TACH_DESC_LEN - 1;
				}

				if (k_sem_take(&fan_tach_data_array_lock, K_FOREVER) == 0) {
					// Valid sensor descriptor found
					LOG_INF("Found configuration for tachometer on channel %d, driver '%s', enabling as ID 0x%08x", channel_id, fan_tach_driver_id, tach_id);

					// Set generic alert threshold values
					fan_alert_thresholds.lower_rpm_threshold_warning = 250;
					fan_alert_thresholds.lower_rpm_threshold_critical = 100;
					fan_alert_thresholds.lower_rpm_threshold_fatal = 0;
					fan_alert_thresholds.upper_rpm_threshold_warning = 6000;
					fan_alert_thresholds.upper_rpm_threshold_critical = 10000;
					fan_alert_thresholds.upper_rpm_threshold_fatal = 15000;
					fan_alert_thresholds.max_design_rpm = 6000;
					fan_alert_thresholds.min_design_rpm = 0;

					kestrel_tach_data[kestrel_tach_count].tach_id = tach_id;
					kestrel_tach_data[kestrel_tach_count].fru_type = FRU_TYPE_FAN_SINGLE_TACH;
					kestrel_tach_data[kestrel_tach_count].channel_id = channel_id;
					kestrel_tach_data[kestrel_tach_count].driver_data_ptr = NULL;
					kestrel_tach_data[kestrel_tach_count].pulses_per_revolution = pulses_per_revolution;
					kestrel_tach_data[kestrel_tach_count].associated_fan_id_count = 0;
					kestrel_tach_data[kestrel_tach_count].tach_value_rpm = 0;
					kestrel_tach_data[kestrel_tach_count].description[0] = 0x0;
					kestrel_tach_data[kestrel_tach_count].fan_alert_thresholds = fan_alert_thresholds;
					kestrel_tach_data[kestrel_tach_count].valid = false;

					// Copy description
					memcpy(kestrel_tach_data[kestrel_tach_count].description, pos1, len);
					kestrel_tach_data[kestrel_tach_count].description[len] = 0x0;

					// Copy driver ID
					strncpy(kestrel_tach_data[kestrel_tach_count].controller_driver_id, fan_tach_driver_id, sizeof(kestrel_tach_data[kestrel_tach_count].controller_driver_id) - 1);
					kestrel_tach_data[kestrel_tach_count].controller_driver_id[sizeof(kestrel_tach_data[kestrel_tach_count].controller_driver_id) - 1] = 0x0;

					kestrel_tach_count++;

					k_sem_give(&fan_tach_data_array_lock);
				}
			}
			pos1 = pos2 + 1;
			if ((pos1 - fan_tach_vpd_map_file_contents) >= file_len) {
				break;
			}
		}

		free(fan_tach_vpd_map_file_contents);
	}
}