/*
 * Copyright (c) 2019 Antmicro Ltd
 * Copyright (c) 2021-2024 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(webservice, LOG_LEVEL_INF);

#include <stdio.h>

#include <zephyr/kernel.h>
#include <zephyr/data/json.h>

#include <zephyr/net/tls_credentials.h>
#include <zephyr/net/http/server.h>
#include <zephyr/net/http/service.h>

#include "http_functions.h"

#include "lz4.h"

#define LZ4_LINUX_HEADER_SIZE 8

#include "webportal.h"

// Compressed static files
DECLARE_EXTERNAL_STATIC_FILE(embedded_file_licenses_inc_lz4)

// #define KESTREL_ENABLE_HTTPS_SIGNED_CA_CERTS 1

// Define ports
#define HTTP_PORT			80
#define HTTPS_PORT			443

static uint16_t kestrel_http_service_port = HTTP_PORT;
HTTP_SERVICE_DEFINE(kestrel_http_service, "0.0.0.0", &kestrel_http_service_port, 1,
		    10, NULL);

bool tls_support_available = false;

static void setup_tls(void)
{
	register uint64_t val __asm__ ("r5") = 0xffffffffffffffffUL;

	// Check if required 'darn' functionality for production mode (i.e. secure mode vs. debug / test mode) TLS is available
	__asm__ __volatile__ (".byte 0xe6,0x05,0xa1,0x7c" : "=r"(val) ::);
	if (val == 0xffffffffffffffffUL) {
		tls_support_available = false;
		return;
	}
	else {
		tls_support_available = true;
	}

#if defined(CONFIG_NET_SOCKETS_SOCKOPT_TLS)
	int err;

#if defined(KESTREL_ENABLE_HTTPS_SIGNED_CA_CERTS)
	err = tls_credential_add(HTTP_SERVER_CERTIFICATE_TAG,
				 TLS_CREDENTIAL_CA_CERTIFICATE,
				 ca_certificate,
				 sizeof(ca_certificate));
	if (err < 0) {
		LOG_ERR("Failed to register CA certificate: %d", err);
	}
#endif /* defined(KESTREL_ENABLE_HTTPS_SIGNED_CA_CERTS) */

	err = tls_credential_add(HTTP_SERVER_CERTIFICATE_TAG,
				 TLS_CREDENTIAL_SERVER_CERTIFICATE,
				 server_certificate,
				 sizeof(server_certificate));
	if (err < 0) {
		LOG_ERR("Failed to register public certificate: %d", err);
	}

	err = tls_credential_add(HTTP_SERVER_CERTIFICATE_TAG,
				 TLS_CREDENTIAL_PRIVATE_KEY,
				 private_key, sizeof(private_key));
	if (err < 0) {
		LOG_ERR("Failed to register private key: %d", err);
	}

#if defined(CONFIG_MBEDTLS_KEY_EXCHANGE_PSK_ENABLED)
	err = tls_credential_add(PSK_TAG,
				 TLS_CREDENTIAL_PSK,
				 psk,
				 sizeof(psk));
	if (err < 0) {
		LOG_ERR("Failed to register PSK: %d", err);
	}

	err = tls_credential_add(PSK_TAG,
				 TLS_CREDENTIAL_PSK_ID,
				 psk_id,
				 sizeof(psk_id) - 1);
	if (err < 0) {
		LOG_ERR("Failed to register PSK ID: %d", err);
	}
#endif /* defined(CONFIG_MBEDTLS_KEY_EXCHANGE_PSK_ENABLED) */
#endif /* defined(CONFIG_NET_SOCKETS_SOCKOPT_TLS) */
}

void start_webservice(void)
{
	// Decompress data blocks
	license_file_decompressed_length = LZ4_decompress_safe(embedded_file_licenses_inc_lz4 + LZ4_LINUX_HEADER_SIZE,
		license_file_decompressed,
		embedded_file_licenses_inc_lz4_SIZE - LZ4_LINUX_HEADER_SIZE,
		sizeof(license_file_decompressed));

	if (license_file_decompressed_length < 0) {
		LOG_ERR("Unable to decompress license file, recode=%lld!  Flash corrupted?", license_file_decompressed_length);
		license_file_decompressed_length = snprintf(license_file_decompressed, sizeof(license_file_decompressed), "%s", "Invalid internal license file detected, possible Flash corruption?"
			"<P>Please visit <A HREF=\"https://gitlab.raptorengineering.com/kestrel-collaboration\">https://gitlab.raptorengineering.com/kestrel-collaboration</A> for current license information.");
	}

	setup_tls();
	http_server_start();
}
