// © 2021-2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _CONFIGURATION_H
#define _CONFIGURATION_H

#define ETH0_NODE					eth0

char * read_vpd_file_contents(const char * vpd_filename);
void set_ethernet_mac_from_vpd(void);

#endif // _CONFIGURATION_H