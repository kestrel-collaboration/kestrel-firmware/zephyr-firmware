// © 2020 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#include "build_config.h"

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <zephyr/shell/shell.h>
#include <zephyr/drivers/uart.h>
#include <generated/csr.h>

#include "kestrel.h"
#include "opencores_i2c.h"
#include "fan_tach.h"
#include "fsi.h"
#include "sbe_fsi.h"
#include "flash_filesystem.h"
#include "nvram.h"
#include "sensors.h"
#include "time.h"

#include <generated/mem.h>

#define KESTREL_CONSOLE_THREAD_PRIORITY	K_PRIO_PREEMPT(CONFIG_NUM_PREEMPT_PRIORITIES - 1)
static K_THREAD_STACK_DEFINE(kestrel_console_thread_stack, 65536);
static struct k_thread kestrel_console_thread_data;

#define VERIFY_KESTREL_START_COMPLETE(shell)		\
		if (!kestrel_basic_init_complete) {	\
			print_kestrel_init_wait(shell);	\
			return -1;			\
		}

#define CONSOLE_ATTACH_BANNER "\n***** CONSOLE ATTACHED *****\n"

static void kestrel_console_thread(const void *p1, const void *p2)
{
	const struct shell *shell = p1;
	void *active_log_backend = (void*)p2;
	uint32_t active_log_level = CONFIG_LOG_DEFAULT_LEVEL;

	// Get shell printf context
	const struct shell_fprintf *sh_fprintf = shell->fprintf_ctx;

	k_mutex_lock(&shell->ctx->wr_mtx, K_FOREVER);

	shell_stop(shell);

	sh_fprintf->fwrite((const struct shell *)sh_fprintf->user_ctx, CONSOLE_ATTACH_BANNER, strlen(CONSOLE_ATTACH_BANNER));

	attach_to_host_console(shell);

	// Wait for host console to exit
	while (host_console_service_task_requested) {
		k_thread_suspend(kestrel_console_thread_id);
	}
	kestrel_console_thread_id = NULL;

	k_mutex_unlock(&shell->ctx->wr_mtx);

	// Enable shell and logging
	if (IS_ENABLED(CONFIG_SHELL_LOG_BACKEND) && active_log_backend) {
		z_shell_log_backend_enable(shell->log_backend, (void *)shell, active_log_level);
	}
	shell_start(shell);
}

static int kestrel_shell_cmd_exit(const struct shell *shell, size_t argc, char **argv)
{
	shell->ctx->selected_cmd = NULL;

	return 0;
}

static int kestrel_shell_cmd_version(const struct shell *shell, size_t argc, char **argv)
{
	shell_print(shell, "%s", "0.9.0");

	return 0;
}

static void print_kestrel_init_wait(const struct shell *shell)
{
	shell_print(shell, "Kestrel is still starting up, please wait...");
}

static void print_kestrel_utility_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s <poweron chassison chassisoff>", command_name);
}

static void print_kestrel_reflash_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s <fpga|bmc|bmc_recovery|pnor|config|storage>", command_name);
}

static void print_kestrel_nvram_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s <show|set [key] [value]|unset [key]|commit|reload>", command_name);
}

static void print_kestrel_memdump_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s <address> [length]", command_name);
}

static void print_kestrel_onewire_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s <scan>", command_name);
}

static void print_kestrel_pwmcontrol_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s <set|show> [device ID] [value]", command_name);
}

static void print_kestrel_pidcontrol_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s <set|show> [channel] [parameter] [value]", command_name);
}

static void print_kestrel_thermal_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s <show|rescan|rescan-full>", command_name);
}

static void print_kestrel_gpiocontrol_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s <channel> <value>", command_name);
}

static void print_kestrel_i2cdetect_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s <channel>", command_name);
}

static void print_kestrel_i2cget_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s <channel> [device] [register]", command_name);
}

static void print_kestrel_i2cset_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s <channel> [device] [register] [value]", command_name);
}

static void print_kestrel_time_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s <sync_ntp> [server]", command_name);
}

static void print_kestrel_sbecontrol_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s [status]", command_name);
}

static void print_kestrel_occcontrol_command_usage(const struct shell *shell, char* command_name) {
	shell_print(shell, "Usage: %s [poll]", command_name);
}

static int kestrel_shell_cmd_utility(const struct shell *shell, size_t argc, char **argv)
{
	if (argc < 2) {
		print_kestrel_utility_command_usage(shell, argv[0]);
		return -1;
	}

	if (strcmp(argv[1], "poweron") == 0) {
		VERIFY_KESTREL_START_COMPLETE(shell)

		return power_on_host();
	}
	else if (strcmp(argv[1], "chassison") == 0) {
		VERIFY_KESTREL_START_COMPLETE(shell)

		return power_on_chassis(0);
	}
	else if (strcmp(argv[1], "chassisoff") == 0) {
		VERIFY_KESTREL_START_COMPLETE(shell)

		power_off_chassis(0);
		return 0;
	}
	else {
		shell_print(shell, "%s: Invalid parameter", argv[0]);
		print_kestrel_utility_command_usage(shell, argv[0]);
		return -1;
	}
}

static int kestrel_shell_cmd_reflash(const struct shell *shell, size_t argc, char **argv)
{
	if (argc < 2) {
		print_kestrel_reflash_command_usage(shell, argv[0]);
		return -1;
	}

	if (strcmp(argv[1], "pnor") == 0) {
		VERIFY_KESTREL_START_COMPLETE(shell)

		return write_flash_buffer_to_flash(shell);
	}
	else if (strcmp(argv[1], "bmc") == 0) {
		return write_firmware_to_flash(shell, "bmc", main_firmware_buffer.buffer_address, main_firmware_buffer.valid_bytes, 1);
	}
	else if (strcmp(argv[1], "bmc_recovery") == 0) {
		return write_firmware_to_flash(shell, "bmc_recovery", main_firmware_buffer.buffer_address, main_firmware_buffer.valid_bytes, 1);
	}
	else if (strcmp(argv[1], "fpga") == 0) {
		return write_firmware_to_flash(shell, "fpga", main_firmware_buffer.buffer_address, main_firmware_buffer.valid_bytes, 0);
	}
	else if (strcmp(argv[1], "config") == 0) {
		return write_firmware_to_flash(shell, "bmc_configuration", main_firmware_buffer.buffer_address, main_firmware_buffer.valid_bytes, 0);
	}
	else if (strcmp(argv[1], "storage") == 0) {
		return write_firmware_to_flash(shell, "bmc_storage", main_firmware_buffer.buffer_address, main_firmware_buffer.valid_bytes, 0);
	}
	else {
		shell_print(shell, "%s: Invalid parameter", argv[0]);
		print_kestrel_reflash_command_usage(shell, argv[0]);
		return -1;
	}
}

static int kestrel_shell_cmd_nvram(const struct shell *shell, size_t argc, char **argv)
{
	int i;
	int ret;

	if (argc < 2) {
		print_kestrel_nvram_command_usage(shell, argv[0]);
		return -1;
	}

	if (strcmp(argv[1], "show") == 0) {
		if (k_sem_take(&nvram_access_lock, K_MSEC(5000)) == 0) {
			if (nvram_settings) {
				for (i = 0; i < nvram_settings->element_count; i++) {
					shell_print(shell, "\t%s: %s", nvram_settings->elements[i].key, nvram_settings->elements[i].value);
				}
			}

			k_sem_give(&nvram_access_lock);

			return 0;
		}
		else {
			shell_print(shell, "%s: Unable to acquire NVRAM access lock.  Unable to print NVRAM contents!", argv[0]);
			return -1;
		}
	}
	else if (strcmp(argv[1], "set") == 0) {
		if (argc < 4) {
			print_kestrel_nvram_command_usage(shell, argv[0]);
			return -1;
		}

		if (k_sem_take(&nvram_access_lock, K_MSEC(5000)) == 0) {
			if (nvram_settings) {
				ret = nvram_set_key_value_or_create_pair(nvram_settings, argv[2], argv[3]);
				k_sem_give(&nvram_access_lock);

				if (ret == -ENOSPC) {
					shell_print(shell, "%s: NVRAM storage full!", argv[0]);
				}
				else if (ret) {
					shell_print(shell, "%s: Uable to set new NVRAM value", argv[0]);
				}

				return ret;
			}
			else {
				k_sem_give(&nvram_access_lock);

				return -1;
			}
		}
		else {
			shell_print(shell, "%s: Unable to acquire NVRAM access lock.  NVRAM unchanged.", argv[0]);
			return -1;
		}
	}
	else if (strcmp(argv[1], "unset") == 0) {
		if (argc < 3) {
			print_kestrel_nvram_command_usage(shell, argv[0]);
			return -1;
		}

		if (k_sem_take(&nvram_access_lock, K_MSEC(5000)) == 0) {
			if (nvram_settings) {
				ret = nvram_set_key_value_or_create_pair(nvram_settings, argv[2], NULL);
				k_sem_give(&nvram_access_lock);

				if (ret) {
					shell_print(shell, "%s: Uable to remove existing NVRAM key", argv[0]);
				}

				return ret;
			}
			else {
				k_sem_give(&nvram_access_lock);

				return -1;
			}
		}
		else {
			shell_print(shell, "%s: Unable to acquire NVRAM access lock.  NVRAM unchanged.", argv[0]);
			return -1;
		}
	}
	else if (strcmp(argv[1], "commit") == 0) {
		if (k_sem_take(&nvram_access_lock, K_MSEC(5000)) == 0) {
			if (nvram_settings) {
				// Write settings to persistent storage
				ret = write_nvram_settings_to_persistent_storage(nvram_settings);
				if (ret) {
					shell_print(shell, "%s: Unable to write NVRAM data to persistent storage.  Flash device failure / filesystem full?", argv[0]);
				}

				// Load stored NVRAM settings back into RAM
				free(nvram_settings);
				nvram_settings = load_nvram_settings_from_persistent_storage();

				k_sem_give(&nvram_access_lock);

				return ret;
			}
			else {
				k_sem_give(&nvram_access_lock);

				return -1;
			}
		}
		else {
			shell_print(shell, "%s: Unable to acquire NVRAM access lock.  NVRAM unchanged.", argv[0]);
			return -1;
		}
	}
	else if (strcmp(argv[1], "reload") == 0) {
		if (k_sem_take(&nvram_access_lock, K_MSEC(5000)) == 0) {
			if (nvram_settings) {
				// Load stored NVRAM settings back into RAM
				free(nvram_settings);
				nvram_settings = load_nvram_settings_from_persistent_storage();

				k_sem_give(&nvram_access_lock);

				return ret;
			}
			else {
				k_sem_give(&nvram_access_lock);

				return -1;
			}
		}
		else {
			shell_print(shell, "%s: Unable to acquire NVRAM access lock.  NVRAM unchanged.", argv[0]);
			return -1;
		}
	}
	else {
		shell_print(shell, "%s: Invalid parameter", argv[0]);
		print_kestrel_nvram_command_usage(shell, argv[0]);
		return -1;
	}
}

static int kestrel_shell_cmd_memdump(const struct shell *shell, size_t argc, char **argv)
{
	uintptr_t address;
	size_t length;
	int pos;
	int remaining;

	if (argc < 2) {
		print_kestrel_memdump_command_usage(shell, argv[0]);
		return -1;
	}

	address = strtoul(argv[1], NULL, 16);
	if (argc > 2) {
		length = strtoul(argv[2], NULL, 16);
	}
	else {
		length = 1;
	}

	for (pos = 0; pos < length; pos += remaining) {
		remaining = MIN(length - pos, SHELL_HEXDUMP_BYTES_IN_LINE);
		shell_hexdump_line(shell, address, (uint8_t*)address, remaining);
		address += remaining;
	}

	shell_print(shell, "");

	return 0;
}

static int kestrel_shell_cmd_i2cdetect(const struct shell *shell, size_t argc, char **argv)
{
	uint8_t *base_address;
	int channel;
	uint8_t dev_addr;
	uint8_t dev_reg;
	uint8_t value;
	int retcode;

	if (argc < 2) {
		print_kestrel_i2cdetect_command_usage(shell, argv[0]);
		return -1;
	}

	channel = strtoul(argv[1], NULL, 10);

	base_address = (uint8_t*)(I2CMASTER0_BASE + (0x20 * channel));

	dev_reg = 0;
	for (dev_addr = 0; dev_addr < 0xff; dev_addr++) {
		value = i2c_read_register_byte(base_address, dev_addr, dev_reg, &retcode);
		if (retcode) {
			printk(".");
		}
		else {
			printk("X");
		}
	}
	printk("\n");

	return 0;
}

static int kestrel_shell_cmd_i2cget(const struct shell *shell, size_t argc, char **argv)
{
	uint8_t *base_address;
	int channel;
	uint8_t dev_addr;
	uint8_t dev_reg;
	uint8_t value;
	int retcode;

	if (argc < 4) {
		print_kestrel_i2cget_command_usage(shell, argv[0]);
		return -1;
	}

	channel = strtoul(argv[1], NULL, 10);
	dev_addr = strtoul(argv[2], NULL, 16);
	dev_reg = strtoul(argv[3], NULL, 16);

	base_address = (uint8_t*)(I2CMASTER0_BASE + (0x20 * channel));

	value = i2c_read_register_byte(base_address, dev_addr, dev_reg, &retcode);
	if (retcode) {
		shell_print(shell, "[ERROR] I2C read failed!");
		return -1;
	}

	shell_print(shell, "%02x: %02x", dev_reg, value);

	return 0;
}

static int kestrel_shell_cmd_i2cset(const struct shell *shell, size_t argc, char **argv)
{
	uint8_t *base_address;
	int channel;
	uint8_t dev_addr;
	uint8_t dev_reg;
	uint8_t value;
	int retcode;

	if (argc < 5) {
		print_kestrel_i2cset_command_usage(shell, argv[0]);
		return -1;
	}

	channel = strtoul(argv[1], NULL, 10);
	dev_addr = strtoul(argv[2], NULL, 16);
	dev_reg = strtoul(argv[3], NULL, 16);
	value = strtoul(argv[4], NULL, 16);

	base_address = (uint8_t*)(I2CMASTER0_BASE + (0x20 * channel));

	retcode = i2c_write_register_byte(base_address, dev_addr, dev_reg, value);
	if (retcode) {
		shell_print(shell, "[ERROR] I2C write failed!");
		return -1;
	}

	return 0;
}

static int kestrel_shell_cmd_onewire(const struct shell *shell, size_t argc, char **argv)
{
	int busses_scanned;

	if (argc < 2) {
		print_kestrel_onewire_command_usage(shell, argv[0]);
		return -1;
	}

	if (strcmp(argv[1], "scan") == 0) {
		busses_scanned = 0;

#if defined(WITH_ZEPHYR)
		scan_onewire_devices();
#elif defined(ONEWIRE0_BASE)
		scan_onewire_devices((uint8_t*)ONEWIRE0_BASE);
		busses_scanned++;
#endif

		if (busses_scanned) {
			shell_print(shell, "Scanned %d 1-wire busses", busses_scanned);
		}
		else {
			shell_print(shell, "No 1-wire bus controllers present!");
			return -1;
		}
	}
	else {
		shell_print(shell, "%s: Invalid parameter", argv[0]);
		print_kestrel_onewire_command_usage(shell, argv[0]);
		return -1;
	}

	return 0;
}

static int kestrel_shell_cmd_pwmcontrol(const struct shell *shell, size_t argc, char **argv)
{
	int device_id;
	int value;

	if (argc < 2) {
		print_kestrel_pwmcontrol_command_usage(shell, argv[0]);
		return -1;
	}

	if (strcmp(argv[1], "set") == 0) {
		// Set PWM values
		if (argc < 4) {
			shell_print(shell, "%s: Missing parameter(s)", argv[0]);
			print_kestrel_pwmcontrol_command_usage(shell, argv[0]);
			return -1;
		}

		device_id = strtoul(argv[2], NULL, 10);
		value = strtol(argv[3], NULL, 10);

		kestrel_fan_data_t * fan_data = get_fan_device_by_id(device_id);

		if (fan_data) {
			if ((value >= 0) && (value <= 100)) {
				// Valid percentage provided
				fan_data->fan_setting_percent = value;
				fan_data->fan_set_online = true;
			}
			else if (value < 0) {
				// Out of range low is an alias for turning the fan off
				fan_data->fan_setting_percent = 0;
				fan_data->fan_set_online = false;
			}
			else {
				shell_print(shell, "%s: Invalid parameter(s)", argv[0]);
				print_kestrel_pwmcontrol_command_usage(shell, argv[0]);
				return -1;
			}

			shell_print(shell, "");
		}
		else {
			shell_print(shell, "[ERROR] Invalid device ID specified!");
		}
	}
	else if (strcmp(argv[1], "show") == 0) {
		// Get PWM / tach status
		int i;

		for (i = 0; i < kestrel_fan_count; i++) {
			if (kestrel_fan_data[i].associated_tach_id_count > 0) {
				kestrel_tach_data_t * tach_data = &kestrel_tach_data[kestrel_fan_data[i].associated_tach_id[0]];
				shell_print(shell, "PWM ID %d: %.2f%% (%.2f RPM)", kestrel_fan_data[i].fan_id, (kestrel_fan_data[i].fan_set_online)?kestrel_fan_data[i].fan_readback_percent:-1, tach_data->tach_value_rpm_averaged);
			}
			else {
				shell_print(shell, "PWM ID %d: %.2f%%", kestrel_fan_data[i].fan_id, (kestrel_fan_data[i].fan_set_online)?kestrel_fan_data[i].fan_readback_percent:-1);
			}
		}
		for (i = 0; i < kestrel_tach_count; i++) {
			if (kestrel_tach_data[i].valid) {
				if (kestrel_tach_data[i].associated_fan_id_count == 0) {
					shell_print(shell, "Tach ID %d: %.2f RPM", kestrel_tach_data[i].tach_id, kestrel_tach_data[i].tach_value_rpm_averaged);
				}
			}
		}
	}
	else {
		shell_print(shell, "%s: Invalid parameter", argv[0]);
		print_kestrel_pwmcontrol_command_usage(shell, argv[0]);
		return -1;
	}

	return 0;
}

static int kestrel_shell_cmd_pidcontrol(const struct shell *shell, size_t argc, char **argv)
{
	int channel;
	int value_int;
	double value_double;

	if (argc < 2) {
		print_kestrel_pidcontrol_command_usage(shell, argv[0]);
		return -1;
	}

	if (strcmp(argv[1], "set") == 0) {
		// Set PID values
		if (argc < 5) {
			shell_print(shell, "%s: Missing parameter(s)", argv[0]);
			print_kestrel_pidcontrol_command_usage(shell, argv[0]);
			return -1;
		}

		channel = strtoul(argv[2], NULL, 10);
		value_int = strtoul(argv[4], NULL, 10);
		value_double = value_int;
		value_double /= 1000.0;

		if ((channel < 0) || (channel >= kestrel_pid_controller_count)) {
			shell_print(shell, "%s: Invalid channel", argv[0]);
			print_kestrel_pidcontrol_command_usage(shell, argv[0]);
			return -1;
		}

		if (strcmp(argv[3], "enabled") == 0) {
			kestrel_pid_controller_data[channel].enabled = value_int;
		}
		else if (strcmp(argv[3], "setpoint") == 0) {
			kestrel_pid_controller_data[channel].setpoint = value_double;
		}
		else if (strcmp(argv[3], "deadband") == 0) {
			kestrel_pid_controller_data[channel].dead_band = value_double;
		}
		else if (strcmp(argv[3], "p") == 0) {
			kestrel_pid_controller_data[channel].p = value_double;
		}
		else if (strcmp(argv[3], "i") == 0) {
			kestrel_pid_controller_data[channel].i = value_double;
		}
		else if (strcmp(argv[3], "d") == 0) {
			kestrel_pid_controller_data[channel].d = value_double;
		}
		else if (strcmp(argv[3], "error_filter_length") == 0) {
			kestrel_pid_controller_data[channel].error_filter_length = value_int;
		}
		else if (strcmp(argv[3], "d_filter_length") == 0) {
			kestrel_pid_controller_data[channel].d_filter_length = value_int;
		}
		else {
			shell_print(shell, "%s: Invalid control field", argv[0]);
		}
	}
	else if (strcmp(argv[1], "show") == 0) {
		// Show PID controller status
		int i;

		for (i = 0; i < kestrel_pid_controller_count; i++) {
			int setpoint = kestrel_pid_controller_data[i].setpoint * 1000;
			int sense = kestrel_pid_controller_data[i].sense * 1000;
			int drive = kestrel_pid_controller_data[i].drive * 1000;
			int dead_band = kestrel_pid_controller_data[i].dead_band * 1000;
			int p_coeff = kestrel_pid_controller_data[i].p * 1000;
			int i_coeff = kestrel_pid_controller_data[i].i * 1000;
			int d_coeff = kestrel_pid_controller_data[i].d * 1000;
			int integrator = kestrel_pid_controller_data[i].integrator * 1000;
			int filtered_error = kestrel_pid_controller_data[i].filtered_error * 1000;
			int filtered_rate = kestrel_pid_controller_data[i].filtered_rate * 1000;

			shell_print(shell, "PID controller %d (x1000): enabled: %d setpoint %d reading %d dead band %d drive %d coefficients: %d:%d:%d integrator: %d filtered error: %d filtered rate: %d error filter length: %ld d filter length %ld", i, kestrel_pid_controller_data[i].enabled, setpoint, sense, dead_band, drive, p_coeff, i_coeff, d_coeff, integrator, filtered_error, filtered_rate, kestrel_pid_controller_data[i].error_filter_length, kestrel_pid_controller_data[i].d_filter_length);
		}
	}
	else {
		shell_print(shell, "%s: Invalid parameter", argv[0]);
		print_kestrel_pidcontrol_command_usage(shell, argv[0]);
		return -1;
	}

	return 0;
}

static int kestrel_shell_cmd_thermal(const struct shell *shell, size_t argc, char **argv)
{
	int i;

	if (argc < 2) {
		print_kestrel_thermal_command_usage(shell, argv[0]);
		return -1;
	}

	if (strcmp(argv[1], "show") == 0) {
		for (i = 0; i < kestrel_temp_sensor_count; i++)
		{
			if (thermal_sensor_valid_last_n_ms(&kestrel_temp_sensor_data[i], SENSOR_MAX_TIMEOUT_MS))
			{
				if (strlen(kestrel_temp_sensor_data[i].description) > 0) {
					shell_print(shell, "Sensor 0x%08x [handle 0x%016llx]: %.2f° C (%.2f° F) description '%s'",
						kestrel_temp_sensor_data[i].sensor_id,
						kestrel_temp_sensor_data[i].device_handle,
						kestrel_temp_sensor_data[i].temperature_c,
						temp_celsius_to_fahrenheit(kestrel_temp_sensor_data[i].temperature_c),
						kestrel_temp_sensor_data[i].description);
				}
				else {
					shell_print(shell, "Sensor 0x%08x [handle 0x%016llx]: %.2f° C (%.2f° F)",
						kestrel_temp_sensor_data[i].sensor_id,
						kestrel_temp_sensor_data[i].device_handle,
						kestrel_temp_sensor_data[i].temperature_c,
						temp_celsius_to_fahrenheit(kestrel_temp_sensor_data[i].temperature_c));
				}
			}
			else
			{
				shell_print(shell, "Sensor 0x%08x [handle 0x%016llx]: OFFLINE",
					kestrel_temp_sensor_data[i].sensor_id,
					kestrel_temp_sensor_data[i].device_handle);
			}
		}
		if (host_cpu_temperature_sensor_count != 0)
		{
			for (i = 0; i < host_cpu_temperature_sensor_count; i++)
			{
				shell_print(shell, "Sensor 0x%08x: %d° C (%.2f° F)",
					host_cpu_temperature_sensor_data[i].sensor_id,
					host_cpu_temperature_sensor_data[i].temperature_c,
					temp_celsius_to_fahrenheit(host_cpu_temperature_sensor_data[i].temperature_c));
			}
		}
	}
	else if (strcmp(argv[1], "rescan") == 0) {
		rescan_all_thermal_sensors = true;
		shell_print(shell, "Thermal sensor rescan started in background thread");
	}
	else if (strcmp(argv[1], "rescan-full") == 0) {
		clear_all_thermal_sensors = true;
		rescan_all_thermal_sensors = true;
		shell_print(shell, "Thermal sensor full rescan started in background thread");
	}
	else {
		shell_print(shell, "%s: Invalid parameter", argv[0]);
		print_kestrel_thermal_command_usage(shell, argv[0]);
		return -1;
	}

	return 0;
}

static int kestrel_shell_cmd_gpiocontrol(const struct shell *shell, size_t argc, char **argv)
{
	int channel;
	int value;

	if (argc < 3) {
		print_kestrel_gpiocontrol_command_usage(shell, argv[0]);
		return -1;
	}

	channel = strtoul(argv[1], NULL, 10);
	value = strtoul(argv[2], NULL, 10);

	if (value) {
		gpio1_out_write(gpio1_out_read() | (0x01 << channel));
	}
	else {
		gpio1_out_write(gpio1_out_read() & ~(0x01 << channel));
	}

	shell_print(shell, "");

	return 0;
}

static int kestrel_shell_cmd_time(const struct shell *shell, size_t argc, char **argv)
{
	int ret;

	if (argc < 2) {
		print_kestrel_time_command_usage(shell, argv[0]);
		return -1;
	}

	if (strcmp(argv[1], "sync_ntp") == 0) {
		if (argc < 3) {
			shell_print(shell, "%s: Missing parameter(s)", argv[0]);
			print_kestrel_time_command_usage(shell, argv[0]);
			return -1;
		}

		ret = sync_time_with_sntp_server(argv[2]);
		if (ret) {
			shell_print(shell, "Unable to synchronize time with remote server!");
			return -1;
		}
	}
	else {
		shell_print(shell, "%s: Invalid parameter", argv[0]);
		print_kestrel_time_command_usage(shell, argv[0]);
		return -1;
	}

	return 0;
}

static int kestrel_shell_cmd_sbecontrol(const struct shell *shell, size_t argc, char **argv)
{
	if (strcmp(argv[1], "status") == 0) {
		// The OCC is accessed via the SBE, so we need to obtain the OCC mutex to avoid an SBE access collision...
		if (k_mutex_lock(&occ_access_mutex, K_MSEC(1000)) != 0) {
			printk("Unable to acquire OCC mutex in a timely manner! %s:%d\n", __FILE__, __LINE__);
			return -EAGAIN;
		}

		print_sbe_status(shell);

		k_mutex_unlock(&occ_access_mutex);
	}
	else {
		shell_print(shell, "%s: Invalid parameter", argv[0]);
		print_kestrel_sbecontrol_command_usage(shell, argv[0]);
		return -1;
	}

	return 0;
}

static int kestrel_shell_cmd_occcontrol(const struct shell *shell, size_t argc, char **argv)
{
	int sensor_number;
	uint8_t response_data[IBM_POWER9_SBE_OCC_RESPONSE_SIZE];
	uint8_t occ_version_string[17];
	size_t response_length = 0;
	struct ibm_power9_occ_poll_response_header_t *occ_response_header;

	if (argc < 2) {
		print_kestrel_occcontrol_command_usage(shell, argv[0]);
		return -1;
	}

	if (strcmp(argv[1], "poll") == 0) {
		if (k_mutex_lock(&occ_access_mutex, K_MSEC(1000)) != 0) {
			printk("Unable to acquire OCC mutex in a timely manner! %s:%d\n", __FILE__, __LINE__);
			return -EAGAIN;
		}

		if (occ_poll(0, 0x2400, IBM_POWER9_SBE_OCC_RESPONSE_SIZE, response_data, &response_length))
		{
			shell_print(shell, "OCC not responding!");
		}
		else
		{
			occ_response_header = (struct ibm_power9_occ_poll_response_header_t*)response_data;
			memcpy(occ_version_string, occ_response_header->occ_version_string, 16);
			occ_version_string[16] = 0;
			shell_print(shell, "Found OCC version '%s'", occ_version_string);
			shell_print(shell, "Found %ld temperature sensors", host_cpu_temperature_sensor_count);
			for (sensor_number = 0; sensor_number < host_cpu_temperature_sensor_count; sensor_number++)
			{
				shell_print(shell, "Temperature sensor 0x%08x: %d° C", host_cpu_temperature_sensor_data[sensor_number].sensor_id, host_cpu_temperature_sensor_data[sensor_number].temperature_c);
			}
		}

		k_mutex_unlock(&occ_access_mutex);
	}
	else {
		shell_print(shell, "%s: Invalid parameter", argv[0]);
		print_kestrel_occcontrol_command_usage(shell, argv[0]);
		return -1;
	}

	return 0;
}

static int kestrel_shell_cmd_console(const struct shell *shell, size_t argc, char **argv)
{
	const void *active_log_backend = NULL;

	VERIFY_KESTREL_START_COMPLETE(shell)

	if (host_console_service_task_requested) {
		shell_print(shell, "%s: Console already active, aborting", argv[0]);
		return -1;
	}

	if (IS_ENABLED(CONFIG_SHELL_LOG_BACKEND)) {
		active_log_backend = shell->log_backend;
		z_shell_log_backend_disable(shell->log_backend);
	}

	kestrel_console_thread_id = k_thread_create(&kestrel_console_thread_data, kestrel_console_thread_stack,
			K_THREAD_STACK_SIZEOF(kestrel_console_thread_stack),
			(k_thread_entry_t)kestrel_console_thread,
			(void*)shell, (void*)active_log_backend, NULL, KESTREL_CONSOLE_THREAD_PRIORITY, 0, K_NO_WAIT);
	k_thread_name_set(kestrel_console_thread_id, "kestrel_console");

	return 0;
}

/* Subcommand array for "kestrel" (level 1). */
SHELL_STATIC_SUBCMD_SET_CREATE(sub_kestrel,
	// Exit to main shell
	SHELL_CMD(exit, NULL, "Exit to main shell", kestrel_shell_cmd_exit),
	// Application version
	SHELL_CMD(version, NULL, "Kestrel main application version", kestrel_shell_cmd_version),
	// OpenBMC utility command (compat)
	SHELL_CMD(obmcutil, NULL, "OBMC-compatible utilty command", kestrel_shell_cmd_utility),
	// OpenBMC console command (compat)
	SHELL_CMD(obmc-console-client, NULL, "OBMC-compatible console command", kestrel_shell_cmd_console),
	// Flash programming command
	SHELL_CMD(reflash, NULL, "Simple Flash utility", kestrel_shell_cmd_reflash),
	// NVRAM commands
	SHELL_CMD(nvram, NULL, "NVRAM access", kestrel_shell_cmd_nvram),
	// Arbitrary memory dump command
	SHELL_CMD(memdump, NULL, "Memory dump tool", kestrel_shell_cmd_memdump),
	// I2C commands
	SHELL_CMD(i2cdetect, NULL, "I2C detect debug utility", kestrel_shell_cmd_i2cdetect),
	SHELL_CMD(i2cget, NULL, "I2C read debug utility", kestrel_shell_cmd_i2cget),
	SHELL_CMD(i2cset, NULL, "I2C write debug utility", kestrel_shell_cmd_i2cset),
	// 1-wire commands
	SHELL_CMD(onewire, NULL, "1-wire utilities", kestrel_shell_cmd_onewire),
	// PWM commands
	SHELL_CMD(pwmcontrol, NULL, "PWM controller", kestrel_shell_cmd_pwmcontrol),
	// PID commands
	SHELL_CMD(pidcontrol, NULL, "PID controller", kestrel_shell_cmd_pidcontrol),
	// Thermal commands
	SHELL_CMD(thermal, NULL, "Thermal command interface", kestrel_shell_cmd_thermal),
	// OCC commands
	SHELL_CMD(sbecontrol, NULL, "SBE command interface", kestrel_shell_cmd_sbecontrol),
	// OCC commands
	SHELL_CMD(occcontrol, NULL, "OCC command interface", kestrel_shell_cmd_occcontrol),
	// GPIO commands
	SHELL_CMD(gpiocontrol, NULL, "GPIO controller", kestrel_shell_cmd_gpiocontrol),
	// Time commands
	SHELL_CMD(time, NULL, "Time commands", kestrel_shell_cmd_time),
	// Command list array terminator
	SHELL_SUBCMD_SET_END
);

// Register main kestrel command prefix
SHELL_CMD_REGISTER(kestrel, &sub_kestrel, "Kestrel commands", NULL);
