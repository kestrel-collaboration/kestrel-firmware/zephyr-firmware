/*
 * Copyright (c) 2021-2024 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: GPL-3.0
 */

#include "build_config.h"

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(webportal, LOG_LEVEL_INF);

#include <stdio.h>

#include <zephyr/kernel.h>
#include <zephyr/version.h>
#include <zephyr/data/json.h>
#include <zephyr/net/http/server.h>
#include <zephyr/net/http/service.h>
#include <zephyr/storage/flash_map.h>

#include "fan_tach.h"
#include "sbe_fsi.h"
#include "sensors.h"
#include "utility.h"
#include "time.h"

#include "flash_filesystem.h"

#include "http_functions.h"
#include "webportal.h"

#include "kestrel.h"
#include <generated/mem.h>

#include "uri_encode.h"

#include "static_files.h"

#define STANDARD_HEADER(title)										\
"<head>" 												\
"<link rel=\"stylesheet\" href=\"style.css\"><meta charset=\"UTF-8\">"					\
"<title>" title "</title>"										\
"</head>"

#define SEND_STANDARD_TITLE_HEADER(state, title)							\
state.tx_buf_pos += snprintf(state.tx_buf + state.tx_buf_pos, state.tx_buf_size - state.tx_buf_pos,	\
	"%s", STANDARD_HEADER(title));

#define SEND_STANDARD_HEADER(state, title)								\
SEND_STANDARD_TITLE_HEADER(state, title)								\
SEND_DATA_BLOCK(state, embedded_file_header_inc, sizeof(embedded_file_header_inc)-1);

#define SEND_STANDARD_FOOTER(state)									\
SEND_DATA_BLOCK(state, embedded_file_footer_inc, sizeof(embedded_file_footer_inc)-1);

// Define firmware file locations
#define EMBEDDED_FILE_FPGA_FIRMWARE_RAW_POINTER	(uint8_t*)((uintptr_t)FPGASPIFLASH_BASE + FIXED_PARTITION_OFFSET(FPGA_FIRMWARE_NODE));
#define EMBEDDED_FILE_FPGA_FIRMWARE_RAW_LENGTH	FIXED_PARTITION_SIZE(FPGA_FIRMWARE_NODE);
#define EMBEDDED_FILE_BMC_FIRMWARE_RAW_POINTER	(uint8_t*)((uintptr_t)FPGASPIFLASH_BASE + FIXED_PARTITION_OFFSET(BMC_FIRMWARE_NODE) + LITEX_HEADER_LENGTH_BYTES);
#define EMBEDDED_FILE_BMC_FIRMWARE_RAW_LENGTH	FIXED_PARTITION_SIZE(BMC_FIRMWARE_NODE);
#define EMBEDDED_FILE_BMC_CONFIG_RAW_POINTER	(uint8_t*)((uintptr_t)FPGASPIFLASH_BASE + FIXED_PARTITION_OFFSET(CONFIG_PARTITION_NODE));
#define EMBEDDED_FILE_BMC_CONFIG_RAW_LENGTH	FIXED_PARTITION_SIZE(CONFIG_PARTITION_NODE);
#define EMBEDDED_FILE_BMC_STORAGE_RAW_POINTER	(uint8_t*)((uintptr_t)FPGASPIFLASH_BASE + FIXED_PARTITION_OFFSET(STORAGE_PARTITION_NODE));
#define EMBEDDED_FILE_BMC_STORAGE_RAW_LENGTH	FIXED_PARTITION_SIZE(STORAGE_PARTITION_NODE);
#define EMBEDDED_FILE_PNOR_FIRMWARE_RAW_POINTER	(uint8_t*)((uintptr_t)HOSTSPIFLASH_BASE);
#define EMBEDDED_FILE_PNOR_FIRMWARE_RAW_LENGTH	FIXED_PARTITION_SIZE(BMC_FIRMWARE_NODE);

KESTREL_WEBSERVICE_DEFINE_PAGE_HANDLER(index_html, "/", "text/html", "Kestrel SoftBMC")
KESTREL_WEBSERVICE_DEFINE_FILE_HANDLER(style_css, "/style.css", "text/css", NULL)
KESTREL_WEBSERVICE_DEFINE_FILE_HANDLER(favicon_ico, "/favicon.ico", "image/vnd.microsoft.icon", NULL)
KESTREL_WEBSERVICE_DEFINE_FILE_HANDLER(raptor_logo_jpg, "/raptor_logo.jpg", "image/jpeg", NULL)
KESTREL_WEBSERVICE_DEFINE_FILE_HANDLER(kestrel_avatar_small_png, "/kestrel_avatar_small.png", "image/png", NULL)

uint8_t license_file_decompressed[LICENSE_FILE_DECOMPRESSION_BUFFER_MAX_LENTH];
int64_t license_file_decompressed_length = 0;

int system_info_handler(zephyr_http_server_request_state_t *state)
{
	uint64_t system_uptime = k_uptime_get();
	time_t unix_time;
	struct tm *civil_time;
	parsed_uptime_stats_t parsed_uptime = parse_uptime_ms(system_uptime);

	SEND_STANDARD_HEADER((*state), "Kestrel Information")

	SEND_CUSTOM_PRINTF((*state), "<h3>BMC information</h3>");
	SEND_CUSTOM_PRINTF((*state), "<ul>\n");
	SEND_CUSTOM_PRINTF((*state), "<li>BMC OS - Zephyr " STRINGIFY(BUILD_VERSION) "</li>\n");
	SEND_CUSTOM_PRINTF((*state), "<li>Application - Kestrel v%d.%d.%d-g" STRINGIFY(APP_BUILD_VERSION) "</li>\n", KESTREL_VERSION_MAJOR, KESTREL_VERSION_MINOR, KESTREL_VERSION_PATCHLEVEL);
	SEND_CUSTOM_PRINTF((*state), "<li>Web Server - Zephyr internal</li>\n");
	SEND_CUSTOM_PRINTF((*state), "<li>Compiled - %s %s</li>\n", __DATE__, __TIME__)
	SEND_CUSTOM_PRINTF((*state), "<li>Compiler - %s %s</li>\n", COMPILER_VENDOR, COMPILER_VERSION);;
	SEND_CUSTOM_PRINTF((*state), "<li>Board - %s</li>\n", CONFIG_BOARD);
	SEND_CUSTOM_PRINTF((*state), "<li>Architecture - %s</li>\n", CONFIG_ARCH);
	unix_time = boot_timestamp_rtc;
	civil_time = gmtime(&unix_time);
	SEND_CUSTOM_PRINTF((*state), "<li>Initialized - %04d-%02d-%02dT%02d:%02d:%02d UTC</li>\n", 1900 + civil_time->tm_year, civil_time->tm_mon, civil_time->tm_mday, civil_time->tm_hour, civil_time->tm_min, civil_time->tm_sec);
	SEND_CUSTOM_PRINTF((*state), "<li>Uptime - %lld days %lld hours %lld minutes %lld seconds</li>\n", parsed_uptime.days, parsed_uptime.hours, parsed_uptime.minutes, parsed_uptime.seconds);
	if (get_current_hw_rtc_time(&unix_time)) {
		SEND_CUSTOM_PRINTF((*state), "<li>RTC - OFFLINE</li>\n");
	}
	else {
		civil_time = gmtime(&unix_time);
		SEND_CUSTOM_PRINTF((*state), "<li>RTC - online %04d-%02d-%02dT%02d:%02d:%02d UTC</li>\n", 1900 + civil_time->tm_year, civil_time->tm_mon, civil_time->tm_mday, civil_time->tm_hour, civil_time->tm_min, civil_time->tm_sec);
	}
	SEND_CUSTOM_PRINTF((*state), "</ul>\n");

	SEND_STANDARD_FOOTER((*state));

	state->http_retcode = 200;

	return 0;
}

KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER(info, "/info", "text/html", generic_rx_handler_noop, system_info_handler, MAX_REQUEST_SIZE_BYTES)

int license_info_handler(zephyr_http_server_request_state_t *state)
{
	SEND_STANDARD_HEADER((*state), "Licenses")

	SEND_CUSTOM_PRINTF((*state),"<h3>Software Licenses</h3>");
	SEND_CUSTOM_PRINTF((*state),"<pre>\n");

	SEND_DATA_BLOCK((*state), license_file_decompressed, license_file_decompressed_length);

	SEND_CUSTOM_PRINTF((*state),"</pre>\n");

	SEND_STANDARD_FOOTER((*state));

	state->http_retcode = 200;

	return 0;
}

KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER(licenses, "/licenses", "text/html", generic_rx_handler_noop, license_info_handler, 128 * 1024)

const char* host_power_status_to_string(int power_status)
{
	if (power_status == HOST_POWER_STATUS_OFFLINE) {
		return "Offline";
	}
	else if (power_status == HOST_POWER_STATUS_POWERING_ON) {
		return "Powering On";
	}
	else if (power_status == HOST_POWER_STATUS_IPLING) {
		return "Initial Program Load";
	}
	else if (power_status == HOST_POWER_STATUS_RUNNING) {
		return "Online";
	}
	else if (power_status == HOST_POWER_STATUS_POWERING_OFF) {
		return "Powering Off";
	}

	return "Unknown";
}

int host_status_handler(zephyr_http_server_request_state_t *state)
{
	int i;
	int temp_sensors_shown = 0;
	int physical_sensors_shown = 0;

	SEND_STANDARD_HEADER((*state), "Host Status")

	SEND_CUSTOM_PRINTF((*state),"<h3>Host status</h3>");
	SEND_CUSTOM_PRINTF((*state),"<p>");
	SEND_CUSTOM_PRINTF((*state),"<b>Synoptic</b><br>");
	SEND_CUSTOM_PRINTF((*state),"<div style=\"margin-left: 20px\">");
	SEND_CUSTOM_PRINTF((*state),"System - %s<br>", host_power_status_to_string(host_power_status));
	if (host_power_status == HOST_POWER_STATUS_IPLING) {
		SEND_CUSTOM_PRINTF((*state)," (ISTEP %02d.%02d)\n", (host_active_post_code >> 8) & 0xff, host_active_post_code & 0xff);
	}
	SEND_CUSTOM_PRINTF((*state),"</div>");
	SEND_CUSTOM_PRINTF((*state),"<p>");
	SEND_CUSTOM_PRINTF((*state),"<b>Mechanical</b><br>");
	SEND_CUSTOM_PRINTF((*state),"<div style=\"margin-left: 20px\">");
	SEND_CUSTOM_PRINTF((*state),"<table>\n");
	for (i = 0; i < kestrel_fan_count; i++) {
		if (kestrel_fan_data[i].valid) {
			if (strlen(kestrel_fan_data[i].description) > 0) {
				SEND_CUSTOM_PRINTF((*state),"<tr><td>%s:</td>", kestrel_fan_data[i].description);
			}
			else {
				SEND_CUSTOM_PRINTF((*state),"<tr><td>PWM channel %d:</td>", kestrel_fan_data[i].channel_id);
			}
			SEND_CUSTOM_PRINTF((*state),"<td>%.2f%%</td>", (kestrel_fan_data[i].fan_set_online)?kestrel_fan_data[i].fan_readback_percent:-1);
			if (kestrel_fan_data[i].associated_tach_id_count > 0) {
				kestrel_tach_data_t * tach_data = &kestrel_tach_data[kestrel_fan_data[i].associated_tach_id[0]];
				SEND_CUSTOM_PRINTF((*state),"<td>(%.2f RPM)</td><td>%s</td></tr>\n", tach_data->tach_value_rpm_averaged, tach_data->description);
			}
			else {
				SEND_CUSTOM_PRINTF((*state),"</td></tr>\n");
			}
		}
	}
	for (i = 0; i < kestrel_tach_count; i++) {
		if (kestrel_tach_data[i].valid) {
			if (kestrel_tach_data[i].associated_fan_id_count == 0) {
				if (strlen(kestrel_tach_data[i].description) > 0) {
					SEND_CUSTOM_PRINTF((*state),"<tr><td>%s:</td>", kestrel_tach_data[i].description);
				}
				else {
					SEND_CUSTOM_PRINTF((*state),"<tr><td>PWM channel %d:</td>", kestrel_tach_data[i].channel_id);
				}
				SEND_CUSTOM_PRINTF((*state),"<td>&nbsp;</td><td>(%.2f RPM)</td></tr>\n", kestrel_tach_data[i].tach_value_rpm_averaged);
			}
 		}
	}
	SEND_CUSTOM_PRINTF((*state),"</table>\n");
	SEND_CUSTOM_PRINTF((*state),"</div>");
	SEND_CUSTOM_PRINTF((*state),"<p>");
	SEND_CUSTOM_PRINTF((*state),"<b>Thermal</b><br>");
	SEND_CUSTOM_PRINTF((*state),"<div style=\"margin-left: 20px\">");
	SEND_CUSTOM_PRINTF((*state),"<table>\n");
	temp_sensors_shown = 0;
	for (i = 0; i < kestrel_temp_sensor_count; i++)
	{
		if (thermal_sensor_valid_last_n_ms(&kestrel_temp_sensor_data[i], SENSOR_MAX_TIMEOUT_MS))
		{
			SEND_CUSTOM_PRINTF((*state),"<tr><td>Sensor 0x%08x:</td><td> %.2f° C (%.2f° F)</td><td>[%.2f s ago]</td><td>%s</td></tr>\n",
				kestrel_temp_sensor_data[i].sensor_id,
				kestrel_temp_sensor_data[i].temperature_c,
				temp_celsius_to_fahrenheit(kestrel_temp_sensor_data[i].temperature_c),
				get_thermal_sensor_update_age_ms(&kestrel_temp_sensor_data[i]) / 1000.0,
				kestrel_temp_sensor_data[i].description);
			temp_sensors_shown = 1;
		}
		else
		{
			SEND_CUSTOM_PRINTF((*state),"<tr><td>Sensor 0x%08x:</td><td>OFFLINE</td><td>[%.2f s ago]</td><td>%s</td></tr>\n",
				kestrel_temp_sensor_data[i].sensor_id,
				get_thermal_sensor_update_age_ms(&kestrel_temp_sensor_data[i]) / 1000.0,
				kestrel_temp_sensor_data[i].description);
			temp_sensors_shown = 1;
		}
	}
	if (host_cpu_temperature_sensor_count != 0)
	{
		for (i = 0; i < host_cpu_temperature_sensor_count; i++)
		{
			SEND_CUSTOM_PRINTF((*state),"<tr><td>Sensor 0x%08x:</td><td> %d° C (%.2f° F)</td><td></td><td></td></tr>\n",
				host_cpu_temperature_sensor_data[i].sensor_id,
				host_cpu_temperature_sensor_data[i].temperature_c,
				temp_celsius_to_fahrenheit(host_cpu_temperature_sensor_data[i].temperature_c));
			temp_sensors_shown = 1;
		}
	}
	if (!temp_sensors_shown)
	{
		SEND_CUSTOM_PRINTF((*state),"<tr><td><I>No thermal sensors currently configured</I></td></tr>\n");
	}
	SEND_CUSTOM_PRINTF((*state),"</table>\n");
	SEND_CUSTOM_PRINTF((*state),"</div>");
	SEND_CUSTOM_PRINTF((*state),"<p>");
	SEND_CUSTOM_PRINTF((*state),"<b>Physical</b><br>");
	SEND_CUSTOM_PRINTF((*state),"<div style=\"margin-left: 20px\">");
	SEND_CUSTOM_PRINTF((*state),"<table>\n");
	physical_sensors_shown = 0;
	for (i = 0; i < kestrel_pressure_sensor_count; i++)
	{
		if (pressure_sensor_valid_last_n_ms(&kestrel_pressure_sensor_data[i], SENSOR_MAX_TIMEOUT_MS))
		{
			SEND_CUSTOM_PRINTF((*state),"<tr><td>Sensor 0x%08x:</td><td>pressure %.2f kPa</td><td>[%.2f s ago]</td><td>%s</td></tr>\n",
				kestrel_pressure_sensor_data[i].sensor_id,
				kestrel_pressure_sensor_data[i].pressure_pa / 1000.0,
				get_pressure_sensor_update_age_ms(&kestrel_pressure_sensor_data[i]) / 1000.0,
				kestrel_pressure_sensor_data[i].description);
			physical_sensors_shown = 1;
		}
		else
		{
			SEND_CUSTOM_PRINTF((*state),"<tr><td>Sensor 0x%08x:</td><td>OFFLINE</td><td>[%.2f s ago]</td><td>%s</td></tr>\n",
				kestrel_pressure_sensor_data[i].sensor_id,
				get_pressure_sensor_update_age_ms(&kestrel_pressure_sensor_data[i]) / 1000.0,
				kestrel_pressure_sensor_data[i].description);
			physical_sensors_shown = 1;
		}
	}
	if (!physical_sensors_shown)
	{
		SEND_CUSTOM_PRINTF((*state),"<tr><td><I>No physical sensors currently configured</I></td></tr>\n");
	}
	SEND_CUSTOM_PRINTF((*state),"</table>\n");
	SEND_CUSTOM_PRINTF((*state),"</div>");
	SEND_CUSTOM_PRINTF((*state),"<p>");
	SEND_CUSTOM_PRINTF((*state),"<b>Control</b><br>");
	SEND_CUSTOM_PRINTF((*state),"<div style=\"margin-left: 20px\">");
	SEND_CUSTOM_PRINTF((*state),"<a href=\"/powercontrol?command=poweron\">Power on</a><br>");
	SEND_CUSTOM_PRINTF((*state),"<a href=\"/powercontrol?command=poweroff\">Power off</a>");
	SEND_CUSTOM_PRINTF((*state),"</div>");

	SEND_STANDARD_FOOTER((*state));

	state->http_retcode = 200;

	return 0;
}

KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER(hoststatus, "/hoststatus", "text/html", generic_rx_handler_noop, host_status_handler, MAX_REQUEST_SIZE_BYTES)

int power_control_handler(zephyr_http_server_request_state_t *state)
{
	char command_buffer[128];

	// Parse input
	*((char *)(state->rx_buf + state->rx_data_len)) = 0;

	if (parse_uri_query_string_find_value_for_key(state->rx_buf, state->rx_data_len, "command", command_buffer, sizeof(command_buffer))) {
		command_buffer[0] = 0;
	}

	SEND_STANDARD_HEADER((*state), "Power control")

	SEND_CUSTOM_PRINTF((*state),"<h3>Command processing</h3>");
	SEND_CUSTOM_PRINTF((*state),"<p>");

	if (strcmp(command_buffer, "poweron") == 0) {
		if (host_power_status == HOST_POWER_STATUS_OFFLINE) {
			SEND_CUSTOM_PRINTF((*state),"Powering on host");
			power_on_host();
		}
		else {
			SEND_CUSTOM_PRINTF((*state),"Host already online");
		}
	}
	else if (strcmp(command_buffer, "poweroff") == 0) {
		if (host_power_status == HOST_POWER_STATUS_OFFLINE) {
			SEND_CUSTOM_PRINTF((*state),"Host already offline");
		}
		else {
			SEND_CUSTOM_PRINTF((*state),"Powering off host");
			power_off_chassis(0);
		}
	}
	else {
		SEND_CUSTOM_PRINTF((*state),"Invalid command!");
	}
	SEND_CUSTOM_PRINTF((*state),"<p>");
	SEND_CUSTOM_PRINTF((*state),"<a href=\"/hoststatus\">Back to host status page</a>");

	SEND_STANDARD_FOOTER((*state));

	state->http_retcode = 200;

	return 0;
}
KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER(powercontrol, "/powercontrol", "text/html", generic_rx_handler_noop, power_control_handler, MAX_REQUEST_SIZE_BYTES)

int firmware_upload_form_handler(zephyr_http_server_request_state_t *state)
{
	SEND_STANDARD_HEADER((*state), "Firmware Management")

	SEND_CUSTOM_PRINTF((*state),"<h3>Firmware Upload</h3>");
	SEND_CUSTOM_PRINTF_CUSTOM_FORMAT((*state),
	          "<form action=\"%s\" method=\"POST\" "
	          "enctype=\"multipart/form-data\">\n", "/firmware.upload");
	SEND_CUSTOM_PRINTF((*state),"<input type=\"file\" name=\"firmwarefile\" multiple>\n");
	SEND_CUSTOM_PRINTF((*state),"<input type=\"submit\" value=\"Upload\">\n");
	SEND_CUSTOM_PRINTF((*state),"</form>\n");

	SEND_CUSTOM_PRINTF((*state),"<p>\n");
	SEND_CUSTOM_PRINTF((*state),"<h3>Firmware Download</h3>\n");
	SEND_CUSTOM_PRINTF((*state),"<b>FPGA</b><br><ul>\n");
	SEND_CUSTOM_PRINTF((*state),"<li><a href=\"/firmware/fpga.img\">FPGA</a></li>\n");
	SEND_CUSTOM_PRINTF((*state),"</ul><p>\n");
	SEND_CUSTOM_PRINTF((*state),"<b>BMC</b><br><ul>\n");
	SEND_CUSTOM_PRINTF((*state),"<li><a href=\"/firmware/bmc.img\">BMC</a><br>\n");
	SEND_CUSTOM_PRINTF((*state),"<li><a href=\"/firmware/config.img\">BMC Configuration</a><br></li>\n");
	SEND_CUSTOM_PRINTF((*state),"<li><a href=\"/firmware/storage.img\">BMC Storage</a></li>\n");
	SEND_CUSTOM_PRINTF((*state),"</ul><p>\n");
	SEND_CUSTOM_PRINTF((*state),"<b>Host System</b><br><ul>\n");
	SEND_CUSTOM_PRINTF((*state),"<li><a href=\"/firmware/pnor.img\">PNOR</a></li>\n");
	SEND_CUSTOM_PRINTF((*state),"</ul>\n");

	SEND_STANDARD_FOOTER((*state));

	state->http_retcode = 200;

	return 0;
}

KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER(firmware, "/firmware", "text/html", generic_rx_handler_noop, firmware_upload_form_handler, MAX_REQUEST_SIZE_BYTES)

int firmware_download_handler(zephyr_http_server_request_state_t *state)
{
	size_t to_send = state->file_len - state->total_sent;
	if (to_send > MAX_REQUEST_SIZE_BYTES) {
		to_send = MAX_REQUEST_SIZE_BYTES;
	}

	SEND_DATA_BLOCK((*state), state->file_ptr + state->total_sent, to_send);

	if (state->total_sent + to_send >= state->file_len) {
		// All data sent!
		state->http_retcode = 200;
		return 0;
	}

	// Need to send more data
	return 1;
}

KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER_FULL(firmware_fpga_img, "/firmware/fpga.img", "application/octet-stream", BIT(HTTP_GET), EMBEDDED_FILE_FPGA_FIRMWARE_RAW_POINTER, EMBEDDED_FILE_FPGA_FIRMWARE_RAW_LENGTH, generic_rx_handler_noop, firmware_download_handler, MAX_REQUEST_SIZE_BYTES)
KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER_FULL(firmware_bmc_img, "/firmware/bmc.img", "application/octet-stream", BIT(HTTP_GET), EMBEDDED_FILE_BMC_FIRMWARE_RAW_POINTER, EMBEDDED_FILE_BMC_FIRMWARE_RAW_LENGTH, generic_rx_handler_noop, firmware_download_handler, MAX_REQUEST_SIZE_BYTES)
KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER_FULL(firmware_bmc_config_img, "/firmware/config.img", "application/octet-stream", BIT(HTTP_GET), EMBEDDED_FILE_BMC_CONFIG_RAW_POINTER, EMBEDDED_FILE_BMC_CONFIG_RAW_LENGTH, generic_rx_handler_noop, firmware_download_handler, MAX_REQUEST_SIZE_BYTES)
KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER_FULL(firmware_bmc_storage_img, "/firmware/storage.img", "application/octet-stream", BIT(HTTP_GET), EMBEDDED_FILE_BMC_STORAGE_RAW_POINTER, EMBEDDED_FILE_BMC_STORAGE_RAW_LENGTH, generic_rx_handler_noop, firmware_download_handler, MAX_REQUEST_SIZE_BYTES)
KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER_FULL(firmware_pnor_img, "/firmware/pnor.img", "application/octet-stream", BIT(HTTP_GET), EMBEDDED_FILE_PNOR_FIRMWARE_RAW_POINTER, EMBEDDED_FILE_PNOR_FIRMWARE_RAW_LENGTH, generic_rx_handler_noop, firmware_download_handler, MAX_REQUEST_SIZE_BYTES)

void firmware_upload_handler_rx(zephyr_http_server_request_state_t *state)
{
	// Parse form upload
	static http_multipart_form_parser_state_t parser_state = {0};
	static size_t last_log_print_file_offset = 0;

	parse_http_multipart_form_run(&parser_state, state->rx_buf, state->rx_data_len);

	if (state->status == HTTP_SERVER_DATA_ABORTED) {
		main_firmware_buffer.locked = 0;
		main_firmware_buffer.valid_bytes = 0;
		parser_state.sliding_window_pos = 0;
		parser_state.current_state = 0;
		last_log_print_file_offset = 0;
	}
	else {
		if (parser_state.current_state == 2) {
			if (!main_firmware_buffer.locked) {
				main_firmware_buffer.locked = 1;
				main_firmware_buffer.overflow = 0;
				main_firmware_buffer.current_write_offset = 0;
				main_firmware_buffer.valid_bytes = 0;
			}

			state->rx_data_len = state->rx_data_len - parser_state.data_offset_in_current_chunk;

			struct firmware_buffer_region *fw_data = &main_firmware_buffer;
	
			if ((fw_data->current_write_offset + state->rx_data_len) > fw_data->buffer_length) {
				// Buffer overflow!
				// Discard the extra data...
				state->rx_data_len = fw_data->buffer_length - fw_data->current_write_offset;
				fw_data->overflow = 1;
			}
			if ((fw_data->current_write_offset - last_log_print_file_offset) > (64 * 1024)) {
				// Printing status every 64k shouldn't swamp the logger subsystem
				LOG_INF("File upload in progress, received %ld byte chunk, %ldkB transferred so far", state->rx_data_len, fw_data->current_write_offset / 1024);
				last_log_print_file_offset = fw_data->current_write_offset;
			}
			memcpy(fw_data->buffer_address + fw_data->current_write_offset, state->rx_buf + parser_state.data_offset_in_current_chunk, state->rx_data_len);
			fw_data->current_write_offset += state->rx_data_len;
			fw_data->valid_bytes = fw_data->current_write_offset;

			if (state->status == HTTP_SERVER_DATA_FINAL) {
				parser_state.sliding_window_pos = 0;
				parser_state.current_state = 0;
				last_log_print_file_offset = 0;

				parse_http_multipart_form_finalize(&parser_state, fw_data->buffer_address, &fw_data->valid_bytes);
			}
		}
	}
}

int firmware_upload_handler_tx(zephyr_http_server_request_state_t *state)
{
	LOG_INF("File upload complete, received %lldkB", main_firmware_buffer.valid_bytes / 1024);

	// Clear out the remainder of the buffer
	memset(main_firmware_buffer.buffer_address + main_firmware_buffer.valid_bytes, 0xff, main_firmware_buffer.buffer_length - main_firmware_buffer.valid_bytes);

	main_firmware_buffer.locked = 0;

	SEND_STANDARD_HEADER((*state), "Firmware Uploaded")

	SEND_CUSTOM_PRINTF((*state),"<h3>Firmware Uploaded</h3>");
	SEND_CUSTOM_PRINTF((*state),"<p>");
	SEND_CUSTOM_PRINTF((*state),"File(s) uploaded:<br>");
	SEND_CUSTOM_PRINTF((*state),"%i files<br>", 1);
	SEND_CUSTOM_PRINTF((*state),"%lld bytes<br>", main_firmware_buffer.valid_bytes);

	if (main_firmware_buffer.overflow) {
		SEND_CUSTOM_PRINTF((*state),"<p>WARNING: Data was discarded due to buffer overflow!");
		SEND_STANDARD_FOOTER((*state));
		state->http_retcode = 500;
	}

	SEND_STANDARD_FOOTER((*state));
	state->http_retcode = 200;

	return 0;
}

KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER_FULL(firmware_upload, "/firmware.upload", "text/html", BIT(HTTP_POST), NULL, 0, firmware_upload_handler_rx, firmware_upload_handler_tx, MAX_REQUEST_SIZE_BYTES)
