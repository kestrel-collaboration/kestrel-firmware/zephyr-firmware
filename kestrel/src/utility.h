// © 2020 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _UTILITY_H
#define _UTILITY_H

#include "build_config.h"

typedef struct parsed_uptime_stats {
	uint64_t days;
	uint64_t hours;
	uint64_t minutes;
	uint64_t seconds;
	uint64_t milliseconds;
} parsed_uptime_stats_t;

#if !(WITH_ZEPHYR)
void usleep(int usecs);
#endif

#if (WITH_ZEPHYR)
int kestrel_strftime(char * buffer, int buffer_length, uint64_t timestamp);
double fabs(double value);
#endif

parsed_uptime_stats_t parse_uptime_ms(uint64_t uptime);

#endif // _UTILITY_H