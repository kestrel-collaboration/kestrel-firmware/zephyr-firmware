// © 2021-2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _WEBPORTAL_H
#define _WEBPORTAL_H

#define LICENSE_FILE_DECOMPRESSION_BUFFER_MAX_LENTH 262144

extern uint8_t license_file_decompressed[LICENSE_FILE_DECOMPRESSION_BUFFER_MAX_LENTH];
extern int64_t license_file_decompressed_length;

#endif // _WEBPORTAL_H
