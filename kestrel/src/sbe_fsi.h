// © 2020 - 2022 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

#ifndef _SBE_FSI_H
#define _SBE_FSI_H

#include <stdint.h>

#define IBM_POWER9_SBE_STATUS_BOOTED		(1 << 31)
#define IBM_POWER9_SBE_HW_FFDC_PRESENT		(1 << 30)

#define IBM_POWER9_SBE_FIFO_STA_PARITY_ERR	(1 << 29)
#define IBM_POWER9_SBE_FIFO_STA_RESET_REQ	(1 << 25)
#define IBM_POWER9_SBE_FIFO_STA_GOT_EOT		(1 << 23)
#define IBM_POWER9_SBE_FIFO_STA_FULL		(1 << 21)
#define IBM_POWER9_SBE_FIFO_STA_EMPTY		(1 << 20)

#define IBM_POWER9_SBE_FIFO_STA_EOT_MASK	0xff
#define IBM_POWER9_SBE_FIFO_STA_EOT_SHIFT	0
#define IBM_POWER9_SBE_FIFO_STA_EOT_HIGH_BIT	(1 << 7)

#define IBM_POWER9_SBE_STATE_MASK		0xf
#define IBM_POWER9_SBE_STATE_SHIFT		20

#define IBM_POWER9_SBE_STATE_UNKNOWN		0x0
#define IBM_POWER9_SBE_STATE_IPLING		0x1
#define IBM_POWER9_SBE_STATE_ISTEP		0x2
#define IBM_POWER9_SBE_STATE_MPIPL		0x3
#define IBM_POWER9_SBE_STATE_RUNTIME		0x4
#define IBM_POWER9_SBE_STATE_DMT		0x5
#define IBM_POWER9_SBE_STATE_DUMP		0x6
#define IBM_POWER9_SBE_STATE_FAILED		0x7
#define IBM_POWER9_SBE_STATE_QUIESCED		0x8

// IBM_POWER9 SBE FIFO register banks
#define IBM_POWER9_SBE_FIFO_UP			0x00
#define IBM_POWER9_SBE_FIFO_DOWN		0x40

// IBM_POWER9 SBE FIFO registers
#define IBM_POWER9_SBE_FIFO_FIFO		0x00
#define IBM_POWER9_SBE_FIFO_STAT		0x04

// IBM_POWER9_SBE FIFO commands
#define IBM_POWER9_SBE_FIFO_CMD_EOT_RAISE	0x0008
#define IBM_POWER9_SBE_FIFO_CMD_EOT_ACK		0x0014
#define IBM_POWER9_SBE_FIFO_CMD_REQUEST_RESET	0x000c
#define IBM_POWER9_SBE_FIFO_CMD_PERFORM_RESET	0x0010
#define IBM_POWER9_SBE_FIFO_CMD_GET_OCC_SRAM	0xa403
#define IBM_POWER9_SBE_FIFO_CMD_PUT_OCC_SRAM	0xa404
#define IBM_POWER9_SBE_FIFO_CMD_GET_FFDC	0xa801

// IBM POWER9 OCC constants
#define IBM_POWER9_SBE_OCC_FFDC_MAX_SIZE	0x2000
#define IBM_POWER9_SBE_OCC_CMD_SIZE		0xffa
#define IBM_POWER9_SBE_OCC_CMD_HEADER_LEN	0x5
#define IBM_POWER9_SBE_OCC_CMD_MAX_SIZE		(IBM_POWER9_SBE_OCC_CMD_SIZE + IBM_POWER9_SBE_OCC_CMD_HEADER_LEN)
#define IBM_POWER9_SBE_OCC_RESPONSE_SIZE	0xff9
#define IBM_POWER9_SBE_OCC_RESPONSE_HEADER_LEN	0x6
#define IBM_POWER9_SBE_OCC_RESPONSE_MAX_SIZE	(IBM_POWER9_SBE_OCC_RESPONSE_SIZE + IBM_POWER9_SBE_OCC_RESPONSE_HEADER_LEN)
#define IBM_POWER9_SBE_OCC_STATUS_LEN_BYTES	0x80
#define IBM_POWER9_SBE_FIFO_OCC_SRAM_CMD_ADDR	0xfffbe000
#define IBM_POWER9_SBE_FIFO_OCC_SRAM_RSP_ADDR	0xfffbf000
#define IBM_POWER9_SBE_FIFO_OCC_XFR_MODE_NORMAL	1
#define IBM_POWER9_SBE_FIFO_OCC_XFR_MODE_CIRC	3
#define IBM_POWER9_SBE_FIFO_OCC_CMD_ATTN	0x20010000

// IBM POWER9 OCC command codes
#define IBM_POWER9_OCC_CMD_POLL			0x20

// IBM POWER9 OCC response codes
#define IBM_POWER9_OCC_RESP_SUCCESS		0x00
#define IBM_POWER9_OCC_RESP_CMD_INVAL		0x11
#define IBM_POWER9_OCC_RESP_CMD_LEN_INVAL	0x12
#define IBM_POWER9_OCC_RESP_DATA_INVAL		0x13
#define IBM_POWER9_OCC_RESP_CHKSUM_ERR		0x14
#define IBM_POWER9_OCC_RESP_INT_ERR		0x15
#define IBM_POWER9_OCC_RESP_BAD_STATE		0x16
#define IBM_POWER9_OCC_RESP_CRIT_EXCEPT		0xe0
#define IBM_POWER9_OCC_RESP_CRIT_INIT		0xe1
#define IBM_POWER9_OCC_RESP_CRIT_WATCHDOG	0xe2
#define IBM_POWER9_OCC_RESP_CRIT_OCB		0xe3
#define IBM_POWER9_OCC_RESP_CRIT_HW		0xe4
#define IBM_POWER9_OCC_RESP_CMD_IN_PRG		0xff

#define IBM_POWER9_OCC_MAX_TEMP_SENSORS		256

struct occ_response_t
{
	uint8_t sequence_number;
	uint8_t cmd_type;
	uint8_t return_status;
	uint16_t length;
	uint16_t checksum;
};

struct __packed ibm_power9_occ_poll_response_header_t
{
	uint8_t status;
	uint8_t extended_status;
	uint8_t occ_present;
	uint8_t configuration_data;
	uint8_t occ_state;
	uint8_t mode;
	uint8_t ips_status;
	uint8_t error_log_id;
	uint32_t error_log_start_address;
	uint16_t error_log_length;
	uint16_t reserved;
	uint8_t occ_version_string[16];
	uint8_t label[6];
	uint8_t sensor_data_block_count;
	uint8_t sensor_data_block_header_version;
	void *sensor_block_data;
};

struct __packed ibm_power9_occ_sensor_data_block_header_t {
	uint8_t label[4];
	uint8_t reserved;
	uint8_t sensor_format;
	uint8_t sensor_length;
	uint8_t sensor_count;
	void *sensor_data;
};

struct __packed ibm_power9_occ_sensor_data_temp_v2_t {
	uint32_t sensor_id;
	uint8_t fru_type;
	uint8_t value;
};

struct ibm_power9_occ_sensor_data_t {
	uint32_t sensor_id;
	uint8_t temperature_c;
};

extern struct ibm_power9_occ_sensor_data_t host_cpu_temperature_sensor_data[IBM_POWER9_OCC_MAX_TEMP_SENSORS];
extern size_t host_cpu_temperature_sensor_count;

int initialize_sbe_fifo_access();
int gather_ffdc_dump(int slave_id, uint32_t base_fsi_address);
int occ_putsram(int slave_id, uint32_t base_fsi_address, uint8_t *command_data, size_t length);
int occ_submit(int slave_id, uint32_t base_fsi_address, uint8_t *request_data, size_t request_length, size_t expected_response_length, uint8_t *response_data, size_t *response_length);
int occ_poll(int slave_id, uint32_t base_fsi_address, size_t expected_response_length, uint8_t *response_data, size_t *response_length);

#endif // _SBE_FSI_H
