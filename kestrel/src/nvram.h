// © 2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _NVRAM_H
#define _NVRAM_H

#include <stdint.h>
#include <stdlib.h>

#define NVRAM_MAX_KEY_VALUE_PAIRS 1024
#define NVRAM_JSON_STRING_BUFFER_SIZE 65536

struct __attribute__((packed, aligned(1))) json_nvram_v1_key_value_pair {
	const char *key;
	const char *value;
};

struct __attribute__((packed, aligned(1))) json_nvram_v1_key_value_list {
	const char *list_name;
	int list_version;
	int list_revision;
	struct json_nvram_v1_key_value_pair elements[NVRAM_MAX_KEY_VALUE_PAIRS];
	size_t element_count;
};

extern struct json_nvram_v1_key_value_list * nvram_settings;
extern struct k_sem nvram_access_lock;

struct json_nvram_v1_key_value_list * load_nvram_settings_from_persistent_storage(void);
int write_nvram_settings_to_persistent_storage(struct json_nvram_v1_key_value_list * settings);

int nvram_get_index_of_key(struct json_nvram_v1_key_value_list * settings, const char * key);
const char * nvram_get_value_for_key(struct json_nvram_v1_key_value_list * settings, const char * key);
int nvram_set_key_value_or_create_pair(struct json_nvram_v1_key_value_list * settings, const char * key, const char * value);

#endif // _NVRAM_H
