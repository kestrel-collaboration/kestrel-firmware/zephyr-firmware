/*
 * Copyright (c) 2021-2024 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: GPL-3.0
 */

#include "build_config.h"

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(redfish, LOG_LEVEL_INF);

#include <stdio.h>

#include <zephyr/kernel.h>
#include <zephyr/data/json.h>
#include <zephyr/net/http/server.h>
#include <zephyr/net/http/service.h>

#include "fan_tach.h"
#include "sbe_fsi.h"
#include "sensors.h"
#include "utility.h"
#include "time.h"

#include "http_functions.h"
#include "redfish.h"

#include "kestrel.h"
#include <generated/mem.h>

#define STANDARD_HEADER(title)										\
"<head>" 												\
"<title>" title "</title>"										\
"</head>"

#define SEND_STANDARD_TITLE_HEADER(state, title)							\
state.tx_buf_pos += snprintf(state.tx_buf + state.tx_buf_pos, state.tx_buf_size - state.tx_buf_pos,	\
	"%s", STANDARD_HEADER(title));

#define SEND_STANDARD_HEADER(conn, title)								\

#define SEND_STANDARD_FOOTER(conn)
#define STANDARD_FOOTER_SIZE 0

// Generate static strings
static const char* redfish_static_string_empty = "";

static const char* redfish_static_string_pressure = "Pressure";
static const char* redfish_static_string_pa = "Pa";
static const char* redfish_static_string_kpa = "kPa";
static const char* redfish_static_string_percent = "Percent";
static const char* redfish_static_string_physical = "Physical";
static const char* redfish_static_string_physicalsensor = "PhysicalSensor";
static const char* redfish_static_string_rpm = "RPM";
static const char* redfish_static_string_thermal = "Thermal";
static const char* redfish_static_string_sensor = "Sensor";

static const char* redfish_static_string_ok = "OK";
static const char* redfish_static_string_off = "Off";
static const char* redfish_static_string_on = "On";
static const char* redfish_static_string_enabled = "Enabled";
static const char* redfish_static_string_disabled = "Disabled";
static const char* redfish_static_string_warning = "Warning";
static const char* redfish_static_string_critical = "Critical";
static const char* redfish_static_string_standbyoffline = "StandbyOffline";
static const char* redfish_static_string_computer_system = "Computer System";

static const char* redfish_static_string_chassis = "Chassis";
static const char* redfish_static_string_forceoff = "ForceOff";
//static const char* redfish_static_string_gracefulrestart = "GracefulRestart";
//static const char* redfish_static_string_gracefulshutdown = "GracefulShutdown";

const char* redfish_base_path = REDFISH_BASE_PATH "/";
const char* system_uuid = "00000000-0000-0000-0000-000000000000";

struct __attribute__((packed, aligned(1))) json_redfish_version {
	const char *v1;
};

struct __attribute__((packed, aligned(1))) json_redfish_odata_string_entry {
	const char *id;
};

struct __attribute__((packed, aligned(1))) json_redfish_status_condition_entry {
	const char *message;
	const char *message_id;
	const char *resolution;
	const char *severity;
	const char *timestamp;
};

struct __attribute__((packed, aligned(1))) json_redfish_resource_status_entry {
	struct json_redfish_status_condition_entry conditions[REDFISH_MAX_STATUS_CONDITION_COUNT];
	size_t conditions_count;
	const char *health;
	const char *health_rollup;
	const char *state;
};

struct __attribute__((packed, aligned(1))) json_redfish_chassis_thermal_entry {
	const char *odata_id;
};

static const struct json_obj_descr json_redfish_version_desc[] = {
	JSON_OBJ_DESCR_PRIM(struct json_redfish_version, v1, JSON_TOK_STRING),
};

static const struct json_obj_descr json_redfish_odata_string_entry_desc[] = {
        JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_odata_string_entry, "@odata.id", id, JSON_TOK_STRING),
};

static const struct json_obj_descr json_redfish_status_condition_entry_desc[] = {
        JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_status_condition_entry, "Message", message, JSON_TOK_STRING),
        JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_status_condition_entry, "MessageId", message_id, JSON_TOK_STRING),
        JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_status_condition_entry, "Resolution", resolution, JSON_TOK_STRING),
        JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_status_condition_entry, "Severity", severity, JSON_TOK_STRING),
        JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_status_condition_entry, "Timestamp", timestamp, JSON_TOK_STRING),
};

static const struct json_obj_descr json_redfish_resource_status_entry_desc[] = {
	JSON_OBJ_DESCR_OBJ_ARRAY_NAMED(struct json_redfish_resource_status_entry, "Conditions", conditions, REDFISH_MAX_STATUS_CONDITION_COUNT, conditions_count, json_redfish_status_condition_entry_desc, ARRAY_SIZE(json_redfish_status_condition_entry_desc)),
        JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_resource_status_entry, "Health", health, JSON_TOK_STRING),
        JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_resource_status_entry, "HealthRollup", health_rollup, JSON_TOK_STRING),
        JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_resource_status_entry, "State", state, JSON_TOK_STRING),
};

static const struct json_obj_descr json_redfish_chassis_thermal_entry_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_chassis_thermal_entry, "@odata.id", odata_id, JSON_TOK_STRING),
};

struct __attribute__((packed, aligned(1))) json_redfish_v1_service_list {
	const char *odata_context;
	const char *odata_id;
	const char *odata_type;
	struct json_redfish_odata_string_entry chassis;
	struct json_redfish_odata_string_entry systems;
	const char *uuid;
};

static const struct json_obj_descr json_redfish_v1_service_list_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_service_list, "@odata.context", odata_context, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_service_list, "@odata.id", odata_id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_service_list, "@odata.type", odata_type, JSON_TOK_STRING),
	JSON_OBJ_DESCR_OBJECT_NAMED(struct json_redfish_v1_service_list, "Chassis", chassis, json_redfish_odata_string_entry_desc),
	JSON_OBJ_DESCR_OBJECT_NAMED(struct json_redfish_v1_service_list, "Systems", systems, json_redfish_odata_string_entry_desc),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_service_list, "UUID", uuid, JSON_TOK_STRING),
};

struct __attribute__((packed, aligned(1))) json_redfish_v1_chassis_list {
	const char *odata_context;
	const char *odata_id;
	const char *odata_type;
	struct json_redfish_odata_string_entry members[1];
	size_t members_count;
	const char *name;
};

static const struct json_obj_descr json_redfish_v1_chassis_list_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_list, "@odata.context", odata_context, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_list, "@odata.id", odata_id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_list, "@odata.type", odata_type, JSON_TOK_STRING),
	JSON_OBJ_DESCR_OBJ_ARRAY_NAMED(struct json_redfish_v1_chassis_list, "Members", members, 1, members_count, json_redfish_odata_string_entry_desc, ARRAY_SIZE(json_redfish_odata_string_entry_desc)),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_list, "Members@odata.count", members_count, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_list, "Name", name, JSON_TOK_STRING),
};

// TODO
// Implement remaining objects (Actions, RelatedItem)
struct __attribute__((packed, aligned(1))) json_redfish_v1_thermal_temperature {
	const char *odata_id;
	const char *description;
	const char *id;
	const char *name;

	int adjusted_max_allowable_operating_value;
	int adjusted_min_allowable_operating_value;
	const char *delta_physical_context;
	double delta_reading_celsius;
	double lower_threshold_critical;
	double lower_threshold_fatal;
	double lower_threshold_noncritical;
	int max_allowable_operating_value;
	double max_reading_range_temp;
	const char *member_id;
	int min_allowable_operating_value;
	double min_reading_range_temp;
	const char *physical_context;
	double reading_celsius;
	int sensor_number;
	struct json_redfish_resource_status_entry status;
	double upper_threshold_critical;
	double upper_threshold_fatal;
	double upper_threshold_noncritical;
};

static const struct json_obj_descr json_redfish_v1_thermal_temperature_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "@odata.id", odata_id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "Description", description, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "Id", id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "Name", name, JSON_TOK_STRING),

	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "AdjustedMaxAllowableOperatingValue", adjusted_max_allowable_operating_value, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "AdjustedMinAllowableOperatingValue", adjusted_min_allowable_operating_value, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "DeltaPhysicalContext", delta_physical_context, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "DeltaReadingCelsius", delta_reading_celsius, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "LowerThresholdCritical", lower_threshold_critical, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "LowerThresholdFatal", lower_threshold_fatal, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "LowerThresholdNonCritical", lower_threshold_noncritical, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "MaxAllowableOperatingValue", max_allowable_operating_value, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "MaxReadingRangeTemp", max_reading_range_temp, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "MemberId", member_id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "MinAllowableOperatingValue", min_allowable_operating_value, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "MinReadingRangeTemp", min_reading_range_temp, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "PhysicalContext", physical_context, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "ReadingCelsius", reading_celsius, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "SensorNumber", sensor_number, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_OBJECT_NAMED(struct json_redfish_v1_thermal_temperature, "Status", status, json_redfish_resource_status_entry_desc),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "UpperThresholdCritical", upper_threshold_critical, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "UpperThresholdFatal", upper_threshold_fatal, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_temperature, "UpperThresholdNonCritical", upper_threshold_noncritical, JSON_TOK_FLOAT),
};

// TODO
// Implement remaining objects (Actions, Location, Redundancy, RelatedItem)
struct __attribute__((packed, aligned(1))) json_redfish_v1_thermal_fan {
	const char *odata_id;
	const char *description;
	const char *id;
	const char *name;

	bool hotpluggable;
	const char *indicator_led;
	double lower_threshold_critical;
	double lower_threshold_fatal;
	double lower_threshold_noncritical;
	const char *manufacturer;
	double max_reading_range;
	const char *member_id;
	double min_reading_range;
	const char *model;
	const char *part_number;
	const char *physical_context;
	int reading;
	const char *reading_units;
	int sensor_number;
	const char *serial_number;
	const char *spare_part_number;
	double speed_rpm;
	struct json_redfish_resource_status_entry status;
	double upper_threshold_critical;
	double upper_threshold_fatal;
	double upper_threshold_noncritical;
};

static const struct json_obj_descr json_redfish_v1_thermal_fan_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "@odata.id", odata_id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "Description", description, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "Id", id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "Name", name, JSON_TOK_STRING),

	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "HotPluggable", hotpluggable, JSON_TOK_TRUE),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "IndicatorLED", indicator_led, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "LowerThresholdCritical", lower_threshold_critical, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "LowerThresholdFatal", lower_threshold_fatal, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "LowerThresholdNonCritical", lower_threshold_noncritical, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "Manufacturer", manufacturer, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "MaxReadingRange", max_reading_range, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "MemberId", member_id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "MinReadingRange", min_reading_range, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "Model", model, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "PartNumber", part_number, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "PhysicalContext", physical_context, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "Reading", reading, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "ReadingUnits", reading_units, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "SensorNumber", sensor_number, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "SerialNumber", serial_number, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "SparePartNumber", spare_part_number, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "SpeedRPM", speed_rpm, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_OBJECT_NAMED(struct json_redfish_v1_thermal_fan, "Status", status, json_redfish_resource_status_entry_desc),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "UpperThresholdCritical", upper_threshold_critical, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "UpperThresholdFatal", upper_threshold_fatal, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal_fan, "UpperThresholdNonCritical", upper_threshold_noncritical, JSON_TOK_FLOAT),
};

// TODO
// Implement remaining objects (Actions, Redundancy)
struct __attribute__((packed, aligned(1))) json_redfish_v1_thermal {
	const char *odata_context;
	const char *odata_id;
	const char *odata_type;
	const char *description;
	const char *id;
	const char *name;
	struct json_redfish_v1_thermal_temperature temperatures[REDFISH_MAX_TEMP_SENSOR_COUNT];
	size_t temperatures_count;
	struct json_redfish_v1_thermal_fan fans[REDFISH_MAX_FAN_COUNT];
	size_t fans_count;
};

static const struct json_obj_descr json_redfish_v1_thermal_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal, "@odata.context", odata_context, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal, "@odata.id", odata_id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal, "@odata.type", odata_type, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal, "Description", description, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal, "Id", id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_thermal, "Name", name, JSON_TOK_STRING),
	JSON_OBJ_DESCR_OBJ_ARRAY_NAMED(struct json_redfish_v1_thermal, "Temperatures", temperatures, REDFISH_MAX_TEMP_SENSOR_COUNT, temperatures_count, json_redfish_v1_thermal_temperature_desc, ARRAY_SIZE(json_redfish_v1_thermal_temperature_desc)),
	JSON_OBJ_DESCR_OBJ_ARRAY_NAMED(struct json_redfish_v1_thermal, "Fans", fans, REDFISH_MAX_FAN_COUNT, fans_count, json_redfish_v1_thermal_fan_desc, ARRAY_SIZE(json_redfish_v1_thermal_fan_desc)),
};

// TODO
// Implement remaining objects (Actions, Links, Location, RelatedItem, SensorGroup, Thresholds)
struct __attribute__((packed, aligned(1))) json_redfish_v1_chassis_sensor {
	const char *odata_id;
	const char *description;
	const char *id;
	const char *name;

	double accuracy;
	double adjusted_max_allowable_operating_value;
	double adjusted_min_allowable_operating_value;
	double apparent_kva_h;
	double apparent_va;
	double average_reading;
	const char *averaging_interval;
	bool averaging_interval_achieved;
	double calibration;
	const char *calibration_time;
	double crest_factor;
	const char *electrical_context;
	const char *implementation;
	double lifetime_reading;
	double lowest_reading;
	const char *lowest_reading_time;
	double max_allowable_operating_value;
	const char *member_id;
	double min_allowable_operating_value;
	double peak_reading;
	const char *peak_reading_time;
	double phase_angle_degrees;
	const char *physical_context;
	const char *physical_subcontext;
	double power_factor;
	double precision;
	double reactive_kvar_h;
	double reactive_var;
	double reading;
	double reading_range_max;
	double reading_range_min;
	const char *reading_time;
	const char *reading_type;
	const char *reading_units;
	const char *sensing_interval;
	const char *sensor_reset_time;
	int speed_rpm;
	struct json_redfish_resource_status_entry status;
	const char *voltage_type;
};

static const struct json_obj_descr json_redfish_v1_chassis_sensor_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "@odata.id", odata_id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "Description", description, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "Id", id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "Name", name, JSON_TOK_STRING),

	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "Accuracy", accuracy, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "AdjustedMaxAllowableOperatingValue", adjusted_max_allowable_operating_value, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "AdjustedMinAllowableOperatingValue", adjusted_min_allowable_operating_value, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "ApparentkVAh", apparent_kva_h, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "ApparentVA", apparent_va, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "AverageReading", average_reading, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "AveragingInterval", averaging_interval, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "AveragingIntervalAchieved", averaging_interval_achieved, JSON_TOK_TRUE),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "Calibration", calibration, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "CalibrationTime", calibration_time, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "CrestFactor", crest_factor, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "ElectricalContext", electrical_context, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "Implementation", electrical_context, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "LifetimeReading", lifetime_reading, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "LowestReading", lowest_reading, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "LowestReadingTime", lowest_reading_time, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "MaxAllowableOperatingValue", max_allowable_operating_value, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "MemberId", member_id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "MinAllowableOperatingValue", min_allowable_operating_value, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "PeakReading", peak_reading, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "PeakReadingTime", peak_reading_time, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "PhaseAngleDegrees", phase_angle_degrees, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "PhysicalContext", physical_context, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "PhysicalSubContext", physical_subcontext, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "PowerFactor", power_factor, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "Precision", precision, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "ReactivekVARh", reactive_kvar_h, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "ReactiveVAR", reactive_var, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "Reading", reading, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "ReadingRangeMax", reading_range_max, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "ReadingRangeMin", reading_range_min, JSON_TOK_FLOAT),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "ReadingTime", reading_time, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "ReadingType", reading_time, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "ReadingUnits", reading_units, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "SensingInterval", sensing_interval, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "SensorResetTime", sensor_reset_time, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "SpeedRPM", speed_rpm, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_OBJECT_NAMED(struct json_redfish_v1_chassis_sensor, "Status", status, json_redfish_resource_status_entry_desc),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensor, "VoltageType", voltage_type, JSON_TOK_STRING),
};

// TODO
// Implement remaining objects (Actions, Redundancy)
struct __attribute__((packed, aligned(1))) json_redfish_v1_chassis_sensors {
	const char *odata_context;
	const char *odata_id;
	const char *odata_type;
	const char *description;
	const char *id;
	const char *name;
	struct json_redfish_v1_chassis_sensor sensors[REDFISH_MAX_CHASSIS_SENSOR_COUNT];
	size_t sensors_count;
};

static const struct json_obj_descr json_redfish_v1_chassis_sensors_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensors, "@odata.context", odata_context, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensors, "@odata.id", odata_id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensors, "@odata.type", odata_type, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensors, "Description", description, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensors, "Id", id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis_sensors, "Name", name, JSON_TOK_STRING),
	JSON_OBJ_DESCR_OBJ_ARRAY_NAMED(struct json_redfish_v1_chassis_sensors, "Sensors", sensors, REDFISH_MAX_CHASSIS_SENSOR_COUNT, sensors_count, json_redfish_v1_chassis_sensor_desc, ARRAY_SIZE(json_redfish_v1_chassis_sensor_desc)),
};

struct __attribute__((packed, aligned(1))) json_redfish_odata_chassis_action_reset {
	const char* allowable_values[16];
	size_t allowable_values_count;
	const char* target;
};

struct __attribute__((packed, aligned(1))) json_redfish_odata_chassis_actions {
	struct json_redfish_odata_chassis_action_reset reset_action;
};

struct __attribute__((packed, aligned(1))) json_redfish_v1_chassis {
	const char *odata_context;
	const char *odata_id;
	const char *odata_type;
	const char *description;
	const char *id;
	const char *name;
	struct json_redfish_odata_chassis_actions actions;
	const char *asset_tag;
	const char *chassis_type;
	int depth_mm;
	const char *environmental_class;
	int height_mm;
	const char *indicator_led;
	const char *manufacturer;
	const char *model;
	const char *part_number;
	const char *power_state;
	const char *serial_number;
	struct json_redfish_resource_status_entry status;
	struct json_redfish_chassis_thermal_entry thermal;
	const char *uuid;
	int weight_kg;
	int width_mm;
};

static const struct json_obj_descr json_redfish_odata_chassis_action_reset_desc[] = {
	JSON_OBJ_DESCR_ARRAY_NAMED(struct json_redfish_odata_chassis_action_reset, "ResetType@Redfish.AllowableValues", allowable_values, 16, allowable_values_count, JSON_TOK_STRING),
        JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_odata_chassis_action_reset, "target", target, JSON_TOK_STRING),
};

static const struct json_obj_descr json_redfish_odata_chassis_actions_desc[] = {
	JSON_OBJ_DESCR_OBJECT_NAMED(struct json_redfish_odata_chassis_actions, "#Chassis.Reset", reset_action, json_redfish_odata_chassis_action_reset_desc),
};

static const struct json_obj_descr json_redfish_v1_chassis_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "@odata.context", odata_context, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "@odata.id", odata_id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "@odata.type", odata_type, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "Description", description, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "Id", id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "Name", name, JSON_TOK_STRING),
	JSON_OBJ_DESCR_OBJECT_NAMED(struct json_redfish_v1_chassis, "Actions", actions, json_redfish_odata_chassis_actions_desc),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "AssetTag", asset_tag, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "ChassisType", chassis_type, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "DepthMm", depth_mm, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "EnvironmentalClass", environmental_class, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "HeightMm", height_mm, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "IndicatorLED", indicator_led, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "Manufacturer", manufacturer, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "Model", model, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "PartNumber", part_number, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "PowerState", power_state, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "SerialNumber", serial_number, JSON_TOK_STRING),
	JSON_OBJ_DESCR_OBJECT_NAMED(struct json_redfish_v1_chassis, "Status", status, json_redfish_resource_status_entry_desc),
	JSON_OBJ_DESCR_OBJECT_NAMED(struct json_redfish_v1_chassis, "Thermal", thermal, json_redfish_chassis_thermal_entry_desc),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "UUID", uuid, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "WeightKg", weight_kg, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_chassis, "WidthMm", width_mm, JSON_TOK_NUMBER),
};

struct __attribute__((packed, aligned(1))) json_redfish_v1_system_list {
	const char *odata_context;
	const char *odata_id;
	const char *odata_type;
	struct json_redfish_odata_string_entry members[1];
	size_t members_count;
	const char *name;
};

static const struct json_obj_descr json_redfish_v1_system_list_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_system_list, "@odata.context", odata_context, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_system_list, "@odata.id", odata_id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_system_list, "@odata.type", odata_type, JSON_TOK_STRING),
	JSON_OBJ_DESCR_OBJ_ARRAY_NAMED(struct json_redfish_v1_system_list, "Members", members, 1, members_count, json_redfish_odata_string_entry_desc, ARRAY_SIZE(json_redfish_odata_string_entry_desc)),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_system_list, "Members@odata.count", members_count, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_system_list, "Name", name, JSON_TOK_STRING),
};

struct __attribute__((packed, aligned(1))) json_redfish_odata_system_action_reset {
	const char* allowable_values[16];
	size_t allowable_values_count;
	const char* target;
};

struct __attribute__((packed, aligned(1))) json_redfish_odata_system_actions {
	struct json_redfish_odata_system_action_reset reset_action;
};

struct __attribute__((packed, aligned(1))) json_redfish_v1_system {
	const char *odata_context;
	const char *odata_id;
	const char *odata_type;
	struct json_redfish_odata_system_actions actions;
	const char *description;
	const char *id;
	const char *indicator_led;
	const char *name;
	const char *power_state;
	struct json_redfish_resource_status_entry status;
	const char *system_type;
};

static const struct json_obj_descr json_redfish_odata_system_action_reset_desc[] = {
	JSON_OBJ_DESCR_ARRAY_NAMED(struct json_redfish_odata_system_action_reset, "ResetType@Redfish.AllowableValues", allowable_values, 16, allowable_values_count, JSON_TOK_STRING),
        JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_odata_system_action_reset, "target", target, JSON_TOK_STRING),
};

static const struct json_obj_descr json_redfish_odata_system_actions_desc[] = {
	JSON_OBJ_DESCR_OBJECT_NAMED(struct json_redfish_odata_system_actions, "#ComputerSystem.Reset", reset_action, json_redfish_odata_system_action_reset_desc),
};

static const struct json_obj_descr json_redfish_v1_system_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_system, "@odata.context", odata_context, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_system, "@odata.id", odata_id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_system, "@odata.type", odata_type, JSON_TOK_STRING),
	JSON_OBJ_DESCR_OBJECT_NAMED(struct json_redfish_v1_system, "Actions", actions, json_redfish_odata_system_actions_desc),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_system, "Description", description, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_system, "Id", id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_system, "IndicatorLED", indicator_led, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_system, "Name", name, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_system, "PowerState", power_state, JSON_TOK_STRING),
	JSON_OBJ_DESCR_OBJECT_NAMED(struct json_redfish_v1_system, "Chassis", status, json_redfish_resource_status_entry_desc),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_system, "SystemType", system_type, JSON_TOK_STRING),
};

struct __attribute__((packed, aligned(1))) json_redfish_odata_system_action_reset_cmd {
	const char* reset_type;
};

static const struct json_obj_descr json_redfish_odata_system_action_reset_cmd_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_odata_system_action_reset_cmd, "ResetType", reset_type, JSON_TOK_STRING),
};

struct __attribute__((packed, aligned(1))) json_redfish_v1_message_extended_info {
	const char *odata_type;
	const char *message_id;
	const char *message;
	const char *severity;
	const char *message_severity;
	int number_of_args;
	const char *resolution;
};

static const struct json_obj_descr json_redfish_v1_message_extended_info_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_message_extended_info, "@odata.type", odata_type, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_message_extended_info, "MessageId", message_id, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_message_extended_info, "Message", message, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_message_extended_info, "Severity", severity, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_message_extended_info, "MessageSeverity", message_severity, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_message_extended_info, "NumberOfArgs", number_of_args, JSON_TOK_NUMBER),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_message_extended_info, "Resolution", resolution, JSON_TOK_STRING),
};

struct __attribute__((packed, aligned(1))) json_redfish_v1_message_response_data {
	const char *code;
	const char *message;
	struct json_redfish_v1_message_extended_info extended_info[1];
	size_t extended_info_count;
};

static const struct json_obj_descr json_redfish_v1_message_response_data_desc[] = {
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_message_response_data, "code", code, JSON_TOK_STRING),
	JSON_OBJ_DESCR_PRIM_NAMED(struct json_redfish_v1_message_response_data, "message", message, JSON_TOK_STRING),
	JSON_OBJ_DESCR_OBJ_ARRAY_NAMED(struct json_redfish_v1_message_response_data, "@Message.ExtendedInfo", extended_info, 1, extended_info_count, json_redfish_v1_message_extended_info_desc, ARRAY_SIZE(json_redfish_v1_message_extended_info_desc)),
};

struct __attribute__((packed, aligned(1))) json_redfish_v1_message_response {
	struct json_redfish_v1_message_response_data message;
};

static const struct json_obj_descr json_redfish_v1_message_response_desc[] = {
	JSON_OBJ_DESCR_OBJECT_NAMED(struct json_redfish_v1_message_response, "error", message, json_redfish_v1_message_response_data_desc),
};

static char * redfish_allocate_and_print_string(size_t max_string_length, const char * fmt, ...)
{
	va_list arg_ptr;

	char * ret = malloc(max_string_length);
	if (ret) {
		va_start(arg_ptr, fmt);
		vsnprintf(ret, max_string_length - 1, fmt, arg_ptr);
		va_end(arg_ptr);
	}

	return ret;
}

int get_chassis_vpd(const char * chassis_id, kestrel_chassis_data_t * chassis_data)
{
	if (strcmp(chassis_id, REDFISH_CHASSIS0_ID) == 0) {
		// FIXME
		strcpy(chassis_data->asset_tag, "0123456789");
		strcpy(chassis_data->chassis_type, "StandAlone");
		chassis_data->depth_mm = 0;
		strcpy(chassis_data->environmental_class, "A4");
		chassis_data->height_mm = 0;
		strcpy(chassis_data->manufacturer, "Raptor Engineering, LLC");
		strcpy(chassis_data->model, "Kestrel SoftBMC");
		strcpy(chassis_data->part_number, "KEST001");
		strcpy(chassis_data->serial_number, "0123456789");
		strcpy(chassis_data->uuid, "00000000-0000-0000-0000-000000000000");
		chassis_data->weight_kg = 0;
		chassis_data->width_mm = 0;

		return 0;
	}

	return -1;
}

int get_chassis_thermal_data(const char * chassis_id, struct json_redfish_v1_thermal * redfish_thermal_list) {
	kestrel_tach_data_t * tach_data;
	int i;

	redfish_thermal_list->temperatures_count = 0;
	redfish_thermal_list->fans_count = 0;

	if (strcmp(chassis_id, REDFISH_CHASSIS0_ID) == 0) {
		// Scan platform temperature sensors
		for (i = 0; i < kestrel_temp_sensor_count; i++)
		{
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].odata_id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].odata_id) {
				continue;
			}
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].id) {
				free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].odata_id);
				continue;
			}
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].name = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].name) {
				free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].odata_id);
				free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].id);
				continue;
			}
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].description = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].description) {
				free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].name);
				free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].odata_id);
				free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].id);
				continue;
			}
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].member_id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].member_id) {
				free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].description);
				free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].name);
				free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].odata_id);
				free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].id);
				continue;
			}

			// Basic parameters
			snprintf((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].odata_id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
				REDFISH_THERMAL_TEMPERATURE_PATH_FORMAT, chassis_id, kestrel_temp_sensor_data[i].sensor_id);
			if (strlen(kestrel_temp_sensor_data[i].description) > 0) {
				// Copy sensor descriptions to Redfish data structures
				strncpy((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].description,
					kestrel_temp_sensor_data[i].description, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
				((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].description)[REDFISH_MAX_STRING_ATTRIBUTE_LENGTH-1] = 0x0;
				strncpy((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].name,
					kestrel_temp_sensor_data[i].description, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
				((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].name)[REDFISH_MAX_STRING_ATTRIBUTE_LENGTH-1] = 0x0;
			}
			else {
				snprintf((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].description, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
					"Platform temperature sensor #%d", kestrel_temp_sensor_data[i].sensor_id);
				snprintf((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].name, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
					"Platform temperature sensor #%d", kestrel_temp_sensor_data[i].sensor_id);
			}
			snprintf((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
				"Temperature_Platform_Sensor_%d", kestrel_temp_sensor_data[i].sensor_id);
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].delta_physical_context = redfish_static_string_empty;
			snprintf((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].member_id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
				"%d", kestrel_temp_sensor_data[i].sensor_id);
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].physical_context = redfish_static_string_empty;

			// Thresholds and Limits
			//
			// FIXME
			// Use generic defaults for now...
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].delta_reading_celsius = 0;
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].lower_threshold_critical = -20;
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].lower_threshold_fatal = -40;
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].lower_threshold_noncritical = -10;
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].max_reading_range_temp = 0;
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].min_reading_range_temp = 0;
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].upper_threshold_critical = 95;
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].upper_threshold_fatal = 100;
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].upper_threshold_noncritical = 90;
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].adjusted_max_allowable_operating_value = 105;
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].adjusted_min_allowable_operating_value = -40;
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].max_allowable_operating_value = 105;
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].min_allowable_operating_value = -40;

			// Sensor status
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions_count = 0;
			if (thermal_sensor_valid_last_n_ms(&kestrel_temp_sensor_data[i], SENSOR_MAX_TIMEOUT_MS))
			{
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.health = redfish_static_string_ok;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.health_rollup = redfish_static_string_ok;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.state = redfish_static_string_enabled;
			}
			else {
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.health = redfish_static_string_critical;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.health_rollup = redfish_static_string_critical;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.state = redfish_static_string_disabled;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions[redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions_count].message =
					redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Thermal sensor failure");
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions[redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions_count].message_id =
					redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Fatal_Sensor_Offline");
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions[redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions_count].resolution =
					redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Check wiring and/or replace defective sensor");
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions[redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions_count].timestamp = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
				if (redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions[redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions_count].timestamp) {
					kestrel_strftime((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions[redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions_count].timestamp, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH - 1,
						kestrel_temp_sensor_data[i].last_valid_timestamp + (uptime_rtc_offset * 1000));
				}
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions[redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions_count].severity =
					redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Critical");
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions_count++;
			}

			// Load sensor data into struct for transmission
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].reading_celsius = kestrel_temp_sensor_data[i].temperature_c;
			redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].sensor_number = kestrel_temp_sensor_data[i].sensor_id;

			redfish_thermal_list->temperatures_count++;
		}

		// Scan CPU temperature sensors
		if (host_cpu_temperature_sensor_count != 0)
		{
			for (i = 0; i < host_cpu_temperature_sensor_count; i++)
			{
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].odata_id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
				if (!redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].odata_id) {
					continue;
				}
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
				if (!redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].id) {
					free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].odata_id);
					continue;
				}
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].name = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
				if (!redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].name) {
					free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].odata_id);
					free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].id);
					continue;
				}
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].description = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
				if (!redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].description) {
					free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].name);
					free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].odata_id);
					free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].id);
					continue;
				}
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].member_id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
				if (!redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].member_id) {
					free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].description);
					free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].name);
					free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].odata_id);
					free((void*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].id);
					continue;
				}

				// Basic parameters
				snprintf((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].odata_id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
					"%s/%d", REDFISH_THERMAL_LIST_PATH, host_cpu_temperature_sensor_data[i].sensor_id);
				snprintf((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].description, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
					"Host CPU temperature sensor #%d", host_cpu_temperature_sensor_data[i].sensor_id);
				snprintf((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
					"Temperature_CPU_Sensor_%d", host_cpu_temperature_sensor_data[i].sensor_id);
				snprintf((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].name, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
					"Host CPU temperature sensor #%d", host_cpu_temperature_sensor_data[i].sensor_id);
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].delta_physical_context = redfish_static_string_empty;
				snprintf((char*)redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].member_id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
					"%d", host_cpu_temperature_sensor_data[i].sensor_id);
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].physical_context = redfish_static_string_empty;

				// Thresholds and Limits
				//
				// FIXME
				// Use generic defaults for now...
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].delta_reading_celsius = 0;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].lower_threshold_critical = -20;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].lower_threshold_fatal = -40;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].lower_threshold_noncritical = -10;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].max_reading_range_temp = 0;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].min_reading_range_temp = 0;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].upper_threshold_critical = 95;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].upper_threshold_fatal = 100;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].upper_threshold_noncritical = 90;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].adjusted_max_allowable_operating_value = 105;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].adjusted_min_allowable_operating_value = -40;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].max_allowable_operating_value = 105;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].min_allowable_operating_value = -40;

				// Sensor status
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.conditions_count = 0;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.health = redfish_static_string_ok;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.health_rollup = redfish_static_string_ok;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].status.state = redfish_static_string_enabled;

				// Load sensor data into struct for transmission
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].reading_celsius = host_cpu_temperature_sensor_data[i].temperature_c;
				redfish_thermal_list->temperatures[redfish_thermal_list->temperatures_count].sensor_number = host_cpu_temperature_sensor_data[i].sensor_id;

				redfish_thermal_list->temperatures_count++;
			}
		}

		// Load platform fan data into Redfish structures
		for (i = 0; i < kestrel_fan_count; i++)
		{
			if (kestrel_fan_data[i].associated_tach_id_count > 0) {
				// Tachometer data and thresholds available
				tach_data = &kestrel_tach_data[kestrel_fan_data[i].associated_tach_id[0]];
			}
			else {
				// No tachometer associated with this fan
				tach_data = NULL;
			}

			redfish_thermal_list->fans[redfish_thermal_list->fans_count].odata_id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_thermal_list->fans[redfish_thermal_list->fans_count].odata_id) {
				continue;
			}
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_thermal_list->fans[redfish_thermal_list->fans_count].id) {
				free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].odata_id);
				continue;
			}
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].name = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_thermal_list->fans[redfish_thermal_list->fans_count].name) {
				free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].odata_id);
				free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].id);
				continue;
			}
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].description = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_thermal_list->fans[redfish_thermal_list->fans_count].description) {
				free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].name);
				free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].odata_id);
				free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].id);
				continue;
			}
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].member_id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_thermal_list->fans[redfish_thermal_list->fans_count].member_id) {
				free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].description);
				free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].name);
				free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].odata_id);
				free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].id);
				continue;
			}

			// Basic parameters
			snprintf((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].odata_id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
				REDFISH_THERMAL_FAN_PATH_FORMAT, chassis_id, kestrel_fan_data[i].fan_id);
			if (strlen(kestrel_temp_sensor_data[i].description) > 0) {
				// Copy sensor descriptions to Redfish data structures
				strncpy((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].description,
					kestrel_fan_data[i].description, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
				((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].description)[REDFISH_MAX_STRING_ATTRIBUTE_LENGTH-1] = 0x0;
				strncpy((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].name,
					kestrel_fan_data[i].description, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
				((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].name)[REDFISH_MAX_STRING_ATTRIBUTE_LENGTH-1] = 0x0;
			}
			else {
				snprintf((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].description, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
					"Platform fan #%d", kestrel_fan_data[i].fan_id);
				snprintf((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].name, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
					"Platform fan #%d", kestrel_fan_data[i].fan_id);
			}
			snprintf((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
				"Platform_Fan_%d", kestrel_fan_data[i].fan_id);
			snprintf((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].member_id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
				"%d", kestrel_fan_data[i].fan_id);
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].physical_context = redfish_static_string_empty;

			// Fan VPD
			//
			// FIXME
			// Use generic data for now...
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].manufacturer = redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "RAPTOR");
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].model = redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "GENERIC");
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].part_number = redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "KESTFAN01");
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].serial_number = redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "0123456789");
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].spare_part_number = redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "KESTFAN02");

			// Thresholds and Limits
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].hotpluggable = kestrel_fan_data[i].hot_plug_capable;
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].indicator_led = (kestrel_fan_data[i].indicator_led_on)?redfish_static_string_on:redfish_static_string_off;
			if (tach_data) {
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].lower_threshold_critical = tach_data->fan_alert_thresholds.lower_rpm_threshold_critical;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].lower_threshold_fatal = tach_data->fan_alert_thresholds.lower_rpm_threshold_fatal;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].lower_threshold_noncritical = tach_data->fan_alert_thresholds.lower_rpm_threshold_warning;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].max_reading_range = tach_data->fan_alert_thresholds.max_design_rpm;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].min_reading_range = tach_data->fan_alert_thresholds.min_design_rpm;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].upper_threshold_critical = tach_data->fan_alert_thresholds.upper_rpm_threshold_critical;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].upper_threshold_fatal = tach_data->fan_alert_thresholds.upper_rpm_threshold_fatal;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].upper_threshold_noncritical = tach_data->fan_alert_thresholds.upper_rpm_threshold_warning;
			}
			else {
				// Drive percentage only
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].lower_threshold_critical = 0;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].lower_threshold_fatal = 0;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].lower_threshold_noncritical = 0;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].max_reading_range = 100;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].min_reading_range = 0;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].upper_threshold_critical = 100;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].upper_threshold_fatal = 100;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].upper_threshold_noncritical = 100;
			}

			// Sensor status
			if (kestrel_fan_data[i].fan_set_online) {
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count = 0;
				if (tach_data) {
					if ((tach_data->tach_value_rpm_averaged < tach_data->fan_alert_thresholds.lower_rpm_threshold_fatal)
						|| (tach_data->tach_value_rpm_averaged > tach_data->fan_alert_thresholds.upper_rpm_threshold_fatal)) {
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.health = redfish_static_string_critical;
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.health_rollup = redfish_static_string_critical;
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions[redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count].message =
							redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Fan failure");
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions[redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count].message_id =
							redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Fatal_Fan_RPM");
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions[redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count].resolution =
							redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Replace defective fan");
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions[redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count].timestamp = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
						if (redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions[redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count].timestamp) {
							// FIXME
							// Extend fan struct to contain data on when fan went outside of defined threshold parameters and return a valid failure timestamp instead of 0
							kestrel_strftime((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions[redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count].timestamp, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH - 1,
								0);
						}
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions[redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count].severity =
							redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Critical");
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count++;
					}
					else if ((tach_data->tach_value_rpm_averaged < tach_data->fan_alert_thresholds.lower_rpm_threshold_critical)
						|| (tach_data->tach_value_rpm_averaged > tach_data->fan_alert_thresholds.upper_rpm_threshold_critical)) {
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.health = redfish_static_string_warning;
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.health_rollup = redfish_static_string_warning;
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions[redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count].message =
							redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Fan speed warning");
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions[redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count].message_id =
							redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Critical_Fan_RPM");
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions[redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count].resolution =
							redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Check for clogged filters and replace failing fan if necessary");
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions[redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count].timestamp = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
						if (redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions[redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count].timestamp) {
							// FIXME
							// Extend fan struct to contain data on when fan went outside of defined threshold parameters and return a valid failure timestamp instead of 0
							kestrel_strftime((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions[redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count].timestamp, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH - 1,
								0);
						}
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions[redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count].severity =
							redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Warning");
						redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count++;
					}
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.state = redfish_static_string_enabled;
				}
				else {
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.health = redfish_static_string_ok;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.health_rollup = redfish_static_string_ok;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.state = redfish_static_string_enabled;
				}
			}
			else {
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count = 0;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.health = redfish_static_string_ok;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.health_rollup = redfish_static_string_ok;
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.state = redfish_static_string_standbyoffline;
			}

			// Load associated sensor data into struct for transmission
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].reading = kestrel_fan_data[i].fan_readback_percent;
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].reading_units = redfish_static_string_percent;
			if (tach_data) {
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].speed_rpm = tach_data->tach_value_rpm_averaged;
			}
			else {
				redfish_thermal_list->fans[redfish_thermal_list->fans_count].speed_rpm = -1;
			}
			redfish_thermal_list->fans[redfish_thermal_list->fans_count].sensor_number = kestrel_fan_data[i].fan_id;

			redfish_thermal_list->fans_count++;
		}

		// Load any leftover tachs without associated fans
		for (i = 0; i < kestrel_tach_count; i++) {
			if (kestrel_tach_data[i].valid) {
				if (kestrel_tach_data[i].associated_fan_id_count == 0) {
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].odata_id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
					if (!redfish_thermal_list->fans[redfish_thermal_list->fans_count].odata_id) {
						continue;
					}
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
					if (!redfish_thermal_list->fans[redfish_thermal_list->fans_count].id) {
						free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].odata_id);
						continue;
					}
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].name = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
					if (!redfish_thermal_list->fans[redfish_thermal_list->fans_count].name) {
						free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].odata_id);
						free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].id);
						continue;
					}
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].description = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
					if (!redfish_thermal_list->fans[redfish_thermal_list->fans_count].description) {
						free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].name);
						free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].odata_id);
						free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].id);
						continue;
					}
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].member_id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
					if (!redfish_thermal_list->fans[redfish_thermal_list->fans_count].member_id) {
						free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].description);
						free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].name);
						free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].odata_id);
						free((void*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].id);
						continue;
					}

					// Basic parameters
					snprintf((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].odata_id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
						REDFISH_THERMAL_FAN_PATH_FORMAT, chassis_id, kestrel_tach_data[i].tach_id);
					if (strlen(kestrel_temp_sensor_data[i].description) > 0) {
						// Copy sensor descriptions to Redfish data structures
						strncpy((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].description,
							kestrel_tach_data[i].description, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
						((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].description)[REDFISH_MAX_STRING_ATTRIBUTE_LENGTH-1] = 0x0;
						strncpy((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].name,
							kestrel_tach_data[i].description, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
						((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].name)[REDFISH_MAX_STRING_ATTRIBUTE_LENGTH-1] = 0x0;
					}
					else {
						snprintf((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].description, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
							"Platform tach #%d", kestrel_tach_data[i].tach_id);
						snprintf((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].name, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
							"Platform tach #%d", kestrel_tach_data[i].tach_id);
					}
					snprintf((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
						"Platform_Tach_%d", kestrel_tach_data[i].tach_id);
					snprintf((char*)redfish_thermal_list->fans[redfish_thermal_list->fans_count].member_id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
						"%d", kestrel_tach_data[i].tach_id);
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].physical_context = redfish_static_string_empty;

					// Tach VPD
					//
					// FIXME
					// Use generic data for now...
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].manufacturer = redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "RAPTOR");
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].model = redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "GENERIC");
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].part_number = redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "KESTTACH01");
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].serial_number = redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "0123456789");
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].spare_part_number = redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "KESTTACH02");

					// Thresholds and Limits
					//
					// FIXME
					// Use generic defaults for now...
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].hotpluggable = false;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].indicator_led = redfish_static_string_off;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].lower_threshold_critical = 100;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].lower_threshold_fatal = 0;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].lower_threshold_noncritical = 250;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].max_reading_range = 0;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].min_reading_range = 0;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].upper_threshold_critical = 10000;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].upper_threshold_fatal = 15000;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].upper_threshold_noncritical = 6000;

					// Sensor status
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.conditions_count = 0;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.health = redfish_static_string_ok;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.health_rollup = redfish_static_string_ok;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].status.state = redfish_static_string_enabled;

					// Load associated sensor data into struct for transmission
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].reading = kestrel_tach_data[i].tach_value_rpm_averaged;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].reading_units = redfish_static_string_rpm;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].speed_rpm = kestrel_tach_data[i].tach_value_rpm_averaged;
					redfish_thermal_list->fans[redfish_thermal_list->fans_count].sensor_number = kestrel_tach_data[i].tach_id;

					redfish_thermal_list->fans_count++;
				}
			}
		}

		return 0;
	}

	return -1;
}

// All other sensors
// NOTE: Thermal and Power sensors are handled in dedicated schemas and are NOT included in this response list!
int get_chassis_sensor_data(const char * chassis_id, struct json_redfish_v1_chassis_sensors * redfish_sensor_list) {
	int i;

	redfish_sensor_list->sensors_count = 0;

	if (strcmp(chassis_id, REDFISH_CHASSIS0_ID) == 0) {
		// Scan platform pressure sensors
		for (i = 0; i < kestrel_pressure_sensor_count; i++)
		{
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].odata_id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].odata_id) {
				continue;
			}
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].id) {
				free((void*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].odata_id);
				continue;
			}
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].name = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].name) {
				free((void*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].odata_id);
				free((void*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].id);
				continue;
			}
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].description = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].description) {
				free((void*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].name);
				free((void*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].odata_id);
				free((void*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].id);
				continue;
			}
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].member_id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
			if (!redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].member_id) {
				free((void*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].description);
				free((void*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].name);
				free((void*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].odata_id);
				free((void*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].id);
				continue;
			}

			// Basic parameters
			snprintf((char*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].odata_id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
				REDFISH_CHASSIS_SENSOR_PATH_FORMAT, chassis_id, kestrel_pressure_sensor_data[i].sensor_id);
			if (strlen(kestrel_pressure_sensor_data[i].description) > 0) {
				// Copy sensor descriptions to Redfish data structures
				strncpy((char*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].description,
					kestrel_pressure_sensor_data[i].description, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
				((char*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].description)[REDFISH_MAX_STRING_ATTRIBUTE_LENGTH-1] = 0x0;
				strncpy((char*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].name,
					kestrel_pressure_sensor_data[i].description, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
				((char*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].name)[REDFISH_MAX_STRING_ATTRIBUTE_LENGTH-1] = 0x0;
			}
			else {
				snprintf((char*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].description, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
					"Platform pressure sensor #%d", kestrel_pressure_sensor_data[i].sensor_id);
				snprintf((char*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].name, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
					"Platform pressure sensor #%d", kestrel_pressure_sensor_data[i].sensor_id);
			}
			snprintf((char*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
				"Pressure_Platform_Sensor_%d", kestrel_pressure_sensor_data[i].sensor_id);
			snprintf((char*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].member_id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH,
				"%d", kestrel_pressure_sensor_data[i].sensor_id);

			// Ensure all strings are reporting either valid or empty strings vs. random system data
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].averaging_interval = redfish_static_string_empty;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].calibration_time = redfish_static_string_empty;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].electrical_context = redfish_static_string_empty;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].implementation = redfish_static_string_physicalsensor;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].lowest_reading_time = redfish_static_string_empty;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].peak_reading_time = redfish_static_string_empty;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].physical_context = redfish_static_string_empty;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].physical_subcontext = redfish_static_string_empty;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].reading_time = redfish_static_string_empty;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].reading_type = redfish_static_string_pressure;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].sensing_interval = redfish_static_string_empty;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].sensor_reset_time = redfish_static_string_empty;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].voltage_type = redfish_static_string_empty;

			// Set default values for all parameters
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].accuracy = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].adjusted_max_allowable_operating_value = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].adjusted_min_allowable_operating_value = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].apparent_kva_h = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].apparent_va = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].average_reading = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].averaging_interval_achieved = false;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].calibration = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].crest_factor = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].lifetime_reading = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].lowest_reading = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].max_allowable_operating_value = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].min_allowable_operating_value = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].peak_reading = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].phase_angle_degrees = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].power_factor = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].precision = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].reactive_kvar_h = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].reactive_var = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].reading = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].reading_range_max = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].reading_range_min = 0.0;
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].speed_rpm = 0.0;

			// Thresholds and Limits
			//
			// FIXME
			// NOT YET IMPLEMENTED

			// Sensor status
			redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions_count = 0;
			if (pressure_sensor_valid_last_n_ms(&kestrel_pressure_sensor_data[i], SENSOR_MAX_TIMEOUT_MS))
			{
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.health = redfish_static_string_ok;
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.health_rollup = redfish_static_string_ok;
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.state = redfish_static_string_enabled;
			}
			else {
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.health = redfish_static_string_critical;
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.health_rollup = redfish_static_string_critical;
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.state = redfish_static_string_disabled;
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions[redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions_count].message =
					redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Pressure sensor failure");
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions[redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions_count].message_id =
					redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Fatal_Sensor_Offline");
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions[redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions_count].resolution =
					redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Check wiring and/or replace defective sensor");
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions[redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions_count].timestamp = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
				if (redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions[redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions_count].timestamp) {
					kestrel_strftime((char*)redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions[redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions_count].timestamp, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH - 1,
						kestrel_pressure_sensor_data[i].last_valid_timestamp + (uptime_rtc_offset * 1000));
				}
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions[redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions_count].severity =
					redfish_allocate_and_print_string(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, "%s", "Critical");
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].status.conditions_count++;
			}

			// Load sensor data into struct for transmission
			// Autorange to kPa at 10kPa, but use Pa below that threshold
			if (kestrel_pressure_sensor_data[i].pressure_pa > 10000.0) {
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].reading = kestrel_pressure_sensor_data[i].pressure_pa / 1000.0;
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].reading_units = redfish_static_string_kpa;
			}
			else {
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].reading = kestrel_pressure_sensor_data[i].pressure_pa;
				redfish_sensor_list->sensors[redfish_sensor_list->sensors_count].reading_units = redfish_static_string_pa;
			}

			redfish_sensor_list->sensors_count++;
		}

		return 0;
	}

	return -1;
}

static int validate_session_authorized(zephyr_http_server_request_state_t *state)
{
	// FIXME
	LOG_WRN("Redfish authentication not implemented!");

	return 1;
}

int redfish_version_handler(zephyr_http_server_request_state_t *state)
{
	struct json_redfish_version redfish_version;
	uint8_t * json_string_buffer = malloc(REDFISH_JSON_STRING_BUFFER_SIZE);
	int ret;

	if (!json_string_buffer) {
		state->http_retcode = 500;
		return 0;
	}

	redfish_version.v1 = redfish_base_path;

	ret = json_obj_encode_buf(json_redfish_version_desc,
				ARRAY_SIZE(json_redfish_version_desc), &redfish_version,
				json_string_buffer,
				REDFISH_JSON_STRING_BUFFER_SIZE - 1);

	if (ret) {
		state->http_retcode = 500;
		free(json_string_buffer);
		return 0;
	}

	SEND_STANDARD_HEADER((*state), "Redfish API")

	SEND_CUSTOM_PRINTF_CUSTOM_FORMAT((*state), "%s", json_string_buffer);

	SEND_STANDARD_FOOTER((*state));

	free(json_string_buffer);

	state->http_retcode = 200;
	return 0;
}

int redfish_v1_service_list_handler(zephyr_http_server_request_state_t *state)
{
	struct json_redfish_v1_service_list redfish_service_list;
	uint8_t * json_string_buffer = malloc(REDFISH_JSON_STRING_BUFFER_SIZE);
	int ret;

	if (!json_string_buffer) {
		state->http_retcode = 500;
		return 0;
	}

	redfish_service_list.odata_context = REDFISH_BASE_PATH "/$metadata#ServiceRoot.ServiceRoot";
	redfish_service_list.odata_id = REDFISH_BASE_PATH;
	redfish_service_list.odata_type = "#ServiceRoot.v1_1_1.ServiceRoot";
	redfish_service_list.chassis.id = REDFISH_CHASSIS_LIST_PATH;
	redfish_service_list.systems.id = REDFISH_SYSTEM_LIST_PATH;
	redfish_service_list.uuid = system_uuid;

	ret = json_obj_encode_buf(json_redfish_v1_service_list_desc,
				ARRAY_SIZE(json_redfish_v1_service_list_desc), &redfish_service_list,
				json_string_buffer,
				REDFISH_JSON_STRING_BUFFER_SIZE - 1);

	if (ret) {
		state->http_retcode = 500;
		free(json_string_buffer);
		return 0;
	}

	SEND_STANDARD_HEADER(conn, "Redfish API")

	SEND_CUSTOM_PRINTF_CUSTOM_FORMAT((*state), "%s", json_string_buffer);

	SEND_STANDARD_FOOTER((*state));

	free(json_string_buffer);

	state->http_retcode = 200;
	return 0;
}

int redfish_v1_chassis_list_endpoint_handler(zephyr_http_server_request_state_t *state)
{
	struct json_redfish_v1_chassis_list redfish_chassis_list;
	uint8_t * json_string_buffer = malloc(REDFISH_JSON_STRING_BUFFER_SIZE);
	int ret;

	if (!json_string_buffer) {
		state->http_retcode = 500;
		return 0;
	}

	if (!validate_session_authorized(state)) {
		state->http_retcode = 401;
		free(json_string_buffer);
		return 0;
	}

	redfish_chassis_list.odata_context = REDFISH_BASE_PATH "/$metadata#ChassisCollection.ChassisCollection";
	redfish_chassis_list.odata_id = REDFISH_CHASSIS_LIST_PATH;
	redfish_chassis_list.odata_type = "#ChassisCollection.ChassisCollection";
	redfish_chassis_list.members[0].id = REDFISH_CHASSIS_LIST_PATH "/" REDFISH_CHASSIS0_ID;
	redfish_chassis_list.members_count = 1;
	redfish_chassis_list.name = "Chassis Collection";

	ret = json_obj_encode_buf(json_redfish_v1_chassis_list_desc,
				ARRAY_SIZE(json_redfish_v1_chassis_list_desc), &redfish_chassis_list,
				json_string_buffer,
				REDFISH_JSON_STRING_BUFFER_SIZE - 1);

	if (ret) {
		state->http_retcode = 500;
		free(json_string_buffer);
		return 0;
	}

	SEND_STANDARD_HEADER((*state), "Redfish API")

	SEND_CUSTOM_PRINTF_CUSTOM_FORMAT((*state), "%s", json_string_buffer);

	SEND_STANDARD_FOOTER((*state));

	free(json_string_buffer);

	state->http_retcode = 200;
	return 0;
}

int redfish_v1_system_list_endpoint_handler(zephyr_http_server_request_state_t *state)
{
	int ret;

	struct json_redfish_v1_system_list * redfish_systems_list = malloc(sizeof(struct json_redfish_v1_system_list));
	if (!redfish_systems_list) {
		state->http_retcode = 500;
		return 0;
	}

	uint8_t * json_string_buffer = malloc(REDFISH_JSON_STRING_BUFFER_SIZE);
	if (!json_string_buffer) {
		free(redfish_systems_list);
		state->http_retcode = 500;
		return 0;
	}

	if (!validate_session_authorized(state)) {
		state->http_retcode = 401;
		free(redfish_systems_list);
		free(json_string_buffer);
		return 0;
	}

	memset(redfish_systems_list, 0, sizeof(struct json_redfish_v1_system_list));

	redfish_systems_list->odata_context = REDFISH_BASE_PATH "/$metadata#ComputerSystemCollection.ComputerSystemCollection";
	redfish_systems_list->odata_id = REDFISH_SYSTEM_LIST_PATH;
	redfish_systems_list->odata_type = "#ComputerSystemCollection.ComputerSystemCollection";
	redfish_systems_list->members[0].id = REDFISH_SYSTEM_PATH;
	redfish_systems_list->members_count = 1;
	redfish_systems_list->name = "Computer System Collection";

	ret = json_obj_encode_buf(json_redfish_v1_system_list_desc,
				ARRAY_SIZE(json_redfish_v1_system_list_desc), redfish_systems_list,
				json_string_buffer,
				REDFISH_JSON_STRING_BUFFER_SIZE - 1);

	if (ret) {
		state->http_retcode = 500;
		free(redfish_systems_list);
		free(json_string_buffer);
		return 0;
	}

	SEND_STANDARD_HEADER((*state), "Redfish API")

	SEND_CUSTOM_PRINTF_CUSTOM_FORMAT((*state), "%s", json_string_buffer);

	SEND_STANDARD_FOOTER((*state));

	free(redfish_systems_list);
	free(json_string_buffer);

	state->http_retcode = 200;
	return 0;
}

int redfish_v1_thermal_list_endpoint_handler(zephyr_http_server_request_state_t *state)
{
	struct json_redfish_v1_thermal * redfish_thermal_list = NULL;
	int chassis_data_fetch_ret;
	size_t prev_tx_pos;
	int ret = 0;
	int i;
	int j;

	if (!validate_session_authorized(state)) {
		state->http_retcode = 401;
		return 0;
	}

	char chassis_id[REDFISH_MAX_URL_PATH_LENGTH] = {0};
	char* path_separator_location;
	strncpy(chassis_id, state->url_path + strlen(REDFISH_CHASSIS_PATH) - 1, sizeof(chassis_id) - 1);
	chassis_id[sizeof(chassis_id) - 1] = 0x0;
	path_separator_location = strstr(chassis_id, "/");
	if (path_separator_location && (path_separator_location > chassis_id)) {
		*path_separator_location = 0x0;
	}
	else {
		state->http_retcode = 404;
		return 0;
	}

	if (!state->app_state) {
		// First pass for this request

		// Allocate state buffer
		// We use this to store the entire encoded JSON response string for chunked transfer
		// The buffer will be automatically freed by the Web server on request completion
		state->app_state = malloc(sizeof(redfish_http_tx_state_data_t));
		redfish_http_tx_state_data_t * app_state = state->app_state;
		if (!app_state) {
			LOG_WRN("Redfish thermal endpoint handler failure 0x01, returning HTTP/500");
			state->http_retcode = 500;
			return 0;
		}
		app_state->tx_pos = 0;
		app_state->total_length = 0;

		redfish_thermal_list = malloc(sizeof(struct json_redfish_v1_thermal));
		if (!redfish_thermal_list) {
			LOG_WRN("Redfish thermal endpoint handler failure 0x02, returning HTTP/500");
			state->http_retcode = 500;
			return 0;
		}

		memset(redfish_thermal_list, 0, sizeof(struct json_redfish_v1_thermal));

		redfish_thermal_list->odata_id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
		if (!redfish_thermal_list->odata_id) {
			LOG_WRN("Redfish thermal endpoint handler failure 0x03, returning HTTP/500");
			state->http_retcode = 500;
			free(redfish_thermal_list);
			return 0;
		}

		redfish_thermal_list->odata_context = REDFISH_BASE_PATH "/$metadata#Thermal.Thermal";
		snprintf((char*)redfish_thermal_list->odata_id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, REDFISH_THERMAL_LIST_PATH_FORMAT, chassis_id);
		redfish_thermal_list->odata_type = "#Thermal.v1_5_1.Thermal";

		redfish_thermal_list->description = redfish_static_string_thermal;
		redfish_thermal_list->name = redfish_static_string_thermal;
		redfish_thermal_list->id = redfish_static_string_thermal;

		chassis_data_fetch_ret = get_chassis_thermal_data(chassis_id, redfish_thermal_list);

		if (!chassis_data_fetch_ret) {
			ret = json_obj_encode_buf(json_redfish_v1_thermal_desc,
						ARRAY_SIZE(json_redfish_v1_thermal_desc), redfish_thermal_list,
						app_state->tx_buffer,
						REDFISH_JSON_STRING_BUFFER_SIZE - 1);
		}

		// Free any allocated data within the Redfish structures
		for (i = 0; i < redfish_thermal_list->temperatures_count; i++) {
			free((void*)redfish_thermal_list->temperatures[i].odata_id);
			free((void*)redfish_thermal_list->temperatures[i].id);
			free((void*)redfish_thermal_list->temperatures[i].name);
			free((void*)redfish_thermal_list->temperatures[i].description);
			free((void*)redfish_thermal_list->temperatures[i].member_id);
			for (j = 0; j < redfish_thermal_list->temperatures[i].status.conditions_count; j++) {
				free((char*)redfish_thermal_list->temperatures[i].status.conditions[j].message);
				free((char*)redfish_thermal_list->temperatures[i].status.conditions[j].message_id);
				free((char*)redfish_thermal_list->temperatures[i].status.conditions[j].resolution);
				free((char*)redfish_thermal_list->temperatures[i].status.conditions[j].severity);
				free((char*)redfish_thermal_list->temperatures[i].status.conditions[j].timestamp);
			}
		}

		for (i = 0; i < redfish_thermal_list->fans_count; i++) {
			free((void*)redfish_thermal_list->fans[i].odata_id);
			free((void*)redfish_thermal_list->fans[i].id);
			free((void*)redfish_thermal_list->fans[i].name);
			free((void*)redfish_thermal_list->fans[i].description);
			free((void*)redfish_thermal_list->fans[i].member_id);

			free((void*)redfish_thermal_list->fans[i].manufacturer);
			free((void*)redfish_thermal_list->fans[i].model);
			free((void*)redfish_thermal_list->fans[i].part_number);
			free((void*)redfish_thermal_list->fans[i].serial_number);
			free((void*)redfish_thermal_list->fans[i].spare_part_number);

			for (j = 0; j < redfish_thermal_list->fans[i].status.conditions_count; j++) {
				free((char*)redfish_thermal_list->fans[i].status.conditions[j].message);
				free((char*)redfish_thermal_list->fans[i].status.conditions[j].message_id);
				free((char*)redfish_thermal_list->fans[i].status.conditions[j].resolution);
				free((char*)redfish_thermal_list->fans[i].status.conditions[j].severity);
				free((char*)redfish_thermal_list->fans[i].status.conditions[j].timestamp);
			}
		}

		free((void*)redfish_thermal_list->odata_id);
		free(redfish_thermal_list);

		if (chassis_data_fetch_ret) {
			state->http_retcode = 404;
			return 0;
		}
		else if (ret) {
			LOG_WRN("Redfish thermal endpoint handler failure 0x04 (%d), returning HTTP/500", ret);
			state->http_retcode = 500;
			return 0;
		}

		SEND_STANDARD_HEADER((*state), "Redfish API")

		prev_tx_pos = state->tx_buf_pos;
		SEND_CUSTOM_PRINTF_CUSTOM_FORMAT((*state), "%s", app_state->tx_buffer);
		app_state->tx_pos += state->tx_buf_pos - prev_tx_pos;

		if (state->tx_buf_pos < (MAX_REQUEST_SIZE_BYTES - STANDARD_FOOTER_SIZE - 1)) {
			SEND_STANDARD_FOOTER((*state));

			state->http_retcode = 200;
			return 0;
		}
		else {
			return 1;
		}
	}
	else {
		redfish_http_tx_state_data_t * app_state = state->app_state;

		prev_tx_pos = state->tx_buf_pos;
		SEND_CUSTOM_PRINTF_CUSTOM_FORMAT((*state), "%s", app_state->tx_buffer + app_state->tx_pos);
		app_state->tx_pos += state->tx_buf_pos - prev_tx_pos;

		if (state->tx_buf_pos < (MAX_REQUEST_SIZE_BYTES - 1)) {
			SEND_STANDARD_FOOTER((*state));

			state->http_retcode = 200;
			return 0;
		}
		else {
			return 1;
		}
	}
}

int redfish_v1_sensor_list_endpoint_handler(zephyr_http_server_request_state_t *state)
{
	struct json_redfish_v1_chassis_sensors * redfish_sensor_list = NULL;
	int chassis_data_fetch_ret;
	size_t prev_tx_pos;
	int ret = 0;
	int i;
	int j;

	if (!validate_session_authorized(state)) {
		state->http_retcode = 401;
		return 0;
	}

	char chassis_id[REDFISH_MAX_URL_PATH_LENGTH] = {0};
	char* path_separator_location;
	strncpy(chassis_id, state->url_path + strlen(REDFISH_CHASSIS_PATH) - 1, sizeof(chassis_id) - 1);
	chassis_id[sizeof(chassis_id) - 1] = 0x0;
	path_separator_location = strstr(chassis_id, "/");
	if (path_separator_location && (path_separator_location > chassis_id)) {
		*path_separator_location = 0x0;
	}
	else {
		state->http_retcode = 404;
		return 0;
	}

	if (!state->app_state) {
		// First pass for this request

		// Allocate state buffer
		// We use this to store the entire encoded JSON response string for chunked transfer
		// The buffer will be automatically freed by the Web server on request completion
		state->app_state = malloc(sizeof(redfish_http_tx_state_data_t));
		redfish_http_tx_state_data_t * app_state = state->app_state;
		if (!app_state) {
			LOG_WRN("Redfish chassis sensor endpoint handler failure 0x01, returning HTTP/500");
			state->http_retcode = 500;
			return 0;
		}
		app_state->tx_pos = 0;
		app_state->total_length = 0;

		redfish_sensor_list = malloc(sizeof(struct json_redfish_v1_chassis_sensors));
		if (!redfish_sensor_list) {
			LOG_WRN("Redfish chassis sensor endpoint handler failure 0x02, returning HTTP/500");
			state->http_retcode = 500;
			return 0;
		}

		memset(redfish_sensor_list, 0, sizeof(struct json_redfish_v1_chassis_sensors));

		redfish_sensor_list->odata_id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
		if (!redfish_sensor_list->odata_id) {
			LOG_WRN("Redfish chassis sensor endpoint handler failure 0x03, returning HTTP/500");
			state->http_retcode = 500;
			free(redfish_sensor_list);
			return 0;
		}

		redfish_sensor_list->odata_context = REDFISH_BASE_PATH "/$metadata#Sensor.Sensor";
		snprintf((char*)redfish_sensor_list->odata_id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, REDFISH_CHASSIS_SENSOR_LIST_PATH_FORMAT, chassis_id);
		redfish_sensor_list->odata_type = "#Sensor.v1_5_0.Sensor";

		redfish_sensor_list->description = redfish_static_string_sensor;
		redfish_sensor_list->name = redfish_static_string_sensor;
		redfish_sensor_list->id = redfish_static_string_sensor;

		chassis_data_fetch_ret = get_chassis_sensor_data(chassis_id, redfish_sensor_list);

		if (!chassis_data_fetch_ret) {
			ret = json_obj_encode_buf(json_redfish_v1_chassis_sensors_desc,
						ARRAY_SIZE(json_redfish_v1_chassis_sensors_desc), redfish_sensor_list,
						app_state->tx_buffer,
						REDFISH_JSON_STRING_BUFFER_SIZE - 1);
		}

		// Free any allocated data within the Redfish structures
		for (i = 0; i < redfish_sensor_list->sensors_count; i++) {
			free((void*)redfish_sensor_list->sensors[i].odata_id);
			free((void*)redfish_sensor_list->sensors[i].id);
			free((void*)redfish_sensor_list->sensors[i].name);
			free((void*)redfish_sensor_list->sensors[i].description);
			free((void*)redfish_sensor_list->sensors[i].member_id);
			for (j = 0; j < redfish_sensor_list->sensors[i].status.conditions_count; j++) {
				free((char*)redfish_sensor_list->sensors[i].status.conditions[j].message);
				free((char*)redfish_sensor_list->sensors[i].status.conditions[j].message_id);
				free((char*)redfish_sensor_list->sensors[i].status.conditions[j].resolution);
				free((char*)redfish_sensor_list->sensors[i].status.conditions[j].severity);
				free((char*)redfish_sensor_list->sensors[i].status.conditions[j].timestamp);
			}
		}

		free((void*)redfish_sensor_list->odata_id);
		free(redfish_sensor_list);

		if (chassis_data_fetch_ret) {
			state->http_retcode = 404;
			return 0;
		}
		else if (ret) {
			LOG_WRN("Redfish chassis sensor endpoint handler failure 0x04 (%d), returning HTTP/500", ret);
			state->http_retcode = 500;
			return 0;
		}

		SEND_STANDARD_HEADER((*state), "Redfish API")

		prev_tx_pos = state->tx_buf_pos;
		SEND_CUSTOM_PRINTF_CUSTOM_FORMAT((*state), "%s", app_state->tx_buffer);
		app_state->tx_pos += state->tx_buf_pos - prev_tx_pos;

		if (state->tx_buf_pos < (MAX_REQUEST_SIZE_BYTES - STANDARD_FOOTER_SIZE - 1)) {
			SEND_STANDARD_FOOTER((*state));

			state->http_retcode = 200;
			return 0;
		}
		else {
			return 1;
		}
	}
	else {
		redfish_http_tx_state_data_t * app_state = state->app_state;

		prev_tx_pos = state->tx_buf_pos;
		SEND_CUSTOM_PRINTF_CUSTOM_FORMAT((*state), "%s", app_state->tx_buffer + app_state->tx_pos);
		app_state->tx_pos += state->tx_buf_pos - prev_tx_pos;

		if (state->tx_buf_pos < (MAX_REQUEST_SIZE_BYTES - 1)) {
			SEND_STANDARD_FOOTER((*state));

			state->http_retcode = 200;
			return 0;
		}
		else {
			return 1;
		}
	}
}

static kestrel_chassis_data_t kestrel_chassis_data = {0};
int redfish_v1_chassis_endpoint_handler(zephyr_http_server_request_state_t *state)
{
	struct json_redfish_v1_chassis redfish_chassis_data_list;
	uint8_t * json_string_buffer = malloc(REDFISH_JSON_STRING_BUFFER_SIZE);
	int i;
	int ret;

	if (!json_string_buffer) {
		state->http_retcode = 500;
		return 0;
	}

	const char * chassis_id = state->url_path + strlen(REDFISH_CHASSIS_PATH) - 1;
	char chassis_actions_reset_action_target_url[REDFISH_MAX_URL_PATH_LENGTH];

	if (!validate_session_authorized(state)) {
		state->http_retcode = 401;
		free(json_string_buffer);
		return 0;
	}

	if (get_chassis_vpd(chassis_id, &kestrel_chassis_data) < 0) {
		state->http_retcode = 404;
		free(json_string_buffer);
		return 0;
	}

	redfish_chassis_data_list.thermal.odata_id = malloc(REDFISH_MAX_STRING_ATTRIBUTE_LENGTH);
	if (!redfish_chassis_data_list.thermal.odata_id) {
		state->http_retcode = 500;
		free(json_string_buffer);
		return 0;
	}

	redfish_chassis_data_list.odata_context = REDFISH_BASE_PATH "/$metadata#Chassis.Chassis";
	redfish_chassis_data_list.odata_id = REDFISH_CHASSIS_PATH;
	redfish_chassis_data_list.odata_type = "#Chassis.v1_9_0.Chassis";
	redfish_chassis_data_list.description = redfish_static_string_chassis;
	redfish_chassis_data_list.id = "chassis";
	redfish_chassis_data_list.name = redfish_static_string_chassis;
	snprintf(chassis_actions_reset_action_target_url, sizeof(chassis_actions_reset_action_target_url)-1, "%s%s%s",  REDFISH_BASE_PATH "/Chassis/", chassis_id, "/Actions/Chassis.Reset");
	redfish_chassis_data_list.actions.reset_action.target = chassis_actions_reset_action_target_url;
	redfish_chassis_data_list.actions.reset_action.allowable_values[0] = redfish_static_string_on;
	redfish_chassis_data_list.actions.reset_action.allowable_values[1] = redfish_static_string_forceoff;
	redfish_chassis_data_list.actions.reset_action.allowable_values_count = 2;
#if 0
	// Not yet supported by Kestrel
	redfish_chassis_data_list.actions.reset_action.allowable_values[2] = redfish_static_string_gracefulrestart;
	redfish_chassis_data_list.actions.reset_action.allowable_values[3] = redfish_static_string_gracefulshutdown;
	redfish_chassis_data_list.actions.reset_action.allowable_values_count = 4;
#endif
	redfish_chassis_data_list.asset_tag = kestrel_chassis_data.asset_tag;
	redfish_chassis_data_list.chassis_type = kestrel_chassis_data.chassis_type;
	redfish_chassis_data_list.depth_mm = kestrel_chassis_data.depth_mm;
	redfish_chassis_data_list.environmental_class = kestrel_chassis_data.environmental_class;
	redfish_chassis_data_list.height_mm = kestrel_chassis_data.height_mm;
	redfish_chassis_data_list.indicator_led = redfish_static_string_off;
	redfish_chassis_data_list.manufacturer = kestrel_chassis_data.manufacturer;
	redfish_chassis_data_list.model = kestrel_chassis_data.model;
	redfish_chassis_data_list.part_number = kestrel_chassis_data.part_number;
	redfish_chassis_data_list.serial_number = kestrel_chassis_data.serial_number;
	redfish_chassis_data_list.uuid = kestrel_chassis_data.uuid;
	redfish_chassis_data_list.weight_kg = kestrel_chassis_data.weight_kg;
	redfish_chassis_data_list.width_mm = kestrel_chassis_data.width_mm;
	if (host_power_status == HOST_POWER_STATUS_OFFLINE) {
		redfish_chassis_data_list.power_state = redfish_static_string_off;
	}
	else {
		redfish_chassis_data_list.power_state = redfish_static_string_on;
	}
	redfish_chassis_data_list.status.conditions_count = 0;
	redfish_chassis_data_list.status.health = redfish_static_string_ok;
	redfish_chassis_data_list.status.health_rollup = redfish_static_string_ok;
	redfish_chassis_data_list.status.state = redfish_static_string_enabled;
	snprintf((char*)redfish_chassis_data_list.thermal.odata_id, REDFISH_MAX_STRING_ATTRIBUTE_LENGTH, REDFISH_THERMAL_LIST_PATH_FORMAT, chassis_id);

	ret = json_obj_encode_buf(json_redfish_v1_chassis_desc,
				ARRAY_SIZE(json_redfish_v1_chassis_desc), &redfish_chassis_data_list,
				json_string_buffer,
				REDFISH_JSON_STRING_BUFFER_SIZE - 1);

	// Free any allocated data within the Redfish structures
	for (i = 0; i < redfish_chassis_data_list.status.conditions_count; i++) {
		free((char*)redfish_chassis_data_list.status.conditions[i].message);
		free((char*)redfish_chassis_data_list.status.conditions[i].message_id);
		free((char*)redfish_chassis_data_list.status.conditions[i].resolution);
		free((char*)redfish_chassis_data_list.status.conditions[i].severity);
		free((char*)redfish_chassis_data_list.status.conditions[i].timestamp);
	}

	if (ret) {
		state->http_retcode = 500;
		free((void*)redfish_chassis_data_list.thermal.odata_id);
		free(json_string_buffer);
		return 0;
	}

	SEND_STANDARD_HEADER((*state), "Redfish API")

	SEND_CUSTOM_PRINTF_CUSTOM_FORMAT((*state), "%s", json_string_buffer);

	SEND_STANDARD_FOOTER((*state));

	free((void*)redfish_chassis_data_list.thermal.odata_id);
	free(json_string_buffer);

	state->http_retcode = 200;
	return 0;
}

int redfish_v1_system_endpoint_handler(zephyr_http_server_request_state_t *state)
{
	struct json_redfish_v1_system redfish_system_data_list;
	uint8_t * json_string_buffer = malloc(REDFISH_JSON_STRING_BUFFER_SIZE);
	int i;
	int ret;

	if (!json_string_buffer) {
		state->http_retcode = 500;
		return 0;
	}

	if (!validate_session_authorized(state)) {
		state->http_retcode = 401;
		free(json_string_buffer);
		return 0;
	}

	redfish_system_data_list.odata_context = REDFISH_BASE_PATH "/$metadata#ComputerSystem.ComputerSystem";
	redfish_system_data_list.odata_id = REDFISH_SYSTEM_PATH;
	redfish_system_data_list.odata_type = "#ComputerSystem.v1_6_0.ComputerSystem";
	redfish_system_data_list.actions.reset_action.target = REDFISH_BASE_PATH "/Systems/" REDFISH_SYSTEM0_ID "/Actions/ComputerSystem.Reset";
	redfish_system_data_list.actions.reset_action.allowable_values[0] = redfish_static_string_on;
	redfish_system_data_list.actions.reset_action.allowable_values[1] = redfish_static_string_forceoff;
	redfish_system_data_list.actions.reset_action.allowable_values_count = 2;
#if 0
	// Not yet supported by Kestrel
	redfish_system_data_list.actions.reset_action.allowable_values[2] = redfish_static_string_gracefulrestart;
	redfish_system_data_list.actions.reset_action.allowable_values[3] = redfish_static_string_gracefulshutdown;
	redfish_system_data_list.actions.reset_action.allowable_values_count = 4;
#endif
	redfish_system_data_list.description = redfish_static_string_computer_system;
	redfish_system_data_list.id = "system";
	redfish_system_data_list.indicator_led = redfish_static_string_off;
	redfish_system_data_list.name = redfish_static_string_computer_system;
	if (host_power_status == HOST_POWER_STATUS_OFFLINE) {
		redfish_system_data_list.power_state = redfish_static_string_off;
	}
	else {
		redfish_system_data_list.power_state = redfish_static_string_on;
	}
	redfish_system_data_list.status.conditions_count = 0;
	redfish_system_data_list.status.health = redfish_static_string_ok;
	redfish_system_data_list.status.health_rollup = redfish_static_string_ok;
	redfish_system_data_list.status.state = redfish_static_string_enabled;
	redfish_system_data_list.system_type = redfish_static_string_physical;

	ret = json_obj_encode_buf(json_redfish_v1_system_desc,
				ARRAY_SIZE(json_redfish_v1_system_desc), &redfish_system_data_list,
				json_string_buffer,
				REDFISH_JSON_STRING_BUFFER_SIZE - 1);

	// Free any allocated data within the Redfish structures
	for (i = 0; i < redfish_system_data_list.status.conditions_count; i++) {
		free((char*)redfish_system_data_list.status.conditions[i].message);
		free((char*)redfish_system_data_list.status.conditions[i].message_id);
		free((char*)redfish_system_data_list.status.conditions[i].resolution);
		free((char*)redfish_system_data_list.status.conditions[i].severity);
		free((char*)redfish_system_data_list.status.conditions[i].timestamp);
	}

	if (ret) {
		state->http_retcode = 500;
		free(json_string_buffer);
		return 0;
	}

	SEND_STANDARD_HEADER((*state), "Redfish API")

	SEND_CUSTOM_PRINTF_CUSTOM_FORMAT((*state), "%s", json_string_buffer);

	SEND_STANDARD_FOOTER((*state));

	free(json_string_buffer);

	state->http_retcode = 200;
	return 0;
}

static void load_stock_redfish_success_message(struct json_redfish_v1_message_response *redfish_message_response)
{
	redfish_message_response->message.code = "Base.1.8.Success";
	redfish_message_response->message.message = "Successfully Completed Request";
	redfish_message_response->message.extended_info[0].odata_type = "#Message.v1_1_1.Message";
	redfish_message_response->message.extended_info[0].message_id = "Base.1.8.Success";
	redfish_message_response->message.extended_info[0].message = "Successfully Completed Request";
	redfish_message_response->message.extended_info[0].severity = redfish_static_string_ok;
	redfish_message_response->message.extended_info[0].message_severity = redfish_static_string_ok;
	redfish_message_response->message.extended_info[0].number_of_args = 0;
	redfish_message_response->message.extended_info[0].resolution = "None";
	redfish_message_response->message.extended_info_count = 1;
}

static void load_stock_redfish_nooperation_message(struct json_redfish_v1_message_response *redfish_message_response)
{
	redfish_message_response->message.code = "Base.1.8.NoOperation";
	redfish_message_response->message.message = "The request body submitted contain no data to act upon and no changes to the resource took place.";
	redfish_message_response->message.extended_info[0].odata_type = "#Message.v1_1_1.Message";
	redfish_message_response->message.extended_info[0].message_id = "Base.1.8.NoOperation";
	redfish_message_response->message.extended_info[0].message = "The request body submitted contain no data to act upon and no changes to the resource took place.";
	redfish_message_response->message.extended_info[0].severity = redfish_static_string_warning;
	redfish_message_response->message.extended_info[0].message_severity = redfish_static_string_warning;
	redfish_message_response->message.extended_info[0].number_of_args = 0;
	redfish_message_response->message.extended_info[0].resolution = "Add properties in the JSON object and resubmit the request.";
	redfish_message_response->message.extended_info_count = 1;
}

http_multipart_form_parser_state_t redfish_v1_system_reset_endpoint_handler_rx_parser_state = {0};
char redfish_v1_system_reset_endpoint_handler_rx_buffer[REDFISH_JSON_STRING_BUFFER_SIZE];
size_t redfish_v1_system_reset_endpoint_handler_rx_buffer_pos = 0;
size_t redfish_v1_system_reset_endpoint_handler_content_length = 0;
void redfish_v1_system_reset_endpoint_handler_rx(zephyr_http_server_request_state_t *state)
{
	size_t len;
	if (state->status == HTTP_SERVER_DATA_ABORTED) {
		redfish_v1_system_reset_endpoint_handler_rx_buffer_pos = 0;
		redfish_v1_system_reset_endpoint_handler_content_length = 0;
	}
	else {
		len = state->rx_data_len;
		if (len > (sizeof(redfish_v1_system_reset_endpoint_handler_rx_buffer) - redfish_v1_system_reset_endpoint_handler_rx_buffer_pos - 1)) {
			len = sizeof(redfish_v1_system_reset_endpoint_handler_rx_buffer) - redfish_v1_system_reset_endpoint_handler_rx_buffer_pos - 1;
		}
		memcpy(redfish_v1_system_reset_endpoint_handler_rx_buffer, state->rx_buf, len);
		redfish_v1_system_reset_endpoint_handler_rx_buffer_pos += len;
		if (state->status == HTTP_SERVER_DATA_FINAL) {
			redfish_v1_system_reset_endpoint_handler_content_length = redfish_v1_system_reset_endpoint_handler_rx_buffer_pos;
			redfish_v1_system_reset_endpoint_handler_rx_buffer[redfish_v1_system_reset_endpoint_handler_rx_buffer_pos] = 0x0;
			redfish_v1_system_reset_endpoint_handler_rx_buffer_pos = 0;
		}
	}
}

int redfish_v1_system_reset_endpoint_handler_tx(zephyr_http_server_request_state_t *state)
{
	uint8_t * json_string_buffer = malloc(REDFISH_JSON_STRING_BUFFER_SIZE);
	struct json_redfish_v1_message_response redfish_message_response;
	struct json_redfish_odata_system_action_reset_cmd cmd_params = {};
	int ret;

	if (!json_string_buffer) {
		state->http_retcode = 500;
		return 0;
	}

	if (!validate_session_authorized(state)) {
		state->http_retcode = 401;
		free(json_string_buffer);
		return 0;
	}

	if (redfish_v1_system_reset_endpoint_handler_content_length < 0) {
		state->http_retcode = 501;
		free(json_string_buffer);
		return 0;
	}

	if (!kestrel_basic_init_complete) {
		state->http_retcode = 503;
		free(json_string_buffer);
		return 0;
	}
	else {
		ret = json_obj_parse(redfish_v1_system_reset_endpoint_handler_rx_buffer, redfish_v1_system_reset_endpoint_handler_content_length,
			json_redfish_odata_system_action_reset_cmd_desc, ARRAY_SIZE(json_redfish_odata_system_action_reset_cmd_desc), &cmd_params);
		if (ret < 0) {
			state->http_retcode = 501;
			free(json_string_buffer);
			return 0;
		}

		if (strcmp(cmd_params.reset_type, "On") == 0) {
			if (host_power_status == HOST_POWER_STATUS_OFFLINE) {
				load_stock_redfish_success_message(&redfish_message_response);
				power_on_host();
			}
			else {
				load_stock_redfish_nooperation_message(&redfish_message_response);
			}
		}
		else if (strcmp(cmd_params.reset_type, "ForceOff") == 0) {
			if (host_power_status != HOST_POWER_STATUS_OFFLINE) {
				load_stock_redfish_success_message(&redfish_message_response);
				power_off_chassis(0);
			}
			else {
				load_stock_redfish_nooperation_message(&redfish_message_response);
			}
		}
		else {
			state->http_retcode = 501;
			free(json_string_buffer);
			return 0;
		}
	}

	ret = json_obj_encode_buf(json_redfish_v1_message_response_desc,
				ARRAY_SIZE(json_redfish_v1_message_response_desc), &redfish_message_response,
				json_string_buffer,
				REDFISH_JSON_STRING_BUFFER_SIZE - 1);

	if (ret) {
		state->http_retcode = 500;
		free(json_string_buffer);
		return 0;
	}

	SEND_STANDARD_HEADER((*state), "Redfish API")

	SEND_CUSTOM_PRINTF_CUSTOM_FORMAT((*state), "%s", json_string_buffer);

	SEND_STANDARD_FOOTER((*state));

	free(json_string_buffer);

	state->http_retcode = 200;
	return 0;
}

KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER(redfish,				"/redfish",				"application/json", generic_rx_handler_noop, redfish_version_handler,			MAX_REQUEST_SIZE_BYTES)
KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER(redfish_v1_base_path,		REDFISH_BASE_PATH "/",			"application/json", generic_rx_handler_noop, redfish_v1_service_list_handler,		MAX_REQUEST_SIZE_BYTES)
KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER(redfish_v1_chassis_list_path,	REDFISH_CHASSIS_LIST_PATH,		"application/json", generic_rx_handler_noop, redfish_v1_chassis_list_endpoint_handler,	MAX_REQUEST_SIZE_BYTES)
KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER(redfish_v1_chassis_path,		REDFISH_CHASSIS_PATH,			"application/json", generic_rx_handler_noop, redfish_v1_chassis_endpoint_handler,	MAX_REQUEST_SIZE_BYTES)
KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER(redfish_v1_system_list_path,	REDFISH_SYSTEM_LIST_PATH,		"application/json", generic_rx_handler_noop, redfish_v1_system_list_endpoint_handler,	MAX_REQUEST_SIZE_BYTES)
KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER(redfish_v1_system_path,		REDFISH_SYSTEM_PATH,			"application/json", generic_rx_handler_noop, redfish_v1_system_endpoint_handler,	MAX_REQUEST_SIZE_BYTES)
KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER(redfish_v1_thermal_list_path,	REDFISH_THERMAL_LIST_PATH,		"application/json", generic_rx_handler_noop, redfish_v1_thermal_list_endpoint_handler,	MAX_REQUEST_SIZE_BYTES)
KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER(redfish_v1_sensor_list_path,	REDFISH_CHASSIS_SENSOR_LIST_PATH,	"application/json", generic_rx_handler_noop, redfish_v1_sensor_list_endpoint_handler,	MAX_REQUEST_SIZE_BYTES)

KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER_FULL(redfish_v1_system_action_reset_path,	REDFISH_COMPUTER_SYSTEM_ACTION_RESET_PATH,	"application/json",  BIT(HTTP_POST), NULL, 0, redfish_v1_system_reset_endpoint_handler_rx, redfish_v1_system_reset_endpoint_handler_tx,	MAX_REQUEST_SIZE_BYTES)
