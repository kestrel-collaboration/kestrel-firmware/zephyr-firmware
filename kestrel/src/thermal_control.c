// © 2021 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#include <string.h>

#include "thermal_control.h"

void intialize_pid_structure(struct pid_data_t *pid_data)
{
	pid_data->enabled = 0;
	pid_data->setpoint = 0;
	pid_data->sense = 0;
	pid_data->drive = 0;
	pid_data->dead_band = 0;
	pid_data->error_filter_length = 0;
	pid_data->p = 0;
	pid_data->i = 0;
	pid_data->d = 0;
	pid_data->d_filter_length = 0;
	pid_data->upper_drive_limit = 0;
	pid_data->lower_drive_limit = 0;
	pid_data->lower_integrator_clamp = 0;
	pid_data->upper_integrator_clamp = 0;
	pid_data->prev_sense = 0;
	pid_data->prev_error_filter_write_pos = 0;
	pid_data->filtered_error = 0;
	pid_data->prev_error = 0;
	pid_data->prev_error_d_write_pos = 0;
	pid_data->integrator = 0;
	pid_data->filtered_rate = 0;

	memset(pid_data->prev_error_filter, 0, sizeof(pid_data->prev_error_filter));
	memset(pid_data->prev_error_d, 0, sizeof(pid_data->prev_error_d));
}

void run_pid_step(struct pid_data_t *pid_data)
{
	double error;
	double d_error;
	int i;

	// Compute instantaneous error
	error = (pid_data->sense - pid_data->setpoint) / 100.0;

	// Gracefully handle changes to error filter length
	if (pid_data->error_filter_length > MAX_SENSE_FILTER_LENGTH_SAMPLES) {
		pid_data->error_filter_length = MAX_SENSE_FILTER_LENGTH_SAMPLES;
	}
	if (pid_data->prev_error_filter_length != pid_data->error_filter_length) {
		memset(pid_data->prev_error_filter, 0, sizeof(pid_data->prev_error_filter));
		pid_data->prev_error_filter_write_pos = 0;
	}

	// Store error value in error filter array
	if (pid_data->prev_error_filter_write_pos >= pid_data->error_filter_length) {
		pid_data->prev_error_filter_write_pos = 0;
	}
	pid_data->prev_error_filter[pid_data->prev_error_filter_write_pos] = error;
	pid_data->prev_error_filter_write_pos++;

	if (pid_data->error_filter_length > 0) {
		// Run error filter
		error = 0;
		for (i = 0; i < pid_data->error_filter_length; i++) {
			error += pid_data->prev_error_filter[i];
		}
		error /= pid_data->error_filter_length;
	}
	pid_data->filtered_error = error;

	// Apply dead band setting
	if ((error > 0) && (error < pid_data->dead_band)) {
		error = 0;
	}
	if ((error < 0) && (error > (pid_data->dead_band * -1.0))) {
		error = 0;
	}

	// Run integration
	pid_data->integrator += (error * pid_data->timestep);

	// Clamp integrator
	if (pid_data->i == 0) {
		pid_data->integrator = 0;
	}
	else {
		if (pid_data->integrator > (pid_data->upper_integrator_clamp / pid_data->i)) {
			pid_data->integrator = pid_data->upper_integrator_clamp / pid_data->i;
		}
		if (pid_data->integrator < (pid_data->lower_integrator_clamp / pid_data->i)) {
			pid_data->integrator = pid_data->lower_integrator_clamp / pid_data->i;
		}
	}

	// Gracefully handle changes to derivative filter length
	if (pid_data->d_filter_length > MAX_DERIVATIVE_FILTER_LENGTH_SAMPLES) {
		pid_data->d_filter_length = MAX_DERIVATIVE_FILTER_LENGTH_SAMPLES;
	}
	if (pid_data->prev_d_filter_length != pid_data->d_filter_length) {
		memset(pid_data->prev_error_d, 0, sizeof(pid_data->prev_error_d));
		pid_data->prev_error_d_write_pos = 0;
	}

	d_error = 0;
	if (pid_data->d_filter_length > 0) {
		// Run derivative sample filter
		for (i = 0; i < pid_data->d_filter_length; i++) {
			d_error += pid_data->prev_error_d[i];
		}
		d_error /= pid_data->d_filter_length;
	}
	else {
		// Filter disabled
		d_error = error - pid_data->prev_error;
	}
	pid_data->filtered_rate = d_error;

	// Calculate new drive value
	pid_data->drive = error * pid_data->p;
	pid_data->drive += pid_data->integrator * pid_data->i;
	pid_data->drive += pid_data->filtered_rate * pid_data->d;

	// Clamp outputs
	if (pid_data->drive > pid_data->upper_drive_limit) {
		pid_data->drive = pid_data->upper_drive_limit;
	}
	if (pid_data->drive < pid_data->lower_drive_limit) {
		pid_data->drive = pid_data->lower_drive_limit;
	}

	// Save previous sense and error values
	pid_data->prev_sense = pid_data->sense;
	pid_data->prev_error = error;

	// Store error value in derivative sample filter
	if (pid_data->prev_error_d_write_pos >= pid_data->d_filter_length) {
		pid_data->prev_error_d_write_pos = 0;
	}
	pid_data->prev_error_d[pid_data->prev_error_d_write_pos] = error - pid_data->prev_error;
	pid_data->prev_error_d_write_pos++;

	// Save last sample and derivative filter lengths
	pid_data->prev_error_filter_length = pid_data->error_filter_length;
	pid_data->prev_d_filter_length = pid_data->d_filter_length;
}