// © 2021-2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _THERMAL_CONTROL_H
#define _THERMAL_CONTROL_H

#include <stdint.h>
#include <stdlib.h>

#include "sensors.h"
#include "fan_tach.h"

#define MAX_PID_CONTROLLERS MAX(MAX_TACH_DEVICES, MAX_FAN_DEVICES)

#define MAX_SENSE_FILTER_LENGTH_SAMPLES		8192
#define MAX_DERIVATIVE_FILTER_LENGTH_SAMPLES	8192

struct pid_data_t
{
	uint8_t enabled;

	double setpoint;
	double sense;
	double drive;
	double upper_drive_limit;
	double lower_drive_limit;
	double upper_integrator_clamp;
	double lower_integrator_clamp;

	double dead_band;
	size_t error_filter_length;
	double p;
	double i;
	double d;
	size_t d_filter_length;
	double timestep;

	// Internal values
	double prev_sense;
	double prev_error_filter[MAX_SENSE_FILTER_LENGTH_SAMPLES];
	size_t prev_error_filter_write_pos;
	size_t prev_error_filter_length;
	double filtered_error;
	double prev_error;
	double prev_error_d[MAX_DERIVATIVE_FILTER_LENGTH_SAMPLES];
	size_t prev_error_d_write_pos;
	size_t prev_d_filter_length;
	double integrator;
	double filtered_rate;
};

void intialize_pid_structure(struct pid_data_t *pid_data);
void run_pid_step(struct pid_data_t *pid_data);

#endif // _THERMAL_CONTROL_H
