// © 2022 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

#ifndef _ONEWIRE_FUNCTIONS_H
#define _ONEWIRE_FUNCTIONS_H

#define ONEWIRE_BUS_CMD_MATCH           0x55
#define ONEWIRE_BUS_CMD_SKIP_ROM        0xcc
#define ONEWIRE_BUS_CMD_READ_SCRATCHPAD 0xbe
#define ONEWIRE_BUS_CMD_SEARCH          0xf0

#define ONEWIRE_DEVICE_FAMILY_TEMPERATURE_SENSOR 0x28

#define ONEWIRE_DS18B20_CMD_CONVERT 0x44

int scan_onewire_bus(uint8_t *base_address, uint64_t *detected_devices, int *device_count, int maximum_device_count);
int access_onewire_device(uint8_t *base_address, uint64_t device_serial_number, int match_all_devices);
int start_simultaneous_ds18b20_temperature_conversions(uint8_t *base_address);
int read_ds18b20_temperature(uint8_t *base_address, uint64_t device_serial_number, int skip_conversion, float *temperature);

#endif // _ONEWIRE_FUNCTIONS_H