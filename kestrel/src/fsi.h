// © 2020 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

#ifndef _FSI_H
#define _FSI_H

#include <stdint.h>
#include <zephyr/shell/shell.h>

typedef enum
{
    FSI_DATA_LENGTH_BYTE = 0,
    FSI_DATA_LENGTH_HALFWORD = 1,
    FSI_DATA_LENGTH_WORD = 2
} fsi_data_length_t;

// Peripheral registers
#define FSI_MASTER_REG_DEVICE_ID_HIGH 0x0
#define FSI_MASTER_REG_DEVICE_ID_LOW  0x4
#define FSI_MASTER_REG_DEVICE_VERSION 0x8
#define FSI_MASTER_REG_SYS_CLK_FREQ   0xc
#define FSI_MASTER_REG_SID_ADR        0x10
#define FSI_MASTER_REG_CONTROL        0x14
#define FSI_MASTER_REG_STATUS         0x18
#define FSI_MASTER_REG_TX_DATA        0x1c
#define FSI_MASTER_REG_RX_DATA        0x20
#define FSI_MASTER_REG_DMA_IRQ        0x24
#define FSI_MASTER_REG_PHY_CFG        0x28

#define FSI_MASTER_PHY_CFG_CLK_DIV_MASK  0xff
#define FSI_MASTER_PHY_CFG_CLK_DIV_SHIFT 0

#define FSI_MASTER_SID_SLAVE_ID_MASK     0x3
#define FSI_MASTER_SID_SLAVE_ID_SHIFT    29
#define FSI_MASTER_SID_ADDRESS_MASK      0x1fffff
#define FSI_MASTER_SID_ADDRESS_SHIFT     8
#define FSI_MASTER_SID_DATA_LENGTH_MASK  0x3
#define FSI_MASTER_SID_DATA_LENGTH_SHIFT 0

#define FSI_MASTER_CTL_CMD_ISSUE_DELAY_MASK  0xff
#define FSI_MASTER_CTL_CMD_ISSUE_DELAY_SHIFT 8
#define FSI_MASTER_CTL_DATA_DIRECTION_MASK   0x1
#define FSI_MASTER_CTL_DATA_DIRECTION_SHIFT  1
#define FSI_MASTER_CTL_ENABLE_CRC_MASK       0x1
#define FSI_MASTER_CTL_ENABLE_CRC_SHIFT      17
#define FSI_MASTER_CTL_ENABLE_EER_MASK       0x1
#define FSI_MASTER_CTL_ENABLE_EER_SHIFT      16
#define FSI_MASTER_CTL_CYCLE_START_MASK      0x1
#define FSI_MASTER_CTL_CYCLE_START_SHIFT     0

#define FSI_MASTER_STAT_CYCLE_ERROR_MASK     0x7
#define FSI_MASTER_STAT_CYCLE_ERROR_SHIFT    8
#define FSI_MASTER_STAT_CYCLE_COMPLETE_MASK  0x1
#define FSI_MASTER_STAT_CYCLE_COMPLETE_SHIFT 0

#define FSI_DIRECTION_READ  0
#define FSI_DIRECTION_WRITE 1

// General FSI register definitions for IBM CFAM slaves
#define IBM_CFAM_FSI_SMODE 0x0800
#define IBM_CFAM_FSI_SISC  0x0802
#define IBM_CFAM_FSI_SSTAT 0x0805

// Boot-related CFAM register definitions for IBM POWER9 processors
#define IBM_POWER9_FSI_A_SI1S         0x081c
#define IBM_POWER9_LL_MODE_REG        0x0840
#define IBM_POWER9_FSI2PIB_CHIPID     0x100a
#define IBM_POWER9_FSI2PIB_INTERRUPT  0x100b
#define IBM_POWER9_FSI2PIB_TRUE_MASK  0x100d
#define IBM_POWER9_CBS_CS             0x2801
#define IBM_POWER9_SBE_CTRL_STATUS    0x2808
#define IBM_POWER9_SBE_MSG_REGISTER   0x2809
#define IBM_POWER9_ROOT_CTRL0         0x2810
#define IBM_POWER9_PERV_CTRL0         0x281a
#define IBM_POWER9_HB_MBX5_REG        0x283c
#define IBM_POWER9_SCRATCH_REGISTER_8 0x283f
#define IBM_POWER9_ROOT_CTRL8         0x2918
#define IBM_POWER9_ROOT_CTRL1_CLEAR   0x2931

// SCOM definitions
#define IBM_POWER9_SCOM_PIB_BASE 0x1000

#define IBM_SCOM_FSI_CMD_REG       0x08
#define IBM_SCOM_FSI_STATUS_REG    0x1c
#define IBM_SCOM_FSI_RESET_REG     0x1c
#define IBM_SCOM_FSI_DATA0_REG     0x00
#define IBM_SCOM_FSI_DATA1_REG     0x04
#define IBM_SCOM_FSI2PIB_RESET_REG 0x18

#define IBM_SCOM_FSI_CMD_READ  0x00000000
#define IBM_SCOM_FSI_CMD_WRITE 0x80000000

#define IBM_SCOM_PIB_RESULT_SUCCESS    0
#define IBM_SCOM_PIB_RESULT_BLOCKED    1
#define IBM_SCOM_PIB_RESULT_OFFLINE    2
#define IBM_SCOM_PIB_RESULT_PARTIAL    3
#define IBM_SCOM_PIB_RESULT_BAD_ADDR   4
#define IBM_SCOM_PIB_RESULT_CLK_ERR    5
#define IBM_SCOM_PIB_RESULT_PARITY_ERR 6
#define IBM_SCOM_PIB_RESULT_TIMEOUT    7

// Platform data
#define IBM_POWER9_SLAVE_ID 0

static inline uint32_t read_openfsi_register(unsigned long base_address, uint8_t reg)
{
    return *((volatile uint32_t *)(base_address + reg));
}

static inline void write_openfsi_register(unsigned long base_address, uint8_t reg, uint32_t data)
{
    *((volatile uint32_t *)(base_address + reg)) = data;
}

int access_fsi_mem(uint8_t slave_id, uint32_t address, fsi_data_length_t data_length, uint8_t write, uint32_t *data);
int access_cfam(uint8_t slave_id, uint32_t cfam_address, fsi_data_length_t data_length, uint8_t write, uint32_t *data);
int access_scom(uint8_t slave_id, uint32_t scom_address, uint8_t write, uint64_t *data);

int run_pre_ipl_fixups(void);
int start_ipl(int side);
int print_sbe_status(const struct shell *shell);

#endif // _FSI_H
