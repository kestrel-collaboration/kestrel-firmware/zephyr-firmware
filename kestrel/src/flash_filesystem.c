// Copyright (c) 2019 Peter Bigot Consulting, LLC
// Copyright (c) 2021-2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#include "build_config.h"

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(flash_filesystem, LOG_LEVEL_DBG);

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/sys/crc.h>
#include <zephyr/fs/fs.h>
#include <zephyr/fs/littlefs.h>
#include <zephyr/drivers/flash.h>
#include <zephyr/storage/flash_map.h>

#include "kestrel.h"

#include "flash_filesystem.h"

extern const struct flash_area *flash_map;
extern const int flash_map_entries;

FS_LITTLEFS_DECLARE_DEFAULT_CONFIG(bmc_config);
struct fs_mount_t kestrel_config_lfs_fs_mount = {
	.type = FS_LITTLEFS,
	.fs_data = &bmc_config,
	.storage_dev = (void *)FIXED_PARTITION_ID(CONFIG_PARTITION_NODE),
	.mnt_point = "/config",
};

FS_LITTLEFS_DECLARE_DEFAULT_CONFIG(storage);
struct fs_mount_t kestrel_storage_lfs_fs_mount = {
	.type = FS_LITTLEFS,
	.fs_data = &storage,
	.storage_dev = (void *)FIXED_PARTITION_ID(STORAGE_PARTITION_NODE),
	.mnt_point = "/lfs",
};

static int littlefs_increase_infile_value(char *fname)
{
	uint8_t boot_count = 0;
	struct fs_file_t file;
	int rc, ret;

	fs_file_t_init(&file);
	rc = fs_open(&file, fname, FS_O_CREATE | FS_O_RDWR);
	if (rc < 0) {
		LOG_ERR("FAIL: open %s: %d", fname, rc);
		return rc;
	}

	rc = fs_read(&file, &boot_count, sizeof(boot_count));
	if (rc < 0) {
		LOG_ERR("FAIL: read %s: [rd:%d]", fname, rc);
		goto out;
	}
	LOG_INF("%s read count:%u (bytes: %d)\n", fname, boot_count, rc);

	rc = fs_seek(&file, 0, FS_SEEK_SET);
	if (rc < 0) {
		LOG_ERR("FAIL: seek %s: %d", fname, rc);
		goto out;
	}

	boot_count += 1;
	rc = fs_write(&file, &boot_count, sizeof(boot_count));
	if (rc < 0) {
		LOG_ERR("FAIL: write %s: %d", fname, rc);
		goto out;
	}

	LOG_INF("%s write new boot count %u: [wr:%d]\n", fname,
		boot_count, rc);

out:
	ret = fs_close(&file);
	if (ret < 0) {
		LOG_ERR("FAIL: close %s: %d", fname, ret);
		return ret;
	}

	return (rc < 0 ? rc : 0);
}

static int initialize_flash_filesystem_for_mp(struct fs_mount_t *mp)
{
	uintptr_t id = (uintptr_t)mp->storage_dev;
	struct fs_statvfs sbuf;
	const struct flash_area *pfa;
	int rc;

	rc = flash_area_open(id, &pfa);
	if (rc < 0) {
		LOG_ERR("FAIL: unable to find flash area %lu: %d",
		       id, rc);
		return -1;
	}

	LOG_INF("Mounting LFS filesystem %s:%ld on '%s'\n", pfa->fa_dev->name, pfa->fa_off, mp->mnt_point);

	/* Optional wipe flash contents */
	if (IS_ENABLED(CONFIG_APP_WIPE_STORAGE)) {
		LOG_WRN("Erasing flash area ... ");
		rc = flash_area_erase(pfa, 0, pfa->fa_size);
		if (rc) {
			LOG_ERR("Flash erase failed!  rc=%d", rc);
		}
	}

	flash_area_close(pfa);

	rc = fs_mount(mp);
	if (rc < 0) {
		LOG_ERR("FAIL: mount id %lu at %s: %d",
		       (uintptr_t)mp->storage_dev, mp->mnt_point,
		       rc);
		return -1;
	}
	LOG_INF("%s mount: %d", mp->mnt_point, rc);

	rc = fs_statvfs(mp->mnt_point, &sbuf);
	if (rc < 0) {
		LOG_ERR("FAIL: statvfs: %d", rc);
		goto fail;
	}

	LOG_INF("%s: bsize = %lu ; frsize = %lu ;"
	       " blocks = %lu ; bfree = %lu",
	       mp->mnt_point,
	       sbuf.f_bsize, sbuf.f_frsize,
	       sbuf.f_blocks, sbuf.f_bfree);

	return 0;

fail:
	rc = fs_unmount(mp);
	LOG_ERR("%s unmount: %d", mp->mnt_point, rc);

	return -1;
}

int initialize_flash_filesystem(void)
{
	char fname[MAX_PATH_LEN];
	int ret;

	ret = initialize_flash_filesystem_for_mp(&kestrel_config_lfs_fs_mount);
	if (initialize_flash_filesystem_for_mp(&kestrel_storage_lfs_fs_mount) == 0) {
		snprintf(fname, sizeof(fname), "%s/boot_count", kestrel_storage_lfs_fs_mount.mnt_point);
		if (littlefs_increase_infile_value(fname)) {
			LOG_ERR("Unable to increment boot count!  Persistent storage failure?");
		}
	}
	else {
		ret = -1;
	}

	return 0;
}

int write_firmware_to_flash(const struct shell *shell, const char* partition_name, const uint8_t* data, size_t length, int add_litex_header)
{
	static struct flash_area const *flash_area;
	int result;

	// Find firmware Flash map area
	flash_area = NULL;
	for (int i = 0; i < flash_map_entries; i++) {
		if (strcmp(flash_area_label(&flash_map[i]), partition_name) == 0) {
			flash_area = &flash_map[i];
			break;
		}
	}
	if (!flash_area) {
		LOG_ERR("Unable to locate '%s' partition!", partition_name);
		return -ENODEV;
	}

	if (length > flash_area->fa_size) {
		length = flash_area->fa_size;
	}

	if (shell)
		shell_print(shell, "Erasing %s firmware partition...", partition_name);
	result = flash_area_erase(flash_area, 0x0, flash_area->fa_size);
	if (result < 0) {
		if (shell)
			shell_print(shell, "Erase failed!");
		return result;
	}

	if (shell)
		shell_print(shell, "Writing %ld bytes to %s firmware partition...", length, partition_name);
	if (add_litex_header) {
		result = flash_area_write(flash_area, 0x0, &length, 4);
		if (!result) {
			uint32_t crc = crc32_ieee(data, length);
			result = flash_area_write(flash_area, 0x4, &crc, 4);
		}
	}
	if (!result) {
		result = flash_area_write(flash_area, (add_litex_header)?LITEX_HEADER_LENGTH_BYTES:0x0, data, length);
	}
	if (result < 0) {
		if (shell)
			shell_print(shell, "Write failed!");
		return result;
	}

	return 0;
}
