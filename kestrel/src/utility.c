// © 2020 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#include "build_config.h"

#include <generated/soc.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define TICKS_PER_US (CONFIG_CLOCK_FREQUENCY / 1000000LL)

#if WITH_ZEPHYR
#include <zephyr/sys_clock.h>
#include <time.h>
#else
#define SEC_PER_MIN 60
#define MIN_PER_HOUR 60
#define HOUR_PER_DAY 24
#endif

#include "utility.h"

static __inline__ unsigned long long rdtsc(void)
{
    unsigned long long int tsc;
    unsigned long int high;
    unsigned long int low;
    unsigned long int scratch;
    __asm__ volatile("0:        \n"
                     "\tmftbu %0    \n"
                     "\tmftb  %1    \n"
                     "\tmftbu %2    \n"
                     "\tcmpw  %2,%0    \n"
                     "\tbne   0b    \n"
                     : "=r"(high), "=r"(low), "=r"(scratch));
    tsc = high << 32;
    tsc |= low;

    return tsc;
}

#if !(WITH_ZEPHYR)
void usleep(int usecs)
{
    unsigned long long start_tsc;
    unsigned long long current_tsc;
    unsigned long long offset;

    offset = 0;
    start_tsc = rdtsc();

    while (1)
    {
        current_tsc = rdtsc();
        if (current_tsc < start_tsc)
        {
            offset = ULONG_MAX - start_tsc;
            start_tsc = 0;
        }
        if (((current_tsc - start_tsc) + offset) >= (TICKS_PER_US * usecs))
        {
            break;
        }
    }
}
#endif

#if (WITH_ZEPHYR)
// FIXME
// This is extremely primitive; it does not attempt to parse months or years
// A minimal attempt is made to keep basic compatibility with ISO date strings,
// but this remains technically incompatible as date will continue to increase
// while month / year remain zero!
int kestrel_strftime(char * buffer, int buffer_length, uint64_t timestamp) {
    parsed_uptime_stats_t dhms;

    dhms = parse_uptime_ms(timestamp);

    return snprintf(buffer, buffer_length, "%04d-%02d-%02lldT%02lld:%02lld:%02lld", 0, 0, dhms.days, dhms.hours, dhms.minutes, dhms.seconds);
}

double fabs(double value) {
	if (value >= 0.0) {
		return value;
	}
	else {
		return value * -1.0;
	}
}
#endif

parsed_uptime_stats_t parse_uptime_ms(uint64_t uptime) {
    parsed_uptime_stats_t parsed_uptime;

    parsed_uptime.seconds = uptime / 1000;
    parsed_uptime.minutes = parsed_uptime.seconds / SEC_PER_MIN;
    parsed_uptime.hours = parsed_uptime.minutes / MIN_PER_HOUR;
    parsed_uptime.days = parsed_uptime.hours / HOUR_PER_DAY;
    parsed_uptime.hours = parsed_uptime.hours - (parsed_uptime.days * HOUR_PER_DAY);
    parsed_uptime.minutes = parsed_uptime.minutes - ((parsed_uptime.days * HOUR_PER_DAY * MIN_PER_HOUR) + (parsed_uptime.hours * MIN_PER_HOUR));
    parsed_uptime.seconds = parsed_uptime.seconds - ((parsed_uptime.days * HOUR_PER_DAY * MIN_PER_HOUR * SEC_PER_MIN) + (parsed_uptime.hours * MIN_PER_HOUR * SEC_PER_MIN) + (parsed_uptime.minutes * SEC_PER_MIN));
    parsed_uptime.milliseconds = uptime - (((parsed_uptime.days * HOUR_PER_DAY * MIN_PER_HOUR * SEC_PER_MIN) + (parsed_uptime.hours * MIN_PER_HOUR * SEC_PER_MIN) + (parsed_uptime.minutes * SEC_PER_MIN) + parsed_uptime.seconds) * 1000);

    return parsed_uptime;
}