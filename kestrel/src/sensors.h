// © 2021-2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _SENSORS_H
#define _SENSORS_H

#include <generated/mem.h>

#include <zephyr/devicetree.h>

#define MAX_ONEWIRE_DEVICES		128

#define FRU_TYPE_THERMAL_OCC		1
#define FRU_TYPE_THERMAL_ONEWIRE	2

#define MAX_THERMAL_SENSORS	255
#define MAX_PRESSURE_SENSORS	255
#define MAX_SENSOR_DESC_LEN	256

#define SENSOR_MAX_TIMEOUT_MS	(30 * 1000)

#define MAX_SENSOR_CALIBRATION_POINTS	32

#if !defined(ONEWIRE0_BASE)
#if DT_NODE_HAS_STATUS(DT_NODELABEL(onewire0), okay)
#define ONEWIRE0_BASE DT_REG_ADDR(DT_NODELABEL(onewire0))
#endif
#endif
#if !defined(ONEWIRE1_BASE)
#if DT_NODE_HAS_STATUS(DT_NODELABEL(onewire1), okay)
#define ONEWIRE1_BASE DT_REG_ADDR(DT_NODELABEL(onewire1))
#endif
#endif

#if !defined(OPENFSIMASTER_BASE)
#define HAS_FSI_CONTROLLER 1
#else
#define HAS_FSI_CONTROLLER 0
#endif

typedef struct kestrel_sensor_calibration_point {
	double sensor_reading;
	double observed_value;
} kestrel_sensor_calibration_point_t;

typedef struct onewire_sensor_driver_data {
	int onewire_channel;
	uint8_t * controller_address;
	char sensor_driver_id[64];
} onewire_sensor_driver_data_t;

struct kestrel_thermal_sensor_data_temp_t {
	uint32_t sensor_id;
	uint8_t fru_type;
	uint64_t device_handle;
	void * driver_data_ptr;
	double temperature_c;
	char description[MAX_SENSOR_DESC_LEN];
	uint64_t last_valid_timestamp;
	bool valid;
	kestrel_sensor_calibration_point_t temperature_calibration_points[MAX_SENSOR_CALIBRATION_POINTS];
	int temperature_calibration_point_count;
};
extern struct kestrel_thermal_sensor_data_temp_t kestrel_temp_sensor_data[MAX_THERMAL_SENSORS];
extern struct k_sem thermal_sensor_data_array_lock;

struct kestrel_pressure_sensor_data_t {
	uint32_t sensor_id;
	uint8_t fru_type;
	uint64_t device_handle;
	void * driver_data_ptr;
	double pressure_pa;
	char description[MAX_SENSOR_DESC_LEN];
	uint64_t last_valid_timestamp;
	bool valid;
	kestrel_sensor_calibration_point_t pressure_calibration_points[MAX_SENSOR_CALIBRATION_POINTS];
	int pressure_calibration_point_count;
};
extern struct kestrel_pressure_sensor_data_t kestrel_pressure_sensor_data[MAX_PRESSURE_SENSORS];
extern struct k_sem pressure_sensor_data_array_lock;

typedef struct onewire_device_list {
    uint64_t devices[MAX_ONEWIRE_DEVICES];
    uint8_t device_type[MAX_ONEWIRE_DEVICES];
    int device_count;
} onewire_device_list_t;

// Utility functions
double temp_celsius_to_fahrenheit(double celsius);
double pressure_bar_to_pa(double bar);

// Thermal sensors
extern struct kestrel_thermal_sensor_data_temp_t kestrel_temp_sensor_data[MAX_THERMAL_SENSORS];
extern int kestrel_temp_sensor_count;

// Pressure sensors
extern struct kestrel_pressure_sensor_data_t kestrel_pressure_sensor_data[MAX_PRESSURE_SENSORS];
extern int kestrel_pressure_sensor_count;

void kestrel_sort_temperature_sensors(void);
void kestrel_sort_pressure_sensors(void);

struct kestrel_thermal_sensor_data_temp_t * get_temperature_sensor_by_id(uint32_t sensor_id);
bool thermal_sensor_valid_last_n_ms(struct kestrel_thermal_sensor_data_temp_t * sensor_data, uint64_t ms);
int64_t get_thermal_sensor_update_age_ms(struct kestrel_thermal_sensor_data_temp_t * sensor_data);

void apply_temperature_calibration_for_thermal_sensor(struct kestrel_thermal_sensor_data_temp_t * sensor_data);
void load_temperature_calibration_data_for_thermal_sensor(struct kestrel_thermal_sensor_data_temp_t * sensor_data);

struct kestrel_pressure_sensor_data_t * get_pressure_sensor_by_id(uint32_t sensor_id);
bool pressure_sensor_valid_last_n_ms(struct kestrel_pressure_sensor_data_t * sensor_data, uint64_t ms);
int64_t get_pressure_sensor_update_age_ms(struct kestrel_pressure_sensor_data_t * sensor_data);

void apply_calibration_for_pressure_sensor(struct kestrel_pressure_sensor_data_t * sensor_data);
void load_calibration_data_for_pressure_sensor(struct kestrel_pressure_sensor_data_t * sensor_data);

void scan_onewire_devices(void);

#endif // _SENSORS_H
