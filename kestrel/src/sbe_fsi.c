// © 2020 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(kestrel_sbe_fsi, LOG_LEVEL_DBG);

#include <zephyr/sys/byteorder.h>
#include <stdlib.h>
#include <errno.h>
#include <zephyr/kernel.h>

#include "fsi.h"
#include "sbe_fsi.h"

// #define DEBUG

#ifdef DEBUG
#define IBM_POWER9_SBE_FIFO_TIMEOUT_MS		10000
#define IBM_POWER9_OCC_CMD_TIMEOUT_MS		10000
#define IBM_POWER9_FIFO_RESET_TIMEOUT_MS	10000
#else
#define IBM_POWER9_SBE_FIFO_TIMEOUT_MS		100
#define IBM_POWER9_OCC_CMD_TIMEOUT_MS		250
#define IBM_POWER9_FIFO_RESET_TIMEOUT_MS	10000
#endif

#define OCC_COMMAND_RETRY_COUNT			3

#define KESTREL_LOG(...) LOG_INF(__VA_ARGS__)

// Mutexes
struct k_mutex sbe_fifo_access_mutex;
struct k_mutex sbe_response_access_mutex;

// Shared buffers
uint32_t fifo_command[IBM_POWER9_SBE_OCC_CMD_MAX_SIZE / 4];
uint32_t fifo_response[IBM_POWER9_SBE_OCC_RESPONSE_MAX_SIZE / 4];
uint32_t sbe_response_data[IBM_POWER9_SBE_OCC_RESPONSE_MAX_SIZE / 4];

uint32_t sbe_ffdc_buffer[IBM_POWER9_SBE_OCC_FFDC_MAX_SIZE / 4];
size_t sbe_ffdc_buffer_count = 0;
uint32_t sbe_ffdc_doorbell = 0;

uint8_t occ_sequence_number = 0;

struct ibm_power9_occ_sensor_data_t host_cpu_temperature_sensor_data[IBM_POWER9_OCC_MAX_TEMP_SENSORS];
size_t host_cpu_temperature_sensor_count = 0;

static void memcpy32_cpu_to_be32(uint32_t *destination, uint32_t *source, int words)
{
	int word;
	for (word = 0; word < words; word++)
	{
		*destination = sys_cpu_to_be32(*source);
		destination++;
		source++;
	}
}

int verify_host_online(int slave_id, uint8_t *async_ffdc_pending)
{
	uint32_t dword;
	uint8_t state;

	// Fetch SBE status mailbox
	dword = 0x00000000;
	if (access_cfam(slave_id, IBM_POWER9_SBE_MSG_REGISTER, FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &dword))
	{
		KESTREL_LOG("SBE status read failed!  Host not online?");
		return -1;
	}
	if (!(dword & IBM_POWER9_SBE_STATUS_BOOTED))
	{
		KESTREL_LOG("SBE responding but host not (yet?) online.");
		return -1;
	}

	// Get SBE state
	state = (dword >> IBM_POWER9_SBE_STATE_SHIFT) & IBM_POWER9_SBE_STATE_MASK;
	if ((state == IBM_POWER9_SBE_STATE_UNKNOWN)
		|| (state == IBM_POWER9_SBE_STATE_FAILED)
		|| (state == IBM_POWER9_SBE_STATE_QUIESCED))
	{
		KESTREL_LOG("SBE failed or offline (state %d).", state);
		return -1;
	}
	else if (state == IBM_POWER9_SBE_STATE_DMT)
	{
		KESTREL_LOG("SBE busy (state %d).", state);
		return -EBUSY;
	}

	if (async_ffdc_pending) {
		// Check for async FFDC presence
		if (dword & IBM_POWER9_SBE_HW_FFDC_PRESENT)
		{
			*async_ffdc_pending = 1;
		}
		else {
			*async_ffdc_pending = 0;
		}
	}

	return 0;
}

int validate_fsi_address_alignment(uint32_t slave_address, size_t length)
{
	if ((length == 4) && (slave_address & 0x3))
	{
		return -1;
	}
	else if ((length == 2) && (slave_address & 0x1))
	{
		return -1;
	}
	else if (length != 1) {
		return -1;
	}

	return 0;
}

int initialize_sbe_fifo_access()
{
	// Initialize mutexes
	k_mutex_init(&sbe_fifo_access_mutex);
	k_mutex_init(&sbe_response_access_mutex);

	return 0;
}

int reset_sbe_fifo(int slave_id, uint32_t base_fsi_address)
{
	uint32_t dword;
	uint32_t fifo_up_status;
	uint64_t request_timestamp;

	dword = 1;
	if (access_fsi_mem(slave_id, base_fsi_address + (IBM_POWER9_SBE_FIFO_FIFO | IBM_POWER9_SBE_FIFO_UP | IBM_POWER9_SBE_FIFO_CMD_REQUEST_RESET), FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &dword))
	{
		KESTREL_LOG("SBE FIFO reset command submission failed!");
		return -1;
	}

	// Get initial timestamp of request
	request_timestamp = k_uptime_get();

	// Get OCC response
	while (1)
	{
		if (access_fsi_mem(slave_id, base_fsi_address + (IBM_POWER9_SBE_FIFO_STAT | IBM_POWER9_SBE_FIFO_UP), FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &fifo_up_status))
		{
			KESTREL_LOG("SBE FIFO status read failed!  Host not online?");
			return -1;
		}

		if (!(fifo_up_status & IBM_POWER9_SBE_FIFO_STA_RESET_REQ))
		{
			return 0;
		}

		if ((k_uptime_get() - request_timestamp) > IBM_POWER9_FIFO_RESET_TIMEOUT_MS)
		{
			return -1;
		}

		 k_usleep(1000000 / CONFIG_SYS_CLOCK_TICKS_PER_SEC);
	}

	// Should never reach this point
	return -1;
}

int synchronize_sbe_fifo(int slave_id, uint32_t base_fsi_address, uint8_t *async_ffdc_pending)
{
	uint32_t dword;
	uint32_t fifo_up_status;
	uint32_t fifo_down_status;

	if (verify_host_online(slave_id, async_ffdc_pending))
	{
		return -1;
	}

	if (access_fsi_mem(slave_id, base_fsi_address + (IBM_POWER9_SBE_FIFO_STAT | IBM_POWER9_SBE_FIFO_UP), FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &fifo_up_status))
	{
		KESTREL_LOG("SBE FIFO status read failed!  Host not online?");
		return -1;
	}
	if (access_fsi_mem(slave_id, base_fsi_address + (IBM_POWER9_SBE_FIFO_STAT | IBM_POWER9_SBE_FIFO_DOWN), FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &fifo_down_status))
	{
		KESTREL_LOG("SBE FIFO status read failed!  Host not online?");
		return -1;
	}

	if (fifo_down_status & IBM_POWER9_SBE_FIFO_STA_RESET_REQ)
	{
		KESTREL_LOG("SBE requested FIFO reset, issuing reset command...");

		dword = IBM_POWER9_SBE_FIFO_CMD_PERFORM_RESET;
		if (access_fsi_mem(slave_id, base_fsi_address + (IBM_POWER9_SBE_FIFO_STAT | IBM_POWER9_SBE_FIFO_UP), FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &dword))
		{
			KESTREL_LOG("SBE FIFO reset command submission failed!");
			return -1;
		}

		return -EAGAIN;
	}

	if ((fifo_up_status & IBM_POWER9_SBE_FIFO_STA_PARITY_ERR)
		|| (fifo_down_status & IBM_POWER9_SBE_FIFO_STA_PARITY_ERR))
	{
		KESTREL_LOG("SBE FIFO parity error detected!  Resetting...");

		if (reset_sbe_fifo(slave_id, base_fsi_address))
		{
			KESTREL_LOG("SBE FIFO reset failed!");
			return -1;
		}

		return -EAGAIN;
	}

	if ((!(fifo_up_status & IBM_POWER9_SBE_FIFO_STA_EMPTY))
		|| (!(fifo_down_status & IBM_POWER9_SBE_FIFO_STA_EMPTY)))
	{
		KESTREL_LOG("SBE FIFO(s) not empty!  Resetting...");

		if (reset_sbe_fifo(slave_id, base_fsi_address))
		{
			KESTREL_LOG("SBE FIFO reset failed!");
			return -1;
		}

		return -EAGAIN;
	}

	return 0;
}

int sbe_fifo_send_command(int slave_id, uint32_t base_fsi_address, uint32_t *command_data, size_t length)
{
	size_t words_sent = 0;
	uint32_t dword;
	uint32_t fifo_up_status;
	uint8_t space_available = 0;
	uint64_t last_tx_timestamp = k_uptime_get();

	while (words_sent < length)
	{
		// Wait for FIFO to have space available
		space_available = 0;
		do
		{
			if (access_fsi_mem(slave_id, base_fsi_address + (IBM_POWER9_SBE_FIFO_STAT | IBM_POWER9_SBE_FIFO_UP), FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &fifo_up_status))
			{
				KESTREL_LOG("SBE FIFO status read failed!  Host not online?");
				return -1;
			}
			if (!(fifo_up_status & IBM_POWER9_SBE_FIFO_STA_FULL))
			{
				space_available = 1;
			}
			else
			{
				if ((k_uptime_get() - last_tx_timestamp) > IBM_POWER9_SBE_FIFO_TIMEOUT_MS)
				{
					KESTREL_LOG("SBE FIFO write timeout, aborting!");
					return -1;
				}

				 k_usleep(1000000 / CONFIG_SYS_CLOCK_TICKS_PER_SEC);
			}
		}
		while (!space_available);

		// Write command word
		if (access_fsi_mem(slave_id, base_fsi_address + (IBM_POWER9_SBE_FIFO_FIFO | IBM_POWER9_SBE_FIFO_UP), FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &command_data[words_sent]))
		{
			KESTREL_LOG("SBE FIFO command write failed!  Host not online?");
			return -1;
		}
		words_sent++;

		last_tx_timestamp = k_uptime_get();
	}

	// Wait for FIFO to have space available
	space_available = 0;
	do
	{
		if (access_fsi_mem(slave_id, base_fsi_address + (IBM_POWER9_SBE_FIFO_STAT | IBM_POWER9_SBE_FIFO_UP), FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &fifo_up_status))
		{
			KESTREL_LOG("SBE FIFO status read failed!  Host not online?");
			return -1;
		}
		if (!(fifo_up_status & IBM_POWER9_SBE_FIFO_STA_FULL))
		{
			space_available = 1;
		}
		else
		{
			if ((k_uptime_get() - last_tx_timestamp) > IBM_POWER9_SBE_FIFO_TIMEOUT_MS)
			{
				KESTREL_LOG("SBE FIFO write timeout, aborting!");
				return -1;
			}

			 k_usleep(1000000 / CONFIG_SYS_CLOCK_TICKS_PER_SEC);
		}
	}
	while (!space_available);

	// Write EOT
	dword = 0;
	if (access_fsi_mem(slave_id, base_fsi_address + (IBM_POWER9_SBE_FIFO_FIFO | IBM_POWER9_SBE_FIFO_UP | IBM_POWER9_SBE_FIFO_CMD_EOT_RAISE), FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &dword))
	{
		KESTREL_LOG("SBE FIFO command write failed!  Host not online?");
		return -1;
	}

	return 0;
}

int sbe_fifo_receive_response(int slave_id, uint32_t base_fsi_address, size_t expected_length, uint32_t *response_data, size_t *response_length)
{
	uint32_t dword;
	uint32_t fifo_down_status;
	uint8_t data_available = 0;
	uint8_t eot_shift;
	uint64_t last_tx_timestamp = k_uptime_get();

	*response_length = 0;

	while ((*response_length) < expected_length)
	{
		// Wait for FIFO to have data available
		data_available = 0;
		do
		{
			if (access_fsi_mem(slave_id, base_fsi_address + (IBM_POWER9_SBE_FIFO_STAT | IBM_POWER9_SBE_FIFO_DOWN), FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &fifo_down_status))
			{
				KESTREL_LOG("SBE FIFO status read failed!  Host not online?");
				return -1;
			}
			if (!(fifo_down_status & IBM_POWER9_SBE_FIFO_STA_EMPTY))
			{
				data_available = 1;
			}
			else
			{
				if ((k_uptime_get() - last_tx_timestamp) > IBM_POWER9_SBE_FIFO_TIMEOUT_MS)
				{
					KESTREL_LOG("SBE FIFO read timeout, aborting!");
					return -1;
				}

				 k_usleep(1000000 / CONFIG_SYS_CLOCK_TICKS_PER_SEC);
			}
		}
		while (!data_available);

		// Read response word
		if (access_fsi_mem(slave_id, base_fsi_address + (IBM_POWER9_SBE_FIFO_FIFO | IBM_POWER9_SBE_FIFO_DOWN), FSI_DATA_LENGTH_WORD, FSI_DIRECTION_READ, &response_data[*response_length]))
		{
			KESTREL_LOG("SBE FIFO response read failed!  Host not online?");
			return -1;
		}
		if (((*response_length) == 0) && (expected_length > response_data[*response_length]))
		{
			expected_length = response_data[*response_length];
		}

		// Find the next EOT position(s) in the FIFO
		// When an EOT bit hits the high bit of this field, the response read is complete
		eot_shift = (fifo_down_status >> IBM_POWER9_SBE_FIFO_STA_EOT_SHIFT) & IBM_POWER9_SBE_FIFO_STA_EOT_MASK;
		if (eot_shift & IBM_POWER9_SBE_FIFO_STA_EOT_HIGH_BIT)
		{
			// Receive complete
			break;
		}

		(*response_length)++;
	}

	// Acknowledge EOT
	dword = 0;
	if (access_fsi_mem(slave_id, base_fsi_address + (IBM_POWER9_SBE_FIFO_FIFO | IBM_POWER9_SBE_FIFO_DOWN | IBM_POWER9_SBE_FIFO_CMD_EOT_ACK), FSI_DATA_LENGTH_WORD, FSI_DIRECTION_WRITE, &dword))
	{
		KESTREL_LOG("SBE FIFO EOT ACK write failed!  Host not online?");
		return -1;
	}

	return 0;
}

int gather_ffdc_dump(int slave_id, uint32_t base_fsi_address)
{
	uint32_t fifo_command[2];

	KESTREL_LOG("Retreiving SBE FFDC dump...");

	sbe_ffdc_doorbell = 0;

	fifo_command[0] = 2;
	fifo_command[1] = IBM_POWER9_SBE_FIFO_CMD_GET_FFDC;

	// Clear buffer
	sbe_ffdc_buffer_count = 0;
	memset(sbe_ffdc_buffer, 0xa5, IBM_POWER9_SBE_OCC_FFDC_MAX_SIZE);

	if (sbe_fifo_send_command(slave_id, base_fsi_address, fifo_command, 2))
	{
		return -1;
	}

	if (sbe_fifo_receive_response(slave_id, base_fsi_address, IBM_POWER9_SBE_OCC_FFDC_MAX_SIZE / 4, sbe_ffdc_buffer, &sbe_ffdc_buffer_count))
	{
		return -1;
	}

	sbe_ffdc_doorbell = 1;

	KESTREL_LOG("SBE FFDC dump received!");

	// FIXME
	// Store dump in Flash memory for later analysis...

	return 0;
}

int sbe_fifo_submit_request(int slave_id, uint32_t base_fsi_address, uint32_t *command_data, size_t command_length, size_t expected_response_length, uint32_t *response_data, size_t *response_length)
{
	uint8_t async_ffdc_pending = 0;

	// Validate SBE command
	if ((command_data[0] != command_length)
		|| (command_length < 2))
	{
		KESTREL_LOG("Invalid SBE command length %ld specified, aborting!", command_length);
		return -1;
	}

	// Synchronize SBE FIFO if needed
	if (synchronize_sbe_fifo(slave_id, base_fsi_address, &async_ffdc_pending))
	{
		return -1;
	}

	// Check if FFDC (First Failure Data Capture) dump is pending, and retrieve if true
	if (async_ffdc_pending)
	{
		if (!gather_ffdc_dump(slave_id, base_fsi_address))
		{
			return -1;
		}
	}

	if (sbe_fifo_send_command(slave_id, base_fsi_address, command_data, command_length))
	{
		return -1;
	}

	if (sbe_fifo_receive_response(slave_id, base_fsi_address, expected_response_length, response_data, response_length))
	{
		return -1;
	}

	return 0;
}

int process_occ_sram_transfer_reply(int slave_id, uint32_t base_fsi_address, uint32_t command, uint32_t *response_data, size_t response_length, size_t *occ_response_length)
{
	// Check response validity
	uint32_t sbe_data_start;
	uint32_t status_signature_1;
	uint32_t status_signature_2;

	if (response_length < 3)
	{
		KESTREL_LOG("Short response %ld, aborting!", response_length);
		return -1;
	}
	sbe_data_start = response_data[response_length - 1];
	if ((sbe_data_start > response_length) || (sbe_data_start < 3))
	{
		KESTREL_LOG("Invalid status offset %d, aborting!", sbe_data_start);
		return -1;
	}
	status_signature_1 = response_data[response_length - sbe_data_start];
	status_signature_2 = response_data[(response_length - sbe_data_start) + 1];
	if (((status_signature_1 >> 16) != 0xc0de) || ((status_signature_1 & 0xffff) != command))
	{
		KESTREL_LOG("Invalid response signature 0x%08x/0x%08x, aborting!", status_signature_1, status_signature_2);
		return -1;
	}
	if (status_signature_2 != 0)
	{
		// FFDC pending
		KESTREL_LOG("SBE error, status 0x%04x/0x%04x, aborting!", status_signature_2 >> 16, status_signature_2 & 0xffff);
		gather_ffdc_dump(slave_id, base_fsi_address);

		return -1;
	}
	response_length = response_length - sbe_data_start;

	if (occ_response_length)
	{
		*occ_response_length = response_length;
	}

	return 0;
}

int occ_putsram(int slave_id, uint32_t base_fsi_address, uint8_t *command_data, size_t length)
{
	size_t command_length;
	size_t response_length;
	size_t occ_response_length;

	// Data length must be a multiple of 8 (64-bit word based transfer)
	// Round up...
	length = ((length + (8-1)) / 8) * 8;
	command_length = 5 + (length / 4);

	// Lock buffers
	if (k_mutex_lock(&sbe_fifo_access_mutex, K_MSEC(1000)) != 0) {
		printk("Unable to acquire SBE FIFO buffer mutex in a timely manner! %s:%d\n", __FILE__, __LINE__);
		return -1;
	}

	// Verify buffers
	if ((command_length * 4) > IBM_POWER9_SBE_OCC_CMD_MAX_SIZE)
	{
		KESTREL_LOG("Command buffer too small, aborting!");
		k_mutex_unlock(&sbe_fifo_access_mutex);
		return -1;
	}

	// Clear buffer
	response_length = 0;
	memset(fifo_response, 0xa5, IBM_POWER9_SBE_OCC_RESPONSE_MAX_SIZE);

	// Assemble command
	fifo_command[0] = command_length;
	fifo_command[1] = IBM_POWER9_SBE_FIFO_CMD_PUT_OCC_SRAM;
	fifo_command[2] = IBM_POWER9_SBE_FIFO_OCC_XFR_MODE_NORMAL;
	fifo_command[3] = IBM_POWER9_SBE_FIFO_OCC_SRAM_CMD_ADDR;
	fifo_command[4] = length;
	memcpy32_cpu_to_be32(fifo_command + 5, (uint32_t*)command_data, length / 4);

	if (sbe_fifo_submit_request(slave_id, base_fsi_address, fifo_command, command_length, IBM_POWER9_SBE_OCC_RESPONSE_MAX_SIZE / 4, fifo_response, &response_length))
	{
		KESTREL_LOG("occ_putsram: Unable to submit OCC command, aborting!");
		k_mutex_unlock(&sbe_fifo_access_mutex);
		return -1;
	}

	if (process_occ_sram_transfer_reply(slave_id, base_fsi_address, IBM_POWER9_SBE_FIFO_CMD_PUT_OCC_SRAM, fifo_response, response_length, &occ_response_length))
	{
		k_mutex_unlock(&sbe_fifo_access_mutex);
		return -1;
	}

	if (occ_response_length != 1)
	{
		KESTREL_LOG("occ_putsram: OCC SRAM read failed, got %ld bytes (wanted %d).  Aborting!", response_length, 1);
	}

	occ_response_length = fifo_response[0];
	if (occ_response_length != length)
	{
		KESTREL_LOG("occ_putsram: SBE FIFO returned %ld bytes submitted to OCC (wanted %ld).  Aborting!", occ_response_length, length);
		k_mutex_unlock(&sbe_fifo_access_mutex);
		return -1;
	}

	k_mutex_unlock(&sbe_fifo_access_mutex);
	return 0;
}

int occ_send_attention_request(int slave_id, uint32_t base_fsi_address)
{
	size_t command_length;
	size_t response_length;
	size_t length;
	size_t occ_response_length;

	// OCC command data length in bytes
	length = 8;
	command_length = 5 + (length / 4);

	// Lock buffers
	if (k_mutex_lock(&sbe_fifo_access_mutex, K_MSEC(1000)) != 0) {
		printk("Unable to acquire SBE FIFO buffer mutex in a timely manner! %s:%d\n", __FILE__, __LINE__);
		return -1;
	}

	// Verify buffers
	if ((command_length * 4) > IBM_POWER9_SBE_OCC_CMD_MAX_SIZE)
	{
		KESTREL_LOG("Command buffer too small, aborting!");
		k_mutex_unlock(&sbe_fifo_access_mutex);
		return -1;
	}

	// Clear buffer
	response_length = 0;
	memset(fifo_response, 0xa5, IBM_POWER9_SBE_OCC_RESPONSE_MAX_SIZE);

	// Assemble command
	fifo_command[0] = command_length;
	fifo_command[1] = IBM_POWER9_SBE_FIFO_CMD_PUT_OCC_SRAM;
	fifo_command[2] = IBM_POWER9_SBE_FIFO_OCC_XFR_MODE_CIRC;
	fifo_command[3] = 0;
	fifo_command[4] = length;
	fifo_command[5] = IBM_POWER9_SBE_FIFO_OCC_CMD_ATTN;
	fifo_command[6] = 0;

	if (sbe_fifo_submit_request(slave_id, base_fsi_address, fifo_command, command_length, IBM_POWER9_SBE_OCC_RESPONSE_MAX_SIZE / 4, fifo_response, &response_length))
	{
		k_mutex_unlock(&sbe_fifo_access_mutex);
		KESTREL_LOG("occ_send_attention_request: Unable to submit OCC command, aborting!");
		return -1;
	}

	if (process_occ_sram_transfer_reply(slave_id, base_fsi_address, IBM_POWER9_SBE_FIFO_CMD_PUT_OCC_SRAM, fifo_response, response_length, &occ_response_length))
	{
		k_mutex_unlock(&sbe_fifo_access_mutex);
		return -1;
	}

	if (occ_response_length != 1)
	{
		KESTREL_LOG("occ_send_attention_request: OCC SRAM read failed, got %ld bytes (wanted %d).  Aborting!", response_length, 1);
	}

	occ_response_length = fifo_response[0];
	if (occ_response_length != length)
	{
		k_mutex_unlock(&sbe_fifo_access_mutex);
		KESTREL_LOG("occ_send_attention_request: SBE FIFO returned %ld bytes submitted to OCC (wanted %ld).  Aborting!", occ_response_length, length);
		return -1;
	}

	k_mutex_unlock(&sbe_fifo_access_mutex);
	return 0;
}

int occ_getsram(int slave_id, uint32_t base_fsi_address, uint32_t sram_address_offset, size_t desired_length, uint8_t *response_data, size_t *response_length)
{
	size_t command_length;
	size_t sbe_response_length;
	size_t occ_response_length;

	// Data length must be a multiple of 8 (64-bit word based transfer)
	// Round up...
	desired_length = ((desired_length + (8-1)) / 8) * 8;
	command_length = 5;

	// Lock buffers
	if (k_mutex_lock(&sbe_fifo_access_mutex, K_MSEC(1000)) != 0) {
		printk("Unable to acquire SBE FIFO buffer mutex in a timely manner! %s:%d\n", __FILE__, __LINE__);
		return -1;
	}
	if (k_mutex_lock(&sbe_response_access_mutex, K_MSEC(1000)) != 0) {
		printk("Unable to acquire SBE response buffer mutex in a timely manner! %s:%d\n", __FILE__, __LINE__);
		k_mutex_unlock(&sbe_fifo_access_mutex);
		return -1;
	}

	// Verify buffers
	if ((command_length * 4) > IBM_POWER9_SBE_OCC_CMD_MAX_SIZE)
	{
		KESTREL_LOG("Command buffer too small, aborting!");
		k_mutex_unlock(&sbe_fifo_access_mutex);
		k_mutex_unlock(&sbe_response_access_mutex);
		return -1;
	}

	// Clear buffers
	sbe_response_length = 0;
	memset(sbe_response_data, 0xa5, desired_length + IBM_POWER9_SBE_OCC_STATUS_LEN_BYTES);

	// Assemble command
	fifo_command[0] = command_length;
	fifo_command[1] = IBM_POWER9_SBE_FIFO_CMD_GET_OCC_SRAM;
	fifo_command[2] = IBM_POWER9_SBE_FIFO_OCC_XFR_MODE_NORMAL;
	fifo_command[3] = IBM_POWER9_SBE_FIFO_OCC_SRAM_RSP_ADDR + sram_address_offset;
	fifo_command[4] = desired_length;

	if (sbe_fifo_submit_request(slave_id, base_fsi_address, fifo_command, command_length, (desired_length + IBM_POWER9_SBE_OCC_STATUS_LEN_BYTES) / 4, sbe_response_data, &sbe_response_length))
	{
		k_mutex_unlock(&sbe_fifo_access_mutex);
		k_mutex_unlock(&sbe_response_access_mutex);
		KESTREL_LOG("occ_getsram: Unable to submit OCC command, aborting!");
		return -1;
	}

	if (process_occ_sram_transfer_reply(slave_id, base_fsi_address, IBM_POWER9_SBE_FIFO_CMD_GET_OCC_SRAM, sbe_response_data, sbe_response_length, &occ_response_length))
	{
		k_mutex_unlock(&sbe_fifo_access_mutex);
		k_mutex_unlock(&sbe_response_access_mutex);
		return -1;
	}

	occ_response_length = sbe_response_data[occ_response_length - 1];
	if (occ_response_length != desired_length)
	{
		k_mutex_unlock(&sbe_fifo_access_mutex);
		k_mutex_unlock(&sbe_response_access_mutex);
		KESTREL_LOG("occ_getsram: SBE FIFO returned %ld bytes submitted to OCC (wanted %ld).  Aborting!", occ_response_length, desired_length);
		return -1;
	}

	if (occ_response_length < 8)
	{
		k_mutex_unlock(&sbe_fifo_access_mutex);
		k_mutex_unlock(&sbe_response_access_mutex);
		KESTREL_LOG("occ_getsram: SBE FIFO short response (%ld bytes).  Aborting!", occ_response_length);
		return -1;
	}

	// Copy data
	memcpy32_cpu_to_be32((uint32_t*)response_data, sbe_response_data, occ_response_length / 4);
	*response_length = occ_response_length;

	k_mutex_unlock(&sbe_fifo_access_mutex);
	k_mutex_unlock(&sbe_response_access_mutex);
	return 0;
}

uint16_t compute_occ_response_checksum(struct occ_response_t occ_response, uint8_t* data, size_t length)
{
	int i;
	uint16_t checksum;

	checksum = occ_response.sequence_number;
	checksum += occ_response.cmd_type;
	checksum += occ_response.return_status;
	checksum += occ_response.length >> 8;
	checksum += occ_response.length & 0xff;
	for (i = 0; i < length; i++) {
		checksum += data[i];
	}

	return checksum;
}

int occ_submit(int slave_id, uint32_t base_fsi_address, uint8_t *request_data, size_t request_length, size_t expected_response_length, uint8_t *response_data, size_t *response_length)
{
	int retry_count;
	int error;
	uint8_t sequence_number;
	uint16_t computed_checksum;
	uint64_t request_timestamp;
	struct occ_response_t occ_response;

	// Clear buffer
	*response_length = 0;
	memset(response_data, 0xa5, expected_response_length);

	if (expected_response_length < 7)
	{
		KESTREL_LOG("Invalid expected response length %ld, aborting!", expected_response_length);
		return -1;
	}

	sequence_number = request_data[0];

	retry_count = 0;
	while (retry_count < OCC_COMMAND_RETRY_COUNT)
	{
		// Send OCC command
		if (occ_putsram(slave_id, base_fsi_address, request_data, request_length))
		{
			retry_count++;
			k_usleep(100000);
			continue;
		}

		// Send OCC attention request
		if (occ_send_attention_request(slave_id, base_fsi_address))
		{
			retry_count++;
			k_usleep(100000);
			continue;
		}

		// Get initial timestamp of request
		request_timestamp = k_uptime_get();

		// Get OCC response
		error = 0;
		while (1)
		{
			// Retrieve header only
			if (occ_getsram(slave_id, base_fsi_address, 0x0, 8, response_data, response_length))
			{
				error = 1;
				break;
			}
	
			// Decode response
			// Big endian response format
			occ_response.sequence_number = response_data[0];
			occ_response.cmd_type = response_data[1];
			occ_response.return_status = response_data[2];
			occ_response.length = response_data[3] << 8;
			occ_response.length |= response_data[4];
	
			if (occ_response.return_status == IBM_POWER9_OCC_RESP_CMD_IN_PRG)
			{
				if ((k_uptime_get() - request_timestamp) > IBM_POWER9_OCC_CMD_TIMEOUT_MS)
				{
					KESTREL_LOG("OCC command timeout!");
					break;
				}

				 k_usleep(1000000 / CONFIG_SYS_CLOCK_TICKS_PER_SEC);
			}
			else {
				break;
			}
		}

		if (error)
		{
			retry_count++;
			k_usleep(100000);
			continue;
		}
		break;
	}

	if (retry_count >= OCC_COMMAND_RETRY_COUNT)
	{
		return -1;
	}

	// Validate header length
	if ((occ_response.length + 7) > expected_response_length)
	{
		KESTREL_LOG("Response length %d too large for buffer, aborting!", occ_response.length);
		return -1;
	}

	if (occ_response.length > 1)
	{
		// The header read command returned the first three bytes of the OCC response
		// Move those response bytes to the beginning of the OCC response buffer
		response_data[0] = response_data[5];
		response_data[1] = response_data[6];
		response_data[2] = response_data[7];

		// Retrieve the rest of the data
		if (occ_getsram(slave_id, base_fsi_address, 0x8, occ_response.length - 1, response_data + 3, response_length))
		{
			return -1;
		}
	}

	// Parse returned checksum
	occ_response.checksum = response_data[occ_response.length] << 8;
	occ_response.checksum |= response_data[occ_response.length + 1];

	// Compute checksum on response data
	computed_checksum = compute_occ_response_checksum(occ_response, response_data, occ_response.length);

	if (computed_checksum != occ_response.checksum)
	{
		KESTREL_LOG("Invalid response checksum 0x%04x (wanted 0x%04x), aborting!", occ_response.checksum, computed_checksum);
		return -1;
	}

	if (sequence_number != occ_response.sequence_number)
	{
		KESTREL_LOG("Invalid response sequence number 0x%02x (wanted 0x%02x), aborting!", occ_response.sequence_number, sequence_number);
		return -1;
	}

	if (occ_response.return_status != IBM_POWER9_OCC_RESP_SUCCESS)
	{
		KESTREL_LOG("Error code 0x%02x returned from OCC, aborting!", occ_response.return_status);
		return -1;
	}

	return 0;
}

int update_temperature_sensors(int sensor_count, uint8_t sensor_data_version, uint8_t* data, size_t length)
{
	int i;

	if (sensor_data_version == 0x2)
	{
		struct ibm_power9_occ_sensor_data_temp_v2_t* temp_data;
		temp_data = (struct ibm_power9_occ_sensor_data_temp_v2_t*)data;

		for (i = 0; i < sensor_count; i++)
		{
			host_cpu_temperature_sensor_data[i].sensor_id = sys_be32_to_cpu(temp_data->sensor_id);
			host_cpu_temperature_sensor_data[i].temperature_c = temp_data->value;
			temp_data++;
		}
		host_cpu_temperature_sensor_count = sensor_count;

		return 0;
	}

	KESTREL_LOG("[WARNING] Unknown OCC temperature sensor data structure version 0x%02x, ignoring!", sensor_data_version);
	host_cpu_temperature_sensor_count = 0;
	return -1;
}

int occ_poll(int slave_id, uint32_t base_fsi_address, size_t expected_response_length, uint8_t *response_data, size_t *response_length)
{
	int i;
	uint32_t sensor_block_length;
	uint8_t sensor_block_label_buffer[5];
	struct ibm_power9_occ_poll_response_header_t *occ_response_header;
	struct ibm_power9_occ_sensor_data_block_header_t *occ_sensor_block_header;
	uint8_t command_data[8];
	uint8_t occ_command = IBM_POWER9_OCC_CMD_POLL;
	uint32_t current_offset;
	uint16_t checksum;

	if (occ_sequence_number == 0)
	{
		// For some reason the SBE / OCC rejects sequence numbers of 0...
		occ_sequence_number = 1;
	}
	checksum = occ_command + occ_sequence_number + 1;

	// Assemble OCC request
	// Big endian format
	command_data[0] = occ_sequence_number;	// Sequence number
	command_data[1] = 0;			// Command type
	command_data[2] = 0;			// Data length (high)
	command_data[3] = 1;			// Data length (low)
	command_data[4] = occ_command;		// Command
	command_data[5] = checksum >> 8;	// Checksum (high)
	command_data[6] = checksum & 0xff;	// Checksum (low)
	command_data[7] = 0;

	// Increment sequence number
	occ_sequence_number++;

	// Submit command
	if (occ_submit(slave_id, base_fsi_address, command_data, 8, expected_response_length, response_data, response_length))
	{
		return -1;
	}

	// Parse response
	occ_response_header = (struct ibm_power9_occ_poll_response_header_t*)response_data;

	current_offset = 0;
	for (i = 0; i < occ_response_header->sensor_data_block_count; i++)
	{
		occ_sensor_block_header = (struct ibm_power9_occ_sensor_data_block_header_t*)(((uint8_t*)&occ_response_header->sensor_block_data) + current_offset);
		sensor_block_length = (occ_sensor_block_header->sensor_count * occ_sensor_block_header->sensor_length) + 8;

		memcpy(sensor_block_label_buffer, occ_sensor_block_header->label, 4);
		sensor_block_label_buffer[4] = 0;
		if (strcmp(sensor_block_label_buffer, "TEMP") == 0)
		{
			// Parse temperature sensor data
			update_temperature_sensors(occ_sensor_block_header->sensor_count, occ_sensor_block_header->sensor_format, (uint8_t*)(&occ_sensor_block_header->sensor_data), sensor_block_length);
		}

		// TODO
		// Other blocks typically seen are 'FREQ', 'POWR', 'CAPS', 'EXTN'

		// Next block
		current_offset += sensor_block_length;
	}

	return 0;
}
