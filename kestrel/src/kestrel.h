// © 2021 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#if (WITH_ZEPHYR)
#include <zephyr/shell/shell.h>
#endif

#include "sensors.h"
#include "thermal_control.h"

#if (WITH_ZEPHYR)
#define KESTREL_SERVICE_THREAD_PRIORITY		K_PRIO_PREEMPT(0)
#define THERMAL_SERVICE_THREAD_PRIORITY		K_PRIO_PREEMPT(0)
#define WATCHDOG_SERVICE_THREAD_PRIORITY	K_PRIO_PREEMPT(1)
#define TIME_SERVICE_THREAD_PRIORITY		K_PRIO_PREEMPT(0)
#endif

#if (WITH_ZEPHYR)
#define SW0_NODE					DT_ALIAS(sw0)
#define SW0_HOG_NODE					DT_ALIAS(swhog0)
#if DT_NODE_HAS_STATUS(SW0_NODE, okay)
#define KESTREL_HOST_POWER_BUTTON_GPIO_NODE		DEVICE_DT_NAME(SW0_NODE)
#define KESTREL_HOST_POWER_BUTTON_GPIO_BUTTON		GPIO_DT_SPEC_GET_OR(SW0_NODE, gpios, {0})
#define KESTREL_HOST_POWER_BUTTON_HOG_GPIO_NODE		DEVICE_DT_NAME(SW0_HOG_NODE)
#define KESTREL_HOST_POWER_BUTTON_HOG_GPIO_BUTTON	GPIO_DT_SPEC_GET_OR(SW0_HOG_NODE, gpios, {0})
#else
#error "No power button defined in device tree"
#endif
#define SW1_NODE					DT_ALIAS(sw1)
#define SW1_HOG_NODE					DT_ALIAS(swhog1)
#if DT_NODE_HAS_STATUS(SW1_NODE, okay)
#define KESTREL_HOST_RESET_BUTTON_GPIO_NODE		DEVICE_DT_NAME(SW1_NODE)
#define KESTREL_HOST_RESET_BUTTON_GPIO_BUTTON		GPIO_DT_SPEC_GET_OR(SW1_NODE, gpios, {0})
#define KESTREL_HOST_RESET_BUTTON_HOG_GPIO_NODE		DEVICE_DT_NAME(SW1_HOG_NODE)
#define KESTREL_HOST_RESET_BUTTON_HOG_GPIO_BUTTON	GPIO_DT_SPEC_GET_OR(SW1_HOG_NODE, gpios, {0})
#else
#error "No reset button defined in device tree"
#endif
#endif

#if (WITH_ZEPHYR)
#define KESTREL_FAN_CONTROL_TIMER_STEP_MS	100
#define CPU_FAN_0_CHANNEL			1
#define CPU_FAN_1_CHANNEL			0
#define CASE_FAN_0_CHANNEL			2
#define CASE_FAN_1_CHANNEL			3
#define CASE_FAN_2_CHANNEL			0
#define CASE_FAN_3_CHANNEL			1
#define CASE_FAN_4_CHANNEL			2
#define CASE_FAN_5_CHANNEL			3
#endif

#define HOST_POWER_STATUS_OFFLINE	0
#define HOST_POWER_STATUS_POWERING_ON	1
#define HOST_POWER_STATUS_IPLING	2
#define HOST_POWER_STATUS_RUNNING	3
#define HOST_POWER_STATUS_POWERING_OFF	4

#if (WITH_ZEPHYR)
#include <zephyr/app_version.h>
#define KESTREL_VERSION_MAJOR		APP_VERSION_MAJOR
#define KESTREL_VERSION_MINOR		APP_VERSION_MINOR
#define KESTREL_VERSION_PATCHLEVEL	APP_PATCHLEVEL
#else
#define KESTREL_VERSION_MAJOR		0
#define KESTREL_VERSION_MINOR		1
#define KESTREL_VERSION_PATCHLEVEL	0
#endif

#if defined (__GNUC__)
	#if defined(__clang__)
		#define COMPILER_VENDOR "Clang"
		#define COMPILER_VERSION __VERSION__
	#else
		#define COMPILER_VENDOR "GCC"
		#define COMPILER_VERSION __VERSION__
	#endif
#else
	#define COMPILER_VENDOR "Unknown"
	#define COMPILER_VERSION "Unknown"
#endif

struct firmware_buffer_region {
	uint8_t *buffer_address;
	unsigned long long buffer_length;
	uintptr_t current_write_offset;
	unsigned long long valid_bytes;
	uint8_t locked;
	uint8_t overflow;
};

extern struct firmware_buffer_region main_firmware_buffer;

extern uint8_t kestrel_basic_init_complete;
extern uint8_t host_background_service_task_active;
extern uint8_t host_console_service_task_requested;
extern uint8_t host_power_status;
extern uint16_t host_active_post_code;

extern struct pid_data_t kestrel_pid_controller_data[MAX_PID_CONTROLLERS];
extern int kestrel_pid_controller_count;

#if (WITH_ZEPHYR)
extern k_tid_t kestrel_service_thread_id;
extern k_tid_t kestrel_console_thread_id;
extern k_tid_t fan_tach_service_thread_id;
extern k_tid_t thermal_service_thread_id;
extern k_tid_t sensor_service_thread_id;
extern k_tid_t time_service_thread_id;
extern k_tid_t fan_tach_init_thread_id;
extern k_tid_t onewire_init_thread_id;
extern k_tid_t watchdog_service_thread_id;
extern struct k_mutex occ_access_mutex;
extern struct k_sem chassis_control_semaphore;
extern struct k_timer fan_control_timer;
extern const struct shell *host_console_service_task_shell;

extern bool thermal_service_thread_online;
extern bool fan_tach_service_thread_online;

extern bool rescan_all_thermal_sensors;
extern bool clear_all_thermal_sensors;
#endif

int kestrel_init(void);
int power_on_host(void);
int power_on_chassis(int has_lock);
void power_off_chassis(int has_lock);
void print_chassis_status(void);
#if (WITH_ZEPHYR)
int write_flash_buffer_to_flash(const struct shell *shell);
#else
int write_flash_buffer_to_flash(void);
#endif

int primary_service_event_loop(void);

#if !(WITH_ZEPHYR)
void scan_onewire_devices(uint8_t* base_addr);
#endif

#if (WITH_ZEPHYR)
int attach_to_host_console(const struct shell *shell);
int host_console_event_loop(const struct shell *shell);
void flush_zephyr_log_buffer(void);
#else
int attach_to_host_console();
#endif
