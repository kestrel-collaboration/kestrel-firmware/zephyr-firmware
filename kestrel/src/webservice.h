// © 2021 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _WEBSERVICE_H
#define _WEBSERVICE_H

void start_webservice(void);

#endif // _WEBSERVICE_H