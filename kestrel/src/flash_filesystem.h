// © 2021 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _FLASH_FILESYSTEM_H
#define _FLASH_FILESYSTEM_H

#include <zephyr/shell/shell.h>

#define LITEX_HEADER_LENGTH_BYTES 0x8

#define FPGA_FIRMWARE_NODE bitstream_primary
#define BMC_FIRMWARE_NODE bmc_primary
#define PNOR_FIRMWARE_NODE pnor
#define CONFIG_PARTITION_NODE bmc_configuration
#define STORAGE_PARTITION_NODE bmc_storage

// Matches LFS_NAME_MAX
#define MAX_PATH_LEN 255

extern struct fs_mount_t kestrel_config_lfs_fs_mount;

int initialize_flash_filesystem(void);

int write_firmware_to_flash(const struct shell *shell, const char* partition_name, const uint8_t* data, size_t length, int add_litex_header);

#endif // _FLASH_FILESYSTEM_H
