// © 2021-2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _FRU_H
#define _FRU_H

#define FRU_CLASS_UNKNOWN		-1
#define FRU_CLASS_THERMAL_SENSOR	0
#define FRU_CLASS_CHILLER_PACK		1

#endif // _FRU_H
