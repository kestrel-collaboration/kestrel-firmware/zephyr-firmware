// © 2020 - 2023 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

#ifndef _OPENCORES_I2C_H
#define _OPENCORES_I2C_H

#include <stdint.h>

#define OPENCORES_I2C_MASTER_DEVICE_ID_LOW  0x0
#define OPENCORES_I2C_MASTER_DEVICE_ID_HIGH 0x4
#define OPENCORES_I2C_MASTER_DEVICE_VERSION 0x8
#define OPENCORES_I2C_MASTER_PRESCALE_LOW   0x10
#define OPENCORES_I2C_MASTER_PRESCALE_HIGH  0x11
#define OPENCORES_I2C_MASTER_PRESCALE_CTL   0x12
#define OPENCORES_I2C_MASTER_TX_RX          0x13
#define OPENCORES_I2C_MASTER_CMD_STATUS     0x14

#define OPENCORES_I2C_DEVICE_ID_HIGH 0x4932434d
#define OPENCORES_I2C_DEVICE_ID_LOW  0x4f504e43

#define OPENCORES_I2C_VERSION_MAJOR_MASK  0xffff
#define OPENCORES_I2C_VERSION_MAJOR_SHIFT 16
#define OPENCORES_I2C_VERSION_MINOR_MASK  0xff
#define OPENCORES_I2C_VERSION_MINOR_SHIFT 8
#define OPENCORES_I2C_VERSION_PATCH_MASK  0xff
#define OPENCORES_I2C_VERSION_PATCH_SHIFT 0

#define OPENCORES_I2C_MASTER_CTL_CORE_EN_MASK  0x1
#define OPENCORES_I2C_MASTER_CTL_CORE_EN_SHIFT 7
#define OPENCORES_I2C_MASTER_CTL_IRQ_EN_MASK   0x1
#define OPENCORES_I2C_MASTER_CTL_IRQ_EN_SHIFT  6

#define OPENCORES_I2C_MASTER_CMD_STA_MASK   0x1
#define OPENCORES_I2C_MASTER_CMD_STA_SHIFT  7
#define OPENCORES_I2C_MASTER_CMD_STO_MASK   0x1
#define OPENCORES_I2C_MASTER_CMD_STO_SHIFT  6
#define OPENCORES_I2C_MASTER_CMD_RD_MASK    0x1
#define OPENCORES_I2C_MASTER_CMD_RD_SHIFT   5
#define OPENCORES_I2C_MASTER_CMD_WR_MASK    0x1
#define OPENCORES_I2C_MASTER_CMD_WR_SHIFT   4
#define OPENCORES_I2C_MASTER_CMD_ACK_MASK   0x0
#define OPENCORES_I2C_MASTER_CMD_ACK_SHIFT  3
#define OPENCORES_I2C_MASTER_CMD_NACK_MASK  0x1
#define OPENCORES_I2C_MASTER_CMD_NACK_SHIFT 3
#define OPENCORES_I2C_MASTER_CMD_IACK_MASK  0x1
#define OPENCORES_I2C_MASTER_CMD_IACK_SHIFT 0

#define OPENCORES_I2C_MASTER_STATUS_RXACK_MASK  0x1
#define OPENCORES_I2C_MASTER_STATUS_RXACK_SHIFT 7
#define OPENCORES_I2C_MASTER_STATUS_BUSY_MASK   0x1
#define OPENCORES_I2C_MASTER_STATUS_BUSY_SHIFT  6
#define OPENCORES_I2C_MASTER_STATUS_ARBL_MASK   0x1
#define OPENCORES_I2C_MASTER_STATUS_ARBL_SHIFT  5
#define OPENCORES_I2C_MASTER_STATUS_TIP_MASK    0x1
#define OPENCORES_I2C_MASTER_STATUS_TIP_SHIFT   1
#define OPENCORES_I2C_MASTER_STATUS_IRQP_MASK   0x1
#define OPENCORES_I2C_MASTER_STATUS_IRQP_SHIFT  0

#define OPENCORES_I2C_MASTER_TX_RX_WRITE_MASK  0x0
#define OPENCORES_I2C_MASTER_TX_RX_WRITE_SHIFT 0
#define OPENCORES_I2C_MASTER_TX_RX_READ_MASK   0x1
#define OPENCORES_I2C_MASTER_TX_RX_READ_SHIFT  0

#define I2C_MASTER_4_ADDR                  0xc0010000L
#define I2C_MASTER_OPERATION_TIMEOUT_VALUE 10000

int initialize_i2c_master(uint8_t *base_address, int i2c_bus_frequency);
int write_i2c_data(uint8_t *base_address, uint8_t slave_address, uint8_t *data, int data_length, uint8_t send_stop_signal);
int read_i2c_data(uint8_t *base_address, uint8_t slave_address, uint8_t *data, int *data_length, int max_data_length, uint8_t send_stop_signal);
uint8_t i2c_read_register_byte(uint8_t *base_address, uint8_t slave_address, uint8_t slave_register, int *error);
uint16_t i2c_read_register_word(uint8_t *base_address, uint8_t slave_address, uint8_t slave_register, int *error);
int i2c_write_register_byte(uint8_t *base_address, uint8_t slave_address, uint8_t slave_register, uint8_t data);
int i2c_write_register_word(uint8_t *base_address, uint8_t slave_address, uint8_t slave_register, uint16_t data);

#endif // _OPENCORES_I2C_H
