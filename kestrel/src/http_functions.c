/*
 * Copyright (c) 2021-2024 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: GPL-3.0
 */

#include <stdint.h>
#include <stdio.h>

#include <zephyr/net/http/server.h>
#include <zephyr/net/http/service.h>

#include "uri_encode.h"

#include "http_functions.h"

void generic_rx_handler_noop(zephyr_http_server_request_state_t *state)
{
	ARG_UNUSED(state);
}

int parse_uri_query_string_find_value_for_key(uint8_t* uri, int uri_len, uint8_t* key, uint8_t* value_buffer, int buffer_size) {
	int i, start_pos, parse_state;
	char uri_pair_key_buffer[128];
	char uri_pair_value_buffer[128];

	start_pos = 0;
	parse_state = 0;
	for (i = 0; i < uri_len; i++) {
		if (*((char *)(uri + i)) == '?') {
			start_pos = i;
			continue;
		}
		if (parse_state == 0) {
			if (*((char *)(uri + i)) == '=') {
				if ((i > start_pos) && ((i - start_pos - 1) < sizeof(uri_pair_key_buffer))) {
					memcpy(uri_pair_key_buffer, uri + start_pos + 1, i - start_pos - 1);
					uri_pair_key_buffer[i - start_pos - 1] = 0;
				}
				start_pos = i;
				parse_state = 1;
			}
			if (*((char *)(uri + i)) == '&') {
				if ((i > start_pos) && ((i - start_pos - 1) < sizeof(uri_pair_key_buffer))) {
					memcpy(uri_pair_key_buffer, uri + start_pos + 1, i - start_pos - 1);
					uri_pair_key_buffer[i - start_pos - 1] = 0;
				}
				start_pos = i;
				parse_state = 0;
			}
		}
		else {
			if (*((char *)(uri + i)) == '&') {
				if ((i > start_pos) && ((i - start_pos - 1) < sizeof(uri_pair_value_buffer))) {
					memcpy(uri_pair_value_buffer, uri + start_pos + 1, i - start_pos - 1);
					uri_pair_value_buffer[i - start_pos - 1] = 0;
					if (strcmp(key, uri_pair_key_buffer) == 0) {
						if (buffer_size >= strlen(uri_pair_value_buffer)) {
							uri_decode(uri_pair_value_buffer, strlen(uri_pair_value_buffer), value_buffer);
							return 0;
						}
						else {
							return -1;
						}
					}
				}
				start_pos = i;
				parse_state = 0;
			}
			else if (i == (uri_len - 1)) {
				if ((i > start_pos) && ((i - start_pos) < sizeof(uri_pair_value_buffer))) {
					memcpy(uri_pair_value_buffer, uri + start_pos + 1, i - start_pos);
					uri_pair_value_buffer[i - start_pos] = 0;
					if (strcmp(key, uri_pair_key_buffer) == 0) {
						if (buffer_size >= strlen(uri_pair_value_buffer)) {
							uri_decode(uri_pair_value_buffer, strlen(uri_pair_value_buffer), value_buffer);
							return 0;
						}
						else {
							return -1;
						}
					}
				}
				start_pos = i;
				parse_state = 0;
			}
		}
	}

	return -1;
}

// HTTP multipart form parser functions
//
// NOTE
// This parser assumes the Zephyr HTTP server has correctly isolated the form upload content from the rest of the HTTP headers,
// i.e. that the first bytes are indeed the start of the unwrapped form boundary string

void parse_http_multipart_form_run(http_multipart_form_parser_state_t * parser_state, uint8_t * new_data, uint64_t new_data_len)
{
	uint8_t * str_pos;

	if ((parser_state->current_state == 0) || (parser_state->current_state == 1)) {
		// Load new data into window
		size_t bytes_to_copy = new_data_len;
		if (bytes_to_copy > (HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE - parser_state->sliding_window_pos - 1)) {
			bytes_to_copy = (HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE - parser_state->sliding_window_pos - 1);
		}
		memcpy(parser_state->sliding_window + parser_state->sliding_window_pos, new_data, bytes_to_copy);
		parser_state->sliding_window[parser_state->sliding_window_pos + bytes_to_copy] = 0x0;

		// Parse current window
		if (parser_state->current_state == 0) {
			str_pos = strstr(parser_state->sliding_window, "\r\n");
			if (str_pos && parser_state->sliding_window[0] != 0x0) {
				strncpy(parser_state->content_boundary_string, "\r\n", 2);
				strncpy(parser_state->content_boundary_string + 2, parser_state->sliding_window, str_pos - parser_state->sliding_window);
				parser_state->content_boundary_string[str_pos - parser_state->sliding_window] = 0x0;
				if (strlen(parser_state->content_boundary_string) > 0) {
					parser_state->current_state = 1;
				}
			}
		}
		if (parser_state->current_state == 1) {
			str_pos = strstr(parser_state->sliding_window, "\r\n\r\n");
			if (str_pos && parser_state->sliding_window[0] != 0x0) {
				parser_state->current_state = 2;
				if ((str_pos - parser_state->sliding_window) >= parser_state->sliding_window_pos) {
					parser_state->data_offset_in_current_chunk = (str_pos - parser_state->sliding_window) - parser_state->sliding_window_pos + 4;
				}
			}
		}

		// Shift window if needed
		parser_state->sliding_window_pos += bytes_to_copy;
		if (parser_state->sliding_window_pos >= ((HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE * 3) / 4)) {
			memcpy(parser_state->sliding_window, parser_state->sliding_window + (HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE / 4), HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE / 4);
			memcpy(parser_state->sliding_window + (HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE / 4), parser_state->sliding_window + (HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE / 2), HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE / 4);
			memcpy(parser_state->sliding_window + (HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE / 2), parser_state->sliding_window + ((HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE * 3) / 4), HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE / 4);
			memset(parser_state->sliding_window + ((HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE * 3) / 4), 0, HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE / 4);
			parser_state->sliding_window_pos = ((HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE * 3) / 4);
		}
	}
	else {
		// Make sure the data read offset gets reset after the single affected chunk is read by the application
		parser_state->data_offset_in_current_chunk = 0;
	}
}

void parse_http_multipart_form_finalize(http_multipart_form_parser_state_t * parser_state, uint8_t * received_data, uint64_t * received_data_len)
{
	uint8_t * mem_pos;
	uint8_t * str_pos;

	// Find end boundary
	*(received_data + *received_data_len) = 0x0;
	str_pos = NULL;
	mem_pos = memchr(received_data, parser_state->content_boundary_string[0], *received_data_len);
	while (mem_pos) {
		str_pos = strstr(mem_pos, parser_state->content_boundary_string);
		if (str_pos) {
			break;
		}
		else {
			if ((mem_pos - received_data) < *received_data_len) {
				mem_pos = memchr(mem_pos + 1, parser_state->content_boundary_string[0], *received_data_len - (mem_pos - received_data) - 1);
			}
			else {
				mem_pos = NULL;
				str_pos = NULL;
				break;
			}
		}
	}
	if (str_pos) {
		*received_data_len = str_pos - received_data;
	}
	else {
		*received_data_len = 0;
	}
}