/*
 * Copyright (c) 2021-2024 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: GPL-3.0
 */

#ifndef _HTTPFUNCTIONS_H
#define _HTTPFUNCTIONS_H

#define MAX_REQUEST_SIZE_BYTES		8192

#define DECLARE_EXTERNAL_STATIC_FILE(name)								\
extern const unsigned char name[];									\
extern const size_t name##_SIZE;

// NOTE
// Due to C preprocessor limitations, the contents of HTTP_RESOURCE_DEFINE are copied directly into
// the #define block below.  Keep in sync with Zephyr core (include/zephyr/net/http/service.h)
#define KESTREL_WEBSERVICE_DEFINE_FILE_HANDLER(name, url, resource_type, encoding)			\
struct http_resource_detail_static name##_resource_detail = {						\
	.common = {											\
			.type = HTTP_RESOURCE_TYPE_STATIC,						\
			.bitmask_of_supported_http_methods = BIT(HTTP_GET),				\
			.content_encoding = encoding,							\
			.content_type = resource_type,							\
		},											\
	.static_data = embedded_file_##name,								\
	.static_data_len = sizeof(embedded_file_##name),						\
};													\
													\
const STRUCT_SECTION_ITERABLE_ALTERNATE(http_resource_desc_kestrel_http_service, http_resource_desc,	\
					name##_resource) = {						\
	.resource = url,										\
	.detail = (void *)(&name##_resource_detail),							\
};

#define SEND_CUSTOM_PRINTF(state, ...)									\
{													\
	int bytes_processed = snprintf(state.tx_buf + state.tx_buf_pos,					\
		state.tx_buf_size - state.tx_buf_pos, __VA_ARGS__);					\
	if (bytes_processed > (state.tx_buf_size - state.tx_buf_pos) - 1) {				\
		bytes_processed = (state.tx_buf_size - state.tx_buf_pos) - 1;				\
	}												\
	state.tx_buf_pos += bytes_processed;								\
}

#define SEND_CUSTOM_PRINTF_CUSTOM_FORMAT(state, fmt, ...)						\
{													\
	int bytes_processed = snprintf(state.tx_buf + state.tx_buf_pos,					\
		state.tx_buf_size - state.tx_buf_pos, fmt, __VA_ARGS__);				\
	if (bytes_processed > (state.tx_buf_size - state.tx_buf_pos) - 1) {				\
		bytes_processed = (state.tx_buf_size - state.tx_buf_pos) - 1;				\
	}												\
	state.tx_buf_pos += bytes_processed;								\
}

#define SEND_DATA_BLOCK(state, data, len)								\
{													\
	int safe_len = len;										\
	if (safe_len > (state.tx_buf_size - state.tx_buf_pos)) {					\
		safe_len = state.tx_buf_size - state.tx_buf_pos;					\
	}												\
	memcpy(state.tx_buf + state.tx_buf_pos, data, safe_len);					\
	state.tx_buf_pos += safe_len;									\
};

// NOTE
// Due to C preprocessor limitations, the contents of HTTP_RESOURCE_DEFINE are copied directly into
// the #define block below.  Keep in sync with Zephyr core (include/zephyr/net/http/service.h)
#define KESTREL_WEBSERVICE_DEFINE_PAGE_HANDLER(name, url, resource_type, title)				\
static uint8_t static_file_##name##_recv_buffer[MAX_REQUEST_SIZE_BYTES];				\
static char* static_file_##name##_service_url_string = url;						\
static int static_file_##name##_handler(struct http_client_ctx *client, enum http_data_status status,	\
		       uint8_t *buffer, size_t len, void *user_data)					\
{													\
	static char print_str[32];									\
	enum http_method method = client->method;							\
	static size_t processed;									\
	static int request_finished = 0;								\
	static zephyr_http_server_request_state_t request_state = {0};					\
													\
	if (status == HTTP_SERVER_DATA_ABORTED) {							\
		LOG_INF("Transaction aborted after %zd bytes.", processed);				\
		free(request_state.app_state);								\
		request_state.app_state = NULL;								\
		processed = 0;										\
		return 0;										\
	}												\
													\
	processed += len;										\
	snprintf(print_str, sizeof(print_str), "%s received (%zd bytes)",				\
		 http_method_str(method), len);								\
	LOG_HEXDUMP_DBG(buffer, len, print_str);							\
													\
	if ((status == HTTP_SERVER_DATA_FINAL) && (!request_finished)) {				\
		LOG_INF("All data received (%zd bytes).", processed);					\
		processed = 0;										\
		request_state.service_url_string = static_file_##name##_service_url_string;		\
		request_state.url_path = client->url_buffer;						\
		request_state.status = status;								\
		request_state.tx_buf = static_file_##name##_recv_buffer;				\
		request_state.tx_buf_size = sizeof(static_file_##name##_recv_buffer);			\
		if (request_state.tx_buf) {								\
			request_state.tx_buf_pos = 0;							\
			SEND_STANDARD_HEADER(request_state, title)					\
			SEND_DATA_BLOCK(request_state,							\
				embedded_file_##name, sizeof(embedded_file_##name)-1)			\
			SEND_STANDARD_FOOTER(request_state)						\
			request_state.http_retcode = 200;						\
			request_state.total_sent += request_state.tx_buf_pos;				\
			if (request_state.tx_buf_pos > 0) {						\
				request_finished = 1;							\
			}										\
			free(request_state.app_state);							\
			request_state.app_state = NULL;							\
			return request_state.tx_buf_pos;						\
		}											\
		else {											\
			return 0;									\
		}											\
	}												\
													\
	request_finished = 0;										\
	request_state.tx_buf = NULL;									\
	request_state.tx_buf_pos = 0;									\
	request_state.total_sent = 0;									\
	free(request_state.app_state);									\
	request_state.app_state = NULL;									\
	return 0;											\
};													\
													\
struct http_resource_detail_dynamic name##_resource_detail = {						\
	.common = {											\
			.type = HTTP_RESOURCE_TYPE_DYNAMIC,						\
			.bitmask_of_supported_http_methods = BIT(HTTP_GET),				\
			.content_type = resource_type							\
		},											\
	.cb = static_file_##name##_handler,								\
	.data_buffer = static_file_##name##_recv_buffer,						\
	.data_buffer_len = sizeof(static_file_##name##_recv_buffer),					\
	.user_data = NULL,										\
};													\
													\
const STRUCT_SECTION_ITERABLE_ALTERNATE(http_resource_desc_kestrel_http_service, http_resource_desc,	\
					name##_resource) = {						\
	.resource = url,										\
	.detail = (void *)(&name##_resource_detail),							\
};

// NOTE
// Due to C preprocessor limitations, the contents of HTTP_RESOURCE_DEFINE are copied directly into
// the #define block below.  Keep in sync with Zephyr core (include/zephyr/net/http/service.h)
#define KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER_FULL(name, url, resource_type,			\
	allowed_http_methods, file_pointer, file_length, rx_callback, tx_callback, buffer_size)		\
static uint8_t dynamic_page_##name##_recv_buffer[buffer_size];						\
static char* dynamic_page_##name##_service_url_string = url;						\
static uint8_t * dynamic_page_##name##_file_ptr = file_pointer;						\
static int dynamic_page_##name##_file_len = file_length;						\
static int dynamic_page_##name##_handler(struct http_client_ctx *client, enum http_data_status status,	\
		       uint8_t *buffer, size_t len, void *user_data)					\
{													\
	static char print_str[32];									\
	enum http_method method = client->method;							\
	static size_t processed;									\
	static int request_finished = 0;								\
	static zephyr_http_server_request_state_t request_state = {0};					\
													\
	if (status == HTTP_SERVER_DATA_ABORTED) {							\
		LOG_INF("Transaction aborted after %zd bytes.", processed);				\
		request_state.service_url_string = dynamic_page_##name##_service_url_string;		\
		request_state.url_path = client->url_buffer;						\
		request_state.status = status;								\
		rx_callback(&request_state);								\
		free(request_state.app_state);								\
		request_state.app_state = NULL;								\
		processed = 0;										\
		return 0;										\
	}												\
													\
	processed += len;										\
	snprintf(print_str, sizeof(print_str), "%s received (%zd bytes)",				\
		 http_method_str(method), len);								\
	LOG_HEXDUMP_DBG(buffer, len, print_str);							\
													\
	request_state.rx_data_len = len;								\
	request_state.rx_buf = buffer;									\
	if ((status == HTTP_SERVER_DATA_FINAL) || (status == HTTP_SERVER_DATA_MORE)) {			\
		request_state.service_url_string = dynamic_page_##name##_service_url_string;		\
		request_state.status = status;								\
		rx_callback(&request_state); 								\
	}												\
													\
	if ((status == HTTP_SERVER_DATA_FINAL) && (!request_finished)) {				\
		LOG_INF("All data received (%zd bytes).", processed);					\
		processed = 0;										\
		request_state.service_url_string = dynamic_page_##name##_service_url_string;		\
		request_state.url_path = client->url_buffer;						\
		request_state.status = status;								\
		request_state.file_ptr = dynamic_page_##name##_file_ptr;				\
		request_state.file_len = dynamic_page_##name##_file_len;				\
		request_state.tx_buf = dynamic_page_##name##_recv_buffer;				\
		request_state.tx_buf_size = sizeof(dynamic_page_##name##_recv_buffer);			\
		if (request_state.tx_buf) {								\
			request_state.tx_buf_pos = 0;							\
			if (tx_callback(&request_state) == 0) {						\
				if (request_state.http_retcode != 200) {				\
					LOG_WRN("Application tried to return HTTP %d, UNIMPLEMENTED!",	\
						request_state.http_retcode);				\
				}									\
				if (request_state.tx_buf_pos > 0) {					\
					request_finished = 1;						\
				}									\
				free(request_state.app_state);						\
				request_state.app_state = NULL;						\
			}										\
			request_state.total_sent += request_state.tx_buf_pos;				\
			return request_state.tx_buf_pos;						\
		}											\
		else {											\
			return 0;									\
		}											\
	}												\
													\
	request_finished = 0;										\
	request_state.tx_buf = NULL;									\
	request_state.tx_buf_pos = 0;									\
	request_state.total_sent = 0;									\
	free(request_state.app_state);									\
	request_state.app_state = NULL;									\
	return 0;											\
};													\
													\
struct http_resource_detail_dynamic name##_resource_detail = {						\
	.common = {											\
			.type = HTTP_RESOURCE_TYPE_DYNAMIC,						\
			.bitmask_of_supported_http_methods = allowed_http_methods,			\
			.content_type = resource_type							\
		},											\
	.cb = dynamic_page_##name##_handler,								\
	.data_buffer = dynamic_page_##name##_recv_buffer,						\
	.data_buffer_len = sizeof(dynamic_page_##name##_recv_buffer),					\
	.user_data = NULL,										\
};													\
													\
const STRUCT_SECTION_ITERABLE_ALTERNATE(http_resource_desc_kestrel_http_service, http_resource_desc,	\
					name##_resource) = {						\
	.resource = url,										\
	.detail = (void *)(&name##_resource_detail),							\
};

#define KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER(name, url, resource_type,			\
	rx_callback, tx_callback, buffer_size)								\
	KESTREL_WEBSERVICE_DEFINE_DYNAMIC_PAGE_HANDLER_FULL(name, url, resource_type,			\
	BIT(HTTP_GET), NULL, 0, rx_callback, tx_callback, buffer_size)					\

typedef struct zephyr_http_server_request_state {
	const char * service_url_string;
	const char * url_path;
	enum http_data_status status;
	uint8_t * file_ptr;
	size_t file_len;
	uint8_t * tx_buf;
	size_t tx_buf_pos;
	size_t tx_buf_size;
	size_t total_sent;
	uint8_t * rx_buf;
	size_t rx_data_len;
	int http_retcode;
	void * app_state;
} zephyr_http_server_request_state_t;

void generic_rx_handler_noop(zephyr_http_server_request_state_t *state);

int parse_uri_query_string_find_value_for_key(uint8_t* uri, int uri_len, uint8_t* key, uint8_t* value_buffer, int buffer_size);

// HTTP multipart form parser functions
//
// NOTE
// This parser assumes the Zephyr HTTP server has correctly isolated the form upload content from the rest of the HTTP headers,
// i.e. that the first bytes are indeed the start of the unwrapped form boundary string

#define HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE ((MAX_REQUEST_SIZE_BYTES * 4) / 3)

typedef struct http_multipart_form_parser_state {
	uint8_t sliding_window[HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE];
	size_t sliding_window_pos;
	int current_state;
	uint8_t content_boundary_string[HTTP_HEADER_PARSE_SLIDING_WINDOW_SIZE];
	size_t data_offset_in_current_chunk;
} http_multipart_form_parser_state_t;

void parse_http_multipart_form_run(http_multipart_form_parser_state_t * parser_state, uint8_t * new_data, uint64_t new_data_len);
void parse_http_multipart_form_finalize(http_multipart_form_parser_state_t * parser_state, uint8_t * received_data, uint64_t * received_data_len);

#endif // _HTTPFUNCTIONS_H