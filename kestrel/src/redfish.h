// © 2021-2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _REDFISH_H
#define _REDFISH_H

#include "http_functions.h"

#define REDFISH_CHASSIS0_ID "kestrel_enclosure_0"
#define REDFISH_SYSTEM0_ID "kestrel_system_0"

#define REDFISH_MAX_STRING_ATTRIBUTE_LENGTH 256
#define REDFISH_MAX_CHASSIS_DATA_ATTRIBUTE_LENGTH 64
#define REDFISH_MAX_URL_PATH_LENGTH 512
#define REDFISH_MAX_TEMP_SENSOR_COUNT 64
#define REDFISH_MAX_FAN_COUNT 64
#define REDFISH_MAX_CHASSIS_SENSOR_COUNT 32
#define REDFISH_MAX_STATUS_CONDITION_COUNT 4

#define REDFISH_JSON_STRING_BUFFER_SIZE 65536

typedef struct kestrel_chassis_data {
	char asset_tag[REDFISH_MAX_CHASSIS_DATA_ATTRIBUTE_LENGTH];
	char chassis_type[REDFISH_MAX_CHASSIS_DATA_ATTRIBUTE_LENGTH];
	int depth_mm;
	char environmental_class[REDFISH_MAX_CHASSIS_DATA_ATTRIBUTE_LENGTH];
	int height_mm;
	char manufacturer[REDFISH_MAX_CHASSIS_DATA_ATTRIBUTE_LENGTH];
	char model[REDFISH_MAX_CHASSIS_DATA_ATTRIBUTE_LENGTH];
	char part_number[REDFISH_MAX_CHASSIS_DATA_ATTRIBUTE_LENGTH];
	char serial_number[REDFISH_MAX_CHASSIS_DATA_ATTRIBUTE_LENGTH];
	char uuid[REDFISH_MAX_CHASSIS_DATA_ATTRIBUTE_LENGTH];
	int weight_kg;
	int width_mm;
} kestrel_chassis_data_t;

typedef struct redfish_http_tx_state_data {
	size_t tx_pos;
	size_t total_length;
	char tx_buffer[REDFISH_JSON_STRING_BUFFER_SIZE];
} redfish_http_tx_state_data_t;

#define REDFISH_BASE_PATH "/redfish/v1"
#define REDFISH_CHASSIS_LIST_PATH REDFISH_BASE_PATH "/Chassis"
#define REDFISH_CHASSIS_PATH REDFISH_BASE_PATH "/Chassis/*"
#define REDFISH_SYSTEM_LIST_PATH REDFISH_BASE_PATH "/Systems"
#define REDFISH_SYSTEM_PATH REDFISH_BASE_PATH "/Systems/" REDFISH_SYSTEM0_ID
#define REDFISH_THERMAL_LIST_PATH REDFISH_CHASSIS_LIST_PATH "/*/Thermal"
#define REDFISH_THERMAL_LIST_PATH_FORMAT REDFISH_CHASSIS_LIST_PATH "/%s/Thermal"
#define REDFISH_THERMAL_PATH REDFISH_BASE_PATH "/Thermal/*"
#define REDFISH_THERMAL_TEMPERATURE_PATH_FORMAT REDFISH_CHASSIS_LIST_PATH "/%s/Thermal#/Temperatures/%d"
#define REDFISH_THERMAL_FAN_PATH_FORMAT REDFISH_CHASSIS_LIST_PATH "/%s/Thermal#/Fans/%d"
#define REDFISH_CHASSIS_SENSOR_LIST_PATH REDFISH_CHASSIS_LIST_PATH "/*/Sensors"
#define REDFISH_CHASSIS_SENSOR_LIST_PATH_FORMAT REDFISH_CHASSIS_LIST_PATH "/%s/Sensors"
#define REDFISH_CHASSIS_SENSOR_PATH_FORMAT REDFISH_CHASSIS_LIST_PATH "/%s/Sensors/%d"
#define REDFISH_COMPUTER_SYSTEM_ACTION_RESET_PATH REDFISH_BASE_PATH "/Systems/system/Actions/ComputerSystem.Reset"

int get_chassis_vpd(const char * chassis_id, kestrel_chassis_data_t * chassis_data);

int redfish_version_handler(zephyr_http_server_request_state_t *state);
int redfish_v1_service_list_handler(zephyr_http_server_request_state_t *state);
int redfish_v1_chassis_list_endpoint_handler(zephyr_http_server_request_state_t *state);
int redfish_v1_system_list_endpoint_handler(zephyr_http_server_request_state_t *state);
int redfish_v1_system_endpoint_handler(zephyr_http_server_request_state_t *state);
void redfish_v1_system_reset_endpoint_handler_rx(zephyr_http_server_request_state_t *state);
int redfish_v1_system_reset_endpoint_handler_tx(zephyr_http_server_request_state_t *state);

#endif // _REDFISH_H
