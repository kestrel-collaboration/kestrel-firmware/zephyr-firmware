// © 2021-2024 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _BUILD_CONFIG_H
#define _BUILD_CONFIG_H

#define WITH_ZEPHYR 1

#endif // _BUILD_CONFIG_H
