// © 2020 - 2023 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

// Host platform configuration
#define HOST_TYPE_BLACKBIRD            0
#define HOST_TYPE_TALOS_II             1
#define HOST_HAS_LPC_CLOCK_BUFFER      1
#define HOST_PLATFORM_FPGA_I2C_ADDRESS 0x31

#define ENABLE_HDMI_TRANSCEIVER        0
