#!/bin/bash
#
# Copyright (c) 2023 - 2024 Raptor Engineering, LLC
# Released under the terms of the GNU AGPL v3

set -e

# Set base repository locations
KESTREL_GITLAB_FIRMWARE_MASTER_URL="https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-firmware/"
KESTREL_GITLAB_TOOLING_MASTER_URL="https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-tooling/"

# Set base repository branch names
KESTREL_GITLAB_ZEPHYR_BRANCH=powerpc-v3.7-branch

# Pull gateware from separate HDL pipeline
GITLAB_INSTANCE="gitlab.raptorengineering.com"
HDL_GROUP="kestrel-collaboration"
HDL_SUBGROUP="kestrel-litex"
HDL_PROJECT="litex-boards"
HDL_PROJECT_ID="${HDL_GROUP}%2F${HDL_SUBGROUP}%2F${HDL_PROJECT}"
if [[ "${1}" == "arctic_tern" ]]; then
	ARTIFACT_PIPELINE_JOB="build_arctic_tern_netboot_hdl"
else
	echo "ERROR: Unknown artifact location!"
	exit 1
fi

echo "Downloading HDL build artifacts..."
#curl -L --header "JOB-TOKEN: $CI_JOB_TOKEN" "https://${GITLAB_INSTANCE}/api/v4/projects/${HDL_PROJECT_ID}/jobs/artifacts/master/download?job=${ARTIFACT_PIPELINE_JOB}" -o hdl-artifacts.zip
# FIXME
# NASTY HACK
# Work around issues in the current Gitlab version deployed at Raptor
# Force usage of the artifact files generated for netboot on litex-boards GIT hash 92929ec3
curl -L "https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-litex/litex-boards/-/jobs/5562/artifacts/download" -o hdl-artifacts.zip

echo "Extracting gateware..."
mkdir hdl-artifacts
cd hdl-artifacts
unzip ../hdl-artifacts.zip
cd ..
rm hdl-artifacts.zip

echo "Moving gateware into build directory..."
rm -rf bootrom/fpga/release || true
mkdir -p bootrom/fpga/release/
cp -Rp hdl-artifacts/litex_boards/targets/build/*/* bootrom/fpga/release/

echo "Installing dependencies..."

mkdir cicd_build
ln -s ${CI_PROJECT_DIR} cicd_build/
cd cicd_build

# Zephyr's version detection system relies on the tags being present, so we have no choice but to pull the entire repo
git clone ${KESTREL_GITLAB_FIRMWARE_MASTER_URL}/zephyr-rtos -b ${KESTREL_GITLAB_ZEPHYR_BRANCH}

# The history of these repos are not parsed at build time, so shallow clone is preferred
git clone --depth=1 ${KESTREL_GITLAB_FIRMWARE_MASTER_URL}/zephyr-littlefs -b ${KESTREL_GITLAB_ZEPHYR_BRANCH}
git clone --depth=1 ${KESTREL_GITLAB_FIRMWARE_MASTER_URL}/zephyr-mbedtls -b ${KESTREL_GITLAB_ZEPHYR_BRANCH}
git clone --depth=1 ${KESTREL_GITLAB_FIRMWARE_MASTER_URL}/zephyr-lz4 -b ${KESTREL_GITLAB_ZEPHYR_BRANCH}
git clone --depth=1 ${KESTREL_GITLAB_TOOLING_MASTER_URL}/gcc

echo "Building soft floating point library..."

cd gcc/libgcc/soft-fp/
# Ignore expected errors
set +e
gcc -c -O2 -msoft-float -mno-string -mno-multiple -mno-vsx -mno-altivec -mlittle-endian -mstrict-align -fno-stack-protector -m64 -mabi=elfv2 -mcmodel=small -I../config/rs6000/ -I../../include -I.. *.c
set -e
ar -crv libsoft-fp.a *.o
cp -Rp libsoft-fp.a ../../../
cd ../../../

export SOURCE_ROOT_DIR=$(pwd)

echo "Dependencies installed"

if [[ "${1}" == "arctic_tern" ]]; then
	echo "Building Zephyr images for RCS Arctic Tern BMC"

	cd zephyr-rtos
	mkdir images

	# Build RCS Blackbird image
	echo "Building for RCS Blackbird host"
	rm -f ${SOURCE_ROOT_DIR}/zephyr-firmware/kestrel/src/host_platform_settings.h
	ln -s ../host_platform_settings/rcs_blackbird.h ${SOURCE_ROOT_DIR}/zephyr-firmware/kestrel/src/host_platform_settings.h
	rm -rf build
	mkdir build
	cd build
	ZEPHYR_BASE=${SOURCE_ROOT_DIR}/zephyr-rtos ZEPHYR_LITTLEFS_KCONFIG=${SOURCE_ROOT_DIR}/zephyr-rtos/modules/littlefs/Kconfig ZEPHYR_MBEDTLS_KCONFIG=${SOURCE_ROOT_DIR}/zephyr-rtos/modules/mbedtls/Kconfig ZEPHYR_TOOLCHAIN_VARIANT=host cmake -DZEPHYR_MODULES="${SOURCE_ROOT_DIR}/zephyr-littlefs;${SOURCE_ROOT_DIR}/zephyr-mbedtls" -DZEPHYR_LITTLEFS_MODULE_DIR=${SOURCE_ROOT_DIR}/zephyr-littlefs -DZEPHYR_LZ4_MODULE_DIR=${SOURCE_ROOT_DIR}/zephyr-lz4 -DBOARD=arctic_tern/kestrel_openpower ${SOURCE_ROOT_DIR}/zephyr-firmware
	cp -Rp ../../libsoft-fp.a zephyr/
	make -j$(nproc)
	cp -Rp zephyr/zephyr.bin ../images/zephyr_rcs_blackbird.bin
	cp -Rp zephyr/zephyr.elf ../images/zephyr_rcs_blackbird.elf
	cp -Rp zephyr/zephyr.dts ../images/zephyr_rcs_blackbird.dts
	cp -Rp zephyr/zephyr.map ../images/zephyr_rcs_blackbird.map
	cd ..

	# Build RCS Talos II image
	echo "Building for RCS Talos II host"
	rm -f ${SOURCE_ROOT_DIR}/zephyr-firmware/kestrel/src/host_platform_settings.h
	ln -s ../host_platform_settings/rcs_talos_ii.h ${SOURCE_ROOT_DIR}/zephyr-firmware/kestrel/src/host_platform_settings.h
	rm -rf build
	mkdir build
	cd build
	ZEPHYR_BASE=${SOURCE_ROOT_DIR}/zephyr-rtos ZEPHYR_LITTLEFS_KCONFIG=${SOURCE_ROOT_DIR}/zephyr-rtos/modules/littlefs/Kconfig ZEPHYR_MBEDTLS_KCONFIG=${SOURCE_ROOT_DIR}/zephyr-rtos/modules/mbedtls/Kconfig ZEPHYR_TOOLCHAIN_VARIANT=host cmake -DZEPHYR_MODULES="${SOURCE_ROOT_DIR}/zephyr-littlefs;${SOURCE_ROOT_DIR}/zephyr-mbedtls" -DZEPHYR_LITTLEFS_MODULE_DIR=${SOURCE_ROOT_DIR}/zephyr-littlefs -DZEPHYR_LZ4_MODULE_DIR=${SOURCE_ROOT_DIR}/zephyr-lz4 -DBOARD=arctic_tern/kestrel_openpower ${SOURCE_ROOT_DIR}/zephyr-firmware
	cp -Rp ../../libsoft-fp.a zephyr/
	make -j$(nproc)
	cp -Rp zephyr/zephyr.bin ../images/zephyr_rcs_talos_ii.bin
	cp -Rp zephyr/zephyr.elf ../images/zephyr_rcs_talos_ii.elf
	cp -Rp zephyr/zephyr.dts ../images/zephyr_rcs_talos_ii.dts
	cp -Rp zephyr/zephyr.map ../images/zephyr_rcs_talos_ii.map
	cd ..
else
	echo "ERROR: Unknown target!"
	exit 1
fi
